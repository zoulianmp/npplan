# -*- coding: utf8 -*-
"""
B{Распределённая система планирования лучевой терапии NPPlan 3}
@author: mrxak, mrvvf
@copyright: MRRC Obninsk 2012
@version: 3.0.1a
@date: 26.06.2012
@summary: Распределённая система планирования лучевой терапии NPPlan 3.
"""

import NPPlan
import os

programPath = os.path.dirname(__file__)
if not programPath:
    programPath = os.getcwd() #.replace('\\', '/')
NPPlan.init(programPath=programPath)


