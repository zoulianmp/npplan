# -*- coding: utf8 -*-
"""
B{Распределённая система планирования лучевой терапии NPPlan 3}
@author: mrxak, mrvvf
@copyright: MRRC Obninsk 2012
@version: 3.0.1a
@date: 26.06.2012
@summary: Распределённая система планирования лучевой терапии NPPlan 3
"""


# @var __version__: версия программы
VERSION_STRING = '3.0.2a'
VERSION_BUILD = 2
VERSION = '%s build: %d' % (VERSION_STRING, VERSION_BUILD)
__version__ = VERSION

__all__ = [
    # main modules
    'model',
    'view',
    'controller',
]

# gettext для локализации
import sys
import gettext
if 'win32' == sys.platform:
    try:
        import gettext_windows
        gettext_windows.setup_env()
    except:
        pass
gettext.install('default', './lang', unicode=True)

_programData = {

}

from NPPlan._core import *
import NPPlan._core
__all__ += [name for name in dir(NPPlan._core) if not name.startswith('_')]
