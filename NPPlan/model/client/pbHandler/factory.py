# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 03.12.12
@summary: 
'''

import NPPlan

from twisted.spread import pb
from twisted.internet.error import ConnectionRefusedError
from twisted.internet import reactor


class npplanPbFactory(pb.PBClientFactory):
    _status = None
    def clientConnectionFailed(self, connector, reason):
        #print 'factory: connection failed'
        NPPlan.log('model.client.pbHandler.factory', 'Factory: connection failed', 5)
        self._status = 0
        reactor.callLater(2, self.setStatusBar, 'off')
        print connector, reason
        NPPlan.log('model.client.pbHandler.factory', str(reason), 5)
    pass

    def clientConnectionMade(self, broker):
        NPPlan.log('model.client.pbHandler.factory', 'Factory: connection made', 5)
        self._status = 1
        reactor.callLater(2, self.setStatusBar, 'on')

    def setStatusBar(self, status):
        # @todo: refactor
        print 'Setting status bar status in pbFactory'
        NPPlan.cHandler.statusBar.controller.setStatus(inf=status)