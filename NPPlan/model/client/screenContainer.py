# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 05.10.12
@summary: 
'''

class screenContainer(object):
    def __init__(self, **kwargs):
        # @todo: maybe use property?
        self.screens = {}

    def add(self, name, screen):
        self.screens[name] = screen

    def get(self, name):
        return self.screens[name]

    def delete(self, name):
        self.screens.__delitem__(name)

    def hasKey(self, name):
        return self.screens.has_key(name)

    def list(self):
        return self.screens
