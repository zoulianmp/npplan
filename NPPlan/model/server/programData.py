# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 11.07.12
@summary: 
'''

class programData():
    _instance = None
    # @C_var _data: словарь данных
    _data = {}
    def __init__(self):
        pass

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(programData, cls).__new__(
                                cls, *args, **kwargs)
        return cls._instance

    def getData(self):
        return self._data

    def setData(self, key, value=""):
        if ("" == value or isinstance(key, dict)):
            #z = dict(x, **y)
            self._data.update(key)
        else:
            self._data[key] = value

