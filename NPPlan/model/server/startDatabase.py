# -*- coding: utf8 -*-
'''
Стартует БД
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 3
@date: 10.07.2012
@summary: стартует БД
'''

import NPPlan
#import NPPlan.model.runner.local as runner
from NPPlan.model.runner.local import runTask

# @todo: use abstract runner
# @todo: перенаправить stdout, обработка

dbRun = NPPlan.getProgramData()['appConfig']['server']['dbrun']
runTask('database', dbRun)

