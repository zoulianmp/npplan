# -*- coding: utf8 -*-
'''
Низкоуровневые классы для незащищённого TCP сервера
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 05.07.2012
@summary: package
'''

from twisted.internet import reactor, protocol
from multiprocessing import Process

class Echo(protocol.Protocol):
    def dataReceived(self, data):
        self.transport.write(data)


def startServer():
    # @todo: to controller!!!!
    print "Starting server"
    factory = protocol.ServerFactory()
    factory.protocol = Echo
    reactor.listenTCP(8000,factory)
    reactor.run()

startServer()