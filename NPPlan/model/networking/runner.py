# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.02.13
@summary: 
'''

from twisted.internet import protocol
import abc

class GPGProtocol(protocol.ProcessProtocol):
    def __init__(self, crypttext):
        self.crypttext = crypttext
        self.plaintext = ""
        self.status = ""
    def connectionMade(self):
        #self.transport.writeToChild(3, '123')
        #self.transport.closeChildFD(3)
        print 'connMade', self.testVar
        #self.transport.writeToChild(0, self.crypttext)
        #self.transport.closeChildFD(0)
        self.transport.closeStdin()
    #def childDataReceived(self, childFD, data):
    #    if childFD == 1: self.plaintext += data
    #    if childFD == 4: self.status += data
    def processEnded(self, status):
        print 'self.crypttext', self.crypttext

        #print 'self.plaintext', self.plaintext
        #print 'self.status', self.status
        rc = status.value.exitCode
        print 'rc', rc
        print self.testVar
        self.testVar = '1235435'
        #self.deferred.callback(self)
        #if rc == 0:
        #    self.deferred.callback(self)
        #else:
        #    self.deferred.errback(rc)
    def outReceived(self, data):
        self.testVar = 'testData'
        print data
        self._data = data
        #self.deferred.callback(self)

class processRunner(protocol.ProcessProtocol):
    def __init__(self, params=[]):
        self._params = params
        self._status = -2 # -2 = init, -1 = runned, >0 = exit code
        self._data = []

    def setStatus(self, status):
        print 'Status set %d' %status
        self._status = status
    def getStatus(self):
        return self._status
    status = property(getStatus, setStatus)

    def connectionMade(self):
        self.status = -1

    def outReceived(self, data):
        self._data.append(data)
        self.parseData(data)

    def parseData(self, data):
        pass

    def processEnded(self, reason):
        self.status = reason.value.exitCode

class mcnpProcessRunner(processRunner):
    def __init__(self, params=[]):
        self._struct = {'params' : params}
        processRunner.__init__(self, params)

    def parseData(self, data):
        if 1 == len(self._data):
            self._struct['version'] = [i for i in data.split(' ') if i != ''][2]
        pass