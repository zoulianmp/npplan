# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 26.10.12
@summary: 
'''

import NPPlan
from NPPlan.model.networking.informer.protocol import echoProtocol
from twisted.internet import protocol

class informerServerFactory(protocol.ServerFactory):
    protocol  = echoProtocol
    clients = []