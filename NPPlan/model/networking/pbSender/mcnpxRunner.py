# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.02.13
@summary: 
'''

from twisted.spread import pb
from twisted.internet import reactor

#funcName = 'runner'
#funcName = 'getRunnerResults'
#task = 'START'
task = 'RESULT'

global factory

def main():
    global factory
    factory = pb.PBClientFactory()
    reactor.connectTCP("localhost", 8800, factory)
    def1 = factory.getRootObject()
    if 'START' == task:
        def1.addCallbacks(makeStart, err_obj1)
    elif 'RESULT' == task:
        def1.addCallbacks(checkResult, err_obj1)
    reactor.run()


def err_obj1(reason):
    print "error getting first object", reason
    reactor.stop()

def got_obj1(obj1):
    def2 = obj1.callRemote('mcnpxRunner', {'id' : 'testRun', 'fileName' : 'tst05q'})
    def2.addCallbacks(got_obj2)

def got_obj2(obj2):
    #print "got second object:", obj2
    print 'Function: mcnpxRunner got', obj2
    def3 = obj2.callRemote("three", 12)
    def3.addCallbacks(got_result)
    #reactor.stop()

def got_result(data):
    print data

def makeStart(obj):
    start = obj.callRemote('mcnpxRunner', {'id' : 'testRun', 'fileName' : 'tst05q'})
    start.addCallbacks(getStarted)

def getStarted(result):
    print 'Result', result
    reactor.stop()

def checkResult(obj):
    result = obj.callRemote('getMcnpxRunnerStatusByTaskId', 'testRun')
    result.addCallbacks(getCheckResult)

def getCheckResult(obj):
    if obj['finished'] == False:
        print 'Running, status %d', obj['status']
        reactor.stop()
    else:
        global factory
        print 'Task %s finished. Asking for results' %(obj['taskId'])
        def2 = factory.getRootObject()
        def2.addCallbacks(makeMcnpxRunnerResults, err_obj1)

def makeMcnpxRunnerResults(obj):
    resultStruct = obj.callRemote('getMcnpxRunnerResults', 'testRun')
    resultStruct.addCallbacks(getResultStruct)

def getResultStruct(obj):
    print 'Got results'
    print obj
    reactor.stop()

main()