#!/usr/bin/env python
# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 17.05.13
@summary: 
'''


from twisted.spread import pb
from twisted.internet import reactor


def main():
    confStream = open('conf', 'r')
    conf = confStream.readlines()
    print conf
    factory = pb.PBClientFactory()
    reactor.connectTCP(conf[0][:-1], 8800, factory)
    def1 = factory.getRootObject()
    def1.addCallbacks(got_obj1, err_obj1)
    reactor.run()


def err_obj1(reason):
    print "error getting first object", reason
    reactor.stop()


def got_obj1(obj1):
    confStream = open('conf', 'r')
    conf = confStream.readlines()
    print "got first object:", obj1
    print "asking to start %s at %s" % (conf[1], conf[0])
    def2 = obj1.callRemote("getTwo", conf[1])
    def2.addCallbacks(got_obj2)


def got_obj2(obj2):
    print 'DONE'
    reactor.stop()
    #def3 = obj2.callRemote("three", conf[1])
    #def3.addCallbacks(got_obj3)

main()