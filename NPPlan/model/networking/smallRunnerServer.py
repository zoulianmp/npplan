#!/usr/bin/env python
# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 17.05.13
@summary: 
'''


from twisted.spread import pb


class Two(pb.Referenceable):
    def remote_three(self, arg):

        print "Two.three was given", arg


class One(pb.Root):
    def remote_getTwo(self, arg):
        print arg
        import NPPlan.model.runner.newThreadTaskRunner as thr
        thr.startMcnpTask(str(arg))
        #two = Two()
        #print "returning a Two called", two
        return '123'

if __name__ == '__main__':
    from twisted.internet import reactor
    reactor.listenTCP(8800, pb.PBServerFactory(One()))
    reactor.run()
