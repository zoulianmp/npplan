# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 11.07.12
@summary: 
'''

from twisted.spread import pb
import NPPlan.model.server.programData as programData
import NPPlan.controller.logger as log
from twisted.internet import reactor
from pbHandler import pbHandler

class PbWrapper(pb.Root):
    def remote_getProgramData(self):
        data = programData.programData()

        return data.getData()

    def remote_closeServer(self, key=""):
        #from NPPlan.model.server.stopDatabase import stopDatabase
        #stopDatabase()
        from NPPlan.model.runner.local import terminateAll
        import NPPlan
        import os
        terminateAll()
        #import NPPlan
        #reactor.callLater(1, reactor.stop)
        NPPlan._programData['runningConfig']['exiting'] = True
        #os._exit(0)
        return 1

    def remote_getMainDbAddress(self, key=""):
        print key
        from NPPlan.model.mongo import connector
        data = connector.getMainDbAddress()
        return data

    def remote_callBackend(self, data):
        try:
            self.__dict__['_npplanData']
        except KeyError:
            self.__dict__['_npplanData'] = pbHandler(self)
        log.log('server.pbWrapper', 'Called method %s, args: %s' %(data['method'], data))
        result = getattr(self.__dict__['_npplanData'], data['method'])(data['args'])
        log.log('server.pbWrapper', 'Method %s, returned: %s' %(data['method'], result))
        return {
            'callId' : data['callId'],
            'result' : result,
        }
        pass

    def remote_test(self, data=''):
        return {
            'test' : 'sucess'
        }

    def remote_runner(self, data=''):
        from NPPlan.model.networking.runner import processRunner
        import NPPlan.model.runner.runnerContainer as runnerContainer
        from twisted.internet.defer import Deferred
        from twisted.internet import reactor
        import os

        #gp = GPGProtocol('test')
        #gp.deferred = Deferred()
        #gp.testVar = 'tv'

        cmd = ["c:\\mcnpx\\executable\\mcnpx.exe", "n=%s" %data]
        runner = processRunner(cmd)
        p = reactor.spawnProcess(runner, cmd[0], cmd, path=os.path.dirname(cmd[0])
                                 )
        #self._gp = gp
        runnerContainer.container().append('t1', runner)
        return 'runned'

    def remote_mcnpxRunner(self, params):
        fileName = params['fileName']
        id = params['id']
        if '' == fileName:
            return 'No filename'
        from NPPlan.model.networking.runner import mcnpProcessRunner
        import NPPlan.model.runner.runnerContainer as runnerContainer
        import time
        from twisted.internet import reactor
        import os

        cmd = ["c:\\mcnpx\\executable\\mcnpx.exe", "n=%s" %fileName]
        runner = mcnpProcessRunner(cmd)
        p = reactor.spawnProcess(runner, cmd[0], cmd, path=os.path.dirname(cmd[0])
                                 )
        runnerContainer.container().append(id, runner)
        timeString = time.strftime("%H:%M:%S ", time.localtime())
        return 'mcnpx %s task runned at %s' %(fileName, timeString)

    def  remote_getMcnpxRunnerStatusByTaskId(self, taskId):
        import NPPlan.model.runner.runnerContainer as runnerContainer
        return {
            'taskId' : taskId,
            'finished' : runnerContainer.container().isFinished(taskId),
            'status' : runnerContainer.container().get(taskId).status,
            }

    def remote_getMcnpxRunnerResults(self, taskId):
        import NPPlan.model.runner.runnerContainer as runnerContainer
        return runnerContainer.container().getDataStruct(taskId)

    def remote_getRunnerResults(self, data=''):
        import NPPlan.model.runner.runnerContainer as runnerContainer
        if runnerContainer.container().isFinished('t1'):
            return runnerContainer.container().getData('t1')
        else:
            return ['Still working. Status %d' %runnerContainer.container().get('t1').status, runnerContainer.container().getData('t1')]
        #return runnerContainer.container().get('t1')._data