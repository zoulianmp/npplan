# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 2
@date: 16.05.2013
@summary: TTD for plan
'''

import NPPlan
import unittest


class testSeries1(unittest.TestCase):
    """
    TestCase для загрузки плана из DICOM-файлов на локальном носителе и проверки правильности чтения
    """
    def setUp(self):
        NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)
        self._plan = NPPlan.planAbstract()

    def test_planCreation(self):
        self.assertEqual(self._plan.planHasActivePlan(), False)
        self._plan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\", skipAnalyzer=True)
        self.assertEqual(self._plan.planHasActivePlan(), True)
        self.assertEqual(self._plan.planHasErrors(), False)

    def test_planGlobal(self):
        self._plan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\", skipAnalyzer=True)
        self.assertEqual(self._plan.planGetSeriesLength(), 35)
        self.assertEqual(self._plan.planGetLocalFolderPath(), "C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")
        self.assertIn('84269306', self._plan.planGetSeriesFilenames())
        self.assertEqual(self._plan.planHasSliceByFilename('84269306'), True)
        self.assertNotIn('3248293', self._plan.planGetSeriesFilenames())
        self.assertEqual(self._plan.planHasSliceByFilename('3248293'), False)

    def test_planDicomGlobal(self):
        self._plan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")
        self.assertEqual(self._plan.planGetOriginalLocationBounds(), (-177.0, -41.0))
        self.assertEqual(self._plan.planGetRescaleParams(1), (-1024, 1))

    def test_planDicomGlobalBitData(self):
        self._plan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")
        # (bit allocated, bit stored, high bit)
        self.assertEqual(self._plan.planGetBitData(1), (16, 12, 11))
        self.assertDictContainsSubset({'highBit': 11}, self._plan.planGetBitData())

    def tearDown(self):
        del self._plan


class testSeries2(unittest.TestCase):
    """
    TestCase для проверки контейнера хранения DICOM-файлов при загрузке с локального носителя
    """
    def setUp(self):
        NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)
        self._plan = NPPlan.planAbstract()
        self._plan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")

    def test_planDicomSingle(self):
        self.assertEqual(self._plan.planGetSlice(num=2).getNumber(), 2)
        self.assertEqual(self._plan.planGetSlice(num=2).number, 2)
        #self.assertEqual(self._plan.getSlice(num=2).getLocation, -171)
        pass

    def tearDown(self):
        del self._plan


class testSeries3(unittest.TestCase):
    """
    TestCase для проверки соединения с базой данных Mongo
    """
    def setUp(self):
        NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)
        self._pat = NPPlan.mongoConnection.patient()
        self._pat['name']['latinName'] = u'TEST'
        self._pat['name']['originalName'] = u'Тест'
        print 'creating', self._pat
        self._pat.save()
        #self._patient = NPPlan.patientDbModel()

    def test_patientSimpleGetter(self):
        p = NPPlan.mongoConnection.patient.find_one({'rank': 0})
        print 'simplegetter', p
        self.assertEqual(p['name']['originalName'], u'Тест')
        self.assertEqual(NPPlan.mongoConnection.patient.find().count(), 1)
        import mongokit
        #self.assertEqual(NPPlan.patientDbModel, NPPlan.mongoConnection.patient)
        #self.assertIsInstance(NPPlan.mongoConnection.patient, mongokit.schema_document.Callablepatient)
        pass

    def test_diffCalling(self):
        p = NPPlan.mongoConnection.patient.find_one({'rank': 0})
        print 'p.get(rank)', p.get('rank')
        print 'p.get(name)', p.get('name')
        print 'p.get(name).o', p.get('name').get('originalName')
        self.assertEqual(p['name']['originalName'], p.get('name').get('originalName'))
        self.assertEqual(p['name']['originalName'], p.name.originalName)
        self.assertEqual(p['name']['originalName'], self._parseDots(p, 'name.originalName'))
        #self.assertEqual(p['name']['originalName'], getattr(p, 'name.originalName'))
        from NPPlan.lib import parseObjectWithDotName as pd
        self.assertEqual(p['name']['originalName'], pd(p, 'name.originalName'))
        self.assertEqual(p['name']['originalName'], pd(p, 'name.originalName', 1))

    def test_pdots(self):
        from NPPlan.lib import parseObjectWithDotName as pd
        self.assertEqual('1', pd({'n': {'m': '1'}}, 'n.m'))

    def _parseDots(self, obj, name):
        for ln in name.split('.'):
            obj = obj.get(ln)
        return obj

    def test_patientInheritedGetter(self):
        p = NPPlan.mongoConnection.patient.find_one({'name.latinName': u'TEST'})
        print 'inheritedgetter', p
        self.assertEqual(p['name']['originalName'], u'Тест')

    def tearDown(self):
        self._pat.delete()


class testSeries4(unittest.TestCase):
    """
    TestCase для проверки сохранения плана
    """
    def setUp(self):
        NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)
        self._plan = NPPlan.planAbstract()
        self._plan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\", skipAnalyzer=True)
        self._plan.planSetDescription('test1')
        self._plan.save()
        self._pid = self._plan.getPlanId()

    def test_getDataModel(self):
        self.assertEqual(self._plan.planGetDescription(), 'test1')
        self._plan.load(self._pid)
        self._plan.planSetDescription('test2')
        self.assertEqual(self._plan.planGetDescription(), 'test2')

        p2 = NPPlan.planAbstract()
        p2.load(self._pid)
        self.assertEqual(p2.planGetDescription(), 'test1')
        #print self._plan.planGetDataModel()

        #self._plan.load('7008e58f-bc6b-11e2-b7f2-001517eafced')
        #print 'dbObject', self._plan.__dict__['dbObject']
        self.assertEqual(True, True)

    def tearDown(self):
        self._plan.delete()


if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(testSeries1))
    suite.addTest(unittest.makeSuite(testSeries2))
    suite.addTest(unittest.makeSuite(testSeries3))
    suite.addTest(unittest.makeSuite(testSeries4))
    unittest.main()
