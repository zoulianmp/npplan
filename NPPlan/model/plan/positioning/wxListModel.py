# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 04.02.14
@summary: 
'''

from wx import Colour
from wx.dataview import DataViewIconText, PyDataViewModel


class planListModel(object):
    def __init__(self, id=0, planId=None, description='', color=None,
                 sourceType=None, sourcePosition=None, targetPosition=None,
                 ):
        self.id = id
        self._planId = planId
        self.description = description
        self.color = color
        self.sourceType = sourceType
        self.sourcePosition = sourcePosition
        self.targetPosition = targetPosition


class wxDVListModel(PyDataViewModel):
    def __init__(self, data):
        PyDataViewModel.__init__(self)
        self.data = data
        self.objmapper.UseWeakRefs(True)

    def GetColumnCount(self):
        return 6

    def GetColumnType(self, col):
        mapper = { 0: 'int',
                   1: 'string',
                   2: 'bitmap',
                   3: 'string',
                   4: 'string',
                   5: 'string',
                   }
        return mapper[col]

    def GetChildren(self, parent, children):
        if not parent:
            for item in self.data:
                children.append(self.ObjectToItem(item))
            return len(self.data)

    def GetValue(self, item, col):
        node = self.ItemToObject(item)
        if isinstance(node, planListModel):
            mapper = {
                0: node.id,
                1: node.description,
                2: Colour(*node.color),
                3: node.sourceType,
                4: node.sourcePosition,
                5: node.targetPosition,
            }
            return mapper[col]

    def SetValue(self, value, item, col):
        node = self.ItemToObject(item)
        if isinstance(node, planListModel):
            if col == 0:
                node.id = value
            elif col == 1:
                node.description = value
            elif col == 2:
                node.color = (value.red, value.green, value.blue)
            elif col == 3:
                node.sourceType = value
            elif col == 4:
                node.sourcePosition = value
            elif col == 5:
                node.targetPosition = value

    def IsContainer(self, item):
        if not item:
            return True
        return False

