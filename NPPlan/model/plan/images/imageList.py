# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 11.12.12
@summary: 
'''

import NPPlan
try:
    #@todo: fix this
    from NPPlan.model.file.dicom.shortStructure import readCommonData
except ImportError:
    pass
from core import *
from NPPlan.model.abstracts.temporary import getDir as getTemporaryDir
import os


class image(object):
    def __init__(self, dir, thumbFlag=True, ftInit=True, **kwargs):
        self.__dict__['imageDir'] = imageDir(dir)
        self.__dict__['imageDir'].assertImageInstance(self)
        self.__dict__['images'] = self.__dict__['imageDir'].retrieveImages()
        if thumbFlag:
            self.createThumbnailsInTemporary()
        else:
            # @todo: implement load without parsing
            pass
            #self.__dict__['thumbnailsDir'] = kwargs.get('thumbnailsDir')
        #self.__dict__['thumbnailsDir'].parse()

    def getImageDir(self):
        """
        @return: директория с превьюшками
        @rtype: NPPlan.model.plan.images.core.imageDir
        """
        return self.__dict__['imageDir']
    imageDir = property(getImageDir)

    def getThumbnailsDir(self):
        """
        @return: директория с превьюшками
        @rtype: NPPlan.model.plan.images.core.thumbnailDir
        """
        return self.__dict__['thumbnailsDir']
    thumbnailsDir = property(getThumbnailsDir)

    def createThumbnailsInTemporary(self):
        tempPath = getTemporaryDir()
        studyPath = os.path.join(tempPath, self.getUniqueStudyKey())
        os.mkdir(studyPath)
        seriesPath = os.path.join(studyPath, self.getUniqueSeriesKey())
        os.mkdir(seriesPath)
        self.__dict__['thumbnailsDir'] = thumbnailDir(True)
        self.__dict__['thumbnailsDir'].assertImageInstance(self)
        self.__dict__['thumbnailsDir'].dirName = seriesPath
        self.__dict__['thumbnailsDir'].parse()


    def getCommonData(self, path):
        """
        @param path: путь
        @type path: NPPlan.model.abstracts.pathBinder.path
        """
        self.__dict__['commonDicomData'] = readCommonData(path.path)

    def hasCommonData(self):
        if not self.__dict__.has_key('commonDicomData'):
            img = self.__dict__['imageDir'].getAny()
            self.getCommonData(img._filePath)

    def getUniqueStudyKey(self):
        self.hasCommonData()
        return self.__dict__['commonDicomData'].StudyInstanceUID

    def getUniqueSeriesKey(self):
        self.hasCommonData()
        return self.__dict__['commonDicomData'].SeriesInstanceUID

    def createThumbnails(self):
        pass


if __name__ == '__main__':
    p = image('C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140')
    print p.getUniqueStudyKey()
    print p.getUniqueSeriesKey()
    print p.imageDir.getByName('84269306').pathString
    print p.imageDir.getByName('84269306').thumbnail.pathString
    print p.thumbnailsDir.getByName('84269306').pathString
    print p.thumbnailsDir.getByName('84269306').image.pathString
    p.thumbnailsDir.getByName('84269306').recreateWithParams(100, -100)