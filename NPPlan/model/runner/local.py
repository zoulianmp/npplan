# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 7
@date: 24.07.12
@summary:
'''

import NPPlan
import os.path
import shlex


#from NPPlan.model.runner.oneTask import oneTask
from NPPlan.model.runner import addTask
from multiprocessing import Process
import subprocess
import subprocess
print subprocess.__file__

class ParallelRunner(Process):
    def __init__(self, task, daemon=False):
        self._task = task
        #self.daemon = daemon

    def run(self):
        self._task.process = subprocess.Popen(self._task)


def runTask(name, param, **kwargs):
    if not isinstance(param, list):
        param = shlex.split(param)

    task = addTask(name, cmd=param, scope='local')
    #task.process = subprocess.Popen(task.cmd)
    #pRunner = ParallelRunner(task)
    #pRunner.start()



