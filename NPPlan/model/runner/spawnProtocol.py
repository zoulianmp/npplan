# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 19.09.13
@summary: 
'''

import NPPlan
from twisted.internet import protocol, reactor


class ProcessProtocol(protocol.ProcessProtocol):
    _data = ''

    def __init__(self, *args, **kwargs):
        # @todo: handle copying mpi results
        #protocol.ProcessProtocol.__init__(self)
        self._fileName = kwargs.get('filename')
        self._path = kwargs.get('path')
        self._resultsDir = kwargs.get('resultsDir', 'C:\\Temp\\mcnpresults\\')
        self._dbHandler = kwargs.get('dbHandler', NPPlan.observer)
        self._failDir = kwargs.get('failDir', 'C:\\Temp\\mcnpfails\\')

    def outReceived(self, data):
        print data, # received chunk of stdout from child
        #global gdata
        #gdata = gdata + data
        if self._data == '':
            import datetime
            obj = self._dbHandler.getObjectForFile(self._fileName)
            obj.times.started = datetime.datetime.now()
            obj.status.running = True
            obj.save()
        self._data = self._data + data

    def processExited(self, reason):
        success = 0
        try:
            cData = self._data[self._data.index('bad trouble'):]
            #fname = cData[cData.index('name=')+5:cData.index('fails')].strip()
            #print fname + 'failed!!!'
            import shutil
            import os
            shutil.copy(os.path.join(self._path, self._fileName),
                        os.path.join(self._failDir, self._fileName))
            if os.path.exists(os.path.join(self._path, self._fileName + 'o')):
                shutil.copy(os.path.join(self._path, self._fileName + 'o'),
                            os.path.join(self._failDir, self._fileName + 'o'))
            obj = self._dbHandler.getObjectForFile(self._fileName)
            obj.status.running = False
            obj.status.completed = False
            obj.errorCode = 1
            obj.save()
        except ValueError:
            success = 1
            pass
        if 1 == success:
            try:
                tw = self._data.index('tally data written')
                print 'mfilename ', self._data[:tw+5]
            except ValueError:
                print 'no mfilename'
            import shutil
            import os
            import datetime
            obj = self._dbHandler.getObjectForFile(self._fileName)
            if obj.useMpi is not None and obj.useMpi:
                ext = '.%d' % obj.mpiProc
            else:
                ext = ''
            shutil.copy(os.path.join(self._path, self._fileName + 'o'),
                        os.path.join(self._resultsDir, self._fileName + 'o' + ext))

            obj.paths.output = unicode(os.path.join(self._resultsDir, self._fileName + 'o' + ext))

            if os.path.exists(os.path.join(self._path, self._fileName + 'm')):
                shutil.copy(os.path.join(self._path, self._fileName + 'm'),
                            os.path.join(self._resultsDir, self._fileName + 'm' + ext))
                obj.paths.mfile = unicode(os.path.join(self._resultsDir, self._fileName + 'm' + ext))
            obj.times.completed = datetime.datetime.now()
            obj.status.running = False
            obj.status.completed = True
            obj.save()
        print "processExited, status %d" % (reason.value.exitCode,)

    def processEnded(self, reason):
        global nprocesses
        #nprocesses -= 1
        #if nprocesses == 0: # all processes ended
        if 1:
            #reactor.stop()
            print "processEnded, status %d" % (reason.value.exitCode,)
            print "quitting"
            print 'gdata'
            #print gdata
            #reactor.stop()

    def connectionMade(self):
        print 'Task started'



if __name__ == '__main__':
    import time
    import os
    print 'run main'
    os.chdir('C:\\mcnpx\\executable\\')
    nprocesses = 2
    for i in xrange(nprocesses):
        print i
        reactor.spawnProcess(ProcessProtocol(), 'C:\\mcnpx\\executable\\mcnpx.exe',
                             args=['mcnpx.exe', 'n=tt'+str(i+1)],) # can change how child buffers stdout
        time.sleep(1)
    reactor.run()