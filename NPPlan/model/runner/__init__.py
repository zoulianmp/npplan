# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 05.07.2012
@summary:
'''

from NPPlan.model.runner.oneTask import oneTask
from multiprocessing import Manager
manager = Manager()
_tasks = manager.list([])
_tasksDict = manager.dict({})
_allowedScopes = ['local', 'remote']

def addTask(name, path=None, cmd=None, scope='local'):
    global _tasks, _tasksDict
    o = oneTask(name, cmd=cmd, path=path, scope=scope)
    i = len(_tasks)
    _tasks[i:] = [o]
    _tasksDict[name] = _tasks[i]
    pass
    print o.name
    print o.cmd
    print o
    print o.__dict__
    print _tasksDict
    return o

def getTask(name):
    global _tasksDict
    return _tasksDict[name]


