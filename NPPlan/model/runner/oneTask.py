# -*- coding: utf8 -*-
'''
Обёртка данных по одиночному заданию

Параметры класса:

B{oneTask.name} - имя задания

B{oneTask.path} - путь задания, может быть пустым, тогда аргументы для запуска будут браться из B{oneTask.cmd}

B{oneTask.cmd} - список аргументов запуска

B{oneTask.daemon} - флаг демона

B{oneTask.state} - состояние запуска. -1 - неизвестное состояние, 1 - в процессе запуска, 2 - запущено, 3 - завершено

B{oneTask.scope} - локальный или сетевой запуск. 'local' - локальный, 'remote' - сетевой

B{oneTask.__dict__.type} - параметр задания. 1 - запуск по пути, 2 - запуск по cmd, 3 - запуск по пути, cmd - параметры

B{oneTask.process} - хендл процесса (класс subprocess.Popen)
@package: NPPlan.model.runner
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 24.07.2012
@summary: Обёртка для одиночного задания
'''

import os.path
import NPPlan
import NPPlan.model.runner

class oneTask(object):
    """
    Обёртка для одиночного задания
    """
    def __init__(self, name, path=None, cmd=None, daemon=False, state=None, scope=None):
        """
        Инициализация. Параметр path может быть не задан, иначе параметры cmd будут добавлены к path
        @param name: имя задания
        @type name: string || unicode
        @keyword path: исполняемый файл
        @type path: string
        @keyword cmd: параметры командной строки
        @type cmd: list
        @keyword daemon: флаг демона
        @type daemon: bool
        @keyword state: состояние запуска
        @type state: int
        @keyword scope: локальный или сетевой запуск
        @type scope: string
        """
        self._name = None
        self.name = name
        self._path = None
        self.path = path
        self._cmd = None
        self.cmd = cmd
        self._type = -1
        self._daemon = None
        self.daemon = daemon
        self._state = None
        self.state = state
        self._scope = None
        self.scope = scope
        self._process = None

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @name.deleter
    def name(self):
        del self._name

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        if not value:
            return
        path = os.path.normpath(value)
        path = os.path.realpath(path)
        if 1 == NPPlan.getOs():
            # windows
            path = path.replace('\\', '/')
        self._path = path
        self._type = 1

    @path.deleter
    def path(self):
        del self._path

    @property
    def cmd(self):
        return self._cmd

    @cmd.setter
    def cmd(self, value):
        if not value:
            return
        self._cmd = value
        if self._path:
            self._type = 3
        else:
            self._type = 2

    @cmd.deleter
    def cmd(self):
        del self._cmd

    @property
    def daemon(self):
        return self._daemon

    @daemon.setter
    def daemon(self, value):
        if not isinstance(value, bool):
            value = False
        self._daemon = value

    @daemon.deleter
    def daemon(self):
        del self._daemon

    @property
    def state(self):
        return self.state

    @state.setter
    def state(self, value):
        if not isinstance(value, int):
            value = -1
        self._state = value

    @state.deleter
    def state(self):
        del self._state

    @property
    def scope(self):
        return self.scope

    @scope.setter
    def scope(self, value):
        if value not in NPPlan.model.runner._allowedScopes:
            return
        self._scope = value

    @scope.deleter
    def scope(self):
        del self._scope

    @property
    def process(self):
        return self.process

    @process.setter
    def process(self, value):
        if value not in NPPlan.model.runner._allowedScopes:
            return
        self._process = value

    @process.deleter
    def process(self):
        # @todo: kill process
        del self.process

    def __repr__(self):
        if self._path and self._cmd:
            return "%s  %s" %(self._path, ' '.join(self._cmd))
        #    return "Name: %s\nTask: %s  %s" %(self._name, self._path, ' '.join(self._cmd))
        return self._path or ' '.join(self._cmd)
