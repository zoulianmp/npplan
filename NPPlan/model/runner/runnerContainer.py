# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.02.13
@summary: 
'''


class runnerContainerClass(object):
    def __init__(self, *args, **kwargs):
        self._container = {}
        pass

    def append(self, id, runnerObject):
        self._container[id] = runnerObject

    def get(self, id):
        return self._container[id]

    def delete(self, id):
        obj = self._container[id]
        if 0 == obj._status:
            del self._container[id]

    def isFinished(self, id):
        return self._container[id].status >= 0

    def getData(self, id):
        return self._container[id]._data

    def getDataStruct(self, id):
        if hasattr(self._container[id], '_struct'):
            return self._container[id]._struct

_container = None
def install():
    global _container
    if _container is None:
        _container = runnerContainerClass()

def container():
    """
    @return: container
    @rtype: runnerContainerClass
    """
    global _container
    return _container

install()