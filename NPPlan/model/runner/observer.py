# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 19.09.13
@summary: 
'''

import NPPlan
import os
import datetime


# @todo: read from config/db
taskDir = 'C:\\Temp\\mcnpqueue\\'


class observer(object):
    pass

# @todo: implement windows service observer
# @todo: implement linux daemon observer

# механизм А - автодобавление новых тасков из директории
# 1. чекаем taskDir
# 2. если есть новые файлы (запись в бд для этого имени отсутствует)
#    то добавляем в базу данных с параметрами times.added, status.created = false, status=0
# 3. копируем файлы в нужную mcnp-директорию, в зависимости от version
#    само version должно где-то задаваться
# 4. ставим status.created = true, отслеживаем имена в папке назначения

# механизм B - ручное добавление напрямую в БД
# 1. по таймеру чекаем новые записи в БД
# 2. если находим запись без status.created
#    то делаем пункты 3, 4 механизма А

# механизм C - пускач файлов
# 1. запускаем расчёт в нужной версии mcnp (5/x) через subproccess/rector.spawnProcess (?)
# 2. обновляем поля времён и статусов
# 3. если ошибка, то ставим errCode
# 4. если процесс завершён, то ставим нужные поля


from twisted.internet import protocol, reactor
import platform


class dbHandler(object):
    def __init__(self, taskDir, targetDir, targetMpiDir):
        self._taskDir = taskDir
        self._targetDir = targetDir
        self._targetMpiDir = targetMpiDir
        pass

    def appendNewFile(self, fname):
        obj = NPPlan.mongoConnection.mcnpqueue()
        obj.name = unicode(fname)
        obj.paths.task = unicode(os.path.join(self._taskDir, fname))
        obj.paths.source = unicode(os.path.join(self._targetDir, fname))
        obj.times.added = datetime.datetime.now()
        obj.status.created = True
        obj.useMpi = False
        obj.machineStr = platform.node()
        obj.version = 'mcnpx'
        obj.save()

    def appendNewMpiFile(self, fname, procCount):
        obj = NPPlan.mongoConnection.mcnpqueue()
        obj.name = unicode(fname)
        obj.paths.task = unicode(os.path.join(self._taskDir, fname))
        obj.paths.source = unicode(os.path.join(self._targetMpiDir, fname))
        obj.times.added = datetime.datetime.now()
        obj.status.created = True
        obj.useMpi = True
        obj.mpiProc = procCount
        obj.machineStr = platform.node()
        obj.version = 'mcnp5mpi'
        obj.save()

    def getObjectForFile(self, fName):
        obj = NPPlan.mongoConnection.mcnpqueue.find_one({'name': fName})
        return obj


def handleExistedFiles(tDir, fName, flag=0):
    import os
    import shutil
    import datetime
    if not os.path.exists(os.path.join(tDir, fName)):
        return ''
    if flag == 0:
        newFName = os.path.join(tDir, '%s_%s' % (fName, datetime.datetime.now().strftime('%Y%m%d%H%M%S')))
        shutil.move(
            os.path.join(tDir, fName),
            newFName
        )
        print 'Moving %s to %s' % (os.path.join(tDir, fName), newFName)
        # @todo: handle database
        return newFName
    elif flag == 10:
        try:
            newName = handleExistedFiles(tDir, fName, 0)
            newMName = handleExistedFiles(tDir, fName + 'm', 0)
            newOName = handleExistedFiles(tDir, fName + 'o', 0)
            os.remove(os.path.join(tDir, fName + 'r'))

            obj = NPPlan.observer.getObjectForFile(fName)
            obj.name = unicode(newName)
            obj.paths.task = unicode(os.path.join(tDir, newName))
            if newOName != '':
                obj.paths.output = unicode(os.path.join(tDir, newOName))
            if newMName != '':
                obj.paths.mfile = unicode(os.path.join(tDir, newMName))
            obj.status.moved = True
            obj.save()
        except IOError:
            pass
        except WindowsError:
            pass


def startSimpleFolderObserver():
    from spawnProtocol import ProcessProtocol
    import time
    import os
    import shutil
    global taskDir
    global reactor
    #os.path.normpath()
    print os.path.normpath(NPPlan.config.appConfig.mcnp.taskdir)
    #targetDir = 'C:\\mcnpx\\executable\\'
    targetDir = os.path.normpath(NPPlan.config.appConfig.mcnp.targetdir)
    targetDirMpi = os.path.normpath(NPPlan.config.appConfig.mcnp.targetmpidir)
    failDir = os.path.normpath(NPPlan.config.appConfig.mcnp.faildir)
    oldPath = os.getcwd()
    NPPlan.observer = dbObserver = dbHandler(
        taskDir=os.path.normpath(NPPlan.config.appConfig.mcnp.taskdir),
        targetDir=targetDir, targetMpiDir=targetDirMpi)
    while 1:
        time.sleep(1)
        print '0. Checking dir'
        lD = os.listdir(taskDir)
        if 0 == len(lD):
            continue
        print 'Found', lD[0]
        fName, fExt = os.path.splitext(lD[0])
        if fExt == '':
            if os.path.exists(os.path.join(targetDir, lD[0])):
                #os.remove()
                print 'File %s already exists at %s ' %(lD[0], targetDir)
                handleExistedFiles(targetDir, lD[0], 10)
                continue
            shutil.move(os.path.join(taskDir, lD[0]), targetDir)
            os.chdir(targetDir)
            dbObserver.appendNewFile(lD[0])
            reactor.spawnProcess(ProcessProtocol(filename=lD[0], path=targetDir, dbHandler=dbObserver,
                                                 failDir=failDir),
                                 os.path.join(targetDir, NPPlan.config.appConfig.mcnp.targetexe),
                                 args=[NPPlan.config.appConfig.mcnp.targetexe, 'n=' + lD[0],])
            os.chdir(oldPath)
        else:
            print 'FOUND MPI'
            mpiProc = int(fExt[1:])
            if mpiProc > 4:
                mpiProc = 4
            if os.path.exists(os.path.join(targetDirMpi, fName)):
                #os.remove()
                print 'File %s already exists at %s ' %(fName, targetDirMpi)
                handleExistedFiles(targetDirMpi, fName, 10)
                continue
            shutil.move(os.path.join(taskDir, lD[0]),
                        os.path.join(targetDirMpi, fName))
            os.chdir(targetDirMpi)
            dbObserver.appendNewMpiFile(fName, mpiProc)
            reactor.spawnProcess(ProcessProtocol(filename=fName, path=targetDirMpi, dbHandler=dbObserver,
                                                 failDir=failDir),
                                 os.path.join(targetDirMpi, NPPlan.config.appConfig.mcnp.targetmpirunnerexe),
                                 args=[NPPlan.config.appConfig.mcnp.targetmpirunnerexe,
                                       #NPPlan.config.appConfig.mcnp.targetmpiparams % mpiProc,
                                       '-np', str(mpiProc),   # handle this
                                       NPPlan.config.appConfig.mcnp.targetmpimcnpexe, 'n=' + fName,])
            os.chdir(oldPath)






if __name__ == '__main__':
    NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)
    #startSimpleFolderObserver()
    #reactor.callInThread(startSimpleFolderObserver())
    reactor.callInThread(startSimpleFolderObserver)
    reactor.run()