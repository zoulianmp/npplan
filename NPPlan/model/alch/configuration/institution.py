# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 06.05.14
@summary: 
'''

import NPPlan
from sqlalchemy import Column, Integer, DateTime, Float, String, Unicode, UnicodeText, BigInteger


class institutionModel(NPPlan.coreBase):
    __tablename__ = 'institution'
    id = Column(Integer, primary_key=True)
    idEGRUL = Column(String)
    name = Column(UnicodeText)
    okpo = Column(String)
    inn = Column(BigInteger)
    kpp = Column(String)
    ogrn = Column(String)
    att = Column(String)
    FIO = Column(String)
    headLead = Column(String)
    phone = Column(String)
    address = Column(String)
    lastModified = Column(DateTime)

    def __init__(self, idEGRUL, name, okpo, inn, kpp, ogrn, att, FIO, headLead, phone, address, lastModified):
        self.idEGRUL = idEGRUL
        self.name = name
        self.okpo = okpo
        self.inn = inn
        self.kpp = kpp
        self.ogrn = ogrn
        self.att = att
        self.FIO = FIO
        self.headLead = headLead
        self.phone = phone
        self.address = address
        self.lastModified = lastModified

