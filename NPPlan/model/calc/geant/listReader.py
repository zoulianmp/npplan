# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 23.01.14
@summary: 
'''

import os
import shutil
import numpy as np

from NPPlan.model.calc.base.systemOfUnits import unitBase as myUnit


def t1():
    #filename = r'C:\Temp\geantProjects\testSyncProtons_build\expResults\e31en110.res'
    filename = r'C:\Temp\geantProjects\testSyncProtons_build\expSeries2\et1\exp0\tissue1det_testSoU.out'
    column = 18

    arrMerged = []

    for line in file(filename):
        arr = line.split(' ')
        if not arr[0].isdigit():
            continue
        arr = [i.replace('\t', '') for i in arr]
        arrMerged.append(arr)
        #print arr

    for i in arrMerged:
        print i[column]


def t2():
    orDir = r'C:\Temp\geantProjects\testSyncProtons_build\expSeries'
    targetDir = r'C:\Temp\geantProjects\testSyncProtons_build\expResults'
    sourceFileName = r'tissue1det.out'
    #print os.listdir(orDir)
    for expDir in os.listdir(orDir):
        expPath = os.path.join(orDir, expDir)
        if os.path.isdir(expPath):
            for expOne in os.listdir(expPath):
                expSeriesPath = os.path.join(expPath, expOne)
                if os.path.isdir(expSeriesPath):
                    print expDir, expOne
                    runMacFile = os.path.join(expSeriesPath, 'run.mac')
                    print runMacFile
                    eng = ''
                    for runMacLine in file(runMacFile):
                        print runMacLine
                        if '/gun/energy' in runMacLine:
                            eng = float(runMacLine.split(' ')[1])
                            print eng
                            break
                    if '' != eng:
                        sourcePath = os.path.join(expSeriesPath, sourceFileName)
                        targetFileName = '%sen%s.res' % (expDir[:3], int(eng))
                        print 'From: %s To: %s' % (sourcePath, targetFileName)
                        shutil.copy(sourcePath, os.path.join(targetDir, targetFileName))

#t1()


class listReaderLineFormat(object):
    def __init__(self):
        self._format = {}
        self._values = []

    def add(self, itemValue, itemMeaning='', itemUnitCategory='', itemUnitMeasure=''):
        self._format[itemValue] = {
            'meaning': itemMeaning,
            'unit': (itemUnitCategory, itemUnitMeasure),
        }
        self._values.append(itemValue)
        pass

    def parseUnit(self, value, item):
        # @todo: add cast value
        value = value.replace("\n", "")
        # item -> self._format[item]
        # value -> real value
        if 'positionIndex' == self._format[item]['meaning']:
            return int(value)
        if ('energy' == self._format[item]['meaning']) or ('energyError' == self._format[item]['meaning']):
            return myUnit(value, "Energy")
        if ('dose' == self._format[item]['meaning']) or ('doseError' == self._format[item]['meaning']):
            #value = value.split(" ")
            #return float(value[0])
            return myUnit(value, "Dose")
        if 'number' == self._format[item]['meaning']:
            return int(value)
        pass


class listReaderBase(object):
    def __init__(self, filename):
        self._filename = filename
        self._data = []
        self._lineFormat = None
        self._file = open(self._filename, "r")
        self._delimiter = '\t'

    def setDelimiter(self, delimiter):
        self._delimiter = delimiter

    def setFormat(self, lineFormat):
        """

        @param lineFormat:
        @type lineFormat: listReaderLineFormat
        @return:
        """
        self._lineFormat = lineFormat
        pass

    def _readHeader(self):
        self._file.seek(0)
        line = self._file.readline()
        #print line, line.split(self._delimiter)
        #print len(self._lineFormat._values)
        pass

    def parseSummaryLine(self, line):
        print 'SUMMARY', line.split('\t')
        pass

    def _readData(self):
        for i in self._file.readlines():
            if 'Summary' in i:
                self.parseSummaryLine(i)
                continue
            #print i, i.split(self._delimiter)
            curRawItem = i.split(self._delimiter)
            newItem = {}
            for elemIndex, elemValue in enumerate(self._lineFormat._values):
                #print 'RAW ITEM', curRawItem[elemIndex]
                newItem[elemValue] = self._lineFormat.parseUnit(curRawItem[elemIndex], elemValue)
            self._data.append(newItem)
            #print self._data
        #print self._data
        #import pprint
        #pprint.pprint(self._data)
        pass


class listReaderVoxels(listReaderBase):
    def __init__(self, filename):
        listReaderBase.__init__(self, filename)

    def analyzeVoxelsPositions(self):
        self._maxPosX = -1
        self._maxPosY = -1
        self._maxPosZ = -1
        for i in self._data:
            if i['posX'] > self._maxPosX:
                self._maxPosX = i['posX']
            if i['posY'] > self._maxPosY:
                self._maxPosY = i['posY']
            if i['posZ'] > self._maxPosZ:
                self._maxPosZ = i['posZ']
        print self._maxPosX, self._maxPosY, self._maxPosZ

    def makeNpDose(self):
        dv = [i['depDose'].getAs('Gy', 1) for i in self._data]
        #print dv
        dv = np.array(dv)
        dv = np.reshape(dv, (self._maxPosX + 1, self._maxPosY + 1, self._maxPosZ + 1))
        #print dv
        return dv
        pass


class listReaderMain(object):
    def __init__(self, filename, delimiter="\t"):
        self._filename = filename
        self._dataDelimiter = delimiter
        self._data = {}
        self._curElem = -1
        self._curRaw = {}
        self._curParsed = {}
        self.parse()

    def _readFile(self):
        for line in file(self._filename):
            #self._raw.append(line)
            ll = line.split(self._dataDelimiter)
            if not ll[0].strip(' ').isdigit():
                # header string
                print 'HEADER', ll
                continue
            #self._data[int(ll[0].strip(' '))] = {}
            self.parseLineToData(ll)
            #print ll

    def parseLineToData(self, splitLine, quantity=6, firstOnce=False):
        if firstOnce:
            # @todo: first element (number of experiment) goes only once
            pass
        self._curRaw = {}
        self._curElem = -1
        for elemPos in range(len(splitLine)):
            #elemPos = elem + 7*particle
            elem = elemPos % quantity
            particle = elemPos // quantity
            print elemPos, splitLine[elemPos], elem, particle
            if splitLine[elemPos] == "\n":
                continue
            self.elemToData(elem, particle, splitLine[elemPos])
        self.rawToData()
        pass

    def elemToData(self, elDesignator, paDesignator, rawValue):
        if 0 == elDesignator:
            # index
            print 'INDEX', rawValue, type(rawValue)
            va = int(rawValue.strip(' '))
            if va not in self._data:
                self._data[va] = {}
                self._curElem = va
            self._curRaw[paDesignator] = {}
        elif 1 == elDesignator:
            self._curRaw[paDesignator]['eg'] = rawValue
            #print 'Energy'
        elif 2 == elDesignator:
            self._curRaw[paDesignator]['egErr'] = rawValue
            #print 'Energy error'
        elif 3 == elDesignator:
            self._curRaw[paDesignator]['noe'] = int(rawValue.strip(' '))
            #print 'NoE'
        elif 4 == elDesignator:
            #print 'Dose'
            self._curRaw[paDesignator]['dose'] = rawValue
        elif 5 == elDesignator:
            self._curRaw[paDesignator]['doseErr'] = rawValue
            #print 'Dose error'
        if 0 == paDesignator:
            print ''
        pass

    def rawToData(self):
        self.parseRaw()
        self._data[self._curElem] = {
            'sum': self._curParsed[0],
            'primaryProtons': self._curParsed[1],
            'allProtons': self._curParsed[2],
            'gamma': self._curParsed[3],
            'alpha': self._curParsed[4],
        }

    def parseRaw(self):
        print self._curRaw
        for ind in self._curRaw:
            elem = self._curRaw[ind]
            self._curParsed[ind] = {
                'dose': (elem['dose'], elem['doseErr']),
                'noe': elem['noe'],
                'energy': (elem['eg'], elem['egErr'])
            }
        pass

    def printColumn(self, particle, dataType):
        for i in self._data:
            elv = self._data[i][particle]
            if 'energy' == dataType:
                #print elv['energy'][0]
                print myUnit(elv['energy'][0], "Energy").getAs("MeV", 1)
            elif 'energyError' == dataType:
                #print elv['energy'][1]
                print myUnit(elv['energy'][1], "Energy").getAs("MeV", 1)
            elif 'dose' == dataType:
                print elv['dose'][0]
            elif 'doseError' == dataType:
                print elv['dose'][1]

    def getColumn(self, particle, dataType):
        ret = []
        for i in self._data:
            elv = self._data[i][particle]
            if 'energy' == dataType:
                #print elv['energy'][0]
                ret.append(myUnit(elv['energy'][0], "Energy").getAs("MeV", 1))
            elif 'energyError' == dataType:
                #print elv['energy'][1]
                ret.append(myUnit(elv['energy'][1], "Energy").getAs("MeV", 1))
            elif 'dose' == dataType:
                ret.append(elv['dose'][0])
            elif 'doseError' == dataType:
                ret.append(elv['dose'][1])
            elif 'noe' == dataType:
                ret.append(elv['noe'])
        return ret

    def parse(self):
        self._readFile()
        print self._data
        pass


def draw2():
    import matplotlib.pyplot as plt
    import numpy as np
    filenameWP20 = r'C:\Temp\geantProjects\protonPlexPhantom_build\WPlex\WithPlex20\ff.out'
    wp20 = listReaderMain(filenameWP20)
    filenameWO20 = r'C:\Temp\geantProjects\protonPlexPhantom_build\WOPlex\WOPlex20\ff.out'
    wo20 = listReaderMain(filenameWO20)
    filenameWP50 = r'C:\Temp\geantProjects\protonPlexPhantom_build\WPlex\WithPlex50\ff.out'
    wp50 = listReaderMain(filenameWP50)
    filenameWO50 = r'C:\Temp\geantProjects\protonPlexPhantom_build\WOPlex\WOPlex50\ff.out'
    wo50 = listReaderMain(filenameWO50)
    filenameWP100 = r'C:\Temp\geantProjects\protonPlexPhantom_build\WPlex\WithPlex100\ff.out'
    wp100 = listReaderMain(filenameWP100)
    filenameWO100 = r'C:\Temp\geantProjects\protonPlexPhantom_build\WOPlex\WOPlex100\ff.out'
    wo100 = listReaderMain(filenameWO100)
    filenameWP200 = r'C:\Temp\geantProjects\protonPlexPhantom_build\WPlex\WithPlex200\ff.out'
    wp200 = listReaderMain(filenameWP200)
    filenameWO200 = r'C:\Temp\geantProjects\protonPlexPhantom_build\WOPlex\WOPlex200\ff.out'
    wo200 = listReaderMain(filenameWO200)

    Xwp = np.arange(len(wp50.getColumn('sum', 'energy')))
    Xwo = Xwp+10

    wo50data = wo100.getColumn('sum', 'energy')
    print 'COLUMNS'
    wo100.printColumn('sum', 'energy')
    print 'COLUMN ERRORS'
    wo100.printColumn('sum', 'energyError')
    print 'EOF'
    wp50data = wp100.getColumn('sum', 'energy')

    wo50data2 = wo50.getColumn('primaryProtons', 'energy')
    wp50data2 = wp50.getColumn('primaryProtons', 'energy')

    fig = plt.figure()
    l1, = plt.plot(Xwo, wo50data, color='r')
    l2, = plt.plot(Xwp, wp50data, color='g')
    #l3, = plt.plot(X, wo50data2)
    #l4, = plt.plot(X, wp50data2)
    plt.show()


def draw(_data):
    import matplotlib
    import pylab
    import numpy as np
    import matplotlib.pyplot as plt

    filename1 = r'C:\Temp\geantProjects\testSyncProtons_build\expSeries2\et2\exp0\tissue1det_testSoU.out'
    c1 = listReaderMain(filename1)
    filename2 = r'C:\Temp\geantProjects\testSyncProtons_build\expSeries2\et2\exp0\tissue1det_testSoU.out'
    c2 = listReaderMain(filename2)
    filename3 = r'C:\Temp\geantProjects\testSyncProtons_build\expSeries2\et3\exp0\tissue1det_testSoU.out'
    c3 = listReaderMain(filename3)

    filename_1 = r'C:\Temp\geantProjects\testSyncProtons_build\expSeries2\et2\exp1\tissue1det_testSoU.out'
    ac1 = listReaderMain(filename_1)
    filename_2 = r'C:\Temp\geantProjects\testSyncProtons_build\expSeries2\et2\exp2\tissue1det_testSoU.out'
    ac2 = listReaderMain(filename_2)

    X = np.arange(len(c1.getColumn('sum', 'energy')))

    sum1 = np.array(c1.getColumn('sum', 'energy'))

    sum_1 = np.array(ac1.getColumn('sum', 'energy'))
    sum_2 = np.array(ac2.getColumn('sum', 'energy'))

    sum2 = np.array(c2.getColumn('sum', 'energy'))
    sum3 = np.array(c3.getColumn('sum', 'energy'))
    pP1 = np.array(c1.getColumn('primaryProtons', 'energy'))

    pP_1 = np.array(ac1.getColumn('primaryProtons', 'energy'))
    pP_2 = np.array(ac2.getColumn('primaryProtons', 'energy'))

    pP2 = np.array(c2.getColumn('primaryProtons', 'energy'))
    pP3 = np.array(c3.getColumn('primaryProtons', 'energy'))

    pAP1 = np.array(c1.getColumn('allProtons', 'energy'))
    pAP2 = np.array(c2.getColumn('allProtons', 'energy'))
    pAP3 = np.array(c3.getColumn('allProtons', 'energy'))

    print 'SUM1', sum1
    print 'SUM2', sum2


    data1 = (sum1 - pP1) / sum1
    data_1 = (sum_1 - pP_1) / sum_1
    data_2 = (sum_2 - pP_2) / sum_2
    #data2 = (sum2 - pP2) / sum2
    #data3 = (sum3 - pP3) / sum3
    #data1 = c1.getColumn('sum', 'energy')
    #data2 = c2.getColumn('sum', 'noe')
    #data3 = c3.getColumn('sum', 'noe')
    fig = plt.figure()
    l1, = plt.plot(X, data1, color='r')
    l2, = plt.plot(X, data_1)
    l3, = plt.plot(X, data_2)
    #plt.plot(X, pP1, color='g')
    #plt.plot(X, pAP1, color='b')

    plt.show()
    pass

if __name__ == '__main__':
    #t1()
    #filename = r'C:\Temp\geantProjects\testSyncProtons_build\expSeries2\et1\exp2\tissue1det_testSoU.out'
    filename = r'C:\Temp\geantProjects\protonRealCase_build\Exp1Ph\Exp1Ph2\RAAllDep.out'
    filename2 = r'C:\Temp\geantProjects\protonRealCase_build\Exp1Ph\Exp1Ph1\RAAllDep.out'
    #c = listReaderMain(filename)
    lineFormat = listReaderLineFormat()
    lineFormat.add('posX', 'number', 'int', 'int')
    lineFormat.add('posY', 'number', 'int', 'int')
    lineFormat.add('posZ', 'number', 'int', 'int')
    lineFormat.add('depEnergy', 'energy', 'Energy', 'MeV')
    lineFormat.add('depEnergyError', 'energyError', 'Energy', 'MeV')
    lineFormat.add('depDose', 'dose', 'Dose', 'Gy')
    lineFormat.add('depDoseError', 'doseError', 'Dose', 'Gy')
    lineFormat.add('noe', 'number', 'int', 'int')
    c = listReaderVoxels(filename)
    c.setFormat(lineFormat)
    c._readHeader()
    c._readData()
    c.analyzeVoxelsPositions()
    d1 = c.makeNpDose()

    expDir = r'C:\Temp\geantProjects\protonRealCase_build\Exp36'
    dAll = None
    for seriesDir in os.listdir(expDir):
        if os.path.isdir(os.path.join(expDir, seriesDir)):
            print seriesDir
            for fName in os.listdir(os.path.join(expDir, seriesDir)):
                #print fName
                #print os.path.splitext(fName)
                if 'RAAllDep' not in fName:
                    continue
                if '.out' == os.path.splitext(fName)[1]:
                    filename = os.path.join(expDir, seriesDir, fName)
                    print filename
                    c = listReaderVoxels(filename)
                    c.setFormat(lineFormat)
                    c._readHeader()
                    c._readData()
                    c.analyzeVoxelsPositions()
                    d = c.makeNpDose()
                    if dAll is None:
                        dAll = d
                    else:
                        dAll += d
                    pass

    '''c2 = listReaderVoxels(filename2)
    c2.setFormat(lineFormat)
    c2._readHeader()
    c2._readData()
    c2.analyzeVoxelsPositions()
    d2 = c.makeNpDose()'''

    print dAll, np.max(dAll), np.sum(dAll) / 8000
    #import sys
    #sys.exit(0)
    #d3 = d1 + d2
    #print d3
    from NPPlan.model.calc.geant.isodoseBuilder import isodoseBuilderFromNumpy
    import NPPlan.model.calc.base.isodoseBuilder as ib
    doseBuilder = isodoseBuilderFromNumpy(mFileObject=dAll, slice=10, axis=0, normalize=True,
                                          contourFlag=ib.NPPLAN_CONTOURS_BODY | ib.NPPLAN_CONTOURS_COLORBAR | ib.NPPLAN_CONTOURS_BORDERS | ib.NPPLAN_NORMALIZE_GLOBAL)
    for _axis in range(0, 3):
        for _slice in range(0, 20):
            doseBuilder.title = '20 cells, _axis %d, slice %d' % (_axis, _slice)
            doseBuilder.slice = _slice
            doseBuilder.axis = _axis
            doseBuilder.reset()
            doseBuilder.makeIsodoseImage(area=((0, 20), (0, 20), (0, 20)), outputFileName="%s\\axis%d%s%d.png" %(r'C:\Temp\geantProjects\protonRealCase_build\Exp36\ResultsIsodoses', _axis, 'slice', _slice))
    #doseBuilder.slice = 10
    #doseBuilder.axis = 1
    #doseBuilder.reset()
    #doseBuilder.makeIsodoseImage(area=((-10, 10), (-10, 10), (-10, 10)), outputFileName='screen')
    #print c._data
    #import pprint
    #pprint.pprint(c._data)
    #c.printColumn('primaryProtons', 'energy')
    #print c.getColumn('sum', 'noe')

    #draw(c.getColumn('sum', 'energy'))
    #draw2()