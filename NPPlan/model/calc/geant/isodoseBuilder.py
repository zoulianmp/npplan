# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 15.02.13
@summary: 
'''

from NPPlan.model.calc.base import isodoseBuilderBase
import NPPlan.model.calc.base.isodoseBuilder as ib
from outReader import readerGeantOut

import numpy as np

class isodoseBuilderFromGeant(isodoseBuilderBase):
    def __init__(self, mFileObject=None, slice=-1, axis=0, normalize=True, contourFlag=ib.NPPLAN_CONTOURS_BODY,
                 linspaceAccuracy=128):
        self._input = mFileObject
        self._shape = mFileObject.getShape()
        self._globalMax = None
        isodoseBuilderBase.__init__(self, mFileObject, slice, axis, normalize, contourFlag, linspaceAccuracy)

    def makeValuesSlice(self):
        print self._shape
        _data = self._input._data
        if 0 == self._axis:
            gridData = np.array(_data[self._slice, 0:self._shape[1], 0:self._shape[2]])
        elif 1 == self._axis:
            gridData = np.array(_data[0:self._shape[0], self._slice, 0:self._shape[2]])
        elif 2 == self._axis:
            gridData = np.array(_data[0:self._shape[0], 0:self._shape[1], self._slice])
        self._gridSlice = gridData

    def makeShape(self):
        if 0 == self._axis:
            return self._shape[1], self._shape[2]
        elif 1 == self._axis:
            return self._shape[0], self._shape[2]
        elif 2 == self._axis:
            return self._shape[0], self._shape[1]
        pass

    def makeStartEndPoints(self, area):
        if 0 == self._axis:
            return area[1][0], area[1][1], area[2][0], area[2][1]
        elif 1 == self._axis:
            return area[0][0], area[0][1], area[2][0], area[2][1]
        elif 2 == self._axis:
            return area[0][0], area[0][1], area[1][0], area[1][1]

    def makeSizes(self, area):
        if 0 == self._axis:
            return area[1][1] - area[1][0], area[2][1] - area[2][0],
        elif 1 == self._axis:
            return area[0][1] - area[0][0], area[2][1] - area[2][0],
        elif 2 == self._axis:
            return area[0][1] - area[0][0], area[1][1] - area[1][0]

    def getGlobalMax(self):
        if self._globalMax is None:
            self._globalMax = np.max(self._input._data)
        return self._globalMax

class isodoseBuilderFromNumpy(isodoseBuilderFromGeant):
    def __init__(self, mFileObject=None, slice=-1, axis=0, normalize=True, contourFlag=ib.NPPLAN_CONTOURS_BODY,
                 linspaceAccuracy=128):
        self._input = mFileObject
        self._shape = mFileObject.shape
        self._globalMax = None
        isodoseBuilderBase.__init__(self, mFileObject, slice, axis, normalize, contourFlag, linspaceAccuracy)
    def makeValuesSlice(self):
        print self._shape
        _data = self._input
        if 0 == self._axis:
            gridData = np.array(_data[self._slice, 0:self._shape[1], 0:self._shape[2]])
        elif 1 == self._axis:
            gridData = np.array(_data[0:self._shape[0], self._slice, 0:self._shape[2]])
        elif 2 == self._axis:
            gridData = np.array(_data[0:self._shape[0], 0:self._shape[1], self._slice])
        self._gridSlice = gridData
    def getGlobalMax(self):
        if self._globalMax is None:
            self._globalMax = np.max(self._input)
        return self._globalMax

if __name__ == '__main__':
    #filename = r'C:\Users\Chernukha\G4dcm\new\dicom.out'
    #filename = r'C:\Users\Chernukha\dicom.out'
    #filename = r'C:\1\McnpVsGeant\gtest1\dicom.out'
    filename = r'C:\Temp\geantProjects\testZParametrised2_build\Release_v2_1mlrd\det_testdetOY.out'
    mFile = readerGeantOut(filename, shape=(30, 30, 10), targetColumn=6)
    doseBuilder = isodoseBuilderFromGeant(mFileObject=mFile, slice=2, axis=0, normalize=True, linspaceAccuracy=400,
                                          contourFlag=ib.NPPLAN_CONTOURS_BODY | ib.NPPLAN_CONTOURS_COLORBAR | ib.NPPLAN_CONTOURS_BORDERS | ib.NPPLAN_NORMALIZE_GLOBAL)
    doseBuilder.slice = 5
    doseBuilder.axis = 2
    doseBuilder.reset()
    doseBuilder.makeIsodoseImage(area=((-10, 10), (-10, 10), (-10, 10)))

    import sys
    sys.exit(0)
    #doseBuilder.slice = 16
    #doseBuilder.reset()
    #doseBuilder.makeIsodoseImage(area=((-10, 10), (-10, 10), (-10, 10)), outputFileName='screen')

    for i in range(0, 320, 20):
        doseBuilder.title = 'axis 1, slice ' + str(i)
        doseBuilder.slice = i
        doseBuilder.axis = 1
        doseBuilder.reset()
        doseBuilder.makeIsodoseImage(area=((0, 21), (0, 319), (0, 289)), outputFileName="%s\\%s.png" % (
            'C:\Temp\ebt\cubes_out\cube1z_DosesFullNormed_v4a',
            'axis1slice' + str(i)
        ))
    import sys
    sys.exit(0)
    for i in range(20):
        doseBuilder.title = '20 cells, XZ, slice ' + str(i)
        doseBuilder.slice = i
        doseBuilder.axis = 1
        doseBuilder.reset()
        doseBuilder.makeIsodoseImage(area=((-10, 10), (-10, 10), (-10, 10)), outputFileName="%s\\%s%d.png" %(r'C:\1\McnpVsGeant\gtest1', 'XZslice', i))
    for i in range(20):
        #doseBuilder = isodoseBuilderFromMCNP(mFileObject=mFile, slice=0, axis=i, normalize=False)
        doseBuilder.title = '20 cells, YZ, slice ' + str(i)
        doseBuilder.slice = i
        doseBuilder.axis = 2
        doseBuilder.reset()
        doseBuilder.makeIsodoseImage(area=((-10, 10), (-10, 10), (-10, 10)), outputFileName="%s\\%s%d.png" %(r'C:\1\McnpVsGeant\gtest1', 'YZslice', i))
    '''
        '''