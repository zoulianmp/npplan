# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 14.02.13
@summary: 
'''

import abc
import numpy as np

class readerBase(object):

    __metaclass__ = abc.ABCMeta

    def __init__(self, filename='', slice=0, axis=0):
        self._filename = filename
        self._fileStream = open(self._filename, 'r')

        self._slice = slice
        self._axis = axis
        self._param = {}
        self._data = []

        self.readData()

        self.reshape()
        pass

    @abc.abstractmethod
    def readData(self):
        pass

    @abc.abstractmethod
    def reshape(self):
        pass

    def setParams(self, shape=(64, 64, 3)):
        self._shape = shape
        self._voxelsQuantity = self._shape[0]*self._shape[1]*self._shape[2]

    def getShape(self):
        return self._shape

class readerGeantOut(readerBase):
    def __init__(self, filename='', slice=0, axis=0, shape=(64, 64, 3), targetColumn=1):
        self.setParams(shape)
        self._targetColumn = targetColumn
        readerBase.__init__(self, filename, slice, axis)


    def getNextValue(self):
        line = self._fileStream.readline()
        t = [i for i in line.split(' ')[0:] if i != '']
        return int(t[0]), float(t[self._targetColumn])

    def getSliceNumByIndex(self, index):
        return int(index)/(self._shape[0]*self._shape[1])

    def readData(self):
        #data = [[]]*self._shape[2]
        #print data
        #index, value = self.getNextValue()
        #for i in range(0, self._voxelsQuantity):
        #    if i <= index:
        #        print self.getSliceNumByIndex(index)
        #        data[self.getSliceNumByIndex(index)].append(value)
        #    else:
        #        try:
        #            index, value = self.getNextValue()
        #        except IndexError:
        #            for i in range(len(data[-1]), self._shape[0]*self._shape[1]):
        #                data[-1].append(data[-1][-1])
                    #data[-1] = data[-1]
                    #print data[-1]
        #    pass
        #print data
        #print len(data[0]), len(data[1]), len(data[2])
        #print len(data[0])+len(data[1])+len(data[2])
        self._data = np.array([0]*self._voxelsQuantity, dtype=np.float64)
        j = 0
        for line in self._fileStream.readlines():
            t = [i for i in line.split(' ')[0:] if i != '']
            try:
                self._data[j] = float(t[self._targetColumn])
                j += 1
            except ValueError:
                continue
            except IndexError:
                pass
        print self._data
        print self._data.shape
        pass

    def _reshape(self, arr):
        newArr = [[[0.0 for k in xrange(self._shape[2])] for j in xrange(self._shape[1])] for i in xrange(self._shape[0])]

        for i in range(self._shape[0]):
            for j in range(self._shape[1]):
                for k in range(self._shape[2]):
                    #ix + iy*noX + iz*noX*noY;
                    ind = i + j*self._shape[0] + k*self._shape[0]*self._shape[1]
                    print "TEST", ind, i, j, k
                    newArr[i][j][k] = arr[ind]
        return np.array(newArr)

    def reshape(self):
        #self._data = np.reshape(self._data, self._shape)
        self._data = self._reshape(self._data)
        print self._data
        print self._data.shape
        pass

    def getValueInCell(self, cell):
        if isinstance(cell, int):
            return self._data.flatten()[cell]
        else:
            return self._data[cell[0], cell[1], cell[2]]


if __name__ == '__main__':
    #filename = r'C:\Users\Chernukha\dicom.out'
    filename = r'C:\Temp\protvino_water2e_x64_build\Release\vox_vox1.out'

    r = readerGeantOut(filename, 0, 0, (1, 50, 50), targetColumn=2)
    print r._data

    #print r.getValueInCell(22)
    #print r.getValueInCell((2, 3, 2))

    #print r._data[0].shape
    #for i in r._data[0]:
    #    for j in i:
    #        print j
