# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 28.10.13
@summary: 
'''

import NPPlan
import matplotlib
import matplotlib
import pylab
import numpy as np
import numpy as np
import matplotlib.pyplot as plt
import re


class particlesReaderBase(object):
    def __init__(self, filename='', method=0, typeval=float, **kwargs):
        self._filename = filename
        self._method = 0
        self._typeval = typeval
        self._data = {}
        if kwargs.get('autoparse', False):
            if 0 == self._method:
                self.parseSimple()
            elif 1 == self._method:
                self.parse2Tuple()
        pass

    def _findFirstDigitInString(self, string):
        """
        Возвращает индекс первого числового элемента в строке или None
        @param string: строка
        @type string: str
        @return: индекс первого числового элемента
        @rtype: int
        """
        # works faster
        m = re.search("\d", string)
        if m:
            return m.start()
        return None

    @staticmethod
    def _findFirstDigitInStringS(string):
        """
        Возвращает индекс первого числового элемента в строке или None
        @param string: строка
        @type string: str
        @return: индекс первого числового элемента
        @rtype: int
        """
        # works faster
        m = re.search("\d", string)
        if m:
            return m.start()
        return None

    def parseDataString(self, line, append=False, curLayer=-1):
        colonSplit = line.split(',')
        #name = colonSplit[0][3:]
        name = colonSplit[0][colonSplit[0].index('[') + 1:]
        try:
            val = self._typeval(colonSplit[1][0:-2])
        except AttributeError:
            val = float(colonSplit[1][0:-2])
        if append:
            if curLayer == -1 and not hasattr(self, '_curLayer'):
                return None
            if curLayer == -1:
                curLayer = self._curLayer
            self._data[curLayer][name] = val
        else:
            return name, val

    def parseSimple(self):
        with open(self._filename) as fp:
            curLayer = -1
            for line in fp:
                if 'Layer' in line:
                    self._data[int(line[6:])] = {}
                    curLayer = int(line[6:])
                if len(line) < 2:
                    curLayer = -1
                if -1 == curLayer:
                    continue
                if '[' in line:
                    self.parseDataString(line, True, curLayer)
                if 'Summary events' in line:
                    self._data[curLayer]['summary'] = int(line.split(':')[1][1:-1])
                    curLayer = -1

    def parse2Tuple(self):
        with open(self._filename) as fp:
            curLayer = -1
            for line in fp:
                if 'Layer' in line:
                    self._data[int(line[6:])] = []
                    curLayer = int(line[6:])
                if len(line) < 2:
                    curLayer = -1
                if -1 == curLayer:
                    continue
                if '[' in line:
                    t = self.parseDataString(line, False)
                    self._data[curLayer].append(t)

    def getAggregatedData(self):
        if hasattr(self, '_aggregatedData'):
            return self._aggregatedData
        self._aggregatedData = {}
        for layer in self._data:
            self._aggregatedData[layer] = {}
            for k in self._data[layer]:
                #m = re.search("\d", k)
                m = self._findFirstDigitInString(k)
                #try:
                    #m = re.search("\d", k)
                if m:
                    try:
                        self._aggregatedData[layer][k[:m]] += self._data[layer][k]
                    except KeyError:
                        self._aggregatedData[layer][k[:m]] = self._data[layer][k]
                #except ValueError:
                else:
                    self._aggregatedData[layer][k] = self._data[layer][k]
                pass
        return self._aggregatedData

    @staticmethod
    def getAggregatedDataS(data):
        ag = {}
        for layer in data:
            ag[layer] = {}
            for k in data[layer]:
                #m = re.search("\d", k)
                m = particlesReaderBase._findFirstDigitInStringS(k)
                #try:
                    #m = re.search("\d", k)
                if m:
                    try:
                        ag[layer][k[:m]] += data[layer][k]
                    except KeyError:
                        ag[layer][k[:m]] = data[layer][k]
                #except ValueError:
                else:
                    ag[layer][k] = data[layer][k]
                pass
        return ag

    def __div__(self, other):
        # @todo: return own type, not dict
        nd = {}
        for layer in self._data:
            sl = self._data[layer]
            ol = other._data[layer]
            #t = type(sl)
            #t = type(sl.values()[0])
            #print t
            if isinstance(sl, dict):
                nd[layer] = {}
                for key in sl:
                    nd[layer][key] = float(sl[key]) / float(ol[key])
        return nd

    def __floordiv__(self, other):
        # don't use this in most cases, cause aggregated data less representative
        nd = {}
        sd = self.getAggregatedData()
        od = other.getAggregatedData()
        for layer in self._data:
            sl = sd[layer]
            ol = od[layer]
            #t = type(sl)
            #t = type(sl.values()[0])
            #print t
            if isinstance(sl, dict):
                nd[layer] = {}
                for key in sl:
                    nd[layer][key] = float(sl[key]) / float(ol[key])
        return nd


class particlesReader(particlesReaderBase):
    def __init__(self, filename='', autoparse=True):
        particlesReaderBase.__init__(self, filename, 0, float, autoparse=autoparse)

    def getData(self):
        return self._data

    def _findFirstDigitInStringOld(self, string):
        # @deprecated
        for i, c in enumerate(string):
            if c.isdigit():
                return i
        return None

    def getNormalizedData(self, nval=100, af='sum', target='d'):
        rd = {}
        if 'd' == target:
            dsource = self._data
        elif 'n' == target:
            dsource = self.getAggregatedData()
        for layer in dsource:
            rd[layer] = {}
            if 'sum' == af:
                nmax = np.sum(self._data[layer].values())
            elif 'max' == af:
                nmax = np.max(self._data[layer].values())
            for k in self._data[layer]:
                rd[layer][k] = nval * float(self._data[layer][k]) / nmax
            pass
        pass
        return rd

    def getSliceForElement(self, element, origin='n'):
        arr = []
        if 'n' == origin:
            data = self.getAggregatedData()
        else:
            # @todo: handle tupled values
            data = self.getData()
        for layer in data:
            #print data[layer]
            try:
                arr.append(data[layer][element])
            except KeyError:
                arr.append(0)
                pass
        return np.array(arr)


class glassSummaryReader(particlesReaderBase):
    def __init__(self, filename, datatype='d'):
        particlesReaderBase.__init__(self, filename, autoparse=False)
        if 'd' == datatype:
            self._data = {}
        elif 'l':
            self._data = []

    def parseDataString(self, line):
        vv = line.split(' ')
        o = {
            'name': vv[1],
            'kineticSum': float(vv[4]),
            'deposit': float(vv[6]),
            'events': float(vv[8][:-1]),
            'kineticAvg': float(vv[4]) / float(vv[8][:-1]),
        }
        return o

    def parseSimple(self):
        with open(self._filename) as fp:
            for line in fp:
                p = self.parseDataString(line)
                if isinstance(self._data, dict):
                    self._data[p['name']] = p
                else:
                    self._data.append(p)

    def getAnyKey(self, key, retType='l'):
        # @todo: implement tuple
        if 's' == retType:
            ret = []
            if isinstance(self._data, dict):
                for k in self._data:
                    ret.append(self._data[k][key])
        elif 'd' == retType:
            ret = {}
            if isinstance(self._data, dict):
                for k in self._data:
                    ret[k] = self._data[k][key]
        elif 'l' == retType:
            ret = []
            if isinstance(self._data, dict):
                for k in self._data:
                    ret.append((k, self._data[k][key]))
        return ret

    def getAggregatedData(self):
        # @todo: implement
        if hasattr(self, '_aggregatedData'):
            return self._aggregatedData
        self._aggregatedData = {}
        return self._aggregatedData

    def normalize(self, source=None):
        if source is None:
            source = self._data
        print 'source'
        print source
        cf = source['C12[0.0]']

        ret = {'C12': cf}
        normSum = 0.0
        for key in source:
            if key == 'C12[0.0]':
                continue
            orKey = key
            if '[' in key:
                key = key[:key.index('[')]
            fdI = self._findFirstDigitInString(key)
            if fdI != -1:
                key = key[:fdI]
            print key
            normSum += source[orKey]
            try:
                ret[key] += source[orKey]
            except KeyError:
                ret[key] = source[orKey]

        for n in ret:
            ret[n] /= normSum
        #ret['C12'] = cf
        return ret
        pass


class particleEnergiesReader(object):
    # @deprecated
    def __init__(self, filename):
        self._filename = filename
        self._data = {}
        self._rawData = {}
        self.parse()

    def parse(self):
        with open(self._filename) as fp:
            curLayer = -1
            for line in fp:
                if 'Layer' in line:
                    self._data[int(line[6:])] = {}
                    curLayer = int(line[6:])
                    self._rawData[curLayer] = []
                if -1 == curLayer:
                    continue
                if '[' in line:
                    colonSplit = line.split(',')
                    #print colonSplit
                    #self._rawData[curLayer] = (colonSplit[0][3:], float(colonSplit[1][0:-2]))
                    self._rawData[curLayer].append((colonSplit[0][3:], float(colonSplit[1][0:-2])))
                    try:
                        self._data[curLayer][colonSplit[0][3:]] += float(colonSplit[1][0:-2])
                    except KeyError:
                        self._data[curLayer][colonSplit[0][3:]] = float(colonSplit[1][0:-2])


# @todo: rework to global histogram builder
class histogramBuilder(object):
    def __init__(self, data=None, targetLayer=-1):
        self._data = data
        self._targetLayer = targetLayer

    def build(self):
        if -1 == self._targetLayer:
            dataSlice = self._data
        else:
            dataSlice = self._data[self._targetLayer]
        try:
            _sum = dataSlice.pop('summary')
            nGroups = len(dataSlice) - 1
        except KeyError:
            _sum = -1
            nGroups = len(dataSlice)
        except TypeError:
            _sum = -1
            nGroups = len(dataSlice)
        nValues = dataSlice.values()
        fig, ax = plt.subplots()
        index = np.arange(nGroups)
        barWidth = 0.35
        opacity = 0.5
        rects1 = plt.bar(index, nValues, barWidth,
                         alpha=opacity,
                         color='b',
                         label='Layer ')
        #plt.xlabel('Group')
        #plt.ylabel('Scores')
        #plt.title('%d events at layer %s' % (_sum, self._targetLayer, ))
        #print np.mean(nValues)
        #plt.title('%d events at layer %s' % (_sum, self._targetLayer, ))
        plt.xticks(index + barWidth, dataSlice.keys())
        plt.legend()

        plt.tight_layout()
        plt.show()
        pass


class graphBuilder(object):
    def __init__(self, dataObject, aggregate=False, autostart=False, normalize=0):
        self._dataObject = dataObject
        if aggregate and hasattr(dataObject, 'getAggregatedData'):
            #import copy
            #self._rawDataObject = copy.copy(self._dataObject)
            self._rawDataObject = self._dataObject
            self._dataObject = self._dataObject.getAggregatedData()
        elif aggregate:
            self._rawDataObject = self._dataObject
            self._dataObject = particlesReaderBase.getAggregatedDataS(self._dataObject)
        elif not aggregate and hasattr(dataObject, 'getData'):
            self._dataObject = self._dataObject.getData()

        self._particles = {}
        self._particleCaptions = {}
        self._normalizeFlag = normalize

        self.initPlot()
        if autostart:
            self.build()
        pass

    def getSliceForElement(self, element):
        if hasattr(self._dataObject, 'getSliceForElement'):
            return self._dataObject.getSliceForElement(element)
        arr = []
        for layer in self._dataObject:
            try:
                arr.append(self._dataObject[layer][element])
            except KeyError:
                arr.append(0)
                pass
        return np.array(arr)

    def addPlotParticle(self, element, parentElement=None, caption=''):
        if 'All' != element:
            if parentElement is None:
                self._particles[element] = self.getSliceForElement(element)
                self._particleCaptions[element] = caption
            else:
                t = self.getSliceForElement(element)
                if parentElement in self._particles:
                    self._particles[parentElement] += t
                else:
                    self._particles[parentElement] = t
        else:
            self._particleCaptions[element] = caption
            arr = []
            for layer in self._dataObject:
                _sum = 0.0
                for particle in self._dataObject[layer]:
                    print self._dataObject[layer][particle]
                    _sum += self._dataObject[layer][particle]
                arr.append(_sum)
            print arr, len(arr), len(self._dataObject)
            self._particles[element] = arr
            pass

    def initPlot(self):
        fig = plt.figure()
        self._ax = fig.add_subplot(111)

    def build(self):
        X = np.arange(len(self._dataObject))
        if 1 == self._normalizeFlag:
            cumul = np.array([0] * X)
            for k in self._particles:
                cumul += self._particles[k]
            for k in self._particles:
                self._particles[k] /= cumul
            pass
        items = []
        for key in self._particles:
            item, = plt.plot(X, self._particles[key])
            items.append(item)
        plt.legend( items, self._particleCaptions.values(), 'center right', shadow=True)
        plt.xticks(np.arange(min(X), max(X)+1, 2.0))
        self._ax.grid(True)
        plt.show()


class barChartReader(object):
    def __init__(self, filename='', ):
        self._filename = filename
        self._started = False
        self._data = {}
        self._curKey = None
        self._listOfParticles = []
        pass

    def _parseLine(self, line):
        if '-------------------' in line:
            if 'END' not in line:
                self._clName = line.split(' ')[2]
                self._started = True
            else:
                self._started = False
                self._curKey = None
        elif 'Gap' in line:
            t = line.split(' ')
            print t
            _k = (int(t[1]), int(t[3][:-1]))
            print _k
            self._curKey = _k
            self._data[_k] = {}
        else:
            if self._curKey is None:
                return
            t = line.split(' ')
            if len(t) < 2:
                return
            print t
            if t[0] not in self._listOfParticles:
                self._listOfParticles.append(t[0])
            self._data[self._curKey][t[0]] = t[1][:-1]

    def read(self):
        with open(self._filename) as fp:
            for line in fp:
                self._parseLine(line)
        print self._data

    def getNonZeroIntervals(self):
        ret = []
        for i in sorted(self._data):
            if len(self._data[i]) != 0:
                ret.append(i)
        return ret

    def getAggregatedData(self):
        ret = {}

        # first level aggregate
        for en in sorted(self._data):
            ret[en] = {}
            for pa in self._data[en]:
                m = particlesReaderBase._findFirstDigitInStringS(pa)
                if m is None:
                    ret[en][pa] = self._data[en][pa]
                else:
                    na = pa[:m]
                    if na in ret[en]:
                        ret[en][na] += float(self._data[en][pa])
                    else:
                        ret[en][na] = float(self._data[en][pa])

        # second level aggregate
        # @todo: write aggregations
        return ret
        pass

    def getChartData(self, particle):
        ret = []
        nz = self.getNonZeroIntervals()
        data = self.getAggregatedData()
        print data
        for en in sorted(data):
            if en in self.getNonZeroIntervals():
                try:
                    ret.append(float(data[en][particle]))
                except KeyError:
                    ret.append(0)
            #print en
            pass
        return ret
        pass


class spectrumBuilder(object):
    def __init__(self, barObjects=None, sType='line', **kwargs):
        self._barObject = barObjects
        self._target = {}
        self._type = sType
        self._title = kwargs.get('title', 'Spectrum')
        pass

    def _expandWithZeroes(self, source, _len):
        ret = []
        for i in range(_len):
            try:
                ret.append(source[i])
            except IndexError:
                ret.append(0.0)
        return ret

    def addParticle(self, name, colors, titles):
        # @todo: expand to many object
        print 'addParticle at spectre'
        d1 =  self._barObject[0].getChartData(name)
        d2 =  self._barObject[1].getChartData(name)
        print d1
        print d2
        if len(d1) < len(d2):
            _len = len(d2)
            d1 = self._expandWithZeroes(d1, len(d2))
        elif len(d2) < len(d1):
            _len = len(d1)
            d2 = self._expandWithZeroes(d2, len(d1))
        else:
            _len = len(d1)
        print d1
        print d2
        self._target[name] = {
            'length' : _len,
            'objects' : {
                0: d1,
                1: d2,
            },
            'colors': colors,
            'title': titles,
        }
        #X1 = np.arange(len(d1))
        #X2 = np.arange(len(d2))
        #l1, = plt.plot(X1, d1)
        #l2, = plt.plot(X2, d2)

        print 'addParticle at spectre ends'
        pass

    def show(self):
        if 'line' == self._type:
            self.showLines()

    def showLines(self):
        print 'Spectrum SHOW'
        appender = []
        allTitles = []
        for i in self._target:
            print i
            x1 = plt.plot(np.arange(self._target[i]['length']), self._target[i]['objects'][0], color=self._target[i]['colors'][0])
            x2 = plt.plot(np.arange(self._target[i]['length']), self._target[i]['objects'][1], color=self._target[i]['colors'][1])
            appender.append(x1[0])
            appender.append(x2[0])
            allTitles.extend(self._target[i]['title'])
        print 'Spectrum ENDS'
        self._indxs = np.arange(len(self._barObject[1].getNonZeroIntervals()))
        xt = self._barObject[1].getNonZeroIntervals()
        xt2 = []
        for i in xt:
            print i, type(i)
            xt2.append(i[1])
        plt.xticks(self._indxs, xt2)
        plt.legend(appender, allTitles, 'center right', shadow=True)
        plt.title(self._title)
        plt.show()


class barChartBuilder(object):
    def __init__(self, barObject, barWidth=0.1, outTo=None):
        """
        @type barObject: barChartReader
        """
        self._barObject = barObject
        self._indxs = np.arange(len(self._barObject.getNonZeroIntervals()))
        self._barWidth = barWidth
        self._outTo = outTo
        self._curP = 0
        self.initPlot()

    def initPlot(self):
        fig, ax = plt.subplots()

    def addParticle(self, particle, color, label):
        data = self._barObject.getChartData(particle)
        print 'data', data
        plt.bar(self._indxs + self._barWidth * self._curP, data, self._barWidth, color=color,
                        label=label)
        self._curP += 1

    def build(self):
        #index = np.arange(len(barObject._data))

        #print barObject.getNonZeroIntervals()

        #print data
        j = 0
        #colors = ('b', 'r', 'g')
        #for pa in barObject._listOfParticles:
        #    print pa
        #    data = barObject.getChartData(pa)
        #    plt.bar(index + bar_width * j, data, bar_width, color=colors[j % 3],
        #                     label=pa)
        #    j += 1
        #data = barObject.getChartData('proton')
        #plt.bar(index + bar_width * 0, data, bar_width, color='r',
        #        label='proton')
        #data = barObject.getChartData('alpha')
        #plt.bar(index + bar_width * 1, data, bar_width, color='g',
        #        label='alpha')
        #data = barObject.getChartData('C')
        #plt.bar(index + bar_width * 2, data, bar_width, color='b',
        #        label='C')

        plt.xlabel('Energy, MeV')
        plt.ylabel('Number of events')
        plt.title('Spectrum for ' + self._barObject._clName)
        xt = self._barObject.getNonZeroIntervals()
        xt2 = []
        for i in xt:
            print i, type(i)
            xt2.append(i[1])
        plt.xticks(self._indxs + self._barWidth, xt2)
        plt.legend()
        if self._outTo is None:
            plt.show()
        else:
            plt.savefig(self._outTo)
        pass
    pass


def layersDep(filename):
    pass
    data = open(filename).readlines()
    X = np.arange(len(data))
    print X
    print data
    res = []
    for line in data:
        res.append(line.split(' ')[2][:-1])
    print res
    res = np.array(res)
    l1, = plt.plot(X, res)
    plt.show()


def layersManyDep(fileList, legend=None, title=None, fileList2=None):
    qq = []
    j = 0
    for filename in fileList:
        data = open(filename).readlines()
        X = np.arange(len(data))
        print X
        print data
        res = []
        for line in data:
            res.append(float(line.split(' ')[2][:-1]))
        print res
        res = np.array(res)
        if fileList2 is not None:
            resCo = []
            dataCo = open(fileList2[j]).readlines()
            for line in dataCo:
                resCo.append(int(line.split(' ')[2][:-1]))
            resCo = np.array(resCo)
            res /= resCo
        l1, = plt.plot(X, res)
        qq.append(l1)
        j += 1
    plt.xlabel('No.')
    if title is not None:
        plt.title(title)
    if legend is not None:
        plt.legend(qq, legend, 'upper right', shadow=True, )
        #plt.legend([plt], legend)
    plt.show()


def cpl(data, obj=None):
    """

    @param data:
    @param obj:
    @type obj: particlesReader
    @return:
    """
    if data is None:
        data = obj.getAggregatedData()
        #data = obj.getData()
    X = np.arange(len(data))
    print X
    print data
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #print obj.getSliceForElement('H')
    #h = obj.getSliceForElement('H')
    h = obj.getSliceForElement('proton')
    deuteron = obj.getSliceForElement('deuteron')
    triton = obj.getSliceForElement('triton')
    #h += proton
    h += deuteron
    h += triton
    he = obj.getSliceForElement('He')
    alpha = obj.getSliceForElement('alpha')
    he += alpha
    li = obj.getSliceForElement('Li')
    be = obj.getSliceForElement('Be')
    b = obj.getSliceForElement('B')
    c = obj.getSliceForElement('C')
    neutron = obj.getSliceForElement('neutron')
    #h /= np.max(h)
    #he /= np.max(he)
    #li /= np.max(li)
    #be /= np.max(be)
    #b /= np.max(b)
    #c /= np.max(c)
    #neutron /= np.max(neutron)
    #h /= np.sum(h)
    #he /= np.sum(he)
    #li /= np.sum(li)
    #be /= np.sum(be)
    #b /= np.sum(b)
    #c /= np.sum(c)
    #neutron /= np.sum(neutron)
    cumul = h + he + li + be + b + c + neutron
    h /= cumul
    he /= cumul
    li /= cumul
    be /= cumul
    b /= cumul
    c /= cumul
    neutron /= cumul
    l1, = plt.plot(X, h)
    l2, = plt.plot(X, he)
    l3, = plt.plot(X, li)
    l4, = plt.plot(X, be)
    l5, = plt.plot(X, b)
    l6, = plt.plot(X, c)
    l7, = plt.plot(X, neutron)
    plt.legend( (l1, l2, l3, l4, l5, l6, l7), ('proton+deutron+neutron', 'He+alpha', 'Li', 'Be', 'B', 'C', 'neutron'), 'upper right', shadow=True)
    plt.xticks(np.arange(min(X), max(X)+1, 2.0))
    ax.grid(True)
    plt.show()
    pass


if __name__ == '__main__':
    #filename = r'c:\1\pgg\particleLayers.out'
    #filename = r'C:\1\pgg1m\particleEnergies.out'
    #filenameKinetic = r'c:\1\pggte100\particleKinetic.out'
    fdir = 'C:\\Temp\\protvino_chambers_exp\\e54o1e1_del\\exp3\\'
    depositFName = fdir+r'particleDepositEnergies.out'
    kineticFName = fdir+r'particleKineticSum.out'
    countFName = fdir+r'particleLayers.out'
    import datetime
    #a = datetime.datetime.now()
    #r = particlesReader(filename)
    #print r._data
    #r = particleEnergiesReader(filename)
    #rk = particleEnergiesReader(filenameKinetic)
    import pprint
    #pprint.pprint(r._data)
    #print 'raw data'
    #pprint.pprint(rk._rawData)
    #print particlesReaderBase.parseString(None,   "[B10[1740.2], 7.09304e-06]\n")
    rd = particlesReader(depositFName)
    #rd.parseSimple()
    #pprint.pprint(rd._data)

    rk = particlesReader(kineticFName)
    #rk.parseSimple()
    #pprint.pprint(rk._data)
    #pprint.pprint(rk.getAggregatedData())

    rc = particlesReader(countFName)
    #rc.parseSimple()

    rMean = rk / rc

    #gdText = r'C:\1\w4bb_r\glassData.out'

    #gdK = glassSummaryReader(gdText)
    #gdK.parseSimple()
    #pprint.pprint(gdK._data)
    #print gdK.getAnyKey('deposit', 'd')

    #q = rd / rc
    #for i in q:
    #    print i, q[i]['C12[0.0]']

    #print 'mean all'
    #pprint.pprint(rMean)
    #print 'mean aggregated'
    #pprint.pprint(rk // rc)

    #r.parseTuples()
    #pprint.pprint(r._data)

    #kineticFName / countFName
    #print gdK.normalize(source=gdK.getAnyKey('deposit', 'd'))

    #hb = histogramBuilder(rMean, 20)
    #hb = histogramBuilder(gdK.normalize(source=gdK.getAnyKey('deposit', 'd')))
    #hb.build()
    #print np.sum(rd.getNormalizedData(100)[0].values())
    #pprint.pprint(rd.getData())
    #print rd.getAggregatedData()
    #print len(rc.getAggregatedData()[0])
    #rcnp = np.array(rd.getAggregatedData()[0].values())
    #print len(rcnp[rcnp>0])
    #cpl(rc.getAggregatedData())
    #cpl(None, rd)
    #cpl(rMean)

    #fLayersDep = fdir+r'edep_all_containersWater.out'
    expNum = 0
    #for i in ['5.1', '5.2', '5.3', '5.4', '5.5']:
    if 0:
        expNum = 4
        i = '5.4'
        fdir = 'C:\\Temp\\protvino_chambers_exp\\el25o1e1_de\\exp%s\\' % expNum
        expNum += 1
        #layersDep(fLayersDep)
        fileList = [fdir+'edep_all_containersWater.out',
                    fdir+'edep_alpha_containersWater.out',
                    fdir+'edep_carbon_containersWater.out',
                    fdir+'edep_proton_containersWater.out']
        fileList2 = [fdir+'c_all_containersWater.out',
                    fdir+'c_alpha_containersWater.out',
                    fdir+'c_carbon_containersWater.out',
                    fdir+'c_proton_containersWater.out']
        legend = ['All', 'Alpha', 'Carbon', 'Proton']
        title = 'Summary deposited energy, Esource=%s GeV' % i
        layersManyDep(fileList, legend, title)
    #import sys
    #sys.exit(0)

    gr = graphBuilder(rc, normalize=0, aggregate=True)
    gr.addPlotParticle('proton', None, 'proton+deutron+triton')
    gr.addPlotParticle('deuteron', 'proton')
    gr.addPlotParticle('triton', 'proton')
    gr.addPlotParticle('gamma', None, 'Gamma')
    gr.addPlotParticle('He', None, 'He+alpha')
    gr.addPlotParticle('alpha', 'He')
    gr.addPlotParticle('Li', None, 'Li')
    gr.addPlotParticle('Be', None, 'Be')
    gr.addPlotParticle('B', None, 'B')
    gr.addPlotParticle('C', None, 'Carbon')
    gr.addPlotParticle('All', None, 'All')
    gr.build()

    #br = barChartReader(fdir+'detector_lastLayer_target_firstSpectrum.out')
    #fdir = 'C:\\Temp\\protvino_chambers_exp\\e54o1e1_del\\exp4\\'
    file0 = r'C:\Temp\protvino_chambers_exp\e54o0e1_del\exp1\detector_lastLayer_target_lastSpectrum.out'
    file1 = r'C:\Temp\protvino_chambers_exp\e54o1e1_del\exp1\detector_lastLayer_target_lastSpectrum.out'
    try:
        br = barChartReader(fdir+'detector_lastLayer_target_lastSpectrum.out')
        br.read()
    except IOError:
        br = barChartReader(fdir+'detector_lastLayer_target_firstSpectrum.out')
        br.read()

    print 'agg start'
    br0 = barChartReader(file0)
    br0.read()
    br1 = barChartReader(file1)
    br1.read()
    spectrum = spectrumBuilder([br0, br1], sType='line', title='Exp1: 19+1 chambers')
    spectrum.addParticle('C', ['#00ffff', '#0000ff'], ['carbon water', 'carbon air'])
    spectrum.addParticle('alpha', ['#000000', '#cccccc'], ['alpha water', 'alpha air'])
    spectrum.addParticle('neutron', ['#ff0000', '#E2007A'], ['neutron water', 'neutron air'])
    spectrum.addParticle('proton', ['#29AB87', '#00ff00'], ['proton water', 'proton air'])
    spectrum.show()
    print 'agg end'

    bb = barChartBuilder(br)
    bb.addParticle('alpha', '#000000', 'alpha')
    bb.addParticle('C', '#00ffff', 'carbon')
    bb.addParticle('neutron', '#00ff00', 'neutron')
    bb.addParticle('proton', '#0000ff', 'proton')
    bb.addParticle('deuteron', '#2205fa', 'deuteron')
    bb.addParticle('Li', '#ff00ff', 'Li')
    bb.addParticle('B', '#ff0000', 'B')
    #bb.addParticle('electron', '#ff2a00', 'electron')
    bb.build()

