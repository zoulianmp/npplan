import unittest
from NPPlan.model.calc.mcnp.mReader import mcnpReaderGrid, mcnpReaderSimple

class testGridReal(unittest.TestCase):
    def setUp(self):
        self._filename = r'c:\mcnp5\mcnplanm'
        self._reader = mcnpReaderGrid(self._filename, shape=(35, 30, 30))

    def test_maxValueShape(self):
        c = self._reader._data[6]['maxValuesPerSlice']
        self.assertEqual(len(c), 35)

    def test_meanErrorShape(self):
        c = self._reader._data[6]['meanErrorsPerSlice']
        self.assertEqual(len(c), 35)

    def test_firstTally(self):
        self.assertEqual(self._reader.getFirstTally(), 6)

    def test_valueInCell(self):
        self.assertEqual(self._reader.getValueInCell(6, 0), 8.97858E-05)
        self.assertEqual(self._reader.getValueInCell(6, 1), 3.68090E-05)
        self.assertEqual(self._reader.getValueInCell(6, 0, 0, 1), 3.68090E-05)
        self.assertEqual(self._reader.getValueInCell(6, (0, 0, 1)), 3.68090E-05)
        self.assertEqual(self._reader.getValueInCell(6, (0, 0, 29)), self._reader.getValueInCell(6, 29))
        self.assertEqual(self._reader.getValueInCell(6, (0, 1, 0)), self._reader.getValueInCell(6, 30))
        self.assertEqual(self._reader.getValueInCell(6, (0, 1, 1)), self._reader.getValueInCell(6, 31))
        self.assertEqual(self._reader.getValueInCell(6, (0, 1, 29)), self._reader.getValueInCell(6, 59))
        self.assertEqual(self._reader.getValueInCell(6, (1, 0, 0)), self._reader.getValueInCell(6, 900))
        self.assertEqual(self._reader.getValueInCell(6, (1, 0, 1)), self._reader.getValueInCell(6, 901))
        self.assertEqual(self._reader.getValueInCell(6, (34, 29, 29)), self._reader.getValueInCell(6, self._reader._data[6]['quantity']-1))
        self.assertEqual(self._reader.getValueInCell(6, (34, 29, 29)), 1.02187E-03)
        print 30*30*5+30*12+20
        self.assertEqual(self._reader.getValueInCell(6, (5, 12, 20)), self._reader.getValueInCell(6, 30*30*5+30*12+20))
        pass

    def test_valueTupleInCell(self):
        self.assertEqual(self._reader.getValueTupleInCell(-1, 0), (8.97858E-05, 0.8605))

    def tearDown(self):
        del self._reader

class testSingle(unittest.TestCase):
    def setUp(self):
        self._filename = r'C:\NPPlan3\NPPlan\stuff\Tubusm'
        self._reader = mcnpReaderSimple(filename=self._filename)

    def test_valueInCell(self):
        self.assertEqual(self._reader.getValueInCell(cell=8, tally=16), 3.51606E-05)
        self.assertEqual(self._reader.getValueInCell(cell=27, tally=16), 3.32260E-05)
        self.assertEqual(self._reader.getValueInCell(cell=12, tally=6), 1.07198E-05)
        self.assertEqual(self._reader.getValueInCell(cell=8, tally=6), 3.51606E-05)

    def test_getFirstTally(self):
        self.assertEqual(self._reader.getFirstTally(), 16)

    def test_others(self):
        self.assertEqual(self._reader._data[6]['others']['d'], 1)
        self.assertEqual(self._reader._data[6]['others']['s'], 0)

    def tearDown(self):
        del self._reader


if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(testGridReal))
    suite.addTest(unittest.makeSuite(testSingle))
    unittest.main()
