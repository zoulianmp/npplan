import unittest

class testImage2Grid(unittest.TestCase):
    def setUp(self):
        from NPPlan.model.calc.mcnp import singleImageToGridParallel
        self.func = singleImageToGridParallel

    def test_oneImg(self):
        self.assertIn({0: 1.0, 128: 0.0, 256: 0.0}, self.func(r'C:\NPPlan2\Data\4e1ad18d5157510c64000000\Segmented\slice.1', jobs=2))

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(testImage2Grid))
    unittest.main()
