# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 22.01.13
@summary: 
'''

import NPPlan
import unittest


class testSequenceElementMCNP(unittest.TestCase):
    def setUp(self):
        from NPPlan.model.calc.mcnp import element
        self.elem1 = element(1, 1)
        self.elem2 = element('1001')
        self.elem3 = element('1001.20c')
        self.elem3 = element('1001.20c')
        self.elem4 = element('U-235')

    def test_materialFromElem(self):
        self.assertEqual(self.elem1.toString(), '1001')

    def test_materialFromString(self):
        self.assertEqual(self.elem2.toString(), '1001')
        self.assertEqual(self.elem2.atomicNumber, 1)
        self.assertEqual(self.elem2.isotopeNumber, 1)

    def test_materialFromStringWithLib(self):
        self.assertEqual(self.elem3.toString(), '1001.20c')
        self.assertEqual(self.elem3.atomicNumber, 1)
        self.assertEqual(self.elem3.isotopeNumber, 1)
        self.assertEqual(self.elem3.library, '20c')

    def test_materialEqual(self):
        self.assertEquals(self.elem1, self.elem2)
        self.assertEquals(self.elem1, self.elem3)
        self.assertEquals(self.elem2, self.elem3)

    def test_materialFromName(self):
        self.assertEquals(self.elem4.toString(), '92235')

    def tearDown(self):
        del self.elem1
        del self.elem2
        del self.elem3

class testSequenceMatCardMCNP(unittest.TestCase):
    def setUp(self):
        from NPPlan.model.calc.mcnp import compositeMap
        self.cm1 = compositeMap({'1001' : 20, '2005' : 30, '5008' : 40})

    def test_matCardSimple(self):
        self.assertEquals(self.cm1.toMCNPString(False), '5008 -0.444444 1001 -0.222222 2005 -0.333333')
        self.assertIn('1001', self.cm1)
        self.assertNotIn('3001', self.cm1)
        self.assertNotIn('U-238', self.cm1)
        pass

    def test_copyConstruct(self):
        from NPPlan.model.calc.mcnp import compositeMap
        cm2 = compositeMap(self.cm1)
        self.assertEqual(self.cm1.toMCNPString(), cm2.toMCNPString())

    def tearDown(self):
        del self.cm1

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(testSequenceElementMCNP))
    suite.addTest(unittest.makeSuite(testSequenceMatCardMCNP))
    unittest.main()