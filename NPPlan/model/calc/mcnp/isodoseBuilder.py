# -*- coding: utf8 -*-
'''
Класс-построитель изодоз из MCNP-файла

Использование:

from isodoseBuilder import isodoseBuilderFromMCNP

from mReader import mcnpReaderGrid

filename = r'C:\mcnp\testm'

mFile = mcnpReaderGrid(filename, shape=(10, 10, 10))

doseBuilder = isodoseBuilderFromMCNP(mFileObject=mFile)

doseBuilder.slice = 5

doseBuilder.axis = 1

doseBuilder.makeIsodoseImage(area=((-134, -114), (-10, 10), (-10, 10)), outputFileName=r'C:\images\isodose.png')


@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 2
@date: 14.02.13
@summary: построитель изодоз, построитель изодоз из mcnp-файла
@see: NPPlan.model.calc.base.isodoseBuider.isodoseBuilderBase
'''

from NPPlan.model.calc.base import isodoseBuilderBase
from NPPlan.model.calc.base import NPPLAN_CONTOURS_BODY, NPPLAN_CONTOURS_BORDERS, NPPLAN_CONTOURS_COLORBAR, NPPLAN_NORMALIZE_GLOBAL, NPPLAN_NORMALIZE_LOCAL

from mReader import mcnpReaderGrid
import numpy as np


class isodoseBuilderFromMCNP(isodoseBuilderBase):
    """
    Построитель изодоз из mcnp-файла
    """
    def __init__(self, mFileObject=None, slice=-1, axis=0, normalize=True, contourFlag=NPPLAN_CONTOURS_BORDERS,
                 linspaceAccuracy=128, targetTally=None):
        self._mFile = mFileObject
        self._shape = mFileObject.getShape()
        if targetTally is not None:
            self.__dict__['targetTally'] = targetTally
        else:
            self.__dict__['targetTally'] = self._mFile.getFirstTally()
        isodoseBuilderBase.__init__(self, mFileObject, slice, axis, normalize, contourFlag, linspaceAccuracy)
        pass



    def makeValuesSlice(self):
        """
        Возвращает срез массива для заданного self._slice параметра номера среза и размеров self._shape
        @return: 3хмерный массив значений с единичной длиной по заданной оси
        @rtype: numpy.array
        """
        print self._shape
        _data = self._mFile._data[self.__dict__['targetTally']]['shapedValues']
        if 0 == self._axis:
            gridData = np.array(_data[self._slice, 0:self._shape[1], 0:self._shape[2]])
        elif 1 == self._axis:
            gridData = np.array(_data[0:self._shape[0], self._slice, 0:self._shape[2]])
        elif 2 == self._axis:
            gridData = np.array(_data[0:self._shape[0], 0:self._shape[1], self._slice])
        self._gridSlice = gridData

    def makeShape(self):
        """
        Возвращает граничные условия для заданной оси
        @return: ( shapeX, shapeY )
        @rtype: tuple
        """
        if 0 == self._axis:
            return self._shape[1], self._shape[2]
        elif 1 == self._axis:
            return self._shape[0], self._shape[2]
        elif 2 == self._axis:
            return self._shape[0], self._shape[1]
        pass

    def makeStartEndPoints(self, area):
        """
        Выделяет из заданной area граничные условия для заданной оси, возвращает tuple из двух элементов
        @param area: ( (x0, xn), (y0, yn), (z0, zn) )
        @type area: tuple
        @return: ( (x0, xn), (y0, yn), (z0, zn) ) -> ( (u0, un), (v0, vn) )
        @rtype: tuple
        """
        #return area[0][0], area[0][1], area[1][0], area[1][1]
        if 0 == self._axis:
            return area[1][0], area[1][1], area[2][0], area[2][1]
        elif 1 == self._axis:
            return area[0][0], area[0][1], area[2][0], area[2][1]
        elif 2 == self._axis:
            return area[0][0], area[0][1], area[1][0], area[1][1]

    def makeSizes(self, area):
        """
        Возвращает размеры области area для заданной оси
        @param area: ( (x0, xn), (y0, yn), (z0, zn) )
        @type area: tuple
        @return: (size1, size2)
        @rtype: tuple
        """
        #return area[0][1] - area[0][0], area[1][1] - area[1][0]
        if 0 == self._axis:
            return area[1][1] - area[1][0], area[2][1] - area[2][0],
        elif 1 == self._axis:
            return area[0][1] - area[0][0], area[2][1] - area[2][0],
        elif 2 == self._axis:
            return area[0][1] - area[0][0], area[1][1] - area[1][0]

    def getGlobalMax(self):
        """
        Возвращает глобальный максимум по всему входному файлу
        @return: максимум
        @rtype: float
        """
        return self._mFile._data[self.__dict__['targetTally']]['max']


if __name__ == '__main__':
    filename = r'C:\mcnpx\executable\ng1vox2m'
    #mFile = mcnpReaderGrid(filename, shape=(34, 30, 30), multiplier=(2e11 * 60 / (9.612e-9 * 6.24e7)))
    mFile = mcnpReaderGrid(filename, shape=(30, 30, 10), multiplier=1)
    doseBuilder = isodoseBuilderFromMCNP(mFileObject=mFile, slice=16, axis=0, normalize=True,
        contourFlag=NPPLAN_CONTOURS_BORDERS | NPPLAN_CONTOURS_BODY | NPPLAN_CONTOURS_COLORBAR | NPPLAN_NORMALIZE_GLOBAL,
        targetTally=4)
    doseBuilder.title = ''
    doseBuilder.slice = 5
    doseBuilder.axis = 2
    doseBuilder.reset()
    doseBuilder.makeIsodoseImage(area=((-10, 10), (-10, 10), (-10, 10)))
    #filename = r'C:\1\tstrg_task\tstrgsm'
    #mFile = mcnpReaderGrid(filename, shape=(41, 41, 41))

    #filename = r'C:\mcnp5\mcnplanm'
    #mFile = mcnpReaderGrid(filename, shape=(35, 30, 30))

    #filename = r'C:\1\tst05m'
    #mFile = mcnpReaderGrid(filename, shape=(20, 20, 20))

    #filename = r'C:\1\tstrgp\tstrgpm'
    import sys
    sys.exit(0)
    '''
    filename = r'C:\1\all\tstrgv2m'
    mFile = mcnpReaderGrid(filename, shape=(22, 21, 21))

    doseBuilder = isodoseBuilderFromMCNP(mFileObject=mFile, slice=10, axis=1)
    for slice in [0, 10, 20]:
        for axis in [0, 1, 2]:
            doseBuilder.slice = slice
            doseBuilder.axis = axis
            doseBuilder.title = 'Slice: %d, axis: %d' %(slice, axis)
            doseBuilder.reset()

            doseBuilder.makeIsodoseImage(area=((-134, -114), (-10, 10), (-10, 10)), outputFileName='C:\\1\\all\\normal_axis%dslice%d.png' %(axis, slice))

    filename2 = r'C:\1\all\tstrgvm'
    mFile2 = mcnpReaderGrid(filename2, shape=(22, 21, 21))
    doseBuilder2 = isodoseBuilderFromMCNP(mFileObject=mFile2, slice=10, axis=1)
    for slice in [0, 10, 20]:
        for axis in [0, 1, 2]:
            doseBuilder2.slice = slice
            doseBuilder2.axis = axis
            doseBuilder.title = 'Slice: %d, axis: %d' %(slice, axis)
            doseBuilder2.reset()

            doseBuilder2.makeIsodoseImage(area=((-134, -114), (-10, 10), (-10, 10)), outputFileName='C:\\1\\all\\all_axis%dslice%d.png' %(axis, slice))
                '''

#'''
    filename = r'C:\mcnpx\executable\pp1am'
    outDir = r'C:\1\mvm\i1'
    mFile = mcnpReaderGrid(filename, shape=(35, 32, 32))
    doseBuilder = isodoseBuilderFromMCNP(mFileObject=mFile, slice=0, axis=0, normalize=False, targetTally=6,
                                         contourFlag=NPPLAN_CONTOURS_BORDERS | NPPLAN_CONTOURS_BODY | NPPLAN_CONTOURS_COLORBAR | NPPLAN_NORMALIZE_GLOBAL)
    for i in range(20):
        doseBuilder.title = '20 cells, XY, slice ' + str(i)
        doseBuilder.slice = i
        doseBuilder.axis = 0
        doseBuilder.reset()
        doseBuilder.makeIsodoseImage(area=((-10, 10), (-10, 10), (-10, 10)), outputFileName="%s\\%s%d.png" %(outDir, 'XYslice', i))
    for i in range(20):
        doseBuilder.title = '20 cells, XZ, slice ' + str(i)
        doseBuilder.slice = i
        doseBuilder.axis = 1
        doseBuilder.reset()
        doseBuilder.makeIsodoseImage(area=((-10, 10), (-10, 10), (-10, 10)), outputFileName="%s\\%s%d.png" %(outDir, 'XZslice', i))
    for i in range(20):
        #doseBuilder = isodoseBuilderFromMCNP(mFileObject=mFile, slice=0, axis=i, normalize=False)
        doseBuilder.title = '20 cells, YZ, slice ' + str(i)
        doseBuilder.slice = i
        doseBuilder.axis = 2
        doseBuilder.reset()
        doseBuilder.makeIsodoseImage(area=((-10, 10), (-10, 10), (-10, 10)), outputFileName="%s\\%s%d.png" %(outDir, 'YZslice', i))
    #doseBuilder.makeIsodoseImage(area=((-134, -114), (-10, 10), (-10, 10)), outputFileName='C:\\1\\all\\opuhol_2cm.png')
#'''



    # / fm6  9.612E-9
        # / 6.54e7    1 нейтрон выделяет столько [сГр]   (e9 -> Гр)
           # * potok (2e11)
               # * 60 min