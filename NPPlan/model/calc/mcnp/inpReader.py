# -*- coding: utf8 -*-
'''
GRID FORMAT EXAMPLE:
{'1': {'cell': 5,
       'cellsRaw': [8, -9, 10, -11, 12, -13],
       'coordinateBounds': {'x': {0: -10.0, 1: 10.0, 'useTransform': 0},
                            'y': {0: -10.0, 1: 10.0, 'useTransform': 0},
                            'z': {0: -10.0, 1: 10.0, 'useTransform': 4}},
       'density': -1.0,
       'fill': 1,
       'grid': {'cell': 500,
                'cellsRaw': [508, -509, 510, -511, 512, -513],
                'coordinateBounds': {'x': {0: -10.0,
                                           1: -9.0,
                                           'useTransform': 0},
                                     'y': {0: -10.0,
                                           1: -9.0,
                                           'useTransform': 0},
                                     'z': {0: -10.0,
                                           1: -9.0,
                                           'useTransform': 4}},
                'density': 0,
                'fillBounds': {'x': {0: -19, 1: 0, 'len': 20},
                               'y': {0: -19, 1: 0, 'len': 20},
                               'z': {0: -19, 1: 0, 'len': 20}},
                'material': 0,
                'startLine': 3},
       'material': 2,
       'startLine': 2}}
@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 02.04.13
@summary: 
'''

import abc
import re
import numpy
from NPPlan.model.calc.base.materials import material

NPPLAN_MCNP_SOURCE_RETURN_ALL    =   0b00000001
NPPLAN_MCNP_SOURCE_RETURN_HEADER =   0b00000010
NPPLAN_MCNP_SOURCE_RETURN_NUMBER =   0b00000100
NPPLAN_MCNP_SOURCE_RETURN_POSITION = 0b00001000
NPPLAN_MCNP_SOURCE_RETURN_TUPLE  =   0b00010000
NPPLAN_MCNP_SOURCE_RETURN_UNIV =     0b00100000
NPPLAN_MCNP_SOURCE_RETURN_MATERIAL = 0b01000000

# >> 8
NPPLAN_MCNP_SOURCE_REAL_COORDINATES     = 0b000100000000
NPPLAN_MCNP_SOURCE_RELATIVE_COORDINATES = 0b001000000000


class sourceReaderBase(object):

    __metaclass__ = abc.ABCMeta

    def __init__(self, filename='', autoread=True, autoparse=True):
        """
        Класс, обеспечивающий парсинг входных mcnp-файлов. Может использоваться
        для тестов вне основной программы планирования
        @param filename: имя файла
        @type filename: str
        @param autoread: флаг автозапуска чтения
        @type autoread: bool
        @param autoparse: флаг автозапуска парсера
        @type autoparse: bool
        """
        self._filename = filename
        self._fileStream = file(self._filename, 'r')
        self._materialsData = {}
        self._transformationData = {}
        self._data = {'cells': [], 'surfaces': [], 'sdef': [], 'surfacesToLineList' : {}}
        if autoread:
            self.start()
        if autoparse:
            self.parseData()

    def start(self):
        """
        Вызывает функции чтения
        """
        self.getTitle()
        self.getCellsSection()
        self.getSurfacesSection()
        self.getLastSection()

    def getTitle(self):
        """
        Возвращает смещение в файле на 0 и читает первую строку
        @return: заголовок расчёта
        @rtype: str
        """
        if 'title' in self._data and '' != self._data['title']:
            return self._data['title']
        self._fileStream.seek(0)
        self._data['title'] = self._fileStream.readline()[:-1]  # remove end line symbol
        return self._data['title']

    def skipLineCondition(self, line):
        """
        Условие пропуска строки. Если длина строки < 3 или стоит комментарий вначале строки, то возвратит
        True, иначе False
        @param line: строка
        @type line: str
        @return: True если строку надо пропустить, иначе False
        @rtype: bool
        """
        if len(line) < 3 or ('c' == line[0] and ' ' == line[1]):
            return True
        return False

    def removeInlineComments(self, line):
        """
        Проверяет, есть ли inline-комментарий в строке и вырезает его. Также обрезает символ новой строки
        @param line: исходная строка
        @type line: str
        @return: строка без комментария
        @rtype: str
        """
        if '$' in line:
            return line[:line.index('$')]
        return line[:-1]

    def getSection(self, target, prepend=None, backend=None):
        """
        Обрабатывает секцию, пока не встретит новую строку ('\n') или конец файла
        @param target: ключ массива self._data, в который будут записаны строки
        @type target: str
        @param prepend: not implemented
        @param backend: функция, которая будет вызвана после вставки с параметрами строки и номера строки
        @type backend: function
        """
        while True:
            line = self._fileStream.readline()
            if 0 == len(line) or line.isspace():
                break
            if self.skipLineCondition(line):
                continue
            line = self.removeInlineComments(line)
            self._data[target].append(line)
            if backend is not None:
                backend(line, len(self._data[target]))

    def getCellsSection(self):
        """
        Парсит ячейки. Вызывает self.getSection с параметром 'cells'
        """
        self.getSection('cells')

    def getSurfacesSection(self):
        """
        Парсит плоскости. Вызывает self.getSection с параметром 'surfaces' и пост-обработчиком
        self.surfaceCollector
        """
        self.getSection('surfaces', backend=self.surfaceCollector)

    def surfaceCollector(self, line, lineNumber):
        """
        Функция, записывающая массив self._data['surfacesToLineList']
        @param line: строка
        @type line: str
        @param lineNumber: порядковый номер строки в массиве данных о плоскостях ( != порядковому номеру в исходном
        файле, так как пропуски строк, комментарии и т. п. не учитываются
        @type lineNumber: int
        """
        t = [t for t in line.split(' ') if t != '']
        s = int(t[0])
        self._data['surfacesToLineList'][s] = lineNumber - 1
        pass

    def getLastSection(self):
        self.getSection('sdef')

    @abc.abstractmethod
    def parseData(self):
        """
        Абстрактный метод-обработчик. Должен быть перегружен в наследнике
        """
        pass

    def parseMaterialString(self, line):
        """
        Разбивает строку с данными материала (АААБББ.ЛЛЛ КОЭФ) в { 'АААБББ.ЛЛЛ' : КОЭФ, 'АААБББ.ЛЛЛ' : КОЭФ', ... }
        где ААА - атомный номер, БББ - номер изотопа, ЛЛЛ - библиотека, КОЭФ - коэффициент
        @param line: объединённая строка материала
        @type line: str
        @return: see NPPlan.model.calc.base.materials.material input format
        @rtype: dict
        """
        ret = {}
        mNums = []
        mCoef = []
        t = [t for t in line.split(' ') if t != '']
        for i in range(len(t)):
            if 0 == i % 2:
                mNums.append(t[i])
            else:
                mCoef.append(t[i])
        for i in range(len(mNums)):
            ret[mNums[i]] = mCoef[i]
        return ret

    def setTransformations(self, startLine):
        """
        Начиная со строки startLine смотрит следующие 4 на предмет описания trX преобразования,
        заполняет массив self._transformationData по ключи trX значением
        { 'tr' : X, 'shift' : [x, y, z], 'cosines' : [xx0, yx0, zx0, xy0, yy0, zy0, xz0, yz0, zz0] },
        где Х - номер tr, x,y,z - смещения, xx0, yx0, zx0, xy0, yy0, zy0, xz0, yz0, zz0 - направляющие
        косинусы преобразования (или пустой list если косинусы не заданы)
        @param startLine: номер строки
        @type startLine: int
        """
        cosines = []
        skipMode = False
        for i in range(startLine, startLine + 4):
            line = self._data['sdef'][i]
            if 'tr' in line:
                tri = line.index('tr')
                if ' ' == line[tri + 2]:
                    continue
                t = [t for t in line.split(' ') if t != '']
                tNum = t[0][2:]
                tVal = t[1:4]
                self._transformationData[tNum] = {'tr': int(tNum), 'shift': [float(i) for i in tVal]}
            if ' ' == line[0]:
                t = [t for t in line.split(' ') if t != '']
                for i in t:
                    cosines.append(float(i))
        if len(cosines) > 0:
            self._transformationData[tNum]['cosines'] = cosines
            cosines = []
        pass

    def setMaterials(self):
        """
        Просматривает весь набор описания 3й части входного файла, выделяет данные материалов
        """
        cm = None
        mats = []
        matNames = []
        skipMode = False
        for i in range(len(self._data['sdef'])):
            line = self._data['sdef'][i]
            if 'mode' in line:
                continue
            if 'tr' in line:
                skipMode = True
                self.setTransformations(i)
                # call tr reader
                continue
            if ' ' != line[0]:
                skipMode = False
            if skipMode:
                continue
            if 'm' == line[0]:
                if cm is not None:
                    mats.append(cm)
                    matNames.append(int(cmNum))
                t = [t for t in line.split(' ') if t != '' and t != '&']
                cmNum = t[0][1:]
                #mats = t[1:]
                #for i in range(len(mats)):
                #    if 1 == i % 2:
                cm = self.parseMaterialString(' '.join(t[1:]))
            elif ' ' == line[0] and cm is not None:
                t = [t for t in line.split(' ') if t != '']
                cm.update(self.parseMaterialString(' '.join(t)))
            else:
                if cm is not None:
                    mats.append(cm)
                    matNames.append(int(cmNum))
                cm = None
        if cm is not None:
            mats.append(cm)
            matNames.append(int(cmNum))
        for i in range(len(mats)):
            self._materialsData[matNames[i]] = material(mats[i], id=matNames[i])
        pass


class gridFillReader(sourceReaderBase):
    def __init__(self, filename='', autoread=True, autoparse=True):
        # @todo: read rpp (!!!!)
        """
        Класс, обеспечивающий парсинг входных mcnp-файлов с решёткой.
        @param filename: имя файла
        @type filename: str
        @param autoread: флаг автозапуска чтения
        @type autoread: bool
        @param autoparse: флаг автозапуска парсера
        @type autoparse: bool
        """
        self._surfaceData = {}
        self._universeData = {}
        sourceReaderBase.__init__(self, filename, autoread, autoparse)

    def acquireSimulation(self, sim):
        """
        Привязывает ридер к своему классу моделирования
        @param sim: объект класса моделирования
        @type sim: NPPlan.model.calc.base.simulation.simulationBase
        @return:
        """
        self._simulation = sim

    def parseData(self):
        """
        Функция, вызывает парсинг всех решёток (fill=N), данных решёток и материалов
        """
        # initial
        self._grids = {}
        self._cells = {}
        # find fill=N in cells section
        for i in range(len(self._data['cells'])):
            line = self._data['cells'][i]
            if 'fill=' in line and ':' not in line:
                # @todo: more accurate check
                num = self.parseTopFill(line, i)
                self.findTally(num)
                pass
            #if 'fill=' in line and 'lat=' in line:
            #    self.parseRectangularLattice(line, i)
        self.getAllTallyData()
        self.setMaterials()
        pass

    def findTally(self, num):
        """
        Функция, ищущая данные (номера ячеек, описывающие их поверхности, параметры заполнения,) для решётки num
        @note: using re
        @param num: номер решётки
        @type num: str or int
        """
        fL = -1
        sL = ''
        cellLineStart = -1
        for i in range(len(self._data['cells'])):
            line = self._data['cells'][i]
            #t = [t for t in line.split(' ') if t != '']
            if 'u' in line:
                # find line with some universe
                # check if we find our top lattice description
                t = [t for t in line.split(' ') if t != '']
                if ('u=%d' % int(num)) in ''.join(t):
                    fL = i
                    sL = line
                    break
        # в строке fL найдено описание для решётки u=num
        # теперь пролистываем строки сверху до поиска описания
        rF = re.compile('\s*\d+\s+0\s+(-?\d+\s+-?\d+\s*){3}')
        for i in range(fL, -1, -1):
            line = self._data['cells'][i]
            if 'fill' in line:
                sL = line + ' ' + sL
            if rF.match(line) is not None:
                cellLineStart = i
                sL = line + ' ' + sL
                break
        # пролистываем вниз, пока не найдем строку с данными
        rD = re.compile('\s*\d+\s+\d+\s*\-?\d+\.?\d+\s*\-?\d+\s*u*s*=\s*\d+(\S)*')
        for i in range(fL + 1, len(self._data['cells']), 1):
            line = self._data['cells'][i]
            if 'r' in line:
                continue
            #if rD.match is not None:
            if 'u' in line:
                endLine = i
                break
            pass
        t = [t for t in sL.split(' ') if t != '']
        tally = {'cell': int(t[0]), 'startLine': cellLineStart, 'endLine': endLine}
        mat = int(t[1])
        if 0 == mat:
            tally['material'] = mat
            tally['density'] = 0
            cells = [int(s) for s in t[2:8]]
        else:
            tally['material'] = mat
            tally['material'] = float(t[2])
            cells = [int(s) for s in t[3:9]]
        coordBounds = self.getCoordBoundsForCells(cells)
        tally['cellsRaw'] = cells
        tally['coordinateBounds'] = coordBounds
        sF = sL[sL.index('fill'):][5:]
        r = re.compile('[: ]+')
        fF = [i for i in r.split(sF) if i != '']
        # @todo: проверять, в том ли порядке плоскости (x, y, z)
        fill = [int(fF[i]) for i in range(6)]
        tally['fillBounds'] = {'x': {0: fill[0], 1: fill[1], 'len': fill[1] - fill[0] + 1},
                               'y': {0: fill[2], 1: fill[3], 'len': fill[3] - fill[2] + 1},
                               'z': {0: fill[4], 1: fill[5], 'len': fill[5] - fill[4] + 1},}
        self._grids[num]['grid'] = tally
        pass

    def parseCellData(self, line, i):
        #self._data['cellsToLineList']
        #t = [t for t in line.split(' ') if t != '']
        pass

    def getGridsKeys(self):
        return self._grids.keys()

    def parseTopFill(self, line, i):
        """
        Функция, заполнящая описание внешнего фантома решётки
        @param line: строка, с которой начинается решётка
        @type line: str
        @param i: номер строки
        @type i: int
        @return: номер решётки
        @rtype: str
        """
        sl = i
        t = [t for t in line.split(' ') if t != '']
        ss = ''.join(t)
        num = ss[ss.index('fill='):][5:]
        #num = t[-1][-1]
        self._grids[num] = {'startLine': sl, 'cell': int(t[0]), 'fill': int(num)}
        mat = int(t[1])
        if 0 == mat:
            # материала нет
            self._grids[num]['material'] = mat
            self._grids[num]['density'] = 0
            cells = [int(s) for s in t[2:-1]]
        else:
            self._grids[num]['material'] = mat
            self._grids[num]['density'] = float(t[2])
            cells = [int(s) for s in t[3:-1]]
            pass
        self._grids[num]['cellsRaw'] = cells
        print 'cells', cells
        coordBounds = self.getCoordBoundsForCells(cells)
        self._grids[num]['coordinateBounds'] = coordBounds
        return num

    def getCoordBoundsForCells(self, cells):
        """
        Выделяет поверхностей для ячейки решётки
        @param cells: список ячеек решётки
        @type cells: list
        @return: {cellNumber : {'x' : {0: left, 1: right, useTranform: True||False}, 'y' : ..., 'z' : ...}}
        @rtype: dict
        """
        coordBounds = {}
        for i in cells:
            # @todo: ???
            #if i < 0:
            #    i *= -1
            surfData = self.getSurfaceDataForNumber(i)
            if 'rpp' == surfData['type']:
                coordBounds['x'] = {
                    0: surfData['values'][0],
                    1: surfData['values'][1]
                }
                coordBounds['y'] = {
                    0: surfData['values'][2],
                    1: surfData['values'][3]
                }
                coordBounds['z'] = {
                    0: surfData['values'][4],
                    1: surfData['values'][5]
                }
                break
                pass
            if surfData['type'][1] not in coordBounds:
                coordBounds[surfData['type'][1]] = {}
            if i > 0:
                coordBounds[surfData['type'][1]][0] = surfData['values']
            else:
                coordBounds[surfData['type'][1]][1] = surfData['values']
            coordBounds[surfData['type'][1]]['useTransform'] = surfData['transform']
            #coordBounds[surfData['type'][1]]['dist'] = numpy.fabs(coordBounds[surfData['type'][1]][0]) + \
            #                                           numpy.fabs(coordBounds[surfData['type'][1]][1])
        for i in coordBounds:
            if 1 == len(i):
                try:
                    coordBounds[i]['dist'] = coordBounds[i][1] - coordBounds[i][0]
                except KeyError:
                    pass
        vol = 1.0
        for i in ['x', 'y', 'z']:
            vol *= coordBounds[i][1] - coordBounds[i][0]
        coordBounds['volume'] = vol
        return coordBounds

    def getSurfaceDataForNumber(self, number):
        """
        Получаем данные поверхности по её номеру number
        @param number: номер поверхности
        @return: {surface, type, transform, values}
        @rtype: dict
        """
        # проверяем, пришло ли число. делаем числом
        if not isinstance(number, int):
            number = int(number)
        # убираем знак у поверхности
        if number < 0:
            number = abs(number)
        # проверяем, парсили ли уже эту поверхность. если да, то возвращаем старый результат
        if number in self._surfaceData:
            return self._surfaceData[number]
        # смотрим, есть ли номер поверхности в списке поверхность->номер_строки
        if number not in self._data['surfacesToLineList']:
            # @todo: throw
            return -1
        # получаем строку данных поверхности
        sLine = self._data['surfaces'][self._data['surfacesToLineList'][number]]
        t = [t for t in sLine.split(' ') if t != '']

        # проверяем, есть ли транфсормация поверхности
        if t[1].isdigit():
            _transform = int(t[1])
        else:
            _transform = 0
        # получаем тип поверхности и значения. Если значение одно, то запомним его, если больше одного,
        # то запомним их массивом
        if 0 == _transform:
            _type = t[1]
            _values = [float(v) for v in t[2:]]
        else:
            _type = t[2]
            _values = [float(v) for v in t[3:]]
        if 1 == len(_values):
            _values = _values[0]
        self._surfaceData[number] = {
            'surface': number,
            'type': _type,
            'values': _values,
            'transform': _transform,
        }
        return self._surfaceData[number]
        pass

    def getAllTallyData(self):
        """
        Функция, вызывающая парсинг данных решётки для каждого номера решётки
        """
        for gridNumber in self._grids:
            self.getTallyData(gridNumber)

    def getTallyData(self, gridNumber):
        """
        Функция, заполняющая self._grid[gridNumber] значениями материалов (universe) решётки.
        Добавляет ключи npRawData = numpy.array([...]) - одномерный массив материалов решётки,
        npArrayData - трёхмерный массив материалов решётки,
        usedUniverses - список используемых материалов (numpy.unique(npRawData))
        @param gridNumber: номер решётки
        @type gridNumber: int
        """
        gridHeader = self._grids[gridNumber]
        #for i in range(gridHeader['grid']['startline'])
        startLine = gridHeader['grid']['startLine']
        endLine = gridHeader['grid']['endLine']
        gridUnivs = []
        for i in range(startLine + 2, endLine):
            line = self._data['cells'][i]
            lineData = [t for t in line.split(' ') if t != '']
            for val in lineData:
                if val.isdigit():
                    gridUnivs.append(int(val))
                else:
                    if 'r' in val:
                        vQu = int(val[:-1])
                        vVal = [gridUnivs[-1]] * vQu
                        gridUnivs.extend(vVal)
                    else:
                        continue
        gridHeader['npRawData'] = numpy.array(gridUnivs)
        shape = (
            gridHeader['grid']['fillBounds']['x']['len'],
            gridHeader['grid']['fillBounds']['y']['len'],
            gridHeader['grid']['fillBounds']['z']['len']
        )
        gridHeader['npArrayData'] = numpy.reshape(gridHeader['npRawData'], shape)
        self.setUniverseData(gridNumber)
        gridHeader['usedUniverses'] = numpy.unique(gridHeader['npRawData'])
        pass

    def getShape(self, gridNumber=-1):
        """
        Возвращает количество ячеек на стороне
        @todo: check z,y,x -> x,y,z
        @param gridNumber:
        @type gridNumber: int
        @return: (количество_по_X, количество_по_Y, количество_по_Z)
        @rtype: tuple
        """
        if -1 == gridNumber:
            gridNumber = self._grids.keys()[0]
        return (self._grids[gridNumber]['grid']['fillBounds']['x']['len'],
                self._grids[gridNumber]['grid']['fillBounds']['y']['len'],
                self._grids[gridNumber]['grid']['fillBounds']['z']['len'])
        #return (self._grids[gridNumber]['grid']['fillBounds']['z']['len'],
        #        self._grids[gridNumber]['grid']['fillBounds']['y']['len'],
        #        self._grids[gridNumber]['grid']['fillBounds']['x']['len'])

    def getUniverseData(self, u=None, gridNumber=None):
        """
        Возвращает решёткн если u в self._grids, или данные материала в ином случае
        @param u: номер universe
        @type u: int or str
        @param gridNumber: deprecated
        @return:
        @rtype: dict
        """
        if str(u) in self._grids:
            return self._grids[u]
        if u is not None:
            u = int(u)
            if u in self._universeData:
                return self._universeData[u]

    def setUniverseData(self, gridNumber):
        """
        Парсит строки описания материалов ячейки. Заполняет self._universeData =
        {cell: номер ячейки, material: номер материала, density: плотность,
        topCell: номер родительской ячейки, number: номер universe, ..., 'lastEntry' - номер
        последней строки описания материалов}
        @note: using re
        @param gridNumber: номер решётки
        """
        rD = re.compile('\s*\d+\s+\d+\s*\-?\d+\.?\d+\s*\-?\d+\s*u*s*=\s*\d+(\S)*')
        lastEntry = self._universeData.get('lastEntry', len(self._data['cells'])-1)
        for i in range(self._grids[gridNumber]['grid']['endLine'], lastEntry + 1):
            line = self._data['cells'][i]
            if rD.match(line) is not None:
                nLastEntry = i
                t = [t for t in line.split(' ') if t != '']
                ut = ''.join(t[4:]).split('=')[1]
                if True:
                    # found universe
                    universe = {
                        'cell': int(t[0]),
                        'material': int(t[1]),
                        'density': float(t[2]),
                        'topCell': int(t[3]),
                        'number': int(ut),
                    }
                    self._universeData[int(ut)] = universe
            pass
        self._universeData['lastEntry'] = nLastEntry
        # return self._universeData[u]
        pass

    def getCellByNumber(self, xN=None, yN=None, zN=None, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_ALL, gridNumber=-1):
        """
        for compability
        @see: self.getter
        """
        return self.getter(xN, yN, zN, returnFlag, gridNumber)

    def getter(self, xN=None, yN=None, zN=None, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_ALL, gridNumber=-1):
        """
        Получает данные в зависимости от переданных параметров
        @param xN:
        @type xN: int
        @param yN:
        @type yN: int
        @param zN:
        @type zN: int
        @param returnFlag: флаг для возврата, один из
           NPPLAN_MCNP_SOURCE_RETURN_ALL:  вернуть все данные по ячейке (xN, [yN, zN])
           NPPLAN_MCNP_SOURCE_RETURN_HEADER: вернуть описание решётки без данных
           NPPLAN_MCNP_SOURCE_RETURN_NUMBER: вернуть 1D-номер ячейки по ячейке (xN, [yN, zN])
           NPPLAN_MCNP_SOURCE_RETURN_POSITION: вернуть координаты ячейки по ячейке (xN, [yN, zN])
           NPPLAN_MCNP_SOURCE_RETURN_TUPLE: вернуть 3D-номер ячейки по ячейке (xN, [yN, zN])
           NPPLAN_MCNP_SOURCE_RETURN_UNIV: вернуть данные universe по ячейке (xN, [yN, zN])
           NPPLAN_MCNP_SOURCE_RETURN_MATERIAL: вернуть данные материала по ячейке (xN, [yN, zN])
        @param gridNumber: номер решётки
        @return: данные в зависимости от флага
        @rtype: dict or int
        """
        if -1 == gridNumber:
            gridNumber = self._grids.keys()[0]
        if returnFlag & NPPLAN_MCNP_SOURCE_RETURN_NUMBER:
            # @todo: implement ??
            if yN is None and zN is None:
                return xN
            else:
                return self.getCellNumberByCellTuple(xN, yN, zN, gridNumber=gridNumber)
        if returnFlag & NPPLAN_MCNP_SOURCE_RETURN_TUPLE:
            if yN is None and zN is None:
                return self.getCellTupleByCellNumber(xN, gridNumber=gridNumber)
            else:
                return xN, yN, zN
        if returnFlag & NPPLAN_MCNP_SOURCE_RETURN_UNIV:
            if yN is None and zN is None:
                return self._grids[gridNumber]['npRawData'][xN]
            else:
                return self._grids[gridNumber]['npArrayData'][xN][yN][zN]
        if returnFlag & NPPLAN_MCNP_SOURCE_RETURN_ALL:
            gridValue = self.getCellByNumber(xN, yN, zN, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_UNIV)
            universe = self._universeData[gridValue]
            material = self._materialsData[universe['material']]
            sNum = -1
            tNum = (-1, -1, -1)
            if yN is not None and zN is not None:
                sNum = self.getCellNumberByCellTuple(xN, yN, zN, gridNumber)
                tNum = (xN, yN, zN)
            else:
                sNum = xN
                tNum = self.getCellTupleByCellNumber(xN, gridNumber)
            ret = {'universe': gridValue, 'material': material, 'density': universe['density'],
                   'coordinates': self.getPositionByCellNumber(*tNum, gridNumber=gridNumber),
                   'singleNum': sNum,
                   'tupleNum': tNum,
                   'gridNumber': gridNumber,
                   'volume': self._grids[gridNumber]['grid']['coordinateBounds']['volume'],
                   'mass': -1.0 * self._grids[gridNumber]['grid']['coordinateBounds']['volume'] * universe['density'],
                   }
            return ret
            pass
        if returnFlag & NPPLAN_MCNP_SOURCE_RETURN_MATERIAL:
            gridValue = self.getCellByNumber(xN, yN, zN, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_UNIV)
            universe = self._universeData[gridValue]
            print self._materialsData
            material = self._materialsData[universe['material']]
            return {'density': universe['density'], 'material': material}
        if returnFlag & NPPLAN_MCNP_SOURCE_RETURN_POSITION:
            return self.getPositionByCellNumber(xN, yN, zN, gridNumber=gridNumber, returnFlag=returnFlag)
            pass
        if returnFlag & NPPLAN_MCNP_SOURCE_RETURN_HEADER:
            ret = self._grids[gridNumber].copy()
            del ret['npArrayData']
            del ret['npRawData']
            return ret
        pass

    def getCellByPosition(self, x, y, z, gridNumber=-1, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_TUPLE):
        # @todo: evaluate transformation
        if -1 == gridNumber:
            gridNumber = self._grids.keys()[0]
        xLen = self._grids[gridNumber]['grid']['fillBounds']['x']['len']
        #xpDist = self._grids[gridNumber]['coordinateBounds']['x']['dist']
        xp0 = self._grids[gridNumber]['coordinateBounds']['x'][0]
        xcDist = self._grids[gridNumber]['grid']['coordinateBounds']['x']['dist']
        yLen = self._grids[gridNumber]['grid']['fillBounds']['y']['len']
        #ypDist = self._grids[gridNumber]['coordinateBounds']['y']['dist']
        yp0 = self._grids[gridNumber]['coordinateBounds']['y'][0]
        ycDist = self._grids[gridNumber]['grid']['coordinateBounds']['y']['dist']
        zLen = self._grids[gridNumber]['grid']['fillBounds']['z']['len']
        #zpDist = self._grids[gridNumber]['coordinateBounds']['z']['dist']
        zp0 = self._grids[gridNumber]['coordinateBounds']['z'][0]
        zcDist = self._grids[gridNumber]['grid']['coordinateBounds']['z']['dist']
        if      (x < xp0 or x > self._grids[gridNumber]['coordinateBounds']['x'][1]) or \
                (y < xp0 or y > self._grids[gridNumber]['coordinateBounds']['y'][1]) or\
                (z < xp0 or z > self._grids[gridNumber]['coordinateBounds']['z'][1]):
            return -1, -1, -1
        xN = yN = zN = -1
        for i in range(0, xLen):
            if xp0 + i * xcDist <= x < xp0 + (i + 1) * xcDist:
                xN = i
                break
        for i in range(0, yLen):
            if yp0 + i * ycDist <= y < yp0 + (i + 1) * ycDist:
                yN = i
                break
        for i in range(0, zLen):
            if zp0 + i * zcDist <= z < zp0 + (i + 1) * zcDist:
                zN = i
                break
        if returnFlag & NPPLAN_MCNP_SOURCE_RETURN_TUPLE:
            return xN, yN, zN
        elif returnFlag & NPPLAN_MCNP_SOURCE_RETURN_NUMBER:
            return zN * xLen * yLen + yN * xLen + xN
        pass

    def getCellNumberByCellTuple(self, xN, yN, zN, gridNumber=-1):
        if -1 == gridNumber:
            gridNumber = self._grids.keys()[0]
        xLen = self._grids[gridNumber]['grid']['fillBounds']['x']['len']
        yLen = self._grids[gridNumber]['grid']['fillBounds']['y']['len']
        zLen = self._grids[gridNumber]['grid']['fillBounds']['z']['len']
        return zN * xLen * yLen + yN * xLen + xN

    def getCellTupleByCellNumber(self, v, gridNumber=-1):
        """
        Возвращает номер ячейки в трёхмерном массиве в зависимости от v - номер ячейки в одномерном массиве
        @param v: номер в одномерном массиве
        @param gridNumber: номер решётки
        @type gridNumber: int
        @return:
        """
        if -1 == gridNumber:
            gridNumber = self._grids.keys()[0]
        xLen = self._grids[gridNumber]['grid']['fillBounds']['x']['len']
        yLen = self._grids[gridNumber]['grid']['fillBounds']['y']['len']
        zLen = self._grids[gridNumber]['grid']['fillBounds']['z']['len']
        zN = int(v / (xLen * yLen))
        yN = int((v - zN * (xLen * yLen)) / xLen)
        xN = v - yN * xLen - zN * (xLen * yLen)
        return xN, yN, zN

    def getPositionByCellNumber(self, xN, yN=-1, zN=-1, gridNumber=-1, returnFlag=NPPLAN_MCNP_SOURCE_RELATIVE_COORDINATES):
        # @todo: evaluate transformation, use transformation flag
        if -1 == gridNumber:
            gridNumber = self._grids.keys()[0]
        xLen = self._grids[gridNumber]['grid']['fillBounds']['x']['len']
        xp0 = self._grids[gridNumber]['coordinateBounds']['x'][0]
        xcDist = self._grids[gridNumber]['grid']['coordinateBounds']['x']['dist']
        yLen = self._grids[gridNumber]['grid']['fillBounds']['y']['len']
        yp0 = self._grids[gridNumber]['coordinateBounds']['y'][0]
        ycDist = self._grids[gridNumber]['grid']['coordinateBounds']['y']['dist']
        zLen = self._grids[gridNumber]['grid']['fillBounds']['z']['len']
        zp0 = self._grids[gridNumber]['coordinateBounds']['z'][0]
        zcDist = self._grids[gridNumber]['grid']['coordinateBounds']['z']['dist']
        if -1 == yN and -1 == zN:
            xN, yN, zN = self.getCellTupleByCellNumber(xN, gridNumber)
        if xN > xLen or yN > yLen or zN > zLen:
            return None, None, None
        return {'bottomLeft': (xp0 + xN * xcDist, yp0 + yN * ycDist, zp0 + zN * zcDist),
                'topRight': (xp0 + (xN + 1) * xcDist, yp0 + (yN + 1) * ycDist, zp0 + (zN + 1) * zcDist)}
        pass


if __name__ == '__main__':
    import pprint
    #filename = r'C:\mcnpx\executable\mcnplan'
    #filename = r'C:\1\McnpVsGeant\mgv1'
    filename = r'C:\mcnpx\executable\ngs15l'
    c = gridFillReader(filename, autoread=False, autoparse=False)
    c.start()
    print c.getCoordBoundsForCells([4])
    #c.parseData()
    #print c._data['cells']
    #print c.getTitle()
    #pprint.pprint(c._grids)
    #pprint.pprint(c._universeData)
    #pprint.pprint(c._materialsData)
    #pprint.pprint(c._transformationData)
    #print c.getSurfaceDataForNumber(777)
    #print c.getTitle()
    #print c._data['cells']