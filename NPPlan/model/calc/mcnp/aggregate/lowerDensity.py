# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 11.07.13
@summary: 
'''

import NPPlan
import numpy as np
from NPPlan.model.calc.base.aggregate import aggregateMethodBase


class lowerDensityMethod(aggregateMethodBase):
    def __init__(self, planParent, sliceIterator):
        aggregateMethodBase.__init__(self, planParent, sliceIterator)
        self._compression = 16
        self._sliceData = {}
        self._ct2density = [(i.ctValue, i.density) for i in NPPlan.mongoConnection.ct2density.find(sort=[('ctValue', 1)])]
        self._density2ct = [(i.density, i.ctValue) for i in NPPlan.mongoConnection.ct2density.find(sort=[('ctValue', 1)])]
        print 'density'
        print self._ct2density
        self._rowsColumns = self._plan.planGetRowsColumns()
        self._matStartNum = 500
        self._allMats = []
        self._registeredMats = {
            'list': [],
            'data': {},
            'base': {},
            'densities': {},
        }
        self._getMatsList()

    def _getMatsList(self):
        for i in NPPlan.mongoConnection.material.find():
            self._allMats.append((i.density if i.densityUnits == 0 else 0.001 * i.density, i.name))
        self._allMats = sorted(self._allMats, key=lambda q: q[0])

    def work(self):
        for i, elem in self._sliceIterator():
            npString = np.copy(elem.getNumpyString())
            slope, intercept = self._plan.planGetRescaleParams(1)
            npString *= slope
            npString += intercept
            self._sliceData[i] = self.npToMatGrid(npString)
        import pprint
        print 'SliceData:'
        pprint.pprint(self._sliceData)
        print 'RegisteredMats:'
        pprint.pprint(self._registeredMats)
        pass

    def meanInArea(self, npString, leftCorner, rightCorner, upperCorner, lowerCorner):
        """
        Возвращает среднее значение в области
        """
        return np.mean(npString[upperCorner:lowerCorner, leftCorner:rightCorner])

    def getMaterialIndex(self, density):
        #print 'getMaterialIndex with density', density
        #print 'allMats'
        #print self._allMats
        #ii = len(self._allMats)
        ii = 0
        for j in self._allMats:
            #print j
            if density >= j[0]:
                #print 'density found', j, ii
                #break
                ii += 1
        #print ii
        #print 'returned', self._allMats[ii-1][1]
        try:
            return self._allMats[ii][1]
        except IndexError:
            return self._allMats[-1][1]

    def npToMatGrid(self, npString):
        print 'npToMatGrid called'
        # @todo: handle density units in material mongokit model
        #
        #print len(npString)
        #print self._rowsColumns, self._rowsColumns[0] * self._rowsColumns[1]
        #npString = npString.reshape((0, self._rowsColumns[0], self._rowsColumns[1]))
        ret = []
        for bounds in self.nextCornerStageIterator(self._compression, self._compression):
            avgInSquare = self.meanInArea(npString, *bounds)
            densityInSquare = self.pixel2density(avgInSquare)
            #if  0.5 < densityInSquare < 1.5:
            #    print 'GOT DENSITY', densityInSquare
            #material = NPPlan.mongoConnection.material.find_one({'density': {'$lt': densityInSquare}}, sort=[('density', -1)])
            #if material is None:
                #print 'density lost', densityInSquare
                #print NPPlan.mongoConnection.material.find_one(sort=[('density', 1)])
            #    material = NPPlan.mongoConnection.material.find_one(sort=[('density', 1)])
            materialName = self.getMaterialIndex(densityInSquare)
            #print materialName
            #print bounds, materialName, densityInSquare
            material = NPPlan.mongoConnection.material.find_one({'name': materialName})
            #break
            if self.checkMatExists(material):
                ret.append(self.getMaterialByMaterial(material, 3))
            #if material not in self._registeredMats['list']:
            #    self._registeredMats['list'].append(material)
            #    self._registeredMats['data'][material.name] = self._matStartNum + len(self._registeredMats['list'])
            else:
                mIndex = self.registerMaterial(material, metaInfo={'density': material.density})
                ret.append(mIndex)
            #print material
        pass
        print ret
        import pprint
        pprint.pprint(self._registeredMats)
        return ret
        #

    def nextCornerStageIterator(self, stepI, stepJ):
        """
        Итератор, возвращающий tuple ( левый край, правый край, верхний край, нижний край ) в соответствие с параметром шага
        @param stepI: шаг по x
        @type stepI: int
        @param stepJ: шаг по y
        @type stepJ: int
        @yield: tuple
        @rtype: tuple
        """
        for i in range(0, self._rowsColumns[0], stepI):
            for j in range(0, self._rowsColumns[1], stepJ):
                lec = j
                upc = i
                loc = i + stepI if i + stepI < self._rowsColumns[0] else self._rowsColumns[0]
                ric = j + stepJ if j + stepJ < self._rowsColumns[1] else self._rowsColumns[1]
                yield (lec, ric, upc, loc)

    def pixel2density(self, pixelValue):
        """
        Преобразование значение пикселя в плотность в соответствие с параметрами калибровки
        @todo: refactor
        """
        # self._ct2density = [(CT value, density), (CT value, density), ..]
        ii = 0
        for j in range(1, len(self._ct2density)):
            if self._ct2density[j-1][0] <= pixelValue < self._ct2density[j][0]:
                deltaCT = self._ct2density[j][0] - self._ct2density[j-1][0]
                deltaDensity = self._ct2density[j][1] - self._ct2density[j-1][1]

                return self._ct2density[j][1] - (( float(self._ct2density[j][0] - pixelValue)*deltaDensity/deltaCT ))
            #print num, dens
        pass
