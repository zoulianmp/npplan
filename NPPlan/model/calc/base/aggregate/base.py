# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 18.07.13
@summary: 
'''

import NPPlan
import NPPlan.controller.logger as log
import abc


class aggregateMethodBase(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, planParent, sliceIterator):
        """

        @param planParent: экземпляр плана облучения
        @type planParent: NPPlan.model.plan.core.planAbstract
        @param sliceIterator: итератор срезов
        @return:
        """
        self._plan = planParent
        self._sliceIterator = sliceIterator

        # количество пикселей по сторонам в единичном квадрате решётки
        # @todo: сделать разное количество для сжатия по x и по y
        self._compression = 16

        # номер, с которого начинать присваивать номера базовым материалам
        self._matStartNum = 500

        self._matBaseContainer = {
            'matList': [],
            'metMetaInfo': {},
        }
        pass

    @abc.abstractmethod
    def work(self):
        pass

    def getCompression(self):
        return self._compression

    def setCompression(self, newCompression):
        log.log('calc.base.aggregate', 'Compression set to ' + newCompression)
        self._compression = newCompression

    compression = property(getCompression, setCompression, doc='Устанавливает значения сжатия по сторонам')

    def setStartNum(self, newStartNum):
        log.log('calc.base.aggregate', 'Material start index set to ' + newStartNum)
        self.rebaseMaterials(self._matStartNum, newStartNum)
        self._matStartNum = newStartNum

    def getStartNum(self):
        return self._matStartNum

    matStartNum = property(getStartNum, setStartNum, doc='Устанавливает стартовый номер для материалов, '
                                                         'вызывает перестройку существующих (not implemented yet)')

    def rebaseMaterials(self, oldNum, newNum):
        """
        @todo: Функция, которая перестраивает существующие материалы относительно нового номера
        not implemented yet
        @param oldNum:
        @type oldNum: int
        @param newNum:
        @type newNum: int
        @return:
        """
        pass

    def checkMatExists(self, material):
        if material in self._matBaseContainer['matList']:
            return True
        return False

    def registerMaterial(self, material, metaInfo=None):
        self._matBaseContainer['matList'].append(material)
        matIndex = self._matStartNum + len(self._matBaseContainer['matList'])
        self._matBaseContainer['metMetaInfo'][matIndex] = metaInfo
        return matIndex

    def getMaterialsNumbers(self):
        return self._matBaseContainer['metMetaInfo'].keys()

    def getMaterialByNumber(self, number, returnFlag=0):
        if 0 == returnFlag:
            return self._matBaseContainer['matList'][number - self._matStartNum - 1]
        elif 1 == returnFlag:
            return self._matBaseContainer['metMetaInfo'][number]
        elif 2 == returnFlag:
            return self._matBaseContainer['matList'][number - self._matStartNum - 1], \
                   self._matBaseContainer['metMetaInfo'][number]
        elif 3 == returnFlag:
            return number
            pass

    def getMaterialByMaterial(self, material, returnFlag=0):
        if not self.checkMatExists(material):
            return None
        if 0 == returnFlag:
            return material
        elif 1 == returnFlag:
            return self._matBaseContainer['metMetaInfo'][
                self._matStartNum + self._matBaseContainer['matList'].index(material) + 1
            ]
        elif 2 == returnFlag:
            return material, self._matBaseContainer['metMetaInfo'][
                self._matStartNum + self._matBaseContainer['matList'].index(material) + 1
            ]
        elif 3 == returnFlag:
            return self._matStartNum + self._matBaseContainer['matList'].index(material) + 1