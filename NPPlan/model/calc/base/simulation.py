# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 05.04.13
@summary: 
'''

import numpy
from NPPlan.model.calc.mcnp.inpReader import gridFillReader as mcnpInputGridReader
from NPPlan.model.calc.mcnp.inpReader import NPPLAN_MCNP_SOURCE_RETURN_NUMBER, \
    NPPLAN_MCNP_SOURCE_RETURN_HEADER, \
    NPPLAN_MCNP_SOURCE_RETURN_ALL, NPPLAN_MCNP_SOURCE_RETURN_MATERIAL, NPPLAN_MCNP_SOURCE_RETURN_TUPLE
from NPPlan.model.calc.mcnp.mReader import mcnpReaderGrid as mcnpOutputGridReader
import abc
from dvhBuilder import *


class cellContainer(object):
    def __init__(self, cellDict={}):
        self.__dict__.update(cellDict)

    def __getattr__(self, item):
        return self.__dict__[item]

    def __str__(self):
        s = 'Cell representation:\n'
        for i in self.__dict__:
            s = '%s%s: %s\n' % (s, i, self.__dict__[i])
        return s

    # @todo: implement __eq__

class simulationGridContainerBase(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, inputClass, resultsClass):
        self._input = inputClass
        self.acquireInputGrid()
        self._results = resultsClass
        self.acquireOutputGrid()

    @abc.abstractmethod
    def acquireInputGrid(self):
        pass

    @abc.abstractmethod
    def acquireOutputGrid(self):
        pass

    def getCellData(self, cellNumber):
        print self._input.getCellByNumber(cellNumber, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_NUMBER)
        print self._results.getValueInCell(x=cellNumber)

    def getDoseAtPoint(self, x, y=None, z=None,):
        if isinstance(x, tuple):
            x, y, z = x
        px, py, pz = self._input.getCellByPosition(x, y, z)
        return self._results.getValueInCell(x=px, y=py, z=pz)

    def getMaterialAtPoint(self, x, y=None, z=None):
        if isinstance(x, tuple):
            x, y, z = x
        px, py, pz = self._input.getCellByPosition(x, y, z)
        return self._input.getCellByNumber(px, py, pz, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_MATERIAL)

    def getDataAtPoint(self, x, y=None, z=None):
        # @todo: implement cache
        if isinstance(x, tuple):
            x, y, z = x
        px, py, pz = self._input.getCellByPosition(x, y, z)
        t = self._input.getCellByNumber(px, py, pz, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_ALL)
        t.update({
            'value': self._results.getValueInCell(x=t['singleNum']),
            'error': self._results.getErrorInCell(x=t['singleNum']),
            'valueMultiplier': self._results._multiplier,
        })
        return cellContainer(t)

    def getDataAtCellTuple(self, xN, yN=None, zN=None):
        # @todo: implement cache
        if isinstance(xN, tuple):
            xN, yN, zN = xN
        t = self._input.getCellByNumber(xN, yN, zN, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_ALL)
        t.update({
            'value': self._results.getValueInCell(x=t['singleNum']),
            'error': self._results.getErrorInCell(x=t['singleNum']),
            'valueMultiplier': self._results._multiplier,
        })
        return cellContainer(t)

    def getDataAtCellNumber(self, xN):
        t = self._input.getCellByNumber(xN, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_ALL)
        t.update({
            'value': self._results.getValueInCell(x=t['singleNum']),
            'error': self._results.getErrorInCell(x=t['singleNum']),
            'valueMultiplier': self._results._multiplier,
        })
        return cellContainer(t)

    def getGridData(self, ):
        return self._input.getter(returnFlag=NPPLAN_MCNP_SOURCE_RETURN_HEADER)


class simulationMcnpGridContainer(simulationGridContainerBase):
    def __init__(self, mcnpInput=None, mcnpOutput=None, fill=-1, tally=-1):
        """

        @param mcnpInput:
        @type mcnpInput: NPPlan.model.calc.mcnp.inpReader.gridFillReader
        @param mcnpOutput:
        @type mcnpOutput: NPPlan.model.calc.mcnp.mReader.mcnpReaderGrid
        @return:
        """
        simulationGridContainerBase.__init__(self, mcnpInput, mcnpOutput)
        self._fill = mcnpInput.getGridsKeys()[0] if -1 == fill else fill
        self._tally = tally
        pass

    def acquireInputGrid(self):
        self._inputGrid = self._input._grids

    def acquireOutputGrid(self):
        self._outputGrid = self._results._data[self._results.getFirstTally()]


class simulationBase(object):
    def __init__(self,
                 source='mcnp',
                 target='image',
                 inputPath='',
                 isInputSeries=0,
                 resultPath='',
                 resultTally=-1,
                 outputPath='',
                 outputBackend=None,
                 **kwargs
                 ):
        self._sim = {'self': self}
        self._type = {'source': source, 'target': target}
        self._inputParams = {
            'inputPath': inputPath,
            'isSeries': isInputSeries,
            'resultPath': resultPath,
            'resultTally': resultTally,
            'other': kwargs.get('inputParams', {})
        }
        self._outputParams = {
            'path': outputPath,
            'backend': outputBackend,
            'other': kwargs.get('outputParams', {})
        }
        self._inputObject = None
        self._resultsObject = None
        self._outputObject = None
        self._readAllData()
        self._initOverloading()
        pass

    def acquireSiblingSimulation(self, sim, name=''):
        """
        Привязывается к родставенному плану
        @param sim: объект класса моделирования
        @type sim: NPPlan.model.calc.base.simulation.simulationBase
        @param name: имя
        @type name: str
        @return:
        """
        if '' == name:
            self._sim['sibling'] = sim
        else:
            self._sim[name] = sim

    def _readAllData(self):
        if 'mcnp' == self._type['source']:
            print self._inputParams
            self._inputObject = mcnpInputGridReader(self._inputParams['inputPath'], )
            print self._inputObject
            print self._inputObject.getShape()
            self._inputObject.acquireSimulation(self)
            self._resultsObject = mcnpOutputGridReader(self._inputParams['resultPath'],
                                                       shape=self._inputObject.getShape(),
                                                       #multiplier=(1 / (9.612E-9 * 6.54e7)) * 2e11 * 60,
                                                       multiplier=(3.7e10 / 6.24e7) * 2e11 * 60,   # Гр/мин
                                                       sim=self)
            self._resultsObject.acquireSimulation(self)
            self._grid = simulationMcnpGridContainer(self._inputObject, self._resultsObject)
        elif 'geant' == self._type['source']:
            pass

        pass

    def _initOverloading(self):
        for i in ['getDoseAtPoint', 'getMaterialAtPoint', 'getDataAtPoint', 'getDataAtCellNumber',
                  'getDataAtCellTuple', 'getGridData']:
            self.__setattr__(i, self._grid.__getattribute__(i))

    def getCellData(self, ):
        pass

    def makeDvh(self):
        dvh = dvhBuilder(grid=self._grid, cumulativeOutputPath='c:\\1\\testdvhn.png')
        dvh.acquireSimulation(self)
        dvh.addTargetCells([2, 3, 4, 5,
                            7, 8, 9, 10,
                            12, 13, 14,
                            16, 17,
                            19], 'tumour', color='#ff0000', label='Tumour')
        dvh.addTargetCells([6, 11, 18, ], 'tissue', color='#00ff00', label='Tissue')
        dvh.build()
        self._dvh = dvh

if __name__ == '__main__':
    import pprint
    inpFileName = r'C:\1\McnpVsGeant\test6\mgv6'
    outFileName = r'C:\1\McnpVsGeant\test6\mgv6m'
    #inpFileName = r'C:\MCNP5\mcnplan'
    #outFileName = r'C:\MCNP5\mcnplanm'
    #inpClass = mcnpInputGridReader(inpFileName)
    #print 'INPUT FULL DATA', inpClass.getCellByNumber(1248)
    #print inpClass.getCellByNumber(returnFlag=NPPLAN_MCNP_SOURCE_RETURN_HEADER)
    #pprint.pprint(inpClass._grids)
    #print inpClass.getCellByPosition(-2.0, -8.0, -6.5, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_NUMBER)
    #print inpClass.getPositionByCellNumber(1248)
    #print inpClass.getPositionByCellNumber(8, 2, 3)
    #outClass = mcnpOutputGridReader(outFileName, shape=inpClass.getShape())
    #c = simulationMcnpGridContainer(inpClass, outClass)
    #c.getCellData(0)
    #print c.getDoseAtPoint(0.0, 0.4, 1.1)
    c = simulationBase(inputPath=inpFileName, resultPath=outFileName)
    cc = c._inputObject.getter(18489, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_TUPLE)
    print 'TUPLE FOR 18489', cc, -29+cc[0], -29+cc[1], -34+cc[2]
    print
    print 'ALLDATA'
    import pprint
    pprint.pprint(c._inputObject.getter(18489, returnFlag=NPPLAN_MCNP_SOURCE_RETURN_ALL))
    #print c.getGridData()
    #print c.getDoseAtPoint(0.0, 0.4, 1.1)
    #print c.getMaterialAtPoint(0.0, 0.4, 1.1)
    #data = c.getDataAtPoint(-8, -2, 8.2)
    #print data

    c.makeDvh()
    #print c._resultsObject._data
    print c._resultsObject._data[6]['fValues']
    print c._inputObject.getter(0)
    print c._resultsObject.getValueInCell(x=0)
    print c._resultsObject.getValueInCell(x=0) * c._inputObject.getter(0)['mass']
    print c._resultsObject._data[6]['massesValues']
    #c._dvh.addTargetCells(range(1, 7), 'tumour')
    #print c._dvh._targetsList

    #print c.getMaterialAtPoint(0.0, 0.4, 1.1)['material'].id

    #pprint.pprint(c._outputGrid)
