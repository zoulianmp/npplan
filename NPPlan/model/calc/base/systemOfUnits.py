# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 29.01.14
@summary: 
'''

from NPPlan.lib import Singleton


@Singleton
class unitMeasures():
    def __init__(self, ):
        self._table = {}
        self._conversionRules = {}
        self._bases = {}
        self._buildTable()
        pass

    def setBaseMeasure(self, category, unit):
        self._bases[category] = unit

    def getBaseMeasure(self, category):
        return self._bases[category]
        pass

    def getSymbol(self, measure):
        for elems in self._table.itervalues():
            for unit in elems:
                if unit[0] == measure:
                    return unit[1]

    def appendValue(self, name, symbol, category, base=0.0, multiplier=1.0, conversionRule={}):
        if not category in self._table:
            self._table[category] = []
        self._table[category].append((name, symbol, base, multiplier))

    def parseConversionRules(self, newRule):
        '''

        @param newRule: { 'to' : list[ ruleTo1Tuple(ruleTarget, multiplier, shift), ruleTo2Tuple, ...],
        'from' : ruleFrom1Tuple(ruleTarget, multiplier, shift),..] }
        @type newRule: dict
        @return:
        '''
        # @todo: implement
        pass

    def _buildTable(self):
        '''

        @return:
        '''

        """
 new G4UnitDefinition(    "electronvolt","eV" ,"Energy",electronvolt);
 new G4UnitDefinition("kiloelectronvolt","keV","Energy",kiloelectronvolt);
 new G4UnitDefinition("megaelectronvolt","MeV","Energy",megaelectronvolt);
 new G4UnitDefinition("gigaelectronvolt","GeV","Energy",gigaelectronvolt);
 new G4UnitDefinition("teraelectronvolt","TeV","Energy",teraelectronvolt);
 new G4UnitDefinition("petaelectronvolt","PeV","Energy",petaelectronvolt);
 new G4UnitDefinition(           "joule","J"  ,"Energy",joule);
        """
        self.appendValue("electronvolt", "eV", "Energy")
        self.appendValue("kiloelectronvolt", "keV", "Energy", "electronvolt", 1e3)
        self.appendValue("megaelectronvolt", "MeV", "Energy", "electronvolt", 1e6)
        self.appendValue("gigaelectronvolt", "GeV", "Energy", "electronvolt", 1e9)
        self.appendValue("teraelectronvolt", "TeV", "Energy", "electronvolt", 1e12)
        self.appendValue("petaelectronvolt", "PeV", "Energy", "electronvolt", 1e15)
        self.appendValue("joule", "J", "Energy",
                         conversionRule={
                             'to': [("electronvolt", 6.2415e18, 0)],
                             'from': [("electronvolt",  1.602176565e-19, 0)]
                         })
        self.setBaseMeasure("Energy", "electronvolt")

        self.appendValue("Grey", "Gy", "Dose")
        pass

    def getTable(self):
        return self._table

    pass


class unitBase(object):
    def __init__(self, unitValue=0.0, unitMeasure="MeV", unitCategory="Energy", base="MeV", unitCastType=float):
        self._unitsTable = unitMeasures.Instance()
        if isinstance(unitValue, str) or isinstance(unitValue, unicode):
            t = unitValue.split(' ')
            unitValue = unitCastType(t[0])
            unitCategory = unitMeasure
            unitMeasure = t[1]
        self._unitValue = unitValue
        self._unitMeasure = unitMeasure
        self._unitCategory = unitCategory
        self._base = base
        pass

    def findRule(self, measure, ind=1):
        for i in self._unitsTable._table[self._unitCategory]:
            if measure == i[ind]:
                return i
            pass

    def getAs(self, targetMeasure, retType=2):
        # @todo: refactor
        if self._unitMeasure == targetMeasure:
            if 1 == retType:
                return self._unitValue
            elif 2 == retType:
                return '%f %s' % (self._unitValue, self._unitMeasure)
            else:
                return self._unitValue, self._unitMeasure
        uFrom = self.findRule(self._unitMeasure)
        uTo = self.findRule(targetMeasure)
        if 0.0 == uFrom[2]:
            retValue = self._unitValue * (1 / uTo[3])
        elif 0.0 == uTo[2]:
            retValue = self._unitValue * uFrom[3]
        elif uFrom[2] == uTo[2]:
            # same origin, simple case
            retValue = self._unitValue * (uFrom[3] / uTo[3])
        else:
            # @todo: implement with conversion table
            retValue = None
        if 1 == retType:
            return retValue
        elif 2 == retType:
            return '%f %s' % (retValue, targetMeasure)
        else:
            return retValue, targetMeasure
        pass

    #def __eq__(self, other):
    #    if self._unitCategory != other._unitCategory:
    #        return False
    #    pass
    def __cmp__(self, other):
        if self._unitCategory != other._unitCategory:
            raise TypeError
        bM = self._unitsTable.getBaseMeasure(self._unitCategory)
        bM = self._unitsTable.getSymbol(bM)
        return cmp(self.getAs(bM, 1), other.getAs(bM, 1))
        pass

    def __str__(self):
        if int(self._unitValue * 1000) == 0:
            return "%s: %s %s" % (self._unitCategory, self._unitValue, self._unitMeasure)
        return "%s: %f %s" % (self._unitCategory, self._unitValue, self._unitMeasure)

    def __add__(self, other):
        if isinstance(other, str):
            other = unitBase(other, self._unitCategory)
        if self._unitCategory != other._unitCategory:
            raise TypeError
        bM = self._unitsTable.getBaseMeasure(self._unitCategory)
        bM = self._unitsTable.getSymbol(bM)
        #print "self, _add_", self.getAs(bM, 1)
        #print "other, _add_", other.getAs(bM, 1)
        #print "sum, _add_", self.getAs(bM, 1) + other.getAs(bM, 1)
        #self._unitValue = self.getAs(bM, 1) + other.getAs(bM, 1)
        #self._unitMeasure = bM
        return unitBase(self.getAs(bM, 1) + other.getAs(bM, 1), bM)
        pass

    def __repr__(self):
        return self.__str__()

    def __sub__(self, other):
        if isinstance(other, str):
            other = unitBase(other, self._unitCategory)
        if self._unitCategory != other._unitCategory:
            raise TypeError
        bM = self._unitsTable.getBaseMeasure(self._unitCategory)
        bM = self._unitsTable.getSymbol(bM)
        return unitBase(self.getAs(bM, 1) - other.getAs(bM, 1), bM)
