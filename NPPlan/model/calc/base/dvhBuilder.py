# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 08.04.13
@summary: 
'''

import numpy as np
import matplotlib.pyplot as plt
import pprint


class dvhBuilder(object):
    def __init__(self, grid=None, volume=1.0, value=-1, distance=1e-4, bins=200, normed=True, **kwargs):
        """

        @param grid:
        @type grid: NPPlan.model.calc.base.simulation.simulationMcnpGridContainer
        @param volume:
        @param value:
        @return:
        """
        self._grid = grid
        print self._grid._inputGrid
        print self._grid._outputGrid
        volume = self._grid._inputGrid[self._grid._fill]['grid']['coordinateBounds']['volume']
        self._bins = bins
        self._normed = normed
        self._targetsList = {}
        self._output = {
            'differential': kwargs.get('differentialOutputPath', 'screen'),
            'cumulative': kwargs.get('cumulativeOutputPath', 'screen'),
        }
        print 'VOLUME', volume
        pass

    def acquireSimulation(self, sim):
        """
        Привязывает построитель к своему классу моделирования
        @param sim: объект класса моделирования
        @type sim: NPPlan.model.calc.base.simulation.simulationBase
        @return:
        """
        self._simulation = sim

    def addTargetCells(self, cells=[], id='', color='#000000', label=''):
        allClist = []
        for i in cells:
            clist = np.where(self._grid._inputGrid[self._grid._fill]['npRawData']==i)
            clist = clist[0]
            allClist.extend(clist)
        self._targetsList[id] = {'cells': allClist, 'values': self._grid._outputGrid['fValues'][allClist],
                                 'color': color, 'label': label}

    def _appendToCumulative(self, plt, data):
        hist, bins = np.histogram(data['values'], bins=self._bins)
        center = (bins[:-1] + bins[1:]) / 2
        if self._normed:
            ld = float(np.sum(hist)) / 100
            hist2 = [float(np.sum(hist[i:])) / ld for i in range(len(hist))]
        else:
            hist2 = [np.sum(hist[i:]) for i in range(len(hist))]
        print hist2
        #print data['cells']
        print 'data[values]', data['values']
        #print '100%', np.where(hist2==100.0)
        return plt.plot(center, hist2, '-', color=data['color'], label=data['label'])

    def _appendToDifferential(self, plt, data):
        hist, bins = np.histogram(data['values'], bins=self._bins)
        width = 0.7 * (bins[1] - bins[0])
        center = (bins[:-1] + bins[1:]) / 2
        plt.bar(center, hist, align='center', width=width, color=data['color'], label=data['label'])

    def build(self):
        # @todo: use sim.target
        # @todo: use multiple legend for cummulative
        #print np.bincount(self._grid._outputGrid['fValues'])
        print 'np.where'
        print np.where(self._grid._inputGrid[self._grid._fill]['npRawData']==1)
        print np.histogram(self._grid._outputGrid['fValues'])
        llist = np.where(self._grid._inputGrid[self._grid._fill]['npRawData']==6)
        target = self._grid._outputGrid['fValues']
        print 'try target llist'
        print target
        print target[llist]
        #print type(target)
        #print llist.shape()
        print len(llist)
        for i in llist[0]:
            print i, target[i]
        #    print 'q'
        #    print i, target[i]
        #    for j in i:
        #        print j
        print llist[0]
        #target = np.array([float(target[i]) for i in llist[0]], dtype=np.float64)
        hist, bins = np.histogram(target, bins=self._bins)
        width = 0.7 * (bins[1] - bins[0])
        center = (bins[:-1] + bins[1:]) / 2
        plt.figure()
        #plt.bar(center, hist, align='center', width=width, color='#000000')
        #plt.bar(bins[:-1], hist, align='center', width=width, color='#ff0000')
        b = []
        b.append(
            self._appendToDifferential(plt, {'values': target, 'color': '#0000ff', 'label': 'Body'})
        )
        for i in self._targetsList:
            b.append(
                self._appendToDifferential(plt, self._targetsList[i])
            )
        plt.legend()
        if 'screen' == self._output['differential']:
            plt.show()
        else:
            plt.savefig(self._output['differential'], format="png")
        plt.close()
        print hist, bins
        print np.sum(hist)
        print len(hist), len(bins)
        #if self._normed:
        #    ld = float(np.sum(hist)) / 100
        #    hist2 = [float(np.sum(hist[i:])) / ld for i in range(len(hist))]
        #else:
        #    hist2 = [np.sum(hist[i:]) for i in range(len(hist))]
        #print hist2
        l = []
        plt.figure()
        l.append(
            self._appendToCumulative(plt, {'values': target, 'color': '#0000ff', 'label': 'Body'})
        )
        #plt.plot(bins[:-1], hist2, '-')
        #plt.plot(center, hist2, '-')
        print self._targetsList
        for i in self._targetsList:
            l.append(
                self._appendToCumulative(plt, self._targetsList[i])
            )
        #    targetData = self._targetsList[i]
        #    hist, bins = np.histogram(targetData['values'], bins=self._bins)
        #    center = (bins[:-1] + bins[1:]) / 2
        #    if self._normed:
        #        ld = float(np.sum(hist)) / 100
        #        hist2 = [float(np.sum(hist[i:])) / ld for i in range(len(hist))]
        #    else:
        #        hist2 = [np.sum(hist[i:]) for i in range(len(hist))]
        #    plt.plot(center, hist2, '-', color=targetData['color'])
            #plt.plot()
        #plt.plot(bins[:-1], hist2, '-')
        #plt.plot(bins[1:], hist2, '-', color='#000000')
        plt.xlabel(_('Dose, sGy/min'))
        plt.ylabel(_('Volume, %'))
        print l
        #plt.legend(l, ["line 2", "line 1"],numpoints=1, loc=1)
        #for i in l:
        #    plt.legend([i], ["line 2"])
        plt.legend()
        if 'screen' == self._output['cumulative']:
            plt.show()
        else:
            plt.savefig(self._output['cumulative'], format="png")
        plt.close()

        pprint.pprint(self._targetsList)
        pass
