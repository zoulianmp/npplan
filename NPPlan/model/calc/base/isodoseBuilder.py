# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 14.02.13
@summary: 
'''

import abc
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
import matplotlib

NPPLAN_CONTOURS_BORDERS  =  0b001
NPPLAN_CONTOURS_BODY     =  0b010
NPPLAN_CONTOURS_COLORBAR =  0b100
NPPLAN_NORMALIZE_LOCAL =  0b01000
NPPLAN_NORMALIZE_GLOBAL = 0b10000
NPPLAN_ISODOSES_GRID =  0b1000000

class isodoseBuilderBase(object):
    """
    @abstract
    Базовый класс-построитель изодоз.
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self, mFileObject=None, slice=-1, axis=0, normalize=True,
                 contourFlag=NPPLAN_CONTOURS_BODY | NPPLAN_CONTOURS_BORDERS,
                 linspaceAccuracy=128):
        """
        @param mFileObject: прочитанный m-file
        @type mFileObject: mcnpReaderGrid
        @param slice: номер среза
        @type slice: int
        """
        self._input = mFileObject
        self._slice = slice
        self._axis = axis
        # @todo: проверять наличие флага типа нормировки при normalize == True
        self._normalize = normalize
        self._title = 'test'
        self._linspaceAccuracy = linspaceAccuracy
        self._contourFlag = contourFlag
        #self.makeValuesSlice()
        pass

    def reset(self):
        """
        Вызывает пересчёт
        """
        self.makeValuesSlice()

    @abc.abstractmethod
    def makeValuesSlice(self):
        """
        Function to implement
        Должна заполнять self._gridSlice в зависимости от среза self._slice и оси self._axis
        """
        self._gridSlice = None

    @abc.abstractmethod
    def makeShape(self):
        """
        Function to implement
        Должна возвращать размеры 2D-массива для изодоз в зависимости от оси self._axis
        @return: размеры
        @rtype: tuple
        """
        return (10, 10)

    @abc.abstractmethod
    def makeStartEndPoints(self, area):
        """
        Function to implement
        Должна возвращать начальные и конечные точки осей для двумерного рисунка
        @rtype: tuple
        """
        return (-10, 10, -10, 10)

    @abc.abstractmethod
    def makeSizes(self, area):
        """
        Function to implement
        Должна возврашать размеры области для рисунка по осям
        @rtype: tuple
        """
        return area[0][1] - area[0][0], area[1][1] - area[1][0]

    @abc.abstractmethod
    def getGlobalMax(self):
        """
        Function to implement
        Должна возвращать глобальный максимум
        @retype: float
        """
        return 0

    def makeGridData(self, area=None, accuracy=128):
        """
        Функция, получающая параметры области для 2D-рисунка и строящая matplotlib.mlab.griddata для заданных параметров.
        Отслеживает флаг self._normalize. Если флаг установлен, то изодозы будут перенормированы на абсолютный максимум
        @param area: зона ( (x0, x1), (y0, yn), (z0, zn) )
        @type area: tuple
        @param accuracy: параметр линейного преобразования для numpy.linspace
        @type accuracy: int
        """
        x = []
        y = []
        shapeX, shapeY = self.makeShape()
        xstart, xend, ystart, yend = self.makeStartEndPoints(area)
        sizeX, sizeY = self.makeSizes(area)
        print 'SHAPES', shapeX, shapeY
        print 'StartEnd Points', xstart, xend, ystart, yend
        print 'sizes', sizeX, sizeY
        for i in range(0, shapeX):
            for j in range(0, shapeY):
                x.append(xstart+float(i)*float(sizeX)/float(shapeX))
                y.append(ystart+float(j)*float(sizeY)/float(shapeY))
        x = np.array(x)
        y = np.array(y)
        if self._normalize:
            if self._contourFlag & NPPLAN_NORMALIZE_LOCAL:
                z = self._gridSlice.flatten() * 100 / np.max(self._gridSlice)
            elif self._contourFlag & NPPLAN_NORMALIZE_GLOBAL:
                z = self._gridSlice.flatten() * 100 / self.getGlobalMax()
        else:
            z = self._gridSlice.flatten()
        #print z
        #print len(self._gridSlice.flatten()[self._gridSlice.flatten()>0])
        #print x.shape, y.shape, z.shape
        xi = np.linspace(xstart, xend, accuracy)
        yi = np.linspace(ystart, yend, accuracy)
        self._gridData = griddata(x, y, z, xi, yi)
        #print self._gridData
        self._contourData = (xi, yi)
        pass

    def makeIsodoseImage(self, area=((-10, 10), (-10, 10), (-10, 10)), outputFileName='screen'):
        """
        Строит изодозы с заданными ограничениями по зоне area в зависимости от существующих параметров. Если outputFileName == 'screen',
        то построит изодозы на экране, иначе сохранит всё поле в файл outputFileName
        @param area: зона ( (x0, x1), (y0, yn), (z0, zn) )
        @type area: tuple
        @param outputFileName: screen или путь к выходному файлу
        @type outputFileName: string
        """
        self.makeGridData(area=area, accuracy=self._linspaceAccuracy)
        fig = plt.figure()
        fig.set_size_inches(20, 20)
        ax = fig.gca()
        # @todo: вычислять параметры для тиков по осям
        sePoints = self.makeStartEndPoints(area)
        print 'SEPOINTS', sePoints
        ax.set_xticks(np.arange(sePoints[0], sePoints[1], float(sePoints[1] - sePoints[0]) / 25))
        #ax.set_yticks(np.arange(-10,10, 0.5))
        ax.set_yticks(np.arange(sePoints[2], sePoints[3], float(sePoints[3] - sePoints[2]) / 25))
        print 'max', np.max(self._gridSlice)
        plt.title('%s %s / %s' %(self._title, np.max(self._gridSlice), self.getGlobalMax()))
        #CS = plt.contour(self._contourData[0], self._contourData[1], self._gridData, levels=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], linewidths=1.0, colors='#000000')
        if self._contourFlag & NPPLAN_CONTOURS_BORDERS:
            if self._contourFlag & NPPLAN_CONTOURS_BODY:
                CS = plt.contour(self._contourData[0], self._contourData[1], self._gridData, linewidths=1.0, colors='#000000')
            else:
                CS = plt.contour(self._contourData[0], self._contourData[1], self._gridData, linewidths=1.0, cmap=plt.cm.jet)
        plt.clabel(CS, fontsize=10, inline=1,fmt='%0.2f')
        #CS2 = plt.contourf(self._contourData[0], self._contourData[1], self._gridData, levels=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], cmap=plt.cm.jet)
        #CS2 = plt.contourf(self._contourData[0], self._contourData[1], self._gridData, levels=np.arange, cmap=plt.cm.jet)
        if self._contourFlag & NPPLAN_CONTOURS_BODY:
            if self._contourFlag & NPPLAN_NORMALIZE_GLOBAL:
                CS2 = plt.contourf(self._contourData[0], self._contourData[1], self._gridData, cmap=plt.cm.jet, norm=matplotlib.colors.Normalize(0, 100))
            else:
                CS2 = plt.contourf(self._contourData[0], self._contourData[1], self._gridData, cmap=plt.cm.jet)
        if self._contourFlag & NPPLAN_CONTOURS_COLORBAR:
            try:
                CB = plt.colorbar(CS2, shrink=0.8, extend='both')
            except UnboundLocalError:
                CB = plt.colorbar(CS, shrink=0.8, extend='both')
        if self._contourFlag & NPPLAN_ISODOSES_GRID:
            # @todo: получать параметры решётки в конструкторе
            plt.grid(color='r', linewidth=2)
        if 'screen' == outputFileName:
            plt.show()
        else:
            # @todo: проверять доступность пути файла
            plt.savefig(outputFileName, dpi=300)
            pass

    def setSlice(self, slice):
        """
        Устанавливает номер среза
        @param slice: номер среза
        @type slice: int
        """
        self._slice = slice
    def getSlice(self):
        return self._slice

    slice = property(getSlice, setSlice)

    def setAxis(self, axis):
        """
        Устанавливает ось
        @param axis: int in [0, 1, 2]
        @type axis: int
        """
        try:
            self._axis = int(axis) if int(axis) in [0, 1, 2] else 0
        except ValueError:
            self._axis = 0
    def getAxis(self):
        return self._axis

    axis = property(getAxis, setAxis)

    def setTitle(self, title):
        """
        Устанавливает текста заголовка
        @param title: заголовок
        @type title: string
        """
        self._title = title
    def getTitle(self):
        return self._title

    title = property(getTitle, setTitle)