# -*- coding: utf8 -*-
'''


@todo: docstrings stuff
@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 2
@date: 26.02.13
@summary: 
'''

import NPPlan
from materials import materialContainer, setStandartMats, NPPLAN_MAT_GET_ID, NPPLAN_MAT_GET_NAME, NPPLAN_MAT_GET_MAT
import numpy as np
import dicom
import operator
import os
import abc


class simulationGrid(object):
    def __init__(self, planObject):
        """
        @type planObject: NPPlan.model.plan.core.planAbstract
        """
        self._plan = planObject
        self._compression = 8
        self._charsInStringLimit = 74

    def start(self, iterator):
        pass
        self._sliceIterator = iterator
        return self

    def addContours(self, contours):
        pass
        return self

    def addFacility(self, facility):
        pass
        return self

    def getSliceXYBounds(self, rType=1, origin=0):
        """

        @param rType: тип возвращаемого значения
          1 - мм
          2 - см
        @type rType: int
        @param origin:
         0 - вернуть от x|y min до x|y max, min < 0, max > 0
         1 - вернуть от 0 до (x|ymax - x|ymin)
        @type origin: int
        @return: tuple ( xmin, xmax, ymin, ymax)
        @rtype: tuple
        """
        pixelSpacingX, pixelSpacingY = NPPlan.currentPlan.planGetPixelSpacing()
        sliceThickness = NPPlan.currentPlan.planGetSliceThickness()
        sliceSpacing = NPPlan.currentPlan.planGetSliceSpacing()
        rows, columns = NPPlan.currentPlan.planGetRowsColumns()
        #print 'aaa'
        #print NPPlan.currentPlan
        #print pixelSpacingX, pixelSpacingY
        # мм
        pixelLocationXM = -pixelSpacingX * columns / 2.
        pixelLocationXP = pixelSpacingX * columns / 2.
        pixelLocationYM = -pixelSpacingY * rows / 2.
        pixelLocationYP = pixelSpacingY * rows / 2.
        if 0 == origin:
            if 1 == rType:
                return pixelLocationXM, pixelLocationXP, pixelLocationYM, pixelLocationYP
            elif 2 == rType:
                return pixelLocationXM / 10.0, pixelLocationXP / 10.0, pixelLocationYM / 10.0, pixelLocationYP / 10.0
        elif 1 == origin:
            if 1 == rType:
                return 0, pixelLocationXP - pixelLocationXM, 0, pixelLocationYP - pixelLocationYM
            elif 2 == rType:
                return 0, (pixelLocationXP - pixelLocationXM) / 10.0, 0, (pixelLocationYP - pixelLocationYM) / 10.0

    def getCellXYBounds(self, rType=1, origin=0):
        """

        @param rType: тип возвращаемого значения,
          1 - мм
          2 - см
        @type rType: int
        @param origin:
         0 - вернуть от x|ymin до x|ymax вокруг 0
         1 - вернуть от 0 до (x|ymax - x|ymin)
         2 - вернуть от -x|yMin до -x|yMin + x|yCell
        @type origin: int
        @return: tuple ( xmin, xmax, ymin, ymax)
        @rtype: tuple
        """
        #print 'cellBounds started'
        if 2 != origin:
            sliceBounds = self.getSliceXYBounds(rType, origin)
        else:
            sliceBounds = self.getSliceXYBounds(rType, 0)
        #print sliceBounds
        rows, columns = NPPlan.currentPlan.planGetRowsColumns()
        print 'compression', columns / self._compression
        #print rows, columns
        if 2 == origin:
            return sliceBounds[0], sliceBounds[0] + 2 * sliceBounds[1] / (columns / self._compression), \
                   sliceBounds[2], sliceBounds[2] + 2 * sliceBounds[3] / (columns / self._compression)
        else:
            return sliceBounds[0] / (columns / self._compression), \
                   sliceBounds[1] / (columns / self._compression), \
                   sliceBounds[2] / (rows / self._compression), \
                   sliceBounds[3] / (rows / self._compression)

    def getCompression(self):
        return self._compression

    def setCompression(self, newCompression):
        self._compression = newCompression

    compression = property(getCompression, setCompression)

    def setGridMethod(self, gridAggregateMethod):
        self._gridAggregateMethodClass = gridAggregateMethod
        return self

    def toMcnp(self, simMethod=0, writeGrid=1, writeDensities=1, writeMatCards=1, fullOutput=1, useSourceTr=0,
               sdef=None, sdefMode=None, universe=1):

        """

        @param simMethod:
        @param writeGrid:
        @param writeDensities:
        @param writeMatCards:
        @param fullOutput:
        @param useSourceTr:
        @param sdef:
        @param sdefMode:
        @param universe:
        """
        grid = self._gridAggregateMethodClass(self._plan, self._sliceIterator)
        grid.compression = self._compression
        grid.work()
        # @todo: refactor for all aggregate methods
        # @todo: use string aggregator instead of print
        print 'bounds'
        print '%d : %d %d : %d %d : %d' % (-1 * (self._plan.planGetRowsColumns()[0] / grid._compression) + 1, 0,
                                            -1 * (self._plan.planGetRowsColumns()[1] / grid._compression) + 1, 0,
                                            -1 * self._plan.planGetSeriesLength() + 1, 0)
        print
        print 'grid table'
        outStrMats = ''
        for i in grid._sliceData:
            data = grid._sliceData[i]
            for val in data:
                if len(outStrMats) > self._charsInStringLimit:
                    print '' + outStrMats + ' &'
                    outStrMats = ''
                outStrMats = '%s %d' % (outStrMats, val)
        print outStrMats + ' &\n u=%d' % universe

        print 'densitites'
        #for i in grid._registeredMats['densities']:
        #    print '%d   %d -%1.4f -70   u=%d' %(i, i, grid._registeredMats['densities'][i], i)
        for i in grid.getMaterialsNumbers():
            print '%d   %d -%1.4f -70   u=%d' %(i, i, grid.getMaterialByNumber(i, 1)['density'], i)

        print 'matcards'
        '''
        for i in grid._registeredMats['data']:
            print 'm%d &' % i
            for mat in grid._registeredMats['data'][i]:
                matVal = -1.0 * grid._registeredMats['data'][i][mat]
                qp = NPPlan.mongoConnection.isotope.find_one({'symbol': mat})
                print '%d%003d %0.6f &' % (qp['atomicNum'], int(np.round(qp['atomicMass'])), matVal)
            pass
        '''
        outStr = ''
        for i in grid.getMaterialsNumbers():
            #print 'm%d ' % i
            outStr = 'm%d ' % i
            cKey, mat = grid.getMaterialByNumber(i, 2)
            if 'rcMap' in mat:
                for matData in mat['rcMap']:
                    #print mat, matData
                    matVal = -1.0 * mat['rcMap'][matData]
                    if 0.0 == float(matVal):
                        continue
                    matKey = NPPlan.mongoConnection.isotope.find_one({'symbol': matData})
                    # @todo: use common function for mcnp matcard generator
                    #print '%d%003d %0.6f &' % (matKey['atomicNum'], int(np.round(matKey['atomicMass'])), matVal)
                    outStr = '%s %d%003d %0.6f &\n' % (outStr, matKey['atomicNum'], int(np.round(matKey['atomicMass'])), matVal)
                pass
            else:
                #print cKey, mat
                for matData in cKey['cmap']:
                    #print '%d%003d -%0.6f &' % (matData[0]['atomicNum'], int(np.round(matData[0]['atomicMass'])), matData[1])
                    outStr = '%s %d%003d -%0.6f &\n' % (outStr, matData[0]['atomicNum'], int(np.round(matData[0]['atomicMass'])), matData[1])
            #print 'blablablas'
            print outStr[:-2]
        pass


class singleDicom(object):
    """
    Базовый класс чтения и преобразования DICOM-файлов
    """
    __metaclass__ = abc.ABCMeta
    def __init__(self, inputFile='', outputFile='', compression='', ):
        """
        @param inputFile: входной файл
        @type inputFile: string
        @param outputFile: выходной файл
        @type outputFile: string
        @param compression: количество пикселей для сжатия в воксель, 1 - без сжатия
        @type compression: int
        """
        self._inputFile = inputFile
        self._dicomInputFile = None
        self._inputArray = None
        self._inputParams = {}
        self._outputFile = outputFile
        self._compression = compression

        self._calibration = self.readCalibration()
        self._calibration = sorted(self._calibration.iteritems(), key=operator.itemgetter(0))

        self.initMaterials()

        #self.read()

    def initMaterials(self):
        """
        Читает стандартные материалы
        @see: setStandartMats.setStandartMats()
        """
        self._mats = materialContainer()
        self._mats = setStandartMats(self._mats)


    def readInputFile(self):
        """
        Считывает исходный файла используя pydicom
        """
        if self._dicomInputFile is None:
            self._dicomInputFile = dicom.ReadFile(self._inputFile)
        pass

    def read2NumpyArray(self):
        """
        Преобразует PixelData (7fe0,0010) в numpy.array
        """
        self._inputArray = np.fromstring(self._dicomInputFile.PixelData,dtype=np.int16)
        self._inputArray = self._inputArray.reshape((1,self._inputParams['columns'], self._inputParams['rows']))

    def readParams(self):
        """
        Читает параметры файла: число строк, колонок, размеры пикселей, параметры преобразования и т. п.
        """
        self._inputParams['columns'] = int(self._dicomInputFile.Columns)
        self._inputParams['rows'] = int(self._dicomInputFile.Rows)
        self._inputParams['pixelSpacing'] = (float(self._dicomInputFile.PixelSpacing[0]), float(self._dicomInputFile.PixelSpacing[1]))
        self._inputParams['rescaleSlope'] = int(self._dicomInputFile[0x0028, 0x1053].value)
        self._inputParams['rescaleIntercept'] = int(self._dicomInputFile[0x0028, 0x1052].value)
        self.readSliceParams()
        self._inputParams['sliceThickness'] = float(self._dicomInputFile.SliceThickness)
        #self._inputParams['sliceSpacing'] = float(self._dicomInputFile.SliceSpacing)

    def readSliceParams(self):
        """
        Читает параметр "Положение среза"
        """
        try:
            self._inputParams['sliceLocation'] = int(self._dicomInputFile.SliceLocation)
        except AttributeError:
            self._inputParams['sliceLocation'] = 0

    def rescale(self):
        """
        Выполняет масштабирование в соответствие с параметрами rescaleSlope, rescaleIntercept
        """
        self._inputArray *= self._inputParams['rescaleSlope']
        self._inputArray += self._inputParams['rescaleIntercept']

    def read(self, skipNumpy=False):
        """
        @param skipNumpy: вызывать ли автоматически чтение в массив numpy и масштабирование
        @type skipNumpy: bool
        """
        self.readInputFile()
        self.readParams()
        if not skipNumpy:
            self.read2NumpyArray()
            self.rescale()

    def meanInArea(self, leftCorner, rightCorner, upperCorner, lowerCorner):
        """
        Возвращает среднее значение в области
        """
        return np.mean(self._inputArray[0, upperCorner:lowerCorner, leftCorner:rightCorner])

    def nextCornerStageIterator(self, stepI, stepJ):
        """
        Итератор, возвращающий tuple ( левый край, правый край, верхний край, нижний край ) в соответствие с параметром шага
        @param stepI: шаг по x
        @type stepI: int
        @param stepJ: шаг по y
        @type stepJ: int
        @yield: tuple
        @rtype: tuple
        """
        stepI = 20 if stepI == -1 else stepI
        stepJ = 20 if stepJ == -1 else stepJ
        for i in range(0, self._inputParams['rows'], stepI):
            for j in range(0, self._inputParams['columns'], stepJ):
                lec = j
                upc = i
                loc = i + stepI if i + stepI < self._inputParams['rows'] else self._inputParams['rows']
                ric = j + stepJ if j + stepJ < self._inputParams['columns'] else self._inputParams['columns']
                yield (lec, ric, upc, loc)

    def readCalibration(self):
        """
        @todo: read from file
        """
        try:
            rD = {}
            for i in NPPlan.mongoConnection.ct2density.find().sort('ctValue'):
                rD[i.ctValue] = i.density
            return rD
        except:
            return {
                -5000: 0.0,
                -1000: 0.0,
                -400: 0.602,
                -150: 0.924,
                100: 1.075,
                300: 1.145,
                2000: 1.856,
                4927: 3.379,
            }

    def pixel2density(self, pixelValue):
        """
        Преобразование значение пикселя в плотность в соответствие с параметрами калибровки
        """
        # self._calibration = [(CT value, density), (CT value, density), ..]
        ii = 0
        for j in range(1, len(self._calibration)):
            if self._calibration[j-1][0] <= pixelValue < self._calibration[j][0]:
                deltaCT = self._calibration[j][0] - self._calibration[j-1][0]
                deltaDensity = self._calibration[j][1] - self._calibration[j-1][1]

                return self._calibration[j][1] - (( float(self._calibration[j][0] - pixelValue)*deltaDensity/deltaCT ))
            #print num, dens
        pass

    def createOutputFile(self):
        """
        Создаёт поток выходного файла (sys.stdout, если выходной файл == 'print')
        @return: поток
        @rtype: file
        """
        if 'print' == self._outputFile:
            import sys
            self._outputFileStream = sys.stdout
        else:
            self._outputFileStream = open(self._outputFile, 'w+')

    @abc.abstractmethod
    def write(self):
        """
        Запись файла. Перегружается в потомках
        """
        pass


class dicom2geant(singleDicom):
    def __init__(self, inputFile='', outputFile='', compression='', ):
        singleDicom.__init__(self, inputFile, outputFile, compression)

        self._charsInStringLimit = 40

        self.read()

    def write(self):
        self.createOutputFile()
        # первая строка - количество материалов
        self._outputFileStream.write('%d\n' % self._mats.getMaterialsQuantity())

        # следующие строки - материалы
        # id имя
        #for i in self._mats.getIdNameList():
        #    self._outputFileStream.write('%d %s \n' %i)
        self._mats.write2G4InputFormat(self._outputFileStream)

        # число вокселей (из исходного DICOM-файла)
        self._outputFileStream.write('%d %d %d\n' %(self._inputParams['columns'], self._inputParams['rows'], 1))

        # pixel spacing (размеры между пикселями)
        self._outputFileStream.write('%d %d\n' % (
            -1*self._inputParams['columns']*self._inputParams['pixelSpacing'][0]/2.,
            self._inputParams['columns']*self._inputParams['pixelSpacing'][0]/2.
            ))
        self._outputFileStream.write('%d %d\n' % (
            -1*self._inputParams['rows']*self._inputParams['pixelSpacing'][1]/2.,
            self._inputParams['rows']*self._inputParams['pixelSpacing'][1]/2.
            ))

        # положение и толщина среза
        thick = np.round(self._inputParams['sliceThickness']/2.0)
        self._outputFileStream.write('%d %d\n' %(
            self._inputParams['sliceLocation']-thick,
            self._inputParams['sliceLocation']+thick
            ))

        # список материалов
        if 1 == self._compression:
            # попиксельное преобразование без сжатия
            outStrMats = ''
            outStrDens = ''
            outStrDensBuff = ''
            for i in range(0, self._inputParams['rows']):
                for j in range(0, self._inputParams['columns']):
                    if len(outStrMats) > self._charsInStringLimit:
                        self._outputFileStream.write(outStrMats+"\n")
                        outStrMats = ''
                    if len(outStrDensBuff) > self._charsInStringLimit:
                        outStrDens = "%s\n %s" %(outStrDens, outStrDensBuff)
                        outStrDensBuff = ''
                    mean = self._inputArray[0, i, j]
                    density = self.pixel2density(mean)
                    material = self._mats.getClosestLowerByDensity(density, flag=NPPLAN_MAT_GET_ID)
                    outStrDensBuff = '%s %f' % (outStrDensBuff, density)
                    outStrMats = "%s %s" %(outStrMats, material)
            if len(outStrMats) > 0:
                self._outputFileStream.write(outStrMats)
            if len(outStrDensBuff) > 0:
                outStrDens = "%s\n %s" %(outStrDens, outStrDensBuff)

            self._outputFileStream.write(outStrDens)
            pass
        else:
            # сжимаем квадрат размерами self._compression * self._compression с учётом средней плотности по квадрату
            outStrMats = ''
            outStrDens = ''
            outStrDensBuff = ''
            # сетка материалов
            for bounds in self.nextCornerStageIterator(self._compression, self._compression):
                if len(outStrMats) > self._charsInStringLimit:
                    self._outputFileStream.write(outStrMats+"\n")
                    outStrMats = ''
                if len(outStrDensBuff) > self._charsInStringLimit:
                    outStrDens = "%s\n %s" %(outStrDens, outStrDensBuff)
                    outStrDensBuff = ''
                avgInSquare = np.mean(self._inputArray[0, bounds[0]:bounds[1], bounds[2]:bounds[3]])
                densityInSquare = self.pixel2density(avgInSquare)
                outStrDensBuff = '%s %f' % (outStrDensBuff, densityInSquare)
                #material = self._mats.getClosestLowerByDensity(densityInSquare, flag=NPPLAN_MAT_GET_NAME)
                material = self._mats.getClosestLowerByDensity(densityInSquare, flag=NPPLAN_MAT_GET_ID)
                outStrMats = "%s %s" %(outStrMats, material)
            if len(outStrMats) > 0:
                self._outputFileStream.write(outStrMats)
            if len(outStrDensBuff) > 0:
                outStrDens = "%s\n %s" %(outStrDens, outStrDensBuff)

            # сетка плотностей
            self._outputFileStream.write(outStrDens)

        pass

class dicomSeries2Geant(dicom2geant):
    def __init__(self, inputDir='', outputDir='', compression=8):
        self._inputDir = inputDir
        self._filesList = os.listdir(inputDir)
        dicom2geant.__init__(self, inputFile=os.path.join(inputDir, self._filesList[0]), outputFile='', compression=compression)
        self._outputDir = outputDir
        self._slicesQuantity = len(self._filesList)
        self.read(skipNumpy=True)

    def write(self):
        for fileName in self._filesList:
            self._inputFile = os.path.join(self._inputDir, fileName)
            self._outputFile = os.path.join(self._outputDir, '%s.g4dcm' % (os.path.basename(self._inputFile)))
            #self.read()
            self._dicomInputFile = None
            self.readInputFile()
            self.read2NumpyArray()
            self.rescale()
            self.readSliceParams()
            #self.createOutputFile()
            dicom2geant.write(self)


class dicom2MCNP(singleDicom):
    """
    Преобразователь единичного среза в формат MCNP-решётки.
    """
    def __init__(self, inputFile='', outputFile='', compression=8, boxPlaneIds=[], cellPlaneIds=[], skipRead=False):
        """

        @param inputFile: имя входного файла
        @type inputFile: str
        @param outputFile: имя выходного файла
        @type outputFile: str
        @param compression: количество пикселей исходного снимка в получаемой ячейке
        @type compression: int
        @param boxPlaneIds: массив с номерами поверхностей для базовых плоскостей фантома, { 'x': [x0, x1], 'y': [y0, y1], 'z': [z0, z1] }
        @type boxPlaneIds: dict
        @param cellPlaneIds: массив с номерами поверхностей для единичной ячейки фантома, { 'x': [x0, x1], 'y': [y0, y1], 'z': [z0, z1] }
        @type cellPlaneIds: dict
        @param skipRead: флаг пропуска автоматического чтения
        @type skipRead: bool
        @return:
        """
        singleDicom.__init__(self, inputFile, outputFile, compression)

        self._usedMaterials = []
        self._charsInStringLimit = 73
        self._topLevelUniverse = 100

        if 0 == len(boxPlaneIds):
            self._boxPlaneIds = {
                'x': [410, 420],
                'y': [430, 440],
                'z': [450, 460],
            }
        else:
            self._boxPlaneIds = boxPlaneIds

        if 0 == len(cellPlaneIds):
            self._cellPlaneIds = {
                'x': [401, 402],
                'y': [403, 404],
                'z': [405, 406],
            }
        else:
            self._cellPlaneIds = cellPlaneIds

        self._slicesQuantity = 1
        if not skipRead:
            self.read()

    def calculateCellsBounds(self):
        """
        Метод для расчёта границ решётки в формате MCNP, возвращает массив с ключами, равными номерам поверхностей,
        задаваемых self._boxPlaneIds, self._cellPlaneIds; значения - координаты в системе MCNP
        @return: массив формата { номер_поверхности1 : координата1, номер_поверхности2 : координата2, ...}
        @rtype: dict
        """
        xB = self._inputParams['rows'] * self._inputParams['pixelSpacing'][0] / 10.0
        yB = self._inputParams['columns'] * self._inputParams['pixelSpacing'][1] / 10.0
        xQ = np.round(float(self._inputParams['rows']) / (self._compression))
        yQ = np.round(float(self._inputParams['columns']) / (self._compression))
        xS = xB / xQ
        yS = yB / yQ
        zB = self._slicesQuantity * float(self._inputParams['sliceThickness']) / 10.0
        zS = float(self._inputParams['sliceThickness']) / 10.0
        #print xB, xS, yB, yS, zB
        _ret = {}

        _ret[self._boxPlaneIds['x'][0]] = -1.0 * xB / 2 + xS / 2
        _ret[self._boxPlaneIds['x'][1]] = +1.0 * xB / 2 - xS / 2

        _ret[self._boxPlaneIds['y'][0]] = -1.0 * yB / 2 + yS / 2
        _ret[self._boxPlaneIds['y'][1]] = +1.0 * yB / 2 - yS / 2

        _ret[self._boxPlaneIds['z'][0]] = -1.0 * zB / 2 + zS / 2
        _ret[self._boxPlaneIds['z'][1]] = +1.0 * zB / 2 - zS / 2

        _ret[self._cellPlaneIds['x'][0]] = -1.0 * xS / 2#+xB/2
        _ret[self._cellPlaneIds['x'][1]] = +1.0 * xS / 2#-xB/2

        _ret[self._cellPlaneIds['y'][0]] = -1.0 * yS / 2#+yB/2
        _ret[self._cellPlaneIds['y'][1]] = +1.0 * yS / 2#-yB/2

        _ret[self._cellPlaneIds['z'][0]] = -1.0 * zS / 2
        _ret[self._cellPlaneIds['z'][1]] = +1.0 * zS / 2

        self._volume = xS*yS*zS

        return _ret
        #return (self._inputParams['columns']*self._inputParams['pixelSpacing'][0]/10.0, self._inputParams['rows']*self._inputParams['pixelSpacing'][0]/10.0)

    def calculateGridBounds(self):
        """
        Возвращает границы решётки для заполнения lattice (== границы для заполнения tally)
        @return: tuple( XS, XE, YS, YE, ZS, ZE ), где XS, XE - начальная и граничная по Х, YS, YE - начальная и конечная по Y, ZS, ZE - начальная и конечная по Z
        @rtype: tuple
        """
        if 1 == self._slicesQuantity:
            zU = 0
            zD = 0
        elif 0 == self._slicesQuantity % 2:
            # четное количество срезов
            zU = 1.0 * np.round(float(self._slicesQuantity) / 2.0) - 1
            zD = -1.0 * np.round(float(self._slicesQuantity) / 2.0)
        else:
        #elif 1 == self._slicesQuantity % 2:
            zU = 1.0 * np.round(float(self._slicesQuantity) / 2.0) - 1
            zD = -1.0 * np.round(float(self._slicesQuantity) / 2.0) + 1
        # @todo: handle odd/even
        return (
            -1.0 * np.round(float(self._inputParams['rows']) / (2 * self._compression)),
            np.round(float(self._inputParams['rows']) / (2 * self._compression)) - 1,
            -1.0 * np.round(float(self._inputParams['rows']) / (2 * self._compression)),
            np.round(float(self._inputParams['columns']) / (2 * self._compression)) - 1,
            zD,
            zU,
        )

    def initMaterials(self):
        """
        Инициализация материалов
        @see: NPPlan.model.calc.base.materials.materialContainer
        """
        self._mats = materialContainer(outerCell=-70)
        self._mats = setStandartMats(self._mats)

    def writeHeader(self):
        """
        Записывает заголовки MCNP-файла, включая имя программы.
        """
        self._outputFileStream.write('grid test\n')
        self._outputFileStream.write('c auto generated\n')

    def writeCellBounds(self):
        """
        Записывает поверхности решётки (для фантома и для ячеек фантома)
        """
        for i in self._boxPlaneIds:
            for plane in self._boxPlaneIds[i]:
                self._outputFileStream.write('%d p%s %1.4f\n' %(plane, i, self._cellBounds[plane]))
        for i in self._cellPlaneIds:
            for plane in self._cellPlaneIds[i]:
                self._outputFileStream.write('%d p%s %1.4f\n' %(plane, i, self._cellBounds[plane]))

    def writeGrid(self):
        """
        Записывает решётку, включая переменные заполнения (lat, fill), саму решётку и параметры ячеек решётки
        """
        gridBounds = self.calculateGridBounds()
        self._gridBounds = gridBounds

        self._outputFileStream.write("       lat=1 fill=    %2d :  %2d    %2d :  %2d    %2d :  %2d\n" % gridBounds)
        self._cellBounds = self.calculateCellsBounds()

        self.writeGridMaterials()

        for i in self._usedMaterials:
            self._outputFileStream.write(i.toMCNPDensityString() + "\n")

    def writeGridMaterials(self):
        outStrMats = ''
        for bounds in self.nextCornerStageIterator(self._compression, self._compression):
            if len(outStrMats) > self._charsInStringLimit:
                self._outputFileStream.write('      ' + outStrMats + "\n")
                outStrMats = ''
            avgInSquare = np.mean(self._inputArray[0, bounds[0]:bounds[1], bounds[2]:bounds[3]])
            densityInSquare = self.pixel2density(avgInSquare)
            material = self._mats.getClosestLowerByDensity(densityInSquare, flag=NPPLAN_MAT_GET_MAT)
            if material not in self._usedMaterials:
                self._usedMaterials.append(material)
            outStrMats = "%s %s" % (outStrMats, material.id)
        outStrMats = "%s u=%d" % (outStrMats, self._topLevelUniverse)
        if len(outStrMats) > 0:
            self._outputFileStream.write('      ' + outStrMats + "\n")
        pass

    def writeBlankLine(self):
        self._outputFileStream.write("\n")

    def writeSurfaces(self):
        self.writeCellBounds()
        self._outputFileStream.write('70  so 200.\n')
        self._outputFileStream.write('20  so 200.\n')
        pass

    def writeMatCards(self):
        for i in self._usedMaterials:
            self._outputFileStream.write(i.toMCNPMatCardString(True) + "\n")
        self._outputFileStream.write('m400   7014  0.7 8016 0.3\n')
        pass

    def writeCells(self):
        if not hasattr(self, 'zeroCell'):
            self.zeroCell = (600, 20)
        self._outputFileStream.write('%d      0  %d\n' % self.zeroCell)
        self._outputFileStream.write('700      400 -0.00114   -20 (-%d:%d:-%d:%d:-%d:%d)\n' % (
            self._boxPlaneIds['x'][0],
            self._boxPlaneIds['x'][1],
            self._boxPlaneIds['y'][0],
            self._boxPlaneIds['y'][1],
            self._boxPlaneIds['z'][0],
            self._boxPlaneIds['z'][1],
        ))
        self._outputFileStream.write('300      0   %d -%d %d -%d %d -%d    fill=100\n' % (
            self._boxPlaneIds['x'][0],
            self._boxPlaneIds['x'][1],
            self._boxPlaneIds['y'][0],
            self._boxPlaneIds['y'][1],
            self._boxPlaneIds['z'][0],
            self._boxPlaneIds['z'][1],
        ))
        self._outputFileStream.write('400      0   %d -%d %d -%d %d -%d\n' % (
            self._cellPlaneIds['x'][0],
            self._cellPlaneIds['x'][1],
            self._cellPlaneIds['y'][0],
            self._cellPlaneIds['y'][1],
            self._cellPlaneIds['z'][0],
            self._cellPlaneIds['z'][1],
        ))
        self.writeGrid()
        pass

    def write(self):
        self.createOutputFile()

        self.writeHeader()

        self.writeCells()

        self.writeBlankLine()

        self.writeSurfaces()

        self.writeBlankLine()

        self.writeMatCards()
        self.writeSourceDef()
        pass

    def writeSourceDef(self):
        self._outputFileStream.write('sdef pos=0 0 30\n')
        self._outputFileStream.write('vol no\n')
        self._outputFileStream.write('sd6 %1.6f\n' % (self._volume))
        self._outputFileStream.write('f6:n (400<400[%2d :  %2d    %2d :  %2d    %2d :  %2d])\n' % (self._gridBounds))
        self._outputFileStream.write('imp:n 0 1 1 1 1 %dr' % (len(self._usedMaterials) - 1))


class dicomSeries2MCNP(dicom2MCNP):
    def __init__(self, inputDir='', outputFile='', compression=8, boxPlaneIds=[], cellPlaneIds=[],):
        self._inputDir = inputDir
        self._filesList = os.listdir(inputDir)
        #print self._filesList
        #print len(self._filesList)
        dicom2MCNP.__init__(
            self,
            inputFile=os.path.join(inputDir, self._filesList[0]),
            outputFile=outputFile,
            compression=compression,
            boxPlaneIds=boxPlaneIds,
            cellPlaneIds=cellPlaneIds,
        )
        self._slicesQuantity = len(self._filesList)
        self.read(skipNumpy=True)
        pass

    def writeGridMaterials(self):
        outStrMats = ''
        for fileName in self._filesList:
            self._inputFile = os.path.join(self._inputDir, fileName)
            #print self._inputFile
            self._dicomInputFile = None
            self.readInputFile()
            self.read2NumpyArray()
            self.rescale()
            self.readSliceParams()
            for bounds in self.nextCornerStageIterator(self._compression, self._compression):
                if len(outStrMats) > self._charsInStringLimit:
                    self._outputFileStream.write('      ' + outStrMats + "\n")
                    outStrMats = ''
                avgInSquare = np.mean(self._inputArray[0, bounds[0]:bounds[1], bounds[2]:bounds[3]])
                densityInSquare = self.pixel2density(avgInSquare)
                material = self._mats.getClosestLowerByDensity(densityInSquare, flag=NPPLAN_MAT_GET_MAT)
                if material not in self._usedMaterials:
                    self._usedMaterials.append(material)
                outStrMats = "%s %s" %(outStrMats, material.id)
        outStrMats = "%s u=%d" %(outStrMats, self._topLevelUniverse)
        if len(outStrMats) > 0:
            self._outputFileStream.write('      ' + outStrMats + "\n")
            pass


if __name__ == '__main__':
    #inputFile = r'C:\Users\Chernukha\Pictures\DICOM\01111140\84269326'
    #parser = dicom2geant(inputFile, outputFile='print', compression=8)

    #parser.write()

    #parser = dicom2MCNP(inputFile, outputFile='print', compression=8)

    #parser.write()

    inputDir = r'C:\Users\Chernukha\Pictures\DICOM\01111140'
    parser = dicomSeries2MCNP(inputDir=inputDir, outputFile='print', compression=16)
    parser.write()
    print parser._usedMaterials
    #parser = dicomSeries2Geant(inputDir=inputDir, outputDir='C:\\Users\\Chernukha\\Pictures\\DICOM\\g401111140\\')
    #parser.write()
