# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 04.03.14
@summary: 
'''

fDir = r'C:\Temp\ebt\cubes_out\cube_1_out'
import os
outPath = r'C:\Temp\ebt\cubes_out\cube1z.txt'
outStream = file(outPath, 'w')

jd = -1
for filename in os.listdir(fDir):
    filepath = os.path.join(fDir, filename)
    #print filename
    #jd = int(filename.split("_")[0][1:])
    #print jd
    jd += 1
    dataDict = {}
    for line in file(filepath):
        if 'mm' in line:
            continue
        if '---' in line:
            continue
        spl = line.split(' ')
        spl = [i for i in spl if i != '']
        x = float(spl[0])
        x = int(x*10)
        y = float(spl[1])
        y = int(y*10)
        val = float(spl[2].replace('\n', ''))
        try:
            dataDict[x][y] = val
        except KeyError:
            dataDict[x] = {}
            dataDict[x][y] = val
    for x in dataDict:
        for y in dataDict[x]:
            outStream.write("%d %d %d %f\n" % (x, y, jd, dataDict[x][y]))

outStream.close()