# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 04.03.14
@summary: 
'''

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

fig = plt.figure()
fig.set_size_inches(20, 20)
ax_ = fig.add_subplot(111, projection='3d')

fn = 'C:\Temp\ebt\cubes_out\cube1z.txt'

ax = []
ay = []
az = []
ac = []

for i in file(fn):
    x, y, z, c = i.split(' ')
    c = c.replace('\n', '')
    x = int(x)
    y = int(y)
    z = int(z)
    c = float(c)
    if x % 10 != 0:
        continue
    if y % 10 != 0:
        continue
    if x>(288/2) and y<(318/2) and z > 10:
        continue

    ax.append(x)
    ay.append(y)
    az.append(z)
    ac.append(c)

ax = np.array(ax)
ay = np.array(ay)
az = np.array(az)
ac = np.array(ac)
ac = ac / np.max(ac)
print ax.shape, ay.shape, az.shape, ac.shape

#x = np.random.standard_normal(100)
#y = np.random.standard_normal(100)
#z = np.random.standard_normal(100)
#c = np.random.standard_normal(100)

ax_.scatter(ax, ay, az, c=ac, cmap=plt.jet())
#ax_.plot_surface(
#    ax, ay, az, rstride=1, cstride=1,
#    facecolors=cm.jet(ac),
#    linewidth=0, antialiased=False, shade=False)
plt.show()
#plt.savefig(r'C:\Temp\ebt\cubes_out\3d1.png', dpi=300)