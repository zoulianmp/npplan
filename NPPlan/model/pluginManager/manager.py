# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 30.07.13
@summary: 
'''

import imp
import os
import NPPlan


class pluginManager(object):
    MainModule = "__init__"
    PluginFolder = "controller/%s/plugins"
    LoadQueue = []
    LoadedPlugins = {}
    plugins = {}

    def __init__(self, autoload=True, pluginType='client'):
        self.__dict__['pluginType'] = pluginType
        self.__dict__['pluginFolder'] = self.PluginFolder % (self.__dict__['pluginType'])
        NPPlan.log('pluginManager', 'Plugin manager initialized')
        if autoload:
            self.getPlugins()
            self.getExtraPlugins()
        pass

    def getPlugins(self):
        # @todo: rework plugins
        # @todo: add autoload
        #PluginFolder = "controller/client/plugins"
        #plugins = []
        PluginFolder = os.path.join(NPPlan.config.appPath, self.__dict__['pluginFolder'])
        NPPlan.log('pluginManager', 'Plugin manager now checking for available plugins at %s' % PluginFolder)
        possibleplugins = os.listdir(PluginFolder)
        for i in possibleplugins:
            location = os.path.join(PluginFolder, i)
            if not os.path.isdir(location) or not self.MainModule + ".py" in os.listdir(location):
                continue
            if os.path.exists(os.path.join(location, '__autoload__.py')):
                self.LoadQueue.append(i)
            info = imp.find_module(self.MainModule, [location])
            self.plugins[i] = {"name": i, "info": info}
        NPPlan.log('pluginManager', 'List of available plugins: ')
        NPPlan.log('pluginManager', self.getPluginsNames())
        NPPlan.log('pluginManager', 'List of plugins to load automatically: ')
        NPPlan.log('pluginManager', self.LoadQueue)
        while len(self.LoadQueue) > 0:
            self.loadPlugin(self.LoadQueue.pop())
        #print 'Available plugins:', plugins
        #return plugins

    def getExtraPlugins(self):
        # 1. Load list of plugins from DB
        # 2. Get plugins info
        # 3. Autoload if needed
        pass

    def getPluginsNames(self):
        return self.plugins.keys()

    def loadPlugin(self, plugin, forceReload=False):
        if plugin in self.LoadedPlugins and not forceReload:
            NPPlan.log('pluginManager', 'Accessing plugin %s' % plugin)
            return self.LoadedPlugins[plugin]
        NPPlan.log('pluginManager', 'Loading plugin %s' % plugin)
        self.LoadedPlugins[plugin] = imp.load_module(self.MainModule, *self.plugins[plugin]["info"])
        return self.LoadedPlugins[plugin]
        #return imp.load_module(self.MainModule, *plugin["info"])

