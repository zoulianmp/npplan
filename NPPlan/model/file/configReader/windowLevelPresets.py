# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 01.11.12
@summary: 
'''

import NPPlan
import xml.etree.ElementTree as ET
from NPPlan.model.file.configReader import readConfig as _readConfig

_root = None

def readConfig():
    global _root
    if _root is not None:
        return
    _root = _readConfig('windowLevelPresets')

def parse():
    global _root
    ret = {}
    _root.findall('preset')
    for i in _root:
        ret[i.attrib['NAME']] = {
            'name' : i.attrib['NAME'],
            'window' : i.attrib['WINDOW'],
            'level' : i.attrib['LEVEL'],
        }
    return ret
