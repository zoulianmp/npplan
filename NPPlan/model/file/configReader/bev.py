# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 24.12.12
@summary: 
'''

from NPPlan.model.file.configReader import readConfig as _readConfig

_root = None
def readConfig(name):
    global _root
    if _root is not None:
        return
    _root = _readConfig(name)

def getWindows():
    global _root
    _ret = {}
    items = _root.findall('./window')
    for win in items:
        name = win.findall('./name')[0].text
        position = int(win.findall('./position')[0].text)
        handler = win.findall('./handler')[0].text
        caption = win.findall('./caption')[0].text
        if win.findall('./caption')[0].attrib.has_key('color'):
            captionColor =  win.findall('./caption')[0].attrib['color']
        else:
            captionColor = 'White'
        _ret[name] = {
            'name' : name,
            'position' : position,
            'handler' : handler,
            'caption' : caption,
            'captionColor' : captionColor,
        }
    return _ret

def getPopups():
    global _root
    _ret = {}
    items = _root.findall('./popup')
    for popups in items:
        print popups.attrib
        _sret = []
        for pItem in popups.findall('./item'):
            q = {}
            q.update(pItem.attrib)
            if not (pItem.attrib.has_key('type') and 'separator' == pItem.attrib['type']):
                name = pItem.findall('./name')[0].text
                caption = pItem.findall('./caption')[0].text
                backend = pItem.findall('./backend')[0].text
                q.update({'name' : name, 'caption' : caption, 'backend' : backend})
            _sret.append(q)
        _i = popups.attrib['onWindows'].split(',')
        for j in _i:
            if _ret.has_key(int(j)):
                _ret[int(j)].extend(_sret)
            else:
                _ret[int(j)] = _sret
    return _ret

def getHelper():
    global _root
    _ret = {}
    items = _root.findall('./helper')
    for hItem in items:
        print hItem.attrib
        _sret = []
        for item in hItem.findall('./item'):
            q = {}
            q.update(item.attrib)
            name = item.findall('./name')[0].text
            icon = item.findall('./icon')[0].text
            type = item.findall('./type')[0].text
            backend = item.findall('./backend')[0].text
            try:
                group = item.findall('./group')[0].text
            except IndexError:
                group = None
            try:
                default = item.findall('./default')[0].text
            except IndexError:
                default = 0
            q.update({
                'name': name,
                'icon': icon,
                'type': type,
                'backend': backend,
                'group': group,
                'default': default,
            })
            _sret.append(q)
        _i = hItem.attrib['onWindows'].split(',')
        for j in _i:
            if _ret.has_key(int(j)):
                _ret[int(j)].extend(_sret)
            else:
                _ret[int(j)] = _sret
    return _ret

