# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 11.01.13
@summary: 
'''

from NPPlan.model.file.configReader import readConfig as _readConfig

_root = None
_ret = None

def readConfig():
    global _root
    if _root is not None:
        return
    _root = _readConfig('volume')

def parseConfig():
    global _root
    volumes = _root.findall('volume')
    _ret = {}
    for i in volumes:
        print i.attrib
        _ret[i.attrib['name']] = {}
        _ret[i.attrib['name']].update(i.attrib)
        items = i.findall('./*')
        for j in items:
            try:
                _ret[i.attrib['name']][j.tag].append(j.attrib)
            except KeyError:
                _ret[i.attrib['name']][j.tag] = [j.attrib]
            print j.tag, j.attrib
    return parseVals(_ret)

def parseVals(arr):
    for i in arr:
        vol = arr[i]
        for prop in vol:
            #print 'prop', prop, vol[prop], type(vol[prop])
            if isinstance(vol[prop], str):
                try:
                    arr[i][prop] = int(arr[i][prop])
                except ValueError:
                    pass
            elif isinstance(vol[prop], list):
                for propItem in range(len(vol[prop])):
                    for val in vol[prop][propItem]:
                        arr[i][prop][propItem][val] = float(arr[i][prop][propItem][val])
    return arr

def getData():
    global _root, _ret
    if _root is None:
        readConfig()
    if _ret is None:
        _ret = parseConfig()
    return _ret