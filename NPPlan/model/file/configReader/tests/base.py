# -*- coding: utf8 -*-
import NPPlan
import unittest


class MyTestCase(unittest.TestCase):
    def setUp(self):
        #NPPlan.init(programPath=u"C:\\Users\\Алексей\\Documents\\npplan\\", test=True,
        NPPlan.init(programPath=u"C:\\npplan3\\", test=True,
                    log=False, utest=True, noPlan=True)

    def testConfigDir(self):
        #self.assertEqual(NPPlan.xmlReader._configDir, '%s/view/client/config' %(NPPlan.config.appPath,))
        #self.assertEqual(NPPlan.xmlReader.getConfigDir(), u'C:\\Users\\Алексей\\Documents\\npplan\\NPPlan\\view\\client\\config')
        print NPPlan.xmlReader.getConfigDir()
        self.assertEqual(True, True)

    def testLoadChecker(self):
        # 1. existing file
        self.assertEqual(NPPlan.xmlReader.loadFile2Cache('stages'), True)
        # 2. Non existing file
        self.assertEqual(NPPlan.xmlReader.loadFile2Cache('nonExistedAnyFile'), False)

    def testBlockingLoad(self):
        self.assertEqual(NPPlan.xmlReader.loadFile('ribbon'), 0)
        self.assertEqual(NPPlan.xmlReader.loadFile('ribbon'), -2)
        self.assertEqual(NPPlan.xmlReader.loadFile('patWorkButtons'), -2)
        print NPPlan.xmlReader._cRoot
        self.assertEqual(NPPlan.xmlReader.state, True)
        self.assertIn('ribbon', NPPlan.xmlReader.getCurrentOpenedInfo())
        self.assertEqual(NPPlan.xmlReader.releaseFile(), 0)
        self.assertEqual(NPPlan.xmlReader.loadFile('ribbon'), 0)
        self.assertEqual(NPPlan.xmlReader.releaseFile(), 0)

    def testStagesList(self):
        import NPPlan.model.file.configReader.stages as stOldReader
        stOldReader.readConfig()
        old = stOldReader.getStagesList()

        new = NPPlan.xmlReader.readStagesList()
        self.assertEqual(old, new)


    def test_something(self):
        print NPPlan.xmlReader
        self.assertEqual(True, True)

    def tearDown(self):
        #del NPPlan
        pass


if __name__ == '__main__':
    unittest.main()
