# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 13.09.12
@summary: 
'''

import NPPlan
import xml.etree.ElementTree as ET

from anyReader import *


def readConfig(file):
    """
    Читает конфиг по указанному пути, возврашает xml.etree.ElementTree.getroot()
    @param file: имя файла
    @type file: string
    @return: корневой элемент
    @rtype: xml.etree.ElementTree
    """
    print NPPlan.config.appPath
    print '%s' %NPPlan.config.appPath
    print NPPlan.config.programPath
    try:
        path = '%s/view/client/config/%s.xml' %(NPPlan.config.appPath, file)
        tree = ET.parse(path)
        root = tree.getroot()
    except IOError:
        path = '%s/view/client/config/%s.xml' %('c:\\NPPlan3\\NPPlan', file)
        tree = ET.parse(path)
        root = tree.getroot()
        print path
    return root

__all__ = [
    'abstractButton',
    'addPatientForm',
    'stages',
    'studyInformer',
    'windowLevelPresets',
]