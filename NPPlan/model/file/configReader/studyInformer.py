# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 29.10.12
@summary: 
'''

import NPPlan
import xml.etree.ElementTree as ET
from NPPlan.model.file.configReader import readConfig as _readConfig

_root = None

def readConfig():
    global _root
    if _root is not None:
        return
    _root = _readConfig('addPatient.study.informer')

def getData():
    global _root
    ret = []
    fields = _root.findall("field")
    for field in fields:
        curField = {}
        curField['name'] = field.findall("name")[0].text
        curField['ruCaption'] = field.findall("ruCaption")[0].text
        dicom = field.findall("dicom")[0]
        print dicom.attrib
        if dicom.attrib.has_key('type') and 'id' == dicom.attrib['type']:
            dicom = dicomIdString2Tuple(dicom.text)
        else:
            dicom = dicom.text
        curField['dicom'] = dicom
        ret.append(curField)
    return ret

def dicomIdString2Tuple(string):
    return (
        int(
            string.split(',')[0],
            16
        ),
        int(
            string.split(',')[1],
            16
        ),
    )
