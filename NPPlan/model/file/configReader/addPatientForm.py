# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 16.10.12
@summary: 
'''

from NPPlan.model.file.configReader import readConfig as _readConfig

_root = {}
def readConfig(name):
    global _root
    if _root.has_key(name):
        return
    _root[name] = _readConfig(name)

def getItem(pan):
  for item in pan:
    if len(item.findall("./size")) > 0:
      size = item.findall("./size")[0].text
    else:
      size = None
    if 'text' == item.attrib['type']:
      yield {
        'type' : item.attrib['type'],
        'name' : item.findall("./name")[0].text,
        'caption' : item.findall("./caption")[0].text,
        'size' : size,
      }
    else:
      vals = item.findall("./option")
      ret = {}
      for j in vals:
        ret[j.attrib['name']] = j.text
      yield {
        'type' : item.attrib['type'],
        'name' : item.findall("./name")[0].text,
        'values' : ret,
        'caption' : item.findall("./caption")[0].text,
        'size' : size,
      }

def getData(name):
    global _root
    curPanel = None
    ret = {'captions' : {}}
    for i in _root[name]:
      #print i.tag, i.text, i.attrib
      if 'panel' == i.tag:
        for pan in i:
          if 'name' == pan.tag:
              curPanel = pan.text
              ret[curPanel] = []
          if 'caption' == pan.tag:
              ret['captions'][curPanel] = pan.text
          #print pan.tag, pan.text, pan.attrib
          if 'items' == pan.tag:
            for i in getItem(pan):
              #ret.append(i)
              ret[curPanel].append(i)
              #print i
    return ret



