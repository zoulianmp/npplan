# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 31.10.12
@summary: 
'''

import NPPlan
import dicom
import os

cache = {}

class shortStructure(object):
    def __init__(self, dictionary, dicom):
        for i in dictionary:
            self.__dict__[i] = getattr(dicom, i)
        pass

    def __getattr__(self, item):
        return self.__dict__[item]

    def __str__(self):
        return "%s" %self.__dict__

def readToStructure(filename, fields):
    global cache
    if cache.has_key(filename):
        dicomData = cache[filename]
    else:
        dicomData = dicom.read_file(filename)
        cache[filename] = dicomData
    #ret = {
    #    'InstanceNumber' : dicomData.InstanceNumber,
    #    'ImagePositionPatient' : dicomData.ImagePositionPatient,
    #    'SliceLocation' : dicomData.SliceLocation,
    #    'ContentTime' : dicomData.ContentTime
    #}
    ret = shortStructure(
        fields,
        dicomData
    )
    return ret
    pass

def readCommonData(filename):
    return readToStructure(filename, ['StudyInstanceUID', 'SeriesInstanceUID'])

def readConcreteData(filename):
    return readToStructure(filename, ['InstanceNumber', 'ImagePositionPatient', 'SliceLocation', 'ContentTime', 'SOPInstanceUID'])

def readSpacingData(dirname):
    #filename = os.listdir(dirname)[0]
    #r = readToStructure("%s%s" %(dirname, filename), ['PixelSpacing', 'SliceThickness'])
    ret = {
        'SliceSpacing': NPPlan.currentPlan.planGetSliceSpacing(),
        'PixelSpacing': NPPlan.currentPlan.planGetPixelSpacing(),
        'PixelSpacingX': NPPlan.currentPlan.planGetPixelSpacing()[0],
        'PixelSpacingY': NPPlan.currentPlan.planGetPixelSpacing()[1],
    }
    return ret

def clear():
    global cache
    for i in cache:
        del cache[i]
        del i
    cache = {}