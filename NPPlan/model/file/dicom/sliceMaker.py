# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 15.02.13
@summary: 
'''

import vtk
import os

class sliceMakerBase(object):
    def __init__(self, volume=None, origin=(0, 0, 0), axis=0):
        self._volume = volume
        self._origin = origin
        self._axis = axis

        self._plane = vtk.vtkPlane()
        self._plane.SetOrigin(*origin)
        if self._axis == 0:
            self._plane.SetNormal((1, 0, 0))
        elif self._axis == 1:
            self._plane.SetNormal((0, 1, 0))
        elif self._axis == 2:
            self._plane.SetNormal((0, 0, 1))

        self._imgCutter = vtk.vtkCutter()
        self._imgCutter.SetInput(self._volume._body.GetOutput())
        self._imgCutter.SetCutFunction(self._plane)
        self._imgCutter.GenerateCutScalarsOff()

        self._imgMapper = vtk.vtkPolyDataMapper()
        self._imgMapper.SetInput(self._imgCutter.GetOutput())
        self._imgMapper.Update()

        print 'bounds', self._imgMapper.GetBounds()

        self._imgActor = vtk.vtkActor()
        self._imgActor.SetMapper(self._imgMapper)
        if self._axis == 0:
            self._imgActor.RotateY(-90)
        elif self._axis == 1:
            self._imgActor.RotateX(-90)
        elif self._axis == 2:
            self._imgActor.RotateZ(180)
            #self._imgActor.RotateY(90)


    def toScreen(self):
        aRenderer = vtk.vtkRenderer()
        renWin = vtk.vtkRenderWindow()
        renWin.AddRenderer(aRenderer)
        iren = vtk.vtkRenderWindowInteractor()
        iren.SetRenderWindow(renWin)
        aCamera = vtk.vtkCamera()
        #aRenderer.SetActiveCamera(aCamera)
        aRenderer.ResetCamera()
        #aCamera.Dolly(1.5)

        aRenderer.AddActor(self._imgActor)

        aRenderer.SetBackground(1, 1, 1)
        renWin.SetSize(512, 512)

        aRenderer.ResetCameraClippingRange()

        c = aRenderer.GetActiveCamera()
        c.Zoom(1.5)

        iren.Initialize()
        renWin.Render()
        iren.Start()

    def toPNGFile(self, name):
        aRenderer = vtk.vtkRenderer()
        renWin = vtk.vtkRenderWindow()
        renWin.AddRenderer(aRenderer)
        iren = vtk.vtkRenderWindowInteractor()
        iren.SetRenderWindow(renWin)
        aCamera = vtk.vtkCamera()
        #aRenderer.SetActiveCamera(aCamera)
        aRenderer.ResetCamera()
        #aCamera.Dolly(1.5)

        aRenderer.AddActor(self._imgActor)

        aRenderer.SetBackground(1, 1, 1)
        renWin.SetSize(512, 512)

        aRenderer.ResetCameraClippingRange()

        c = aRenderer.GetActiveCamera()
        c.Zoom(1.5)

        iren.Initialize()
        renWin.Render()

        w2if = vtk.vtkWindowToImageFilter()
        w2if.SetInput(renWin)
        wr = vtk.vtkPNGWriter()
        wr.SetInput(w2if.GetOutput())
        outfile = name
        #cDir = os.getcwd()
        #os.chdir(os.path.dirname(outfile))
        #wr.SetFileName(os.path.basename(outfile))
        wr.SetFileName(outfile)
        #os.chdir(cDir)
        wr.Write()


class simpleVolume(object):
    def __init__(self, inputDir=''):
        self._inputDir = inputDir

        self._reader = vtk.vtkDICOMImageReader()
        self._reader.SetDirectoryName(self._inputDir)

        self._shrinkFactor = 4
        self._shrink = vtk.vtkImageShrink3D()
        self._shrink.SetShrinkFactors(self._shrinkFactor, self._shrinkFactor, 1)
        self._shrink.SetInput(self._reader.GetOutput())

        im = vtk.vtkImageMapToWindowLevelColors()
        im.SetWindow(1200.0)
        im.SetLevel(800.0)
        im.SetInput(self._reader.GetOutput())
        #im.SetInput(self._shrink.GetOutput())
        self._wlReader = im
        self._body = vtk.vtkImageReslice()

        self._body.SetInput(self._wlReader.GetOutput())
        self._body.AutoCropOutputOn()


def getCenter(dirName):
    # pixel spacing: 0028,0030
    # slice thickness: 0018,0050
    # rows: 0028, 0010
    # columns: 0028, 0011
    # length: number of
    number = len(os.listdir(dirName))
    anyFile = os.path.join(dirName, os.listdir(dirName)[0])
    import dicom
    d = dicom.read_file(anyFile)
    pixelSpacing = float(d[(0x0028, 0x0030)].value[0]), float(d[(0x0028, 0x0030)].value[1])
    thick = float(d[(0x0018, 0x0050)].value)
    rows = float(d[(0x0028, 0x0010)].value)
    columns = float(d[(0x0028, 0x0011)].value)

    print pixelSpacing, thick

    centerX = rows * pixelSpacing[0] / 2
    centerY = rows * pixelSpacing[1] / 2
    centerZ = thick * number / 2

    return centerX, centerY, centerZ

if __name__ == '__main__':
    #volume = simpleVolume('C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\')
    #volume = simpleVolume('C:\\Users\\Chernukha\\Pictures\\DICOM\\11070516\\t2_tse_tra\\')
    #volume = simpleVolume('C:\\Users\\Chernukha\\Pictures\\DICOM\\11070516\\t1_se_tra\\')
    volume = simpleVolume('C:\\Users\\Chernukha\\Pictures\\DICOM\\11070516\\t1_se_sag\\')
    #volume = simpleVolume('C:\\Users\\Chernukha\\Pictures\\DICOM\\11070516\\t1_se_cor\\')
    #volume = simpleVolume('C:\\Users\\Chernukha\\Pictures\\DICOM\\11070516\\localizer\\')

    print getCenter(volume._inputDir)


    #slice = sliceMakerBase(volume=volume, origin=(26, 109, 30), axis=1)
    #slice.toScreen()
    #slice.toPNGFile(u'C:\\MCNP5\\1.png')
    for i in [0, 1, 2]:
        slice = sliceMakerBase(volume=volume, origin=getCenter(volume._inputDir), axis=i)
        slice.toScreen()

