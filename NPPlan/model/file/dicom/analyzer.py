# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 29.04.13
@summary: 
'''


def getInitialLocationBounds(container):
    """

    @param container:
    @type container: NPPlan.model.plan.images.container.sliceContainerBase
    @return:
    """
    locs = []
    for i, sliceData in container.iterateSlices():
        pass
        locs.append(float(sliceData[(0x0020, 0x1041)].value))
        pass
    sMin, sMax = min(locs), max(locs)
    return sMin, sMax


def getFirstLastSlices(container):
    for i, sliceData in container.iterateSlices():
        pass
    pass


def getRescaleParams(slice):
    # @todo: check number for slope/intercept
    try:
        return {'slope': int(slice[(0x0028, 0x1053)].value),
                'intercept': int(slice[(0x0028, 0x1052)].value)}
    except AttributeError:
        return {'slope': 1, 'intercept': 0}
    pass