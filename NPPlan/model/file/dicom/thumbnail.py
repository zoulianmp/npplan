# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 22.10.12
@summary: 
'''

try:
    import NPPlan
except ImportError:
    pass
import vtk
import Queue
import multiprocessing
import os

# @todo: abstract dicom reader
# @todo: window/level

_window = 3139
_level = 545

def simpleFile(inputPath, outputPath, window, level):
    print inputPath, outputPath, window, level
    if os.path.exists(outputPath):
        os.remove(outputPath)
            # vtk read
    reader = vtk.vtkDICOMImageReader()
    reader.SetFileName(inputPath)

    # vtk set window/level
    im = vtk.vtkImageMapToWindowLevelColors()
    im.SetWindow(window)
    im.SetLevel(level)

    # apply w/l to img
    im.SetInput(reader.GetOutput())

    # resizer
    resizer = vtk.vtkImageResize()
    resizer.SetOutputDimensions(128, 128, -1)

    # resize
    resizer.SetInput(im.GetOutput())

    # writer
    writer = vtk.vtkPNGWriter()
    writer.SetFileName('%s' %(outputPath))

    # write
    writer.SetInput(resizer.GetOutput())
    writer.Write()


class singleFile(object):
    def __init__(self, iF, oD, oN='123', **kwargs):
        self.inputName = iF
        self.outputDir = oD
        self._outName = oN
        self.__dict__['window'] = kwargs.get('window', _window)
        self.__dict__['level'] = kwargs.get('level', _level)
        print iF, oD, oN, self.__dict__['window'], self.__dict__['level']
        self.worker()
        pass

    def getInputName(self):
        return self._input
    def setInputName(self, iF):
        self._input = iF
    inputName = property(getInputName, setInputName)

    def getOutputDir(self):
        return self._outputDir
    def setOutputDir(self, oD):
        self._outputDir = oD
    outputDir = property(getOutputDir, setOutputDir)

    def getOutputName(self):
        return self._outName
    outputName = property(getOutputName)

    def genOutFileName(self):
        return
        pass
        self._outName = '123'

    def worker(self):
        self.genOutFileName()

        # vtk read
        reader = vtk.vtkDICOMImageReader()
        reader.SetFileName(self._input)

        # vtk set window/level
        im = vtk.vtkImageMapToWindowLevelColors()
        im.SetWindow(self.__dict__['window'])
        im.SetLevel(self.__dict__['level'])

        # apply w/l to img
        im.SetInput(reader.GetOutput())

        # resizer
        resizer = vtk.vtkImageResize()
        resizer.SetOutputDimensions(128, 128, -1)

        # resize
        resizer.SetInput(im.GetOutput())

        # writer
        writer = vtk.vtkPNGWriter()
        writer.SetFileName('%s%s.png' %(self.outputDir, self.outputName))

        # write
        writer.SetInput(resizer.GetOutput())
        writer.Write()
        pass

class parseDirWorker(multiprocessing.Process):
    def __init__(self, workQueue, resultQueue, inputFile, outputFile, window=3133, level=590):
        # init multiprocessing
        multiprocessing.Process.__init__(self)

        # input data
        self.inputDir = inputFile
        self.outputDir = outputFile

        # queue managemenet
        self.workQueue = workQueue
        self.resultQueue = resultQueue

        # w/l management
        self._window = window
        self._level = level

    def run(self):
        try:
            job = self.workQueue.get_nowait()
        except Queue.Empty:
            return
            pass

        obj = singleFile(job['file'], self.outputDir, job['file'].split('/')[-1], window=self._window, level=self._level)
        self.resultQueue.put({'inp' : obj.inputName, 'out': obj.outputName, 'index' : job['index']})

        pass

def parseDir(inpDir, outDir, window=3133, level=590):
    l = 0
    if '\\' in inpDir:
        inpDir = inpDir.replace('\\', '/')
    if '\\' in outDir:
        outDir = outDir.replace('\\', '/')
    if '/' != inpDir[-1]:
        inpDir = inpDir + '/'
    if '/' != outDir[-1]:
        outDir = outDir + '/'
    workQueue = multiprocessing.Queue()
    for iF in os.listdir(inpDir):
        if '.' == iF[0]:
            continue

        workQueue.put({'file' :inpDir+iF, 'index' : l})
        l += 1

    resultQueue = multiprocessing.Queue()
    for i in range(l):
        worker = parseDirWorker(workQueue, resultQueue, '', outDir, window, level)
        worker.start()

    results = []
    for i in range(l):
        results.append(resultQueue.get())
    print results

    return results


if __name__ == '__main__':
    import sys
    print sys.argv

    iF = 'C:/Users/Chernukha/Pictures/DICOM/01111140/84269586'
    iD = 'C:/Temp/WorkDir/1.3.12.2.1107.5.1.4.43085.30000005112506212893700000422/1.3.12.2.1107.5.1.4.43085.30000005112506212893700000423/'
    oD = 'C:/Temp/WorkDir/1.3.12.2.1107.5.1.4.43085.30000005112506212893700000422/t1.3.12.2.1107.5.1.4.43085.30000005112506212893700000423/'

    #o = singleFile(iF, oD, '123')
    #print 'Input file: %s, \nOutput file: %s' %(o.inputName, o.outputDir+o.outputName)
    #print o.outputName
    parseDir(iD, oD)