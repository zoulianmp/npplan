# -*- coding: utf8 -*-
'''
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 27.06.2012
@summary: модуль работы с локальными файлами
'''
import os
import shutil
import NPPlan.controller.logger as log


def readFile(fileName, mode="r", **kwargs):
    pass


def appendFile(fileName, str, **kwargs):
    """
    
    """
    if (not os.path.exists(fileName)):
        file = open(fileName, 'w')
        file.write(str)
        file.close()
        return
    file = open(fileName, 'a')
    file.write(str)
    file.close()


def copyFile(source, destination, rewrite=True):
    # @todo: implement rewrite
    if not os.path.exists(source):
        return -1
    if rewrite and not os.path.isdir(destination) and os.path.exists(destination):
        os.remove(destination)
    shutil.copy(source, destination)
    log.log('model.file.local', 'File copy from %s to %s' % (source, destination))

def remove(filePath, backup=False):
    if backup:
        pass
    else:
        if os.path.exists(filePath):
            log.log('model.file.local', '%s deleted' % (filePath))
            os.remove(filePath)
