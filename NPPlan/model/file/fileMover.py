# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 18.10.13
@summary: 
'''


import os
import time
import shutil


class fileMoverBase(object):
    pass


class fileMoverAny(fileMoverBase):
    def __init__(self, sourceDir='', period=0, skipExtensions=[], skipFullNames=[], targetDir='',
                 createTargetDir=True, recursiveWalk=False):
        if not os.path.isdir(sourceDir):
            return
        lTime = time.localtime()
        for fName in os.listdir(sourceDir):
            if os.path.isdir(os.path.join(sourceDir, fName)):
                print 'dir found'
                continue
            cTime = os.path.getctime(os.path.join(sourceDir, fName))
            if (time.mktime(lTime) - cTime) > period:
                print 'Move candidate'
            else:
                print 'No moving'
            print 'fName', fName, 'date', cTime, time.localtime(cTime), os.path.splitext(fName)
        pass

if __name__ == '__main__':
    c = fileMoverAny(r'c:\\mcnpx\\executable\\',
                     skipExtensions=['exe', 'dll'],
                     skipFullNames=['xsdir', 'actia'],
                     period=3*24*60*60
    )