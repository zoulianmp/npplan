# -*- coding: utf8 -*-
"""
Модуль работы с конфигурационным файлом NPPlan.ini
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 27.06.2012
@summary: обработка конфигурационного файла
"""

import NPPlan
import ConfigParser

def readConfig(fileName, **kwargs):
    """
    Читает конфигурационный файл по пути fileName, возвращает dict из вложенных объектов,
    ключи верхнего уровня - секции, ключи нижнего уровня - ключи в секции, значения читаются как строки
    названия ключей приводятся к lowercase
    @summary: читает конфигурационный файл по пути fileName
    @param fileName: имя файла
    @type fileName: string || unicode
    """
    config = ConfigParser.ConfigParser()
    config.read(fileName)
    sections = config.sections()
    NPPlan.config.appConfig = {}
    dataStore = NPPlan.config.appConfig
    for i in sections:
        options = config.options(i)
        dataStore[i.lower()] = {}
        for j in options:
            dataStore[i.lower()][j.lower()] = config.get(i, j, True)
    pass