# -*- coding: utf8 -*-
"""
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 27.06.2012
@summary: package
"""

__version__ = '3.0.1a'

__all__ = [
    # main modules
    'config',
    'configReader',
    'dicom',
    'runner',
    'local',
]
  