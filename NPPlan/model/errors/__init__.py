# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 30.07.13
@summary: 
'''


#class NPPlanExceptionBase(Exception):
#    def __init__(self, value, meta=None):
#        self.value = value
#        self._meta = meta
#
#    def __str__(self):
#        if self._meta is not None:
#            return repr(self.value) + repr(self._meta)
#        return repr(self.value)
class NPPlanExceptionBase(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

from fatal import *