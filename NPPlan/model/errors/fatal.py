# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 30.07.13
@summary: 
'''

from __future__ import division, absolute_import

from NPPlan.model.errors import NPPlanExceptionBase


class NPPlanFatalError(NPPlanExceptionBase):
    pass


class NPPlanEnvironmentError(NPPlanExceptionBase):
    pass


class NPPlanStartError(NPPlanExceptionBase):
    pass