# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 11.12.12
@summary: 
'''

import NPPlan
import tempfile

_dir = None

def getDir():
    global _dir
    if _dir is None:
        createDir()
    return _dir

def createDir():
    global _dir
    _dir = tempfile.mkdtemp('', 'npplan_')