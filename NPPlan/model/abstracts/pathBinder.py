# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 08.11.12
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController
from hashlib import md5

USE_LOCAL  =   0b0000000001
USE_HTTP   =   0b0000000010
USE_SAMBA  =   0b0000001000
USE_FTP    =   0b0000010000
USE_GETTER =   0b0000100000

FORCE_REMOTE = 0b1000000000

import os

class path(object):
    def __init__(self, localPath='', remotePath='', synchronizeFlag=USE_LOCAL|USE_SAMBA, type=''):
        # @todo: implement synchronize
        pass
        if '' == remotePath and '' != localPath:
            self.__dict__['localPath'] = localPath
            # @todo: implement flags
            #self.__dict__['remotePath'] = remotePathClass('plan://get/%s' %(localPath, ))
        elif '' == localPath and '' != remotePath:
            self.__dict__['remotePath'] = remotePathClass(remotePath)
            if FORCE_REMOTE & synchronizeFlag:
                self.__dict__['localPath'] = self.__dict__['remotePath'].getLocalAndStore()
            else:
                self.__dict__['localPath'] = None
        else:
            self.__dict__['localPath'] = localPath
            self.__dict__['remotePath'] = remotePathClass(remotePath)
        self.__dict__['type'] = type

    def getPath(self):
        if self.__dict__['localPath'] is not None:
            return self.__dict__['localPath']
        else:
            return self.__dict__['remotePath'].getLocal()

    path = property(getPath)

    def listDir(self):
        if self.__dict__['localPath'] is None:
            # retrieve
            pass
        else:
            if os.path.isdir(self.__dict__['localPath']):
                return os.listdir(self.__dict__['localPath'])
            else:
                return self.__dict__['localPath']

    def __repr__(self):
        return self.path

class remoteRetrieval(baseController):
    def __init__(self, pathClassInstance):
        """
        @param pathClassInstance: инстан класса remotePathClass
        @type pathClassInstance: remotePathClass
        """
        self.path = pathClassInstance

    def pathHash(self, path):
        m = md5()
        m.update(path)
        return m.hex_digest()

    def retrieve(self, path, type='plan'):
        self.handler.pbCallbacker.controller.doCall(
            'getServerPath',
            {'type' : type, 'path' : path},
            self.path.callback,
            'retrieval_%s' %(self.pathHash(path))
        )

class remotePathClass(object):
    def __init__(self, remote):
        self.retrieval = remoteRetrieval(self)
        self.parse(remote)

    def callback(self, data):
        self.data = data

    def parse(self, remote):
        if remote.startswith('plan://'):
            path = remote[7:]
            self.retrieval.retrieve(path)
        pass

    def getLocalAndStore(self):
        pass

    def getLocal(self):
        pass
