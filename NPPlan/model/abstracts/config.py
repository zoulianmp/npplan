# -*- coding: utf8 -*-
"""

@author: mrxak_home
@copyright: MRRC Obninsk 2012
@version: 1
@date: 16.11.12
@summary:
"""

import NPPlan


class configObject(object):
    def __init__(self, key, value):
        setattr(self, key, value)

    def __getattr__(self, item):
        try:
            return self.__dict__[item]
        except KeyError:
            return None
    def __setattr__(self, key, value):
        if isinstance(value, dict):
            self.__dict__[key] = configObject('_config', self)
            for i in value:
                setattr(self.__dict__[key], i, value[i])
        else:
            self.__dict__[key] = value
    def __getitem__(self, item):
        return self.__dict__[item]
    def __setitem__(self, key, value):
        if isinstance(value, dict):
            self.__dict__[key] = configObject('_config', self)
            for i in value:
                setattr(self.__dict__[key], i, value[i])
        else:
            self.__dict__[key] = value

    def __repr__(self):
        return str(self.__dict__)

    def __contains__(self, item):
        return item in self.__dict__


class config(object):
    def __init__(self, name=''):
        if '' != name:
            self.__dict__['_name'] = name
        self.__dict__['_storage'] = configObject('_config', self)

    def __getattr__(self, item):
        try:
            return getattr(self.__dict__['_storage'], item)
        except KeyError:
            setattr(self.__dict__['_storage'], item, {})
            return getattr(self.__dict__['_storage'], item)
    def __setattr__(self, key, value):
        setattr(self.__dict__['_storage'], key, value)
    def __repr__(self):
        return str(self.__dict__['_storage'])

NPPlan.config = config()

if '__main__' == __name__:
    conf = config('test')
    conf.a = 'aaa'
    conf.b = {'qqq' : 'ppp', 'qqr' : 'ppd'}
    conf.c.a = 'lll'
    conf.c.b = 'er'
    print conf.a
    print conf.b
    print conf.b.__dict__
    print conf.b.qqq
    print conf.b.qqr
    print conf.c
    print conf.c.a
    print conf.c.b
    #print conf.b.qqq
    #print conf.b.qqr