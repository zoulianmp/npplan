# -*- coding: utf8 -*-
'''
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 30.06.2012
@summary: package
'''

import unittest
from NPPlan.model.abstracts.config import config as configManager


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self._config = configManager()

    def test_simple(self):
        self._config.a = 'test'
        self.assertEqual(self._config.a, 'test')

    def test_wide(self):
        self._config.a.a = '123'
        self._config.a.b = '456'
        self.assertEqual(self._config.a.a, '123')
        self.assertEqual(self._config.a.b, '456')
        self.assertIn('a', self._config.a)
        self.assertIn('b', self._config.a)
        self.assertDictContainsSubset({'a' : '123', 'b' : '456'}, self._config.a)

    def test_dictFill(self):
        self._config.a = {'t1' : 'p', 't2' : 'rrr', 'v' : 845}
        self.assertEqual(self._config.a.t1, 'p')
        self.assertEqual(self._config.a.t2, 'rrr')
        self.assertEqual(self._config.a.v, 845)
        self.assertIn('t1', self._config.a)
        self.assertNotIn('t3', self._config.a)

    def test_dictFill2(self):
        self._config.a = { 't1' : 'p', 't2' : {'i1' : 'ii', 'i3' : 'iii'}}
        self.assertEqual(self._config.a.t2.i1, 'ii')
        self.assertIn('i1', self._config.a.t2)

if __name__ == '__main__':
    unittest.main()
