# -*- coding: utf8 -*-
"""

@author: mrxak_home
@copyright: MRRC Obninsk 2012
@version: 1
@date: 16.11.12
@summary:
"""

import NPPlan
import Queue
import multiprocessing

taskQueue = multiprocessing.Queue()
tasksQuantity = 0
resultQueue = multiprocessing.Queue()
def init():
    global taskQueue, resultQueue

def addTask(task, type):
    global taskQueue, tasksQuantity
    taskQueue.put({
        'task' : task,
        'type' : type,
        'planInfo' : NPPlan.getProgramData()
    })
    tasksQuantity += 1

class forkerWorker(multiprocessing.Process):
    def __init__(self, workQueue, resultQueue):
        multiprocessing.Process.__init__(self)

        # queue managemenet
        self.workQueue = workQueue
        self.resultQueue = resultQueue

    def run(self):
        try:
            job = self.workQueue.get_nowait()
        except Queue.Empty:
            return
            pass

        obj = job()
        self.resultQueue.put({'inp' : obj.inputName, 'out': obj.outputName, 'index' : job['index']})

def starter(taskQueue, resultQueue):
    for task in iter(taskQueue.get, 'STOP'):
        print task
        func = task['task'](task['planInfo'])
        resultQueue.put(func)

def start():
    global taskQueue, tasksQuantity, resultQueue
    for i in range(tasksQuantity):
        print 'task i %d' %i
        #p = Process(target=starter, args=(taskQueue, resultQueue))
        p = forkerWorker(taskQueue, resultQueue)
        p.start()

    for i in range(tasksQuantity):
        print '\t', resultQueue.get().query

    for i in range(tasksQuantity):
        taskQueue.put('STOP')
        print "Stopping Process #%s" % i