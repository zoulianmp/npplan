# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 26.10.12
@summary: 
'''

import NPPlan
import NPPlan.controller.logger as log
from mongokit import Connection
from pymongo.errors import AutoReconnect, ConnectionFailure

try:
    NPPlan.mongoConnection = Connection(
        host=NPPlan.config.appConfig.database.host,
        port=int(NPPlan.config.appConfig.database.port)
    )
    log.log('model.mongo.connector', 'Connected to database at %s:%d' % (
        NPPlan.config.appConfig.database.host,
        int(NPPlan.config.appConfig.database.port))
    )
    log.log('model.mongo.connector', NPPlan.mongoConnection.__dict__, 10)
except AutoReconnect:
    NPPlan.mongoConnection = None
    log.log('model.mongo.connector', 'Failed to connect to database at %s: %d' % (
        NPPlan.config.appConfig.database.host,
        int(NPPlan.config.appConfig.database.port))
    )
except ConnectionFailure:
    NPPlan.mongoConnection = None
    log.log('model.mongo.connector', 'Failed to connect to database at %s: %d' % (
        NPPlan.config.appConfig.database.host,
        int(NPPlan.config.appConfig.database.port))
    )

#print NPPlan.mongoConnection