# -*- coding: utf8 -*-
import NPPlan
import unittest
import json


class dbAIsotope(unittest.TestCase):
    def setUp(self):
        NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)
        matFile = open(NPPlan.config.appPath + '/stuff/mat.json', 'r')

        NPPlan.mongoConnection['npplan3']['isotopes'].remove()

        self._u = json.load(matFile, )
        print 'test'

    def test_something(self):
        self.assertEqual(len(self._u), 103)
        for i in self._u:
            isotope = NPPlan.mongoConnection.isotope()
            isotope.name = self._u[i][u'Name']
            isotope.symbol = i
            isotope.atomicMass = self._u[i][u'Atomic mass']
            isotope.atomicNum = self._u[i][u'Atomic no']
            isotope.pid = '0'
            isotope.save()

        D = NPPlan.mongoConnection.isotope()
        D.name = 'Deuterium'
        D.rusName = u'Дейтерий'
        D.symbol = 'D'
        D.pid = 'H'
        D.atomicNum = 1
        D.atomicMass = 2.0141017778
        D.save()

        T = NPPlan.mongoConnection.isotope()
        T.name = 'Tritium'
        T.rusName = u'Тритий'
        T.symbol = 'T'
        T.pid = 'H'
        T.atomicNum = 1
        T.atomicMass = 3.0160492777
        T.save()

        self.assertEqual(NPPlan.mongoConnection.isotope.find().count(), 105)
        pass


class dbBMaterial(unittest.TestCase):
    def setUp(self):
        NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)

        H = NPPlan.mongoConnection.isotope.find_one({'symbol': 'H'})
        N = NPPlan.mongoConnection.isotope.find_one({'symbol': 'N'})
        O = NPPlan.mongoConnection.isotope.find_one({'symbol': 'O'})
        C = NPPlan.mongoConnection.isotope.find_one({'symbol': 'C'})
        Cl = NPPlan.mongoConnection.isotope.find_one({'symbol': 'Cl'})
        S = NPPlan.mongoConnection.isotope.find_one({'symbol': 'S'})
        Na = NPPlan.mongoConnection.isotope.find_one({'symbol': 'Na'})
        Ar = NPPlan.mongoConnection.isotope.find_one({'symbol': 'Ar'})
        Mg = NPPlan.mongoConnection.isotope.find_one({'symbol': 'Mg'})
        P = NPPlan.mongoConnection.isotope.find_one({'symbol': 'P'})
        Ca = NPPlan.mongoConnection.isotope.find_one({'symbol': 'Ca'})
        Fe = NPPlan.mongoConnection.isotope.find_one({'symbol': 'Fe'})
        K = NPPlan.mongoConnection.isotope.find_one({'symbol': 'K'})

        #NPPlan.mongoConnection.material.delete()
        # print NPPlan.mongoConnection.material
        NPPlan.mongoConnection['npplan3']['materials'].remove()

        air = NPPlan.mongoConnection.material()
        air.name = 'Air'
        air.rusName = u'Воздух (общий)'
        air.density = 1.290
        air.densityUnits = 1
        cmap = []
        cmap.append((N, 0.7))
        cmap.append((O, 0.3))
        air.cmap = cmap
        air.save()

        air2 = NPPlan.mongoConnection.material()
        air2.name = 'AirReal'
        air2.rusName = u'Воздух (реальный)'
        air2.density = 1.205E-03
        air2.densityUnits = 0
        cmap = []
        cmap.append((C, 0.000124))
        cmap.append((N, 0.755268))
        cmap.append((O, 0.231781))
        cmap.append((Ar, 0.012827))
        air2.cmap = cmap
        air2.save()

        adiposeTissue = NPPlan.mongoConnection.material()
        adiposeTissue.name = 'AdiposeTissue'
        adiposeTissue.rusName = u'Жировая ткань'
        adiposeTissue.density = 0.967
        adiposeTissue.densityUnits = 0
        cmap = []
        cmap.append((H, 0.114))
        cmap.append((C, 0.598))
        cmap.append((N, 0.007))
        cmap.append((O, 0.278))
        cmap.append((Na, 0.001))
        cmap.append((S, 0.001))
        cmap.append((Cl, 0.001))
        adiposeTissue.cmap = cmap
        adiposeTissue.save()

        water = NPPlan.mongoConnection.material()
        water.name = 'Water'
        water.rusName = u'Вода'
        water.density = 1.0
        cmap = []
        cmap.append((H, 0.112))
        cmap.append((O, 0.888))
        water.cmap = cmap
        water.save()

        water2 = NPPlan.mongoConnection.material()
        water2.name = 'WaterTwo'
        water2.rusName = u'Вода'
        water2.density = 1.0
        cmap = []
        cmap.append((H, 0.143716))
        cmap.append((O, 0.856284))
        water2.cmap = cmap
        water2.save()

        bone = NPPlan.mongoConnection.material()
        bone.name = 'BoneGeneral'
        bone.rusName = u'Кость (обобщённая)'
        bone.density = 1.92

        cmap = []
        cmap.append((H, 0.034000))
        cmap.append((C, 0.155000))
        cmap.append((N, 0.042000))
        cmap.append((O, 0.435000))
        cmap.append((Na, 0.001000))
        cmap.append((Mg, 0.002000))
        cmap.append((P, 0.103000))
        cmap.append((S, 0.003000))
        cmap.append((Ca, 0.225000))
        bone.cmap = cmap
        bone.save()

        trBone = NPPlan.mongoConnection.material()
        trBone.name = 'TrabecularBone'
        trBone.rusName = u'Трубчатая кость'
        trBone.density = 1.159
        cmap = []
        cmap.append((H, 0.085))
        cmap.append((C, 0.404))
        cmap.append((N, 0.058))
        cmap.append((O, 0.367))
        cmap.append((Na, 0.001))
        cmap.append((Mg, 0.001))
        cmap.append((P, 0.034))
        cmap.append((S, 0.002))
        cmap.append((Cl, 0.002))
        cmap.append((K, 0.001))
        cmap.append((Ca, 0.044))
        cmap.append((Fe, 0.001))
        trBone.cmap = cmap
        trBone.save()

        dBone = NPPlan.mongoConnection.material()
        dBone.name = 'DenseBone'
        dBone.rusName = u'Плотная кость'
        dBone.density = 1.575
        cmap = []
        cmap.append((H, 0.056))
        cmap.append((C, 0.235))
        cmap.append((N, 0.050))
        cmap.append((O, 0.434))
        cmap.append((Na, 0.001))
        cmap.append((Mg, 0.001))
        cmap.append((P, 0.072))
        cmap.append((S, 0.003))
        cmap.append((Cl, 0.001))
        cmap.append((K, 0.001))
        cmap.append((Ca, 0.146))
        dBone.cmap = cmap
        dBone.save()

        tissue = NPPlan.mongoConnection.material()
        tissue.name = 'TissueGeneral'
        tissue.rusName = u'Ткань (обобщённая, ICRU-88)'
        tissue.density = 1.06
        cmap = []
        cmap.append((H, 0.102))
        cmap.append((C, 0.143000))
        cmap.append((N, 0.034000))
        cmap.append((O, 0.708000))
        cmap.append((Na, 0.002000))
        cmap.append((P, 0.003000))
        cmap.append((S, 0.003000))
        cmap.append((Cl, 0.002000))
        cmap.append((K, 0.003000))
        tissue.cmap = cmap
        tissue.save()
        pass

    def test_something(self):
        for i in NPPlan.mongoConnection.material.find():
            print i
        self.assertEqual(True, True)


class dbCConversion(unittest.TestCase):
    def setUp(self):
        NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)

        NPPlan.mongoConnection['npplan3']['ctdens'].remove()

        v = NPPlan.mongoConnection.ct2density()
        v.ctValue = -5000
        v.density = 0.0
        v.save()

        v = NPPlan.mongoConnection.ct2density()
        v.ctValue = -1000
        v.density = 0.0
        v.save()

        v = NPPlan.mongoConnection.ct2density()
        v.ctValue = -400
        v.density = 0.602
        v.save()

        v = NPPlan.mongoConnection.ct2density()
        v.ctValue = -150
        v.density = 0.924
        v.save()

        v = NPPlan.mongoConnection.ct2density()
        v.ctValue = 100
        v.density = 1.075
        v.save()

        v = NPPlan.mongoConnection.ct2density()
        v.ctValue = 300
        v.density = 1.145
        v.save()

        v = NPPlan.mongoConnection.ct2density()
        v.ctValue = 2000
        v.density = 1.856
        v.save()

        v = NPPlan.mongoConnection.ct2density()
        v.ctValue = 4927
        v.density = 3.379
        v.save()

    def test_something(self):
        self.assertEqual(NPPlan.mongoConnection.ct2density.find().count(), 8)
        for i in NPPlan.mongoConnection.ct2density.find():
            print i
        self.assertEqual(True, True)


class dbFacilities(unittest.TestCase):
    def setUp(self):
        NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)

        NPPlan.mongoConnection['npplan3']['facilities'].remove()

        v = NPPlan.mongoConnection.facility()
        v.name = 'FastNeutron'
        v.rusName = u'Быстрые нейтроны'
        v.level = 0
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'EpithermalNeutron'
        v.rusName = u'Медленные и эпитепловые нейтроны'
        v.level = 0
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'Protons'
        v.rusName = u'Протоны'
        v.level = 0
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'Gamma'
        v.rusName = u'Гамма'
        v.level = 0
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'HeavyIons'
        v.rusName = u'Тяжёлые ионы'
        v.level = 0
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'NG-24'
        v.rusName = u'НГ-24'
        v.description = u'Нейтронный генератор на газонаполненной нейтронной трубке НГ-24'
        v.pName = 'FastNeutron'
        v.level = 1
        v.mcnpData.template = u'ng24'
        v.data.mainParticleType = 'n'
        v.data.mainParticleEnergy = [14, 14]
        v.data.extraParticlesData = {
            'fluence': 1.4e11,
            'energyDim': 'MeV',
            'markRadius': 0.75,
        }
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'NG-14'
        v.rusName = u'НГ-14'
        v.description = u'Нейтронный генератор на газонаполненной нейтронной трубке НГ-14\n' \
                        u'Поток нейтронов: 2·10^10 н/с\n' \
                        u'Режим излучения: непрерывный\n' \
                        u'Энергия нейтронов: 14 МэВ'
        v.pName = 'FastNeutron'
        v.level = 1
        v.mcnpData.template = u'ng14'
        v.data.mainParticleType = 'n'
        v.data.mainParticleEnergy = [14, 14]
        v.data.extraParticlesData = {
            'fluence': 2e10,
            'energyDim': 'MeV',
            'markRadius': 1.5,
        }
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'NG-14L'
        v.rusName = u'НГ-14 (режим 2)'
        v.description = u'Нейтронный генератор на газонаполненной нейтронной трубке НГ-14'
        v.pName = 'EpithermalNeutron'
        v.level = 1
        v.mcnpData.template = u'ng14'
        v.data.mainParticleType = 'n'
        v.data.mainParticleEnergy = [2.5, 2.5]
        v.data.extraParticlesData = {
            'fluence': 2e10,
            'energyDim': 'MeV',
            'markRadius': 1.5,
        }
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'BR10'
        v.rusName = u'БР10'
        v.description = u'Реактор БР-10 с фильтрами'
        v.pName = 'EpithermalNeutron'
        v.level = 1
        v.data.mainParticleType = 'n'
        v.data.mainParticleEnergy = [0.5, 1.5]
        v.data.extraParticlesData = {
            'energyDim': 'MeV',
        }
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'Balakin'
        v.rusName = u'Синхротрон Балакина'
        v.description = u'Синхротрон Балакина'
        v.pName = 'Protons'
        v.level = 1
        v.data.mainParticleType = 'p'
        v.data.mainParticleEnergy = [70, 250]
        v.data.extraParticlesData = {
            'energyDim': 'MeV',
        }
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'Gamma'
        v.rusName = u'Обобщённый гамма-источник'
        v.description = u'Обобщённый гамма-источник'
        v.pName = 'Gamma'
        v.level = 1
        v.data.mainParticleType = 'g'
        #v.data.mainParticleEnergy = [0.5, 1.5]
        #v.data.extraParticlesData = {
        #    'energyDim': 'MeV',
        #}
        v.save()

        v = NPPlan.mongoConnection.facility()
        v.name = 'IonSource'
        v.rusName = u'Ионный источник (ИФВЭ)'
        v.description = u'Ионный источник Института Физикик Высоких Энергий'
        v.pName = 'HeavyIons'
        v.level = 1
        v.data.mainParticleType = 'i'
        v.data.mainParticleEnergy = [417, 417]
        v.data.extraParticlesData = {
            'energyDim': 'MeV/nucleon',
            'fieldSize': 5,
            'fieldDim': 'cm',
            'ionType': 'C12'
        }
        v.save()

    def test_something(self):
        self.assertEqual(NPPlan.mongoConnection.facility.find().count(), 12)
        for i in NPPlan.mongoConnection.facility.find():
            print i




if __name__ == '__main__':
    suite = unittest.TestSuite()
    #suite.addTest(unittest.makeSuite(dbIsotope))
    #suite.addTest(unittest.makeSuite(dbMaterial))
    #suite.addTest(unittest.makeSuite(dbConversion))
    #suite.addTests([unittest.makeSuite(dbAIsotope), unittest.makeSuite(dbBMaterial), unittest.makeSuite(dbCConversion)])
    suite.addTest(unittest.makeSuite(dbFacilities))
    unittest.main()

