# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 11.03.14
@summary: 
'''

import NPPlan
from mongokit import Document, Connection
import datetime

@NPPlan.mongoConnection.register
class userRoles(Document):
    use_dot_notation = True
    __collection__ = 'roles'
    __database__ = 'npplan3'
    structure = {
        'name': {
            'latinName': unicode,
            'originalName': unicode,
        },
        'dateCreated': datetime.datetime,
        'dateLastModified': datetime.datetime,
        'rank': int,
        'grant': list,
        'top': unicode,
    }
    default_values = {
        'rank': 0,
        'dateCreated': datetime.datetime.utcnow,
    }

def setup():
    pass