# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 11.03.14
@summary: 
'''

__all__ = [
    'roles',
    'privileges'
]

from roles import *
from privileges import *