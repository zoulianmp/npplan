# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.06.13
@summary: 
'''

import NPPlan
from mongokit import Document, Connection

# @todo: IMPORTANT (!!!) CMAP по количеству атомов или по весу (вес - для geant, -1*вес - для mcnp), handle

@NPPlan.mongoConnection.register
class material(Document):
    use_dot_notation = True
    __collection__ = 'materials'
    __database__ = 'npplan3'
    structure = {
        'name': basestring,
        'rusName': unicode,
        'density': float,
        'densityUnits': int,
        'cmap': list,
        'geantSimData': dict,
        'mcnpSimData': dict,
        'tomoData': {
            'ctLowValue': float,
            'ctHighValue': float,
            'contourIntValue': int,
        },
    }
    default_values = {
        'densityUnits': 0,
    }