# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 10.06.13
@summary: 
'''

import NPPlan
from mongokit import Document, Connection
import datetime

@NPPlan.mongoConnection.register
class organs(Document):
    use_dot_notation = True
    __collection__ = 'contours'
    __database__ = 'npplan3'
    structure = {
        'name': {
            'latinName': unicode,
            'originalName': unicode,
        },
        'uid': basestring,
        'pid': basestring,
        'dateCreated': datetime.datetime,
        'dateLastModified': datetime.datetime,
        'active': int,
        'allowed': int,
        'contourData': {
            'color': list,
            'opacity': float,

        },
        'simulationData': {
            'density': float,
            'cmap': dict,
            'mcnpMaterial': int,
            'mcnpLibrary': str,
            'geantMaterial': int,
            'geantLibrary': str,
            'wlMap': dict,
        },
        'rank': int,
    }
    default_values = {
        'rank': 0,
        'dateCreated': datetime.datetime.utcnow,
        'contourData.color': [1.0, 0.0, 0.0],
        'contourData.opacity': 1.0,
    }