# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 01.07.13
@summary: 
'''

import NPPlan
from mongokit import Document

@NPPlan.mongoConnection.register
class ct2density(Document):
    use_dot_notation = True
    __collection__ = 'ctdens'
    __database__ = 'npplan3'
    structure = {
        'ctValue': int,
        'density': float,
    }

# class mri2density(Document):
# class mri2ct(Document):
