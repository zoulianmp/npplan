# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 13.05.13
@summary: 
'''

import NPPlan
from mongokit import Document, Connection
import datetime

@NPPlan.mongoConnection.register
class patient(Document):
    use_dot_notation = True
    __collection__ = 'patients'
    __database__ = 'npplan3'
    structure = {
        'name': {
            'latinName': unicode,
            'originalName': unicode,
        },
        'date_creation': datetime.datetime,
        'rank': int,
    }
    #required_fields = ['name']
    default_values = {
        'rank': 0,
        'date_creation': datetime.datetime.utcnow
    }

#NPPlan.patientDbModel = patient
#NPPlan.mongoConnection.register([patient])