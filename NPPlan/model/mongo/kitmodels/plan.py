# -*- coding: utf8 -*-
'''
Задаёт базовые модели для хранения данных посредством mongokit,
определяет поля для плана пациента в системе планирования
@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 2
@date: 16.05.13
@summary: 
'''

import NPPlan
from mongokit import Document, Connection
import datetime
import uuid


class planCore(Document):
    use_dot_notation = True
    __collection__ = 'plan'
    __database__ = 'npplan3'
    structure = {
        'planUID': uuid.UUID,
        'created': datetime.datetime,
        'edited': datetime.datetime,
        'source': basestring,
        'status': int,
    }
    required_fields = ['planUID']


class planBase(planCore):
    structure = {                           # DICOM ID
        'seriesUID': basestring,            # 0x0020 0x0003
        'dicomImageType': dict,             # 0x0008 0x0008
        'dates': {
            'study': basestring,            # 0x0008 0x0020
            'series': basestring,           # 0x0008 0x0021
        },
        'times': {
            'study': basestring,            # 0x0008 0x0030
            'series': basestring,           # 0x0008 0x0031
        },
        'description': {
            'study': basestring,            # 0x0008 0x1030
            'series': basestring,           # 0x0008 0x103e
            'plan': basestring,
        },
        'creator': {
            'institution': basestring,
            'institutionInfo': basestring,
            'manufacturer': basestring,
            'station': basestring,
            'rphysician': basestring,
            'pphysician': basestring,
            'operator': basestring,
            'device': basestring,
            'private': basestring,
        },
        'sliceThickness': float,            # 0x0018 0x0050
        'bodyPart': basestring,             # 0x0018 0x0015
        'samplesPerPixel': int,             # 0x0028 0x0002
        'pixelSpacing': {
            'x': float,
            'y': float,
        },
        'bitsData': {
            'allocated': int,
            'stored': int,
            'high': int,
        },
        'rescale': {
            'slope': int,
            'intercept': int,
        },
        'seriesLength': int,
    }
    #required_fields = ['seriesUID']


class planContourData(planCore):
    structure = {
        'contours': dict
    }


class sliceDbElement(object):
    structure = {
        'id': str,
        'location': float,
        'number': int,
        'filename': str,
        'thumbnail': str,
    }


class planSliceData(planCore):
    structure = {
        #'locationToUid': dict,
        #'numberToUid': dict,
        #'fileNameToUid': dict,
        'slices': dict,
        'image': {
            'rows': int,
            'columns': int,
        },
        'path': {
            'useLocal': bool,
            'local': str,
            'network': str,
            'plan': str,
        },
        'thumbnails': {
            'localFolderPath': str,
            'extension': str,
        }
    }


class planPositionData(planCore):
    structure = {
        'facility': {
            'name': basestring,
            'colmat': basestring,
            'colmatShape': basestring,
            'colmatSize': basestring,
        },
        'beams': dict,
    }


class planSimulationData(planCore):
    structure = {
        'simulationSpecificParams': dict,
        'simulationType': basestring,
        'simulationMethod': basestring,
        'simulationPath': {
            'local': str,
            'network': str,
            'plan': str,
        },
        'completed': bool,
        'completedDate': datetime.datetime
    }


class planResultsData(planCore):
    structure = {
        'isolinesPath': {
            'local': str,
            'network': str,
            'plan': str,
        }
    }


@NPPlan.mongoConnection.register
class systemPlan(planBase, planContourData, planSliceData, planPositionData, planSimulationData):
    __collection__ = 'plan'
    __database__ = 'npplan3'
    use_dot_notation = True
    pass

