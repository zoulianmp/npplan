# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 17.09.13
@summary: 
'''

import NPPlan
from mongokit import Document, Connection
import datetime

@NPPlan.mongoConnection.register
class facility(Document):
    use_dot_notation = True
    __collection__ = 'facilities'
    __database__ = 'npplan3'
    structure = {
        'name': basestring,
        'rusName': unicode,
        'description': unicode,
        'pName': basestring,
        'level': int,
        'data': {
            'mainParticleType': basestring,
            'mainParticleEnergy': list,
            'extraParticlesData': dict,
        },
        'mcnpData': {
            'template': unicode,
            'data': dict,
        },
        'geantData': {
            'template': unicode,
            'data': dict,
        },
    }
    default_values = {
        'rusName': u'',
        'level': 0,
    }