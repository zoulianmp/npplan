# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 08.11.12
@summary: 
'''

import NPPlan
import NPPlan.model.abstracts.pathBinder as pathBinder


class slice(object):
    def __init__(self, parent, path, tags=[]):
        self.__dict__['parent'] = parent
        self.__dict__['path'] = pathBinder.path(localPath=path, synchronizeFlag=pathBinder.USE_LOCAL|pathBinder.FORCE_REMOTE)

    def getPath(self):
        return self.__dict__['path'].path

    path = property(getPath)

    def getContourData(self):
        return self.__dict__['contour']
    def setContourData(self, contour):
        self.__dict__['contour'] = contour
    contour = property(getContourData, setContourData)

    def getUniqueKey(self):
        # @todo: implement
        return '123'
    key = property(getUniqueKey)

class patientDataItem(object):
    def __init__(self, name, value='', type='text'):
        self.__dict__['name'] = name
        self.__dict__['value'] = value
        self.__dict__['type'] = type

    def __getattr__(self, item):
        try:
            return self.__dict__[item]
        except KeyError:
            return None
            pass

    def __setattr__(self, key, value):
        self.__dict__[key] = value

    def getItem(self, key):
        return self.__dict__['items'][key]

class patientData(object):
    def __init__(self, name):
        self.__dict__['_name'] = name
        self.__dict__['items'] = {}
        self.getKeys()
        pass

    def getKeys(self):
        from NPPlan.model.file.configReader.addPatientForm import readConfig, getData
        readConfig(self.__dict__['_name'])
        data = getData(self.__dict__['_name']).copy()
        data.pop('captions')
        for panel in data:
            for i in data[panel]:
                self.__dict__['items'][i['name']] = patientDataItem(i['name'], '', i['type'])

    def getItem(self, key):
        return self.__dict__['items'][key]

    def __getattr__(self, item):
        return self.__dict__['items'][item].value
    def __setattr__(self, key, value):
        self.__dict__['items'][key].value = value

    def toDbObject(self):
        ret = {}
        for i in self.__dict__['items']:
            ret[i] = self.__dict__['items'][i].value
        return ret

class patient(object):
    def __init__(self, data='', slices=[], ):
        #self.__dict__['slices'] = {}
        #for i in slices:
        #    cSlice = slice(self, i)
        #    #print cSlice.key
        #    self.__dict__['slices'][cSlice.key] = cSlice
        self.__dict__['personal'] = patientData('addPatient.personal')

    def getPersonal(self):
        """
        @return: класс персональных данных
        @rtype: patientData
        """
        return self.__dict__['personal']
    personal = property(getPersonal, doc="@see: getPersonal")

    def toDbObject(self):
        return {
            'personal' : self.personal.toDbObject(),
        }

if __name__ == '__main__':
    # test
    NPPlan.init(programPath="c:/NPPlan3/", test=True, log=True)
    pat = patient('123', ['123', '234', '345'])
    pat.personal.surname = "abs"
    print pat.personal.surname
    from NPPlan.model.mongo.connector import connection

    #db = connection('127.0.0.1', 27017, 'NPPlan3test')

    #patDb = db.testPatient
    #patDb.insert(pat.toDbObject())

    #print patDb.find()
    #for i in patDb.find():
    #    print i

    print pat
    print pat.toDbObject()
