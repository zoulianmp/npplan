
from NPPlan.lib.fTail import tail
import unittest


class MyTestCase(unittest.TestCase):
    def test_singleLineSmall(self):
        ff = file(r'c:\mcnpx\executable\tt8')
        self.assertEqual(tail(ff, 1)[0], 'ctme 1')

    def test_singleLineBig(self):
        ll = ' mcnp     version 5     11042003                     02/08/13 12:20:51                     probid =  02/08/13 11:55:01 '
        ff = file(r'C:\MCNP5\LIST\LISTtstrg') # 11Mb
        self.assertEqual(tail(ff, 1)[0], ll)
        pass

    def test_4linesLine900Mb(self):
        ll = ' computer time = 4171.89 minutes\n'
        ff = file(r'c:\mcnpx\executable\pot40co')
        q = tail(ff, 4)
        self.assertEqual(q[1], ll)


if __name__ == '__main__':
    unittest.main()
