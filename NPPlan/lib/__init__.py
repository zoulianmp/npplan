# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 14.05.13
@summary: 
'''

from dotArray import *
from colors import *
#from melkmanSort import *
from cSingleton import *

__all__ = [
    'dotArray',
    'colors',
    #'melkmanSort',
]