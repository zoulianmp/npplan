# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 07.06.13
@summary: 
'''

import NPPlan
#from NPPlan.controller.client.mainC.stages.contouring.vtkHandler import contourPoint


'''
// isLeft(): tests if a point is Left|On|Right of an infinite line.
//    Input:  three points P0, P1, and P2
//    Return: >0 for P2 left of the line through P0 and P1
//            =0 for P2 on the line
//            <0 for P2 right of the line
isLeft( coord *p0, coord *p1, coord *p2 ) {
    return (p1->x - p0->x)*(p2->y - p0->y) - (p2->x - p0->x)*(p1->y - p0->y);
}'''


def isLeft(p0, p1, p2):
    """
    @param p0:
    @type p0: contourPoint
    @param p1:
    @type p1: contourPoint
    @param p2:
    @type p2: contourPoint
    @return:
    """
    return (p1.getWCoord()[0] - p0.getWCoord()[0]) * (p2.getWCoord()[1] - p0.getWCoord()[1]) - \
           (p2.getWCoord()[0] - p0.getWCoord()[0]) * (p1.getWCoord()[1] - p0.getWCoord()[1])
    pass

'''
Алгоритм начинает с пустого дека D.
Дек - список с операциями:
push v - положить элемент на верх дека,     list.insert(0, x)
insert v - положить элемент под низ дека,   list.append(x)
pop - убрать верхний элемент дека,          list.pop(0)
remove - удалить нижний элемент дека.       list.pop()





  Псевдокод.



Шаг 1:
          Ввод v1, v2, v3;
          if (v1,v2,v3)>0
            then {push v1; push v2;}
              else {push v2; push v1;}
          push v3; insert v3;

Шаг 2:

          Ввод v;
          until ((v,db,db+1)<0 or (dt-1,dt,c)<0) do { ввод v };

Шаг 3:

          until ((dt-1,dt,v)>0) do pop;
          push v;

Шаг 4:

          until ((v,db,db+1)>0) do remove;
          insert v;
          Идти на Шаг 2.
'''

def melkmanSortIterator(cPoints):
    deq = []
    v1 = cPoints[cPoints.keys()[0]]
    v2 = cPoints[cPoints.keys()[1]]
    v3 = cPoints[cPoints.keys()[2]]
    if isLeft(v1, v2, v3):
        deq.insert(0, {v1.getId(): v1})
        deq.insert(0, {v2.getId(): v2})
    else:
        deq.insert(0, {v2.getId(): v2})
        deq.insert(0, {v1.getId(): v1})
    deq.insert(0, {v3.getId(): v3})
    deq.append({v3.getId(): v3})
    for i in range(3, len(cPoints)):
        v = cPoints[cPoints.keys()[i]]

    for i in deq:
        yield i

if __name__ == '__main__':
    NPPlan.init(programPath="c:/NPPlan3/", test=True, log=False, utest=True)

    a1 = contourPoint(None, worldCoordinates=(1, 0, 0), id=0)
    a2 = contourPoint(None, worldCoordinates=(1, 1, 0), id=1)
    a3 = contourPoint(None, worldCoordinates=(1, -1, 0), id=2)
    a4 = contourPoint(None, worldCoordinates=(-1, -1, 0), id=3)

    cPoints = {}
    for i in [a1, a2, a3]:
        cPoints[i.getId()] = i

    print cPoints

    for i in melkmanSortIterator(cPoints):
        print i
