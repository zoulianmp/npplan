# -*- coding: utf8 -*-
'''
Функции b{createGeantRepresentation}, b{createMcnpRepresentation} преобразуют массив данных data в строковое
представление элемента для соответствующей системы моделирования
@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 27.06.13
@summary: 
'''

import numpy as np

def createGeantRepresentation(data, dtype='isotope'):
    if 'isotope' == dtype:
        return '  G4Element* el%s = new G4Element( name = "%s", \n'\
               '                           symbol = "%s",\n' \
               '                           z = %d.0, a = %2.3f * g/mole );' %(
            data['symbol'], data['name'], data['symbol'], data['atomicNum'], data['atomicMass'],
        )
    elif 'material' == dtype:
        s = '  G4Material* %s = new G4Material( "%s", \n' \
        '                               density = %0.3f*%s,\n' \
        '                               numberofElements = %d );\n' % (
            data['name'].lower(),
            data['name'],
            data['density'],
            'g/cm3' if 0 == data['densityUnits'] else 'mg/cm3',
            len(data['cmap']),
        )
        for i in data['cmap']:
            s = '%s\n%s->AddElement(el%s,%0.3f);' % (s, data['name'].lower(), i[0]['symbol'], i[1])
        return s


def createMcnpRepresentation(data, dtype='isotope'):
    if 'isotope' == dtype:
        # causing system crash ???
        #if 'library' in data['mcnpSimData']:
        try:
            return '%d%003d.%s' % (data['atomicNum'], int(np.round(data['atomicMass'])), data['mcnpSimData']['library'])
        except KeyError:
        #else:
            return '%d%003d' %(data['atomicNum'], int(np.round(data['atomicMass'])))
    elif 'material' == dtype:
        s = 'm%num% '
        for i in data['cmap']:
            s = '%s %d%003d -%0.6f &\n' % (s, i[0]['atomicNum'], int(np.round(i[0]['atomicMass'])), i[1])
        return s[:-2]
    elif 'cell' == dtype:
        if 'densityUnits' in data and 1 == data['densityUnits']:
            return '%cell% %num% ' + '-%0.6f' % (float(data['density']) / 1000.0) + ' %topcell% u=%univ%'
        return '%cell% %num% ' + '-%0.6f' % data['density'] + ' %topcell% u=%univ%'
        pass