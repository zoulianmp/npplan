# -*- coding: utf8 -*-
'''
задаёт допустимые аргументы комадной строки, использует библиотеку argparse
установленные ключи запуска программы:

-h - помощь, создаётся по умолчанию

--ver - вывод версии

-m, --mode - тип запуска, допустимые значения B{client}, B{server}, B{diag}

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 2
@date: 26.06.2012
@summary: модуль обработки командной строки запуска
'''

import argparse
import NPPlan

parser = argparse.ArgumentParser(description='NPPlan3')
parser.add_argument('--ver', action='version', version=NPPlan.__version__)
parser.add_argument('--mode', '-m', action='store', default='client', choices=['client', 'server', 'diag'],
                    help='Start mode')
args = parser.parse_args()
#NPPlan._programData['args'] = args
NPPlan.config.runningConfig.args = args
