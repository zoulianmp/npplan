# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 31.07.13
@summary: 
'''

import NPPlan
import wx
from NPPlan.controller.client.mainC.abstracts import baseController
from NPPlan.controller.client.mainC.stages.planning import emptyPlanController
from NPPlan.controller.client.mainC.stages.planning import addPatientController

from NPPlan.view.client.main.stages.planning.planPanel import planPanelView
from NPPlan.view.client.main.stages.planning import patientTopPanel
from NPPlan.view.client.main.stages.planning.patientList import patientListView


class patientAndPlans(baseController):
    def __init__(self, parent):
        self.__dict__['parent'] = parent

    def _addPanelToPanel(self, panel, newWxClass):
        panel.GetParent().Freeze()
        panel.Freeze()
        pane = newWxClass(panel)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pane, 1, wx.ALIGN_CENTER | wx.EXPAND)
        panel.SetSizer(sizer)
        panel.Layout()
        panel.Thaw()
        panel.GetParent().Thaw()
        panel.setCreated()
        return pane

    def patientList(self, panel):
        NPPlan.config.runningConfig.curSubStageType = 'patientList'
        if not panel.isCreated():
            pane = self._addPanelToPanel(panel, patientListView)
            # c =
            # self.handler.register
            pass
        else:
            pass
        pass

    def patientAdd(self, panel):
        NPPlan.config.runningConfig.curSubStageType = 'patientAdd'
        if not panel.isCreated():
            pane = self._addPanelToPanel(panel, patientTopPanel)
            c = addPatientController(wxParent=pane, parentController=self)
            self.handler.register('patientAdd', c, pane)
        else:
            self.handler.patientAdd.work()
        pass

    def planAdd(self, panel):
        NPPlan.config.runningConfig.curSubStageType = 'planAdd'
        if not panel.isCreated():
            pane = self._addPanelToPanel(panel, planPanelView)
            c = emptyPlanController(wxParent=pane, parentController=self)
            self.handler.register('planAdd', c, pane)
            pass
        else:
            self.handler.planAdd.work()
            pass
        pass

    def testPlan(self, panel):
        plan = NPPlan.planAbstract()
        plan.planLoadDicomSeriesFromFile("C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\")
        plan.planSetThumbsDir("C:\\Users\\Chernukha\\Pictures\\DICOM\\thumb\\")
        NPPlan.currentPlan = plan
        NPPlan.config.runningConfig.curStageType = 'main'
        NPPlan.config.runningConfig.curSubStageType = 'contouring'
        self.handler.main.view.Freeze()
        self.handler.main.controller.getCenter().createCenter()
        #self.handler.main.controller.getCenter().SetSelection(1)
        self.handler.main.view.Thaw()
