# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 17.05.13
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts.baseController import baseController
import NPPlan.controller.logger as log
from NPPlan.view.client.main.stages.planning import planPanelView
import wx
from NPPlan.model.file.dicom.seriesEvaluator import seriesEvaluator
import time
from NPPlan.model.file.wd import getWorkDir
import os
from NPPlan.model.file.dicom.thumbnail import parseDir as parseThumbDir


class planControllerBase(baseController):
    def __init__(self, wxParent=None, *args, **kwargs):
        self.updateRibbon()
        pass

    def updateRibbon(self):
        self.handler.ribbon.controller.update()
        #self.handler.ribbon.controller.setActivePageByPageName('f_patient')


class emptyPlanController(planControllerBase):
    def __init__(self, wxParent=None, *args, **kwargs):
        # @todo: accurate handling thumbnails
        planControllerBase.__init__(self, wxParent, *args, **kwargs)
        self._wxParent = wxParent
        self._path = None
        self._parent = kwargs.get('parentController', None)
        self._panel = wxParent

    def start(self, **kwargs):
        log.log('controller.stages.planning', 'Started plan')
        #self._panel = planPanelView(self._wxParent)
        #sz = wx.BoxSizer(wx.VERTICAL)
        #sz.Add(self._panel, 0, flag=wx.EXPAND)
        #self._wxParent.SetSizer(sz)

        self.registerEvents()

    def registerEvents(self):
        self._panel.Bind(wx.EVT_BUTTON, self.clickAddDataset, self._panel._folderButton)
        self._panel.tree.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.onActivate)
        self._panel.Bind(wx.EVT_BUTTON, self.startPlanning, self._panel._goBtn)

    def clickAddDataset(self, event):
        # to controller
        dlg = wx.DirDialog(self._panel, _("Choose a directory:"),
                          style=wx.DD_DEFAULT_STYLE
                           | wx.DD_DIR_MUST_EXIST
                           #| wx.DD_CHANGE_DIR
                           )
        if dlg.ShowModal() == wx.ID_OK:
            self._path = dlg.GetPath()
            self._panel._folderField.Disable()
            self._panel._folderField.SetValue(self._path)
        dlg.Destroy()
        self.readSeries()

    def work(self, **kwargs):
        self.updateRibbon()
        log.log('controller.stages.planning', 'Started plan')
        pass

    def readSeries(self):
        #path = self._panel._folderField.GetValue()
        #print path
        if self._path is None:
            return
        se = seriesEvaluator(self._path,
                             globalFields=[
                                 ('pname', (0x0010, 0x0010), 'str'),
                                 ('sid', (0x0020, 0x0011), 'int'),
                                 ('description', (0x0008, 0x1030), 'str'),
                                 ('ldate', (0x0008, 0x0020), 'str'),
                                 ('ltime', (0x0008, 0x0030), 'str'),
                                 ('manufacturer', (0x0008, 0x0070), 'str'),
                                 ('station', (0x0008, 0x1010), 'str'),
                                 ('device', (0x0018, 0x1000), 'str'),
                                 ('suid', (0x0020, 0x000e), 'str'),
                             ]
                             )
        self._se = se
        print se.getSeriesNames()

        self.root = self._panel.tree.AddRoot("%s" % se.sid)
        self._panel.tree.SetItemText(self.root, str(se.allLen), 1)
        self._panel.tree.SetItemText(self.root, se.description, 2)
        for i in se.getSeriesNames():
            data = se.getSeriesData(i)
            child = self._panel.tree.AppendItem(self.root, str(data['sid']))
            self._panel.tree.SetItemText(child, str(data['_len']), 1)
            self._panel.tree.SetItemText(child, str(data['description']), 2)
            self._panel.tree.SetItemPyData(child, {'_id': i})

            for vals in data['_list']:
                last = self._panel.tree.AppendItem(child, vals)
                self._panel.tree.SetItemText(last, '', 1)
                self._panel.tree.SetItemText(last, '', 2)
                self._panel.tree.SetItemPyData(last, {'_imFileName': vals, '_topId': i})

        self._panel.tree.Expand(self.root)

        self._panel._dateField.SetValue(time.strftime('%Y', self.getStudyDateTime(se.ldate, se.ltime)))
        self._panel._patField.SetValue(se.pname)
        self._panel._studyField.SetValue(se.description)
        self._panel._facField.SetValue('%s %s %s' %(se.manufacturer, se.station, se.device))

    def getStudyDateTime(self, ldate, ltime):
        print ldate, ltime
        ltime = str(int(float(ltime)))
        datetime = time.strptime('%s %s' % (ldate, ltime),
                                 "%Y%m%d %H%M%S")
        return datetime
        pass

    def onActivate(self, event):
        #self._panel.tree.GetItemPyData()
        print 'OnActivate: ', self._panel.tree.GetItemText(event.GetItem()), event.GetItem()
        print self._panel.tree.GetItemPyData(event.GetItem())
        data = self._panel.tree.GetItemPyData(event.GetItem())

        if '_id' in data:
            #print NPPlan.planAbstract.planGetThumbnailButtons()
            # 1. copy this images
            if not self.checkSeries(data['_id']):
                self.moveSeries(data['_id'])
            if hasattr(NPPlan, 'currentPlan'):
                del NPPlan.currentPlan
            NPPlan.currentPlan = NPPlan.planAbstract()
            NPPlan.currentPlan.planLoadDicomSeriesFromFile(self.getStudyDir(data['_id']))
            NPPlan.currentPlan.planSetThumbsDir(self.getThumbsDir(data['_id']))
            # 2. load to thumbnailer
            self._panel.setThumbnails(buttons=NPPlan.currentPlan.__dict__['data']._getThumbnailButtons(),
                                      data=NPPlan.currentPlan.__dict__['data']._getThumbnailItems(),
                                      backend=self.thumbnailClickEvent)

            pass
        if '_imFileName' in data:
            # 0. check if series is loaded and is active (via _topId). If not, perform previous stage
            # 1. select this file in thumbnailer
            pass

    def getStudyDir(self, _id):
        return os.path.join(getWorkDir(), self._se.suid, self._se.getSeriesData(_id)['suid'])

    def getThumbsDir(self, _id):
        return os.path.join(getWorkDir(), self._se.suid, 't' + self._se.getSeriesData(_id)['suid'])

    def checkSeries(self, _id):
        if not os.path.exists(os.path.join(getWorkDir(), self._se.suid)):
            os.mkdir(os.path.join(getWorkDir(), self._se.suid))
        if not os.path.exists(self.getThumbsDir(_id)):
            os.mkdir(self.getThumbsDir(_id))
        if os.path.exists(self.getStudyDir(_id)):
            return True
        return False

    def moveSeries(self, _id):
        self._se.moveSeriesTo(_id, self.getStudyDir(_id))
        parseThumbDir(self.getStudyDir(_id), self.getThumbsDir(_id))
        pass

    def thumbnailClickEvent(self, event, *args, **kwargs):
        """
        @param event: событие
        @type event: wx.lib.agw.thumbnailctrl.ThumbnailEvent
        @return:
        """
        print event.GetSelection(), event.GetId(), event.GetClientData()
        print self._panel.thumbnailer._thumbnailer.GetSelection()
        print self._panel.thumbnailer.getCurrentSelected()
        print 'Selected thumb event', args, kwargs
        pass

    def startPlanning(self, event):
        print 'Start planning'
        log.log('controller.stages.planning', 'Initiating next stage')
        #NPPlan.goNextStage()
        pass