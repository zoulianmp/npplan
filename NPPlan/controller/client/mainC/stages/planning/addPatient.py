# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 28.04.14
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController
try:
    from pubsub import pub
except ImportError:
    from wx.lib.pubsub import pub


class addPatientController(baseController):
    def __init__(self, wxParent, parentController):
        """

        @param wxParent:
        @type wxParent: NPPlan.view.client.main.stages.planning.patientTop.patientTopPanel
        @param parentController:
        @type parentController: NPPlan.controller.client.mainC.stages.patientAndPlans.patientAndPlans
        @return:
        """
        self._wxParent = wxParent
        self._parentController = parentController
        pub.subscribe(self.savePatientData, "patientSave")
        #pub.subscribe(self.stop_listening, "progress_sleep")

        pass

    def start(self, *args, **kwargs):
        from wx import xrc
        #res = xrc.XmlResource(NPPlan.config.appPath+'/view/client/main/stages/patient/patiendDataMain.xml')
        #self.panel = xrc.XRCCTRL(self.frame, 'panel')
        #panel = res.LoadPanel(None, "dMain")
        from NPPlan.view.client.main.stages.planning.patientDataMain import patiendDataMainView
        from NPPlan.view.client.main.stages.planning.patientDataAdditional import patiendDataAdditionalView
        from NPPlan.view.client.main.stages.planning.patientRad import patientRadView
        self._dMain = patiendDataMainView(self._wxParent)
        self._wxParent.addPage(self._dMain, _("Main data"))
        self._dAdd = patiendDataAdditionalView(self._wxParent)
        self._wxParent.addPage(self._dAdd, _("Personal data"))
        self._iRad = patientRadView(self._wxParent)
        self._wxParent.addPage(self._iRad, _("Treatment data"))
        pass

    def work(self, *args, **kwargs):
        pass

    def savePatientData(self, caller=''):
        pass