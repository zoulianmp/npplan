# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 01.03.13
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController
import vtk
import numpy
from scipy.spatial.distance import cdist, pdist
from NPPlan.model.math.linear import newPoint


class beamContainer(baseController):
    def __init__(self, parent):
        self.__dict__['parent'] = parent
        self.__dict__['beams'] = {}

    def appendBeam(self, beamClass, beamParams, id, active=False):
        self.__dict__['beams'][id] = {
            'beam': beamClass,
            'params': beamParams,
            'active': active,
            'tracer': beamClass._tracer,
        }

    def activateById(self, beamNumber):
        r = None
        for i in self.__dict__['beams']:
            if i == beamNumber:
                self.__dict__['beams'][i]['active'] = True
                r = self.__dict__['beams'][i]['beam']
            else:
                self.__dict__['beams'][i]['active'] = False
        print 'beam', beamNumber, ' instance', r, ' now active'
        print self.__dict__
        return r

    def getData4Simulation(self, ):
        print self.__dict__['beams']
        for i in self.__dict__['beams']:
            self.__dict__['beams'][i]['source'] = self.__dict__['beams'][i]['beam'].getSourceCoordinates()
        return self.__dict__['beams']

    def rotateNonActive(self, angle=1, axis=2):
        for i in self.__dict__['beams']:
            if not self.__dict__['beams'][i]['active']:
                self.__dict__['beams'][i]['beam']._rotateSingle(angle, axis)
                #self.__dict__['beams'][i]['tracer']._rotateSingle(angle, axis)

    def rotateActive(self, angle=1, axis=2):
        for i in self.__dict__['beams']:
            if self.__dict__['beams'][i]['active']:
                self.__dict__['beams'][i]['beam']._rotateSingle(angle, axis)
                self.__dict__['beams'][i]['tracer']._rotateSingle(angle, axis)
                return

    def rotateAll(self, angle, axis):
        for i in self.__dict__['beams']:
            self.__dict__['beams'][i]['beam']._rotateSingle(angle, axis)
            self.__dict__['beams'][i]['tracer']._rotateSingle(angle, axis)

    def rotateTracers(self, angle, axis):
        for i in self.__dict__['beams']:
            self.__dict__['beams'][i]['tracer']._rotateSingle(angle, axis)

    def getActive(self):
        for i in self.__dict__['beams']:
            if self.__dict__['beams'][i]['active']:
                return i, self.__dict__['beams'][i]['beam']

    def getLength(self):
        return len(self.__dict__['beams'])
        pass


class bevBeamControllerBase(baseController):
    pass


class bevBeamController(bevBeamControllerBase):
    def __init__(self, uId=0, parent=None, axialController=None, sagittalController=None, coronalController=None,
                 threeDimController=None, target=(0, 0, 0), **kwargs):
        """
        Инициализация единичного пучка
        @param uId: идентификатор пучка
        @type uId: int
        @param parent: родитель
        @type parent: NPPlan.controller.client.mainC.stages.positioning.bev.bevController
        @param axialController:
        @type axialController: NPPlan.controller.client.mainC.stages.positioning.bev.axialHandler
        @param sagittalController:
        @type sagittalController: NPPlan.controller.client.mainC.stages.positioning.bev.sagittalHandler
        @param coronalController:
        @type coronalController: NPPlan.controller.client.mainC.stages.positioning.bev.coronalHandler
        @param threeDimController:
        @type threeDimController: NPPlan.controller.client.mainC.stages.positioning.bev.threeDimHandler
        @param target: фокальная точка
        @type target: tuple
        @keyword color: цвет пучка
        @type color: tuple
        @return:
        """
        print type(threeDimController)
        self._axial = axialController
        self._coronal = coronalController
        self._sagittal = sagittalController
        self._threeDim = threeDimController
        self._target = target
        # parent and check stuff
        if parent is not None:
            if axialController is None:
                self._axial = parent.axial
            else:
                self._axial = axialController
            if sagittalController is None:
                self._sagittal = parent.sagittal
            else:
                self._sagittal = sagittalController
            if coronalController is None:
                self._coronal = parent.coronal
            else:
                self._coronal = coronalController
            if threeDimController is None:
                self._threeDim = parent.threeDim
            else:
                self._threeDim = threeDimController
            self._parent = parent

        self._translation = (0, 0, 0)
        self._points = {}
        self._beamColor = kwargs.get('color', (1, 0, 0))
        self._beamRotation = {'anglesDegrees': [0, 0], 'anglesRadians': [0, 0]}

        self._obbtree = None

        self.doInitialBeamSettings()
        self.drawBeam()
        self.doPostBeamSettings()

    def doInitialBeamSettings(self):
        self._beamHeight = 800
        self._beamRadius = 5

    def drawBeam(self):
        print 'Drawing beam'
        self._beam = vtk.vtkCylinderSource()
        self._beam.SetHeight(self._beamHeight)
        self._beam.SetRadius(5)
        self._beam.SetResolution(18)
        self._beam.CappingOn()

        #self._beam.SetCenter(*self._target)

        self._startBeam = vtk.vtkCylinderSource()
        self._startBeam.SetHeight(30)
        self._startBeam.SetRadius(12)
        self._startBeam.SetResolution(18)
        self._startBeam.CappingOn()

        #self._startBeam.SetCenter(*self._target)

        self._transform = vtk.vtkTransform()
        #self._transform.Translate(0.0, -1.0 * float(self._beamHeight) / 2, 0)
        self._transformF = vtk.vtkTransformPolyDataFilter()
        self._transformF.SetInputConnection(self._beam.GetOutputPort())
        self._transformF.SetTransform(self._transform)

        self._startTransform = vtk.vtkTransform()
        #self._startTransform.Translate(0.0, -1.0 * float(self._beamHeight) / 2, 0)
        self._startTransformF = vtk.vtkTransformPolyDataFilter()
        self._startTransformF.SetInputConnection(self._startBeam.GetOutputPort())
        self._startTransformF.SetTransform(self._startTransform)
        #self._startBeam.SetCenter(23, 23, 23)

        self._beamNormals = vtk.vtkPolyDataNormals()
        #self._beamNormals.SetInput(transformF.GetOutput())
        self._beamNormals.SetInput(self._transformF.GetOutput())
        self._startBeamNormals = vtk.vtkPolyDataNormals()
        self._startBeamNormals.SetInput(self._startTransformF.GetOutput())


        self._beamMapper = vtk.vtkPolyDataMapper()
        self._beamMapper.SetInput(self._beamNormals.GetOutput())
        self._startBeamMapper = vtk.vtkPolyDataMapper()
        self._startBeamMapper.SetInput(self._startBeamNormals.GetOutput())

        self._beamActor = vtk.vtkActor()
        self._beamActor.SetMapper(self._beamMapper)
        self._beamActor.GetProperty().SetColor(*self._beamColor)
        self._startBeamActor = vtk.vtkActor()
        #self._startBeamActor.SetOrigin(-300, 0, 0)
        self._startBeamActor.SetMapper(self._startBeamMapper)
        self._startBeamActor.GetProperty().SetColor(0, 0, 0)
        print 'Drawing beam completed'
        self._threeDim.addBeamActor(self._beamActor)
        self._threeDim.addBeamActor(self._startBeamActor)
        print 'Drawing beam completed 2 '

        #self._startTransform.Translate(0, -150, 0)
        #print self._threeDim._skinActor
        #print self._threeDim._skinActor.GetResliceAxesDirectionCosines()
        #print self._threeDim._skinActor.GetMatrix()
        #print self._beamActor.GetMatrix()

        pass

    def rotateBodyAndSource(self, angles):
        # rotate back
        print 'ROTATE BACKE', angles
        self._parent._beamContainer.rotateAll(angles[0], 0)
        self._parent._beamContainer.rotateAll(angles[1], 2)
        self._threeDim._skinActor.RotateZ(angles[0])
        self._threeDim._skinActor.RotateY(angles[1])
        pass

    def getAngle(self, mat):
        radians = 1 / numpy.radians(1)
        #mat = self._skinActor.GetMatrix()
        angle_y = D = numpy.arcsin(float('%2.2f' %mat.GetElement(0, 2)))
        C = numpy.cos(angle_y)
        angle_y *= radians
        if (numpy.fabs(C) > 0.005):
            trX = mat.GetElement(2,3)
            trY = -mat.GetElement(1,3)

            angle_x = numpy.arctan2(trY, trX)*radians

            trX = mat.GetElement(0, 0) / C
            trY = -mat.GetElement(0, 1) / C

            angle_z = numpy.arctan2(trY, trX)*radians
        else:
            angle_x = 0

            trX = mat.GetElement(1, 1)
            trY = mat.GetElement(1,0)

            angle_z = numpy.arctan2(trY, trX)*radians

        return angle_x, angle_y, angle_z

    def getSourceCoordinates(self):
        print 'COORD LOOP START'
        print 'body rotation', self._threeDim.getBodyRotation()
        bodyAngles = numpy.array(self._threeDim.getBodyRotation()['anglesDegrees'])
        #if bodyAngles[0] != 0 and bodyAngles[1] != 0:
        self.rotateBodyAndSource(-1.0 * bodyAngles)
        print self._beamRotation
        print 'startTransform', self._startTransform
        print 'body', self._threeDim._skinStripper
        print pdist([(0, 0, 0), self._startTransform.GetPosition()])
        x, y, z = self._startTransform.GetPosition()
        r = pdist([(0, 0, 0), self._startTransform.GetPosition()])
        phi = numpy.arctan(y / x)
        theta = numpy.arctan(numpy.sqrt(x ** 2 + y ** 2) / z)
        print r, theta, phi

        tree = vtk.vtkOBBTree()
        tree.SetDataSet(self._threeDim._skinStripper.GetOutput())
        tree.BuildLocator()

        lineP1 = numpy.array(self._startTransform.GetPosition())
        lineP2 = numpy.array([0, 0, 0])
        intersectPoints = vtk.vtkPoints()

        ids = vtk.vtkIdList()

        tree.IntersectWithLine(lineP1, lineP2, intersectPoints, ids)
        n = intersectPoints.GetNumberOfPoints()
        print 'intersections', intersectPoints, ids, n
        #d = 10000000000000
        c = intersectPoints.GetPoint(0)
        ds = pdist([self._startTransform.GetPosition(), c])
        dt = pdist([c, (0, 0, 0)])

        #for i in range(n):
        #    c = intersectPoints.GetPoint(i)
        #    print 'INTERSECTION AT', c
        #    dt = pdist([self._startTransform.GetPosition(), c])
        #    if dt < d:
        #        d = dt
        print ds, dt, ds+dt
        # x = r cos fi sin theta
        # y = r sin fi sin theta
        # z = r cos theta
        ssd = 100.0
        nr = dt + ssd
        print 'nr =', nr
        print 'x = ', nr * numpy.cos(phi) * numpy.sin(theta)
        print 'y = ', nr * numpy.sin(phi) * numpy.sin(theta)
        print 'z = ', nr * numpy.cos(theta)

        #print self._startBeamActor.GetMatrix()
        #print self._beamActor.GetMatrix()
        #print self._startTransform.GetPosition()
        #print self._transform.GetPosition()
        print 'COORD LOOP END'
        #self.makeIntersections(sourceVolume=self._threeDim._skinStripper, color=(0, 0, 1))
        #if bodyAngles[0] != 0 and bodyAngles[1] != 0:
        srcMatrix = self.matrixFromVtkToArray(self._startTransform.GetMatrix())
        srcAnglesDegrees = self.getAngle(self._startTransform.GetMatrix())
        self.rotateBodyAndSource(1.0 * bodyAngles)
        return {'positionXYZ':
                    (nr * numpy.cos(phi) * numpy.sin(theta),
                    nr * numpy.sin(phi) * numpy.sin(theta),
                    nr * numpy.cos(theta)),
                'bodyAngles': bodyAngles,
                'angles': (phi, theta),
                'ssd': ssd,
                'sst': nr,
                'srcMatrix': srcMatrix,
                'srcAnglesDegrees': srcAnglesDegrees,}

    def matrixFromVtkToArray(self, matrix):
        return [(matrix.GetElement(0, 0), matrix.GetElement(0, 1), matrix.GetElement(0, 2),),
                (matrix.GetElement(1, 0), matrix.GetElement(1, 1), matrix.GetElement(1, 2),),
                (matrix.GetElement(2, 0), matrix.GetElement(2, 1), matrix.GetElement(2, 2),),
        ]

    def center(self, coords):
        self._beam.SetCenter(*coords)
        self._startBeam.SetCenter(*coords)

    def flushTrunslation(self):
        coords = (-1.0 * self._translation[0], -1.0 * self._translation[1], -1.0 * self._translation[2])
        self._transform.Translate(*coords)
        self._startTransform.Translate(*coords)

    def translate(self, coords):
        self._translation = coords
        self._transform.Translate(*coords)
        self._startTransform.Translate(*coords)
        pass

    def rotateWXYZ(self, coords, flushStartTranslation=False):
        if flushStartTranslation:
            self._startTransform.Translate(0, float(self._beamHeight) / 2.0, 0)
        self._transform.RotateWXYZ(*coords)
        self._startTransform.RotateWXYZ(*coords)
        if flushStartTranslation:
            self._startTransform.Translate(0, -1.0 * float(self._beamHeight) / 2.0, 0)

    def rotateSingle(self, angle):
        self._rotateSingle(angle, 2)

    def _rotateSingle(self, angle, axis=2):
        if 0 == axis:
            self._transform.RotateX(angle)
            self._beamRotation['anglesDegrees'][0] += angle
            self._beamRotation['anglesRadians'][0] = numpy.radians(self._beamRotation['anglesDegrees'][0])
        elif 1 == axis:
            self._transform.RotateY(angle)
        elif 2 == axis:
            self._transform.RotateZ(angle)
            self._beamRotation['anglesDegrees'][1] += angle
            self._beamRotation['anglesRadians'][1] = numpy.radians(self._beamRotation['anglesDegrees'][1])
        self._rotateStart(angle, axis)

    def _rotateStart(self, angle, axis=2):
        self._startTransform.Translate(0, float(self._beamHeight) / 2.0, 0)
        if 0 == axis:
            self._startTransform.RotateX(angle)
        elif 1 == axis:
            self._startTransform.RotateY(angle)
        elif 2 == axis:
            self._startTransform.RotateZ(angle)
        self._startTransform.Translate(0, -1.0 * float(self._beamHeight) / 2.0, 0)

    def showPoint(self, coords, _id, color=(0.0, 1.0, 0.0)):
        points = vtk.vtkPoints()
        p = coords

        # Create the topology of the point (a vertex)
        vertices = vtk.vtkCellArray()

        id = points.InsertNextPoint(p)
        vertices.InsertNextCell(1)
        vertices.InsertCellPoint(id)

        # Create a polydata object
        point = vtk.vtkPolyData()

        # Set the points and vertices we created as the geometry and topology of the polydata
        point.SetPoints(points)
        point.SetVerts(vertices)

        # Visualize
        mapper = vtk.vtkPolyDataMapper()
        if vtk.VTK_MAJOR_VERSION <= 5:
            mapper.SetInput(point)
        else:
            mapper.SetInputData(point)

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetPointSize(20)
        actor.GetProperty().SetColor(*color)

        self._points[_id] = {'actor': actor, 'coords': coords}

        self._threeDim._spaceRenderer.AddActor(actor)

    def makeIntersections(self, sourceVolume=None, color=(0, 1, 0)):

        #lineP0 = self._target
        target = numpy.array(self._target)
        lineP0 = numpy.array(self._transform.GetPosition())
        lineP1 = numpy.array(self._startTransform.GetPosition())

        print lineP0
        print lineP1
        print lineP0 + target

        #lineP2 = newPointByCoords(lineP0[0], lineP0[1], lineP0[2], lineP1[0], lineP1[1], lineP1[2], 10)
        #lineP0 = lineP0 + target
        #lineP1 = lineP1 + target
        lineP2 = newPoint(lineP0, lineP1, -10)
        print lineP0, lineP1, lineP2

        #return
        tree = self._obbtree
        print 'INTERSECTION'
        print sourceVolume
        print tree

        intersectPoints = vtk.vtkPoints()

        ids = vtk.vtkIdList()

        tree.IntersectWithLine(lineP1, lineP2, intersectPoints, ids)
        n = intersectPoints.GetNumberOfPoints()
        print 'intersections', intersectPoints, ids, n
        for i in range(n):
            c = intersectPoints.GetPoint(i)
            print c

            #self.showPoint(c, 'l%d' % i, color=color)

        pass

    def doPostBeamSettings(self, ):
        #self.rotateSingle(40)
        self.translate(self._target)
        print self._startBeam.GetCenter()
        print self._startTransform.GetPosition()
        self._startTransform.Translate(0, -1.0 * float(self._beamHeight) / 2.0, 0)

        print self._startTransform.GetPosition()

        self._threeDim.update()
        #self._threeDim.hidSkin()

        #print ints

        #self.makeIntersections(sourceVolume=self._threeDim._skinStripper)
        #self.makeIntersections(sourceVolume=self._threeDim._outlineData, color=(1, 0, 1))
        print self._threeDim._skinStripper
        self._threeDim._skinStripper.UpdateWholeExtent()
        print 'B0', self._threeDim._skinActor.GetBounds()
        #self._threeDim._skinActor.RotateZ(-40)
        #self._threeDim._outlineActor.RotateZ(-40)
        self._threeDim._skinStripper.UpdateWholeExtent()
        #self._threeDim._boneActor.RotateZ(-40)
        print self._threeDim._skinStripper
        self._threeDim._skinStripper.Update()
        self._threeDim._skinNormals.Update()
        self._threeDim._skinMapper.Update()
        #self.rotateSingle(60)
        #print self._threeDim._skinActor.GetMatrix()
        print 'B1', self._threeDim._skinActor.GetBounds()
        self.rotateSingle(90)
        self._threeDim.update()
        #self.makeIntersections(sourceVolume=self._threeDim._skinStripper, color=(0, 0, 1))
        # @todo: handle intersections while rotating

        tree = vtk.vtkOBBTree()
        tree.SetDataSet(self._threeDim._skinStripper.GetOutput())
        tree.BuildLocator()
        self._obbtree = tree
        self._threeDim.update()
        try:
            self._tracer = beamSliceTracer(beamClass=self, bevClass=self._parent)
            self._tracer.start()
        except AttributeError:
            print 'No tracer'
            pass


class beamSliceTracer(object):
    def __init__(self, beamClass, bevClass):
        """
        @param beamClass:
        @type beamClass: bevBeamController
        @param bevClass:
        @type bevClass: NPPlan.controller.client.mainC.stages.positioning.bev.bevController
        @return:
        """
        self._beamClass = beamClass
        self._bevClass = bevClass

    def _rotateSingle(self, angle, axis=2):
        if 0 == axis:
            self._trans.RotateX(angle)
        elif 1 == axis:
            self._trans.RotateY(angle)
        elif 2 == axis:
            self._trans.RotateZ(angle)

    def start(self):
        print 'beam tracer started'
        #self._beam = self._beamClass._beam.Copy()
        #self._beam2Transform = vtk.vtkTransformPolyDataFilter()
        #self._beam2Transform.SetInput(self._beamClass._beam.GetOutput())
        #self._beam2Transform.SetTransform(self._beamClass._transform)
        c = self._bevClass.threeDim.getTarget()
        self._beam2 = vtk.vtkCylinderSource()
        self._beam2.SetRadius(5)
        self._beam2.SetHeight(5000)
        self._beam2.SetResolution(18)
        self._trans = vtk.vtkTransform()
        #self._trans.RotateZ(90)
        self._trans.Translate(c[0], c[1], c[2])
        self._trans.RotateZ(90)
        self._beam2Transform = vtk.vtkTransformPolyDataFilter()
        self._beam2Transform.SetInput(self._beam2.GetOutput())
        self._beam2Transform.SetTransform(self._trans)
        #self._beam = vtk.vtkCylinderSource()
        #self._beam.SetHeight(self._beamClass._beamHeight)
        #self._beam.SetRadius(5)
        #self._beam.SetResolution(18)
        #self._beam.CappingOn()
        #self._beam2Transform = vtk.vtkTransformPolyDataFilter()
        #self._beam2Transform.SetInput(self._beam.GetOutput())
        #self._beam2Transform.Translate(c[0], c[1], c[2])
        #self._beam2Transform.SetTransform(self._beamClass._transform)
        #self._beam.GetTransform().Translate(c[0], c[1], c[2])
        #self._beam2Transform.GetTransform().Translate(-c[0], -c[1], -c[2])

        #print self._bevClass.threeDim._transform.GetPosition()
        #self._beam2Transform.SetTransform(self._bevClass.threeDim._transform)

        self._aCutter = vtk.vtkCutter()
        self._aCutter.SetInput(self._beam2Transform.GetOutput())
        self._aCutter.SetCutFunction(self._bevClass.axial._plane)
        self._aCutter.GenerateCutScalarsOn()

        self._sCutter = vtk.vtkCutter()
        self._sCutter.SetInput(self._beam2Transform.GetOutput())
        self._sCutter.SetCutFunction(self._bevClass.sagittal._plane)
        self._sCutter.GenerateCutScalarsOn()

        self._cCutter = vtk.vtkCutter()
        self._cCutter.SetInput(self._beam2Transform.GetOutput())
        self._cCutter.SetCutFunction(self._bevClass.coronal._plane)
        self._cCutter.GenerateCutScalarsOn()
        self._cCutter.SetSortByToSortByCell()

        actors = []
        for i in [self._aCutter, self._sCutter, self._cCutter]:
            cm = vtk.vtkPolyDataMapper()
            cm.SetInput(i.GetOutput())
            cm.ScalarVisibilityOn()
            cm.Update()

            actor = vtk.vtkActor()
            actor.SetMapper(cm)
            actor.GetProperty().SetColor(*self._beamClass._beamColor)
            actor.GetProperty().SetLineWidth( 2.0 )
            actor.GetProperty().SetOpacity(1.0)
            actor.GetProperty().SetDiffuseColor(1, 1, 0)
            actor.GetProperty().SetDiffuse(0.8)
            actor.GetProperty().SetEdgeColor(*self._beamClass._beamColor)
            actor.GetProperty().SetSpecular(0.3)
            actor.GetProperty().SetSpecularPower(20)
            actor.GetProperty().SetRepresentationToSurface()
            actor.GetProperty().EdgeVisibilityOn()
            actors.append(actor)

        #self._cutterMapper = vtk.vtkPolyDataMapper()
        #self._cutterMapper.SetInput(self._aCutter.GetOutput())
        #self._cutterMapper.SetInput(self._beam2Transform.GetOutput())
        #self._cutterMapper.SetInput(self._beamClass._beam.GetOutput())
        #self._cutterMapper.ScalarVisibilityOn()

        self.axialActor = actors[0]
        self.sagittalActor = actors[1]
        self.coronalActor = actors[2]


        print 'beam tracer actor creation'
        #self._beamAxialCutterActor = vtk.vtkActor()
        #self._beamAxialCutterActor.SetMapper(self._cutterMapper)
        #self._beamAxialCutterActor.GetProperty().SetColor(1, 0, 0)
        #print 'tracer', self._beamClass._beamColor
        #self._beamAxialCutterActor.GetProperty().SetColor(*self._beamClass._beamColor)
        #self._beamAxialCutterActor.GetProperty().SetLineWidth( 2.0 )
        #self._beamAxialCutterActor.GetProperty().SetOpacity(1.0)
        #self._beamAxialCutterActor.GetProperty().SetDiffuseColor(1, 1, 0)
        #self._beamAxialCutterActor.GetProperty().SetEdgeColor(*self._beamClass._beamColor)
        #self._beamAxialCutterActor.GetProperty().SetDiffuse(0.8)
        #self._beamAxialCutterActor.GetProperty().SetSpecular(0.3)
        #self._beamAxialCutterActor.GetProperty().SetSpecularPower(20)
        #self._beamAxialCutterActor.GetProperty().SetRepresentationToSurface()
        #self._beamAxialCutterActor.GetProperty().EdgeVisibilityOn()

        self._bevClass.axial.addBeamTrace(actors[0])
        self._bevClass.sagittal.addBeamTrace(actors[1])
        self._bevClass.coronal.addBeamTrace(actors[2])

        #self._beam2Transform.GetTransform().Translate(-c[0], -c[1], -c[2])

    def updateSlices(self):
        self._bevClass.axial.update()
        self._bevClass.sagittal.update()
        self._bevClass.coronal.update()

    def delete(self):
        self._beamClass.axial.delBeamTrace(self.axialActor)
        self._beamClass.axial.delBeamTrace(self.coronalActor)
        self._beamClass.axial.delBeamTrace(self.sagittalActor)





