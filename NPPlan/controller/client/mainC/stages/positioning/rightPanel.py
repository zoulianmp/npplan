# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 15.11.12
@summary: 
'''

import NPPlan
from NPPlan.model.file.configReader.volume import getData as getVolumeData

from NPPlan.controller.client.mainC.abstracts.baseController import baseController
from NPPlan.controller.client.mainC.stages.positioning.bev import rightToBev
import os

import wx
import wx.lib.colourselect as csel
#from NPPlan.view.client.main.stages.positioning import rightPanelView
from NPPlan.view.client.main.stages.positioning.rightPanel import *

#import wx.lib.platebtn as platebtn


class rightPanelBackends(object):
    '''def __init__(self, panel):
        """
        @param panel: панель
        @type panel: NPPlan.view.client.main.abstracts.rightPanelContainer.rightPanelContainer
        """
        #self._panel = panel
        #self._panel.renderItem = rightPanelView(self._panel)
        #self._panel.doLayout()
        pass'''

    def setForView(self, type):
        '''
        @deprecated
        @param type:
        @return:
        '''
        print type
        self._panel.renderItem.clearAll()
        if '3d' == type:
            self._panel.renderItem.addPanel('3dVwr', viewerSettings, _('Viewer settings'))
            self._panel.renderItem.addPanel('3dClickParams',
                                            viewerClickSettings,
                                            _('Button handler settings'),
                                            choices=[
                                                '',
                                                'Azimuth',
                                                'Roll',
                                                'Elevation',
                                                'Yaw',
                                                'Pitch',
                                                'RotateX',
                                                'RotateY',
                                                'RotateZ',
                                                ]
            )
            volumes = getVolumeData()
            self._panel.renderItem.addPanel('3dVolumes', volumeMapperView, _('Volumes'), volumesList=volumes.keys())
            vwr = self._panel.renderItem.getViewForId('3dVwr')
            vcl = self._panel.renderItem.getViewForId('3dClickParams')
            vols = self._panel.renderItem.getViewForId('3dVolumes')
            self.volIdToName = {}
            self.doBindings('3dVwr', vwr)
            self.doBindings('3dClickParams', vcl)
            self.doBindings('3dVolumes', vols)
        elif 'bev' == type:
            self._panel.renderItem.addPanel('bevSlices', bevPosition, _('BEV position'))
            self._panel.renderItem.addPanel('bevSetup', bevInformer, _('BEV settings'), size=(-1, 600))
            bSlc = self._panel.renderItem.getViewForId('bevSlices')
            bInf = self._panel.renderItem.getViewForId('bevSetup')
            bInfC = bevBeamParser(bInf)
            self.handler.register('bevBeamParser', bInfC, bInf)
            self.doBindings('bSlc', bSlc)
        self._panel.renderItem.doLayout()
        self._panel.renderItem.collapseAll()

    def setDefaults(self, type, values={}, **kwargs):
        if 'bSlc' == type:
            self.bevSetSliceDefaults(values)

    def bevSetSliceDefaults(self, values={}):
        bSlc = self._panel.renderItem.getViewForId('bevSlices')
        for i in values:
            getattr(bSlc, '_%sText' % i).SetValue(str(values[i]))
            #print 'SLICE POSITION', self.handler.positioningBEV.controller.getSlicesPosition()

    def doBindings(self, type, wxParent):
        """
        @deprecated
        @param type: тип
        @type type: string
        @param wxParent: окно
        @type wxParent: proxy of wx.Panel
        """
        if '3dVwr' == type:
            wxParent._bgPicker.Bind(csel.EVT_COLOURSELECT, self.onSelectColor)
            wxParent.Bind(wx.EVT_CHECKBOX, self.outlineCheck, wxParent._outlineCheck)
        elif 'bSlc' == type:
            wxParent.Bind(wx.EVT_BUTTON, self.aquireSlice, wxParent._btn)
        elif '3dClickParams' == type:
            wxParent.Bind(wx.EVT_CHOICE, self.setLeftXFunction, wxParent.leftX)
            wxParent.Bind(wx.EVT_CHOICE, self.setLeftYFunction, wxParent.leftY)
            wxParent.Bind(wx.EVT_CHOICE, self.setRightXFunction, wxParent.rightX)
            wxParent.Bind(wx.EVT_CHOICE, self.setRightYFunction, wxParent.rightY)
        elif '3dVolumes' == type:
            wxChecks = wxParent.items
            for i in wxChecks:
                self.volIdToName[wxChecks[i].GetId()] = i
                wxParent.Bind(wx.EVT_CHECKBOX, self.setVolumesViewClick, wxChecks[i])
            pass
        pass

    def setVolumesViewClick(self, event):
        print event.GetId()
        self.setVolumesView(self.volIdToName[event.GetId()], event.IsChecked())
        pass

    def setVolumesView(self, volumeName, val):
        if 0 == val:
            print 'uncheck'
            self.handler.positioningThreeDim.controller.vtkHandler.hideVolume(volumeName)
        else:
            self.handler.positioningThreeDim.controller.vtkHandler.showVolume(volumeName)
        pass

    def setLeftXFunction(self, event):
        print 'leftX', event.GetString()
        if 0 == len(event.GetString()):
            val = None
        else:
            val = event.GetString()
        self.handler.positioningThreeDim.controller.vtkHandler.setMouseMoveFunction('leftX', val)
        pass

    def setLeftYFunction(self, event):
        print 'leftY', event.GetString()
        if 0 == len(event.GetString()):
            val = None
        else:
            val = event.GetString()
        self.handler.positioningThreeDim.controller.vtkHandler.setMouseMoveFunction('leftY', val)
        pass

    def setRightXFunction(self, event):
        print 'rightX', event.GetString()
        if 0 == len(event.GetString()):
            val = None
        else:
            val = event.GetString()
        self.handler.positioningThreeDim.controller.vtkHandler.setMouseMoveFunction('rightX', val)

    def setRightYFunction(self, event):
        if 0 == len(event.GetString()):
            val = None
        else:
            val = event.GetString()
        self.handler.positioningThreeDim.controller.vtkHandler.setMouseMoveFunction('rightY', val)
        print 'rightY', event.GetString()

    def aquireSlice(self, event):
        bSlc = self._panel.renderItem.getViewForId('bevSlices')
        axial = bSlc._axialText.GetValue()
        coronal = bSlc._coronalText.GetValue()
        sagittal = bSlc._sagittalText.GetValue()
        print axial, coronal, sagittal
        # @todo: use defaults
        axial = float(axial) if len(axial) > 0 else 20
        coronal = float(coronal) if len(coronal) > 0 else 20
        sagittal = float(sagittal) if len(sagittal) > 0 else 20
        if self.handler.positioningBEV.controller.axial.origin != axial:
            self.handler.positioningBEV.controller.axial.origin = axial
            self.handler.positioningBEV.controller.axial.setCaption(str(axial))
            self.handler.positioningBEV.controller.axial.update()
        if self.handler.positioningBEV.controller.coronal.origin != coronal:
            self.handler.positioningBEV.controller.coronal.origin = coronal
            self.handler.positioningBEV.controller.coronal.setCaption(str(coronal))
            self.handler.positioningBEV.controller.coronal.update()
        if self.handler.positioningBEV.controller.sagittal.origin != sagittal:
            self.handler.positioningBEV.controller.sagittal.origin = sagittal
            self.handler.positioningBEV.controller.sagittal.setCaption(str(sagittal))
            self.handler.positioningBEV.controller.sagittal.update()
        #self.handler.positioningBEV.controller.axial.origin = float(axial) if len(axial) > 0 else 20
        #self.handler.positioningBEV.controller.coronal.origin = float(coronal) if len(coronal) > 0 else 20
        #self.handler.positioningBEV.controller.sagittal.origin = float(sagittal) if len(coronal) > 0 else 20

    def outlineCheck(self, event):
        if event.IsChecked():
            self.handler.positioningThreeDim.controller.vtkHandler.hideOutline()
        else:
            self.handler.positioningThreeDim.controller.vtkHandler.showOutline()

    # @todo: refactor
    def colorFromVtkToWx(self, color):
        newColor = (int(color[0]*255), int(color[1]*255), int(color[2]*255))
        return newColor

    # @todo: refactor
    def colorFromWxToVtk(self, color):
        newColor = (float(color[0])/255, float(color[1])/255, float(color[2])/255)
        return newColor

    def onSelectColor(self, event):
        print event.GetValue()
        color = (event.GetValue()[0], event.GetValue()[1], event.GetValue()[2])
        self.handler.positioningThreeDim.controller.vtkHandler.background = self.colorFromWxToVtk(color)

#    def start(self):
#        self.setForView('3d')
#        pass


class bevBeamParser(baseController):
    def __init__(self, panel):
        """
        @param panel: panel
        @type panel: NPPlan.view.client.main.stages.positioning.rightPanel.bevInformer
        @return:
        """
        print type(panel)
        print 'bevBeamParser init'
        self._panel = panel
        pass

    def start(self):
        print 'bevBeamParser start'
        pass

    def activate(self):
        print 'bevBeamParser activated'

    def addBeamClicked(self, *args, **kwargs):
        print args, kwargs


class rightPanelThreeDimController(baseController, rightPanelBackends):
    def __init__(self, parentC, wxParent):
        """
        Контроллер для правой панели при виде 3D
        @param parentC: родительский контроллер
        @type parentC: NPPlan.controller.client.mainC.stages.positioning.threeDimensional.threeDimensionalController
        @param wxParent:
        @type wxParent: NPPlan.view.client.main.stages.positioning.rightPanel.rightPanelThreeDim
        @return:
        """
        self._controller = parentC
        self._panel = wxParent
        self.volIdToName = {}

    def start(self):
        self._panel.createFoldPanel()

        item = self._panel.appendNewItem(_('Viewer settings'), False)
        viewer = viewerSettings(item)
        viewer._bgPicker.Bind(csel.EVT_COLOURSELECT, self.onSelectColor)
        viewer.Bind(wx.EVT_CHECKBOX, self.outlineCheck, viewer._outlineCheck)
        self._panel._pnl.AddFoldPanelWindow(item, viewer)

        item = self._panel.appendNewItem(_('Button handler settings'), False)
        btnHandler = viewerClickSettings(item, choices=[
                                                        '',
                                                        'Azimuth',
                                                        'Roll',
                                                        'Elevation',
                                                        'Yaw',
                                                        'Pitch',
                                                        'RotateX',
                                                        'RotateY',
                                                        'RotateZ',
                                                        ])
        btnHandler.Bind(wx.EVT_CHOICE, self.setLeftXFunction, btnHandler.leftX)
        btnHandler.Bind(wx.EVT_CHOICE, self.setLeftYFunction, btnHandler.leftY)
        btnHandler.Bind(wx.EVT_CHOICE, self.setRightXFunction, btnHandler.rightX)
        btnHandler.Bind(wx.EVT_CHOICE, self.setRightYFunction, btnHandler.rightY)

        self._panel._pnl.AddFoldPanelWindow(item, btnHandler)

        item = self._panel.appendNewItem(_('Volume picker'), False)
        volumes = getVolumeData()
        vols = volumeMapperView(item, volumesList=volumes.keys())
        wxChecks = vols.items
        for i in wxChecks:
            self.volIdToName[wxChecks[i].GetId()] = i
            vols.Bind(wx.EVT_CHECKBOX, self.setVolumesViewClick, wxChecks[i])
        self._panel._pnl.AddFoldPanelWindow(item, vols)
        #viewer.SetSize((300, 1000))
        pass

    def activate(self, **kwargs):
        self._panel.makeSize()
        print 'active three dim right panel controller with', \
            self._controller, \
            self._controller._parentController, \
            self._panel, \
            type(self._controller), \
            type(self._controller._parentController), \
            type(self._panel)
        pass

    def deactivate(self, **kwargs):
        pass


class rightPanelBevController(baseController, rightPanelBackends):
    def __init__(self, parentC, wxParent):
        """
        Контроллер для правой панели при виде BEV
        @param parentC: родительский контроллер
        @type parentC: NPPlan.controller.client.mainC.stages.positioning.threeDimensional.threeDimensionalController
        @param wxParent:
        @type wxParent: NPPlan.view.client.main.stages.positioning.rightPanel.rightPanelBev
        @return:
        """
        self._controller = parentC
        self._panel = wxParent

    def start(self):
        self._panel.createFoldPanel()

        item = self._panel.appendNewItem(_('Slices positions'), True)
        bevSlices = bevPosition(item)
        self._panel._pnl.AddFoldPanelWindow(item, bevSlices)
        self.bevSlicesView = bevSlices
        self._rightToBev = rightToBev(rightController=self, wxView=self.bevSlicesView)
        self.bevSlicesView.Bind(wx.EVT_BUTTON, self.rightToBev, self.bevSlicesView._btn)
        self.bevSlicesView._axialText.Bind(FS.EVT_FLOATSPIN, self.onSpin)
        self.bevSlicesView._coronalText.Bind(FS.EVT_FLOATSPIN, self.onSpin)
        self.bevSlicesView._sagittalText.Bind(FS.EVT_FLOATSPIN, self.onSpin)

        facilities = [i['rusName'] for i in NPPlan.mongoConnection.facility.find({'level': 1}, {'rusName': 1})]

        item = self._panel.appendNewItem(_('Beams'), False)
        bevBeams = bevInformer(item,
                               cParent=self,
                               columns={
                                   'name': {'text': _('Name'), 'order': 1, 'image': 'iconText16.png'},
                                   'color': {'text': _('Color'), 'order': 0, 'size': 30, 'image': 'iconColors16.png', 'type' : 'bitmap'},
                                   'target': {'text': _('Target'), 'order': 2, 'image': 'iconTarget16.png'},
                                   'source': {'text': _('Source'), 'order': 3, 'image': 'iconStar16.png'}
                               },
                               controls={
                                   'add': {'text': _('Add beam'), 'backend': self.addBeamClicked,
                                           'icon': 'iconPlus32.png'},
                                   'del': {'text': _('Delete beam'), 'backend': self.delBeamClicked,
                                           'icon': 'iconMinus32.png', 'activation': 1},
                                   'save': {'text': _('Save beam'), 'backend': self.saveBeamClicked,
                                            'icon': 'iconSave32.png', 'activation': 1},
                               },
                               controlsPosition='down',
                               facilities=facilities,
                               )
        bevBeams._controller = self
        self._panel._pnl.AddFoldPanelWindow(item, bevBeams)
        for i in [bevBeams.fX, bevBeams.fY, bevBeams.fZ, bevBeams.fR, bevBeams.fFi, bevBeams.fTheta]:
            i.Enable(False)
        self.bevBeamView = bevBeams
        self.bevBeamView.assignController(self)
        bevBeams.Bind(wx.EVT_CHOICE, self.shapeParser, bevBeams.cShape)
        bevBeams.Bind(wx.EVT_CHOICE, self.sizeParser, bevBeams.cSize)
        bevBeams.cShape.SetItems(['Rectangular', 'Circular', 'Multi'])
        bevBeams.cShape.SetSelection(0)
        self.shapeParser()

        self.facilityParamsBind()
        #self._panel._pnl.pnl
        pass

    def facilityParamsBind(self):
        #self.Bind(wx.EVT_CHOICE, self._controller.facilityParamsFacilityChosen, self.ch)
        self.bevBeamView.Bind(wx.EVT_CHOICE, self.facilityParamsFacilityChosen, self.bevBeamView.choiceFacility)

    def facilityParamsFacilityChosen(self, event):
        """

        @param event:
        @type event: wx._core.CommandEvent
        @return:
        """
        print event, type(event), event.GetString(), event.GetSelection()
        data = NPPlan.mongoConnection.facility.find_one({'rusName': event.GetString()})
        img = self.facilityParamsGetImage(data.name)
        self.bevBeamView.updateTip1(data, img)

        pass

    def facilityParamsGetImage(self, name):
        """

        @param name:
        @type name: string
        @return:
        @rtype: wx.Bitmap
        """
        # 0. Check existance
        basedir = os.path.join(NPPlan.getIconPath(), 'facilities')
        if not os.path.exists(basedir) or not os.path.isdir(basedir):
            return wx.Bitmap(os.path.join(NPPlan.getIconPath(), 'noFacility100a.png'))
        # 1. Load wx.Bitmap ( lowername.png )
        fName = os.path.join(basedir, name.lower() + '.png')
        if not os.path.exists(fName):
            return wx.Bitmap(os.path.join(NPPlan.getIconPath(), 'noFacility100a.png'))
        # 2. Rescale
        bmp = wx.Bitmap(fName)
        print bmp.GetSize()
        if bmp.GetSize()[0] <= 100 or bmp.GetSize()[1] <= 100:
            return bmp
        if bmp.GetSize()[0] < bmp.GetSize()[1]:
            newWidth = 100
            newHeight = bmp.GetSize()[1] * 100 / bmp.GetSize()[0]
        else:
            newHeight = 100
            newWidth = bmp.GetSize()[0] * 100 / bmp.GetSize()[1]
        return bmp.ConvertToImage().Scale(newWidth, newHeight, wx.IMAGE_QUALITY_HIGH).ConvertToBitmap()
        pass

    def facilityParamspositionerChosen(self, event):
        pass

    def collectBevBeams(self):
        return {
            'bevName': self.bevBeamView.description.GetValue(),
            'bevColor': self.bevBeamView.cSel.GetValue(),
            'fX': self.bevBeamView.fX.GetValue(),
            'fY': self.bevBeamView.fY.GetValue(),
            'fZ': self.bevBeamView.fZ.GetValue(),
        }

    def populateBevBeams(self, data):
        self.bevBeamView.description.SetValue(data['bevName'])
        self.bevBeamView.cSel.SetValue(data['bevColor'])
        self.bevBeamView.fX.SetValue(data['fX'])
        self.bevBeamView.fY.SetValue(data['fY'])
        self.bevBeamView.fZ.SetValue(data['fZ'])

    def shapeParser(self, event=None):
        # visual
        shape = self.bevBeamView.cShape.GetStringSelection()
        if 'Circular' == shape:
            self.bevBeamView.cSize.SetItems(['4', '5', '6'])
        else:
            self.bevBeamView.cSize.SetItems(['4x4', '5x5', '6x6'])
        self.bevBeamView.cSize.SetSelection(0)

        #vtk
        pass

    def selectionParser(self, event=None, data=None, aType='selection'):
        # @todo: rewrite selection
        print event, data, aType
        if 'selection' == aType:
            print event.m_itemIndex
        index = self.bevBeamView.listWC.GetFirstSelected()
        index2 = self.bevBeamView.listWC.selected
        print index, index2, index == index2
        self.handler.positioningBEV.controller.activateBeam(index)
        pass

    def sizeParser(self, event):
        # visual
        pass

        # vtk
        pass

    def createBmpByColorTuple(self, red, green=None, blue=None):
        if isinstance(red, tuple):
            green = red[1]
            blue = red[2]
            red = red[0]
        bmp = wx.EmptyBitmap(16, 16, 32)
        pixelData = wx.AlphaPixelData(bmp)
        for pixel in pixelData:
            pixel.Set(red, green, blue, 255)
        return bmp

    def addBeamClicked(self, *args, **kwargs):
        print args, kwargs
        #print self.bevBeamView.listWC.getColumnsData()
        #print self.bevBeamView.listWC.getColumnsLength()
        #self.bevBeamView.listWC.addLine([wx.Color(255, 0, 255), '1', '3'])
        bevBeamParams = self.collectBevBeams()
        print bevBeamParams
        #r, g, b, = bevBeamParams['bevColor'][0], bevBeamParams['bevColor'][1], bevBeamParams['bevColor'][2]
        targetStr = 'X: %s\n Y: %s\n Z: %s' % (bevBeamParams['fX'], bevBeamParams['fY'], bevBeamParams['fZ'])
        #self.bevBeamView.listWC.addLine([bevBeamParams['bevColor'], bevBeamParams['bevName'], targetStr])
        newClr = self.createBmpByColorTuple(bevBeamParams['bevColor'].red, bevBeamParams['bevColor'].green,
                                            bevBeamParams['bevColor'].blue)
        self.bevBeamView.listWC.addItem((newClr, bevBeamParams['bevName'], targetStr, ''))
        #self.bevBeamView.listWC.addItem({
        #    'color': bevBeamParams['bevColor'],
        #    'name': bevBeamParams['bevName'],
        #    'target': targetStr,
        #    'source': '',
        #})
        #self.handler.positioningBEV.controller.addBeam(bevBeamParams, id=self.bevBeamView.listWC.getItemCount())
        #self.handler.positioningBEV.controller.addBeam(bevBeamParams, id=self.bevBeamView.listWC.getLastItemIndex())
        if not hasattr(self, '_bid'):
            self._bid = 0
        else:
            self._bid += 1
        self.handler.positioningBEV.controller.addBeam(bevBeamParams, id=self._bid)
        #self.bevBeamView.listWC.setCurrentSelected(-1)

    def delBeamClicked(self, *args, **kwargs):
        pass

    def saveBeamClicked(self, *args, **kwargs):
        pass

    def onSpin(self, event):
        spin = event.GetEventObject()
        print spin
        # @todo: reset increment while non-lock
        if spin == self.bevSlicesView._axialText:
            self._rightToBev.rightToBev(caller='Axial')
        elif spin == self.bevSlicesView._sagittalText:
            self._rightToBev.rightToBev(caller='Sagittal')
        elif spin == self.bevSlicesView._coronalText:
            self._rightToBev.rightToBev(caller='Coronal')
        pass

    def rightToBev(self, event=None):
        self._rightToBev.rightToBev()

    def activate(self, **kwargs):
        self._panel.makeSize()
        #self.handler._panel.assignController(self)
        print 'active beam right panel controller with', \
            self._controller, \
            self._controller._parentController, \
            self._panel, \
            type(self._controller), \
            type(self._controller._parentController), \
            type(self._panel)
        pass

    def deactivate(self, **kwargs):
        pass