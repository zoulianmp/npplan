# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 16.11.12
@summary: 
'''

import NPPlan
from NPPlan.controller.client.main import getHandler
from NPPlan.controller.client.mainC.abstracts import abstractTwoPanels

from mainPanel import mainPanel

class testTwoPanels(abstractTwoPanels):
    def __init__(self, panel):
        abstractTwoPanels.__init__(self, panel)

    def start(self):
        print "LOADING ITEMS mp AND rp"
        from NPPlan.view.client.main.stages.test.mainPanel import mainPanel as mp
        from NPPlan.view.client.main.stages.test.rightPanel import rightPanel as rp
        self._container = NPPlan.panelContainer()
        self.mainPanel = mp(self.__dict__['mainPanel'])
        self.rightPanel = rp(self.__dict__['rightPanel'])

        self._container.rightPanelStart = self.rightPanel


        self.__dict__['child'].doLayout()
        self.layout()

        from NPPlan.controller.client.mainC.stages.test.mainPanel import mainPanel as mpC
        mpObj = mpC(self.mainPanel)
        getHandler().register('testMainPanel', mpObj, self.mainPanel)
        print mpObj.getAssociatedView() == self.mainPanel

    def makeSecondPanel(self, event):
        print event
        if not self._container.hasPanel('rightPanelNew'):
            from NPPlan.view.client.main.stages.test.rightPanel import rightPanelTwo as rp
            sdp = rp(self.__dict__['rightPanel'])
            self._container.rightPanelNew = sdp
        self._container.replace(self.__dict__['rightPanel'].getSizer(), 'rightPanelStart', 'rightPanelNew')
        #self.__dict__['child'].doLayout()
        #self.layout()
        #sdp.SetSize(self.__dict__['rightPanel'].GetSize())
        pass

    def revertFirstPanel(self, event):
        #print event
        self._container.replace(self.__dict__['rightPanel'].getSizer(), 'rightPanelNew', 'rightPanelStart', )
        pass
