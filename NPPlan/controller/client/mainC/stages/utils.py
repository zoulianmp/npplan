# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 26.09.12
@summary: 
'''

import NPPlan
import NPPlan.model.file.configReader.abstractButton as pt
import sys
from NPPlan.controller.client.mainC.abstracts import baseController

import wx

_frame = None
btns = None


class utils(baseController):
    def __init__(self, parent):
        self.__dict__['parent'] = parent

    def _addPanelToPanel(self, panel, newWxClass):
        panel.GetParent().Freeze()
        panel.Freeze()
        pane = newWxClass(panel)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pane, 1, wx.ALIGN_CENTER | wx.EXPAND)
        panel.SetSizer(sizer)
        panel.Layout()
        panel.Thaw()
        panel.GetParent().Thaw()
        panel.setCreated()
        return pane

    def wxEbtViewer(self, panel):
        """

        @param panel:
        @return:
        """
        self.handler.main.view.Freeze()
        NPPlan.config.runningConfig.curSubStageType = 'wxEbtViewer'
        from NPPlan.stuff.wxEbtViewer import mainPanel
        self._addPanelToPanel(panel, mainPanel)
        self.handler.main.view.Thaw()

    def dicomViewer(self, panel):
        self.handler.main.view.Freeze()
        NPPlan.config.runningConfig.curSubStageType = 'dicomViewer'
        from NPPlan.view.client.main.utils.dicomTree.dicomTree import dicomTree
        self._addPanelToPanel(panel, dicomTree)
        self.handler.main.view.Thaw()


def init(frame):
    print 'UTILS'
    global btns, _frame
    _frame = frame
    pt.readConfig('utilsButtons')
    btns = pt.getButtons('utilsButtons')
    print btns
    frame.addButtons(btns, sys.modules[__name__])
    print 'created utils'

def work():
    print 'working utils'
    pass

def newPatient():
    from NPPlan.controller.client.main import getApp
    print 'starting new patient'
    getApp().runPatientsWindow(type='addPatient')

def runDicomTree():
    from NPPlan.controller.client.main import getApp
    from NPPlan.view.client.main.emptyPanel import emptyPanel
    _screens = getApp().getCenter().getScreens()
    if _screens.hasKey('welcome.utils.dicom'):
        pass
    else:
        _screens.add('welcome.utils.dicom', emptyPanel(getApp().getCenter().getBook()))
        #_screens.add('welcome.utils.dicom', emptyPanel(getApp().getCenter().getCurrentScreen()))
        getApp().getCenter().getCurInfo().deleteAllPages()
        getApp().getCenter().getCurInfo().addManyPages(
            [
                'Utils',
                'Dicom Tree'
            ],
            [
                returnUtils,
                returnDicomTree
            ]
        )
        getApp().getCenter().getCurInfo().setCurrentPage(1)
        pass
    pass

def returnUtils():
    print 'returning Utils'
    from NPPlan.controller.client.main import getApp
    print getApp().getCenter().getScreens().list()
    # already created, just show
    getApp().getCenter().shiftCurrentScreen('welcome.utils')
    pass

def returnDicomTree():
    from NPPlan.controller.client.main import getApp
    from NPPlan.controller.client.mainC.utils.dicomTree import init as dTreeInit
    from NPPlan.controller.client.mainC.utils.dicomTree import work as dTreeWork
    screens = getApp().getCenter().getScreens()
    newPan = screens.get('welcome.utils.dicom')
    if not newPan.isCreated():
        from NPPlan.view.client.main.utils.dicomTree.dicomTree import dicomTree
        sz = wx.BoxSizer(wx.VERTICAL)
        dT = dicomTree(newPan)
        sz.Add(dT, 1, wx.EXPAND|wx.ALL)
        newPan.SetSizer(sz)
        newPan.Layout()
        getApp().getCenter().shiftCurrentScreen('welcome.utils.dicom')
        newPan.setCreated()
        dTreeInit(dT)
    else:
        getApp().getCenter().shiftCurrentScreen('welcome.utils.dicom')
        dTreeWork()
        pass

def runMitk():
    pass