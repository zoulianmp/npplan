# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 10.06.13
@summary: 
'''

import NPPlan
import wx
from NPPlan.controller.client.mainC.abstracts import baseController
import NPPlan.view.client.main.stages.configuration as configViews
import NPPlan.model.mongo.kitmodels.organs
from NPPlan.lib.representation import createMcnpRepresentation, createGeantRepresentation
from NPPlan.lib import colorFromVtkToWx, colorFromWxToVtk
import numpy as np
try:
    from agw import pybusyinfo as PBI
except ImportError:
    import wx.lib.agw.pybusyinfo as PBI

try:
    from agw import genericmessagedialog as GMD
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.genericmessagedialog as GMD


class configurationControllerBase(baseController):
    def __init__(self, wxParent=None, *args, **kwargs):
        self._wxParent = wxParent
        self._topSizer = wx.BoxSizer(wx.VERTICAL)
        #self._busy = PBI.PyBusyInfo(_('Please wait'), parent=None, title=_('Working'),)
        #wx.Yield()
        self.handler.main.controller.createBusy()
    pass


class configurationControllerMain(configurationControllerBase):
    def doBindings(self, wxView=None, type='main'):
        if wxView is None:
            return
        if 'main' == type:
            #wxView.Bind(wx.EVT_BUTTON, self.showOrgansConfig, wxView._organsButton)
            #wxView.Bind(wx.EVT_BUTTON, self.showMaterialsConfig, wxView._materialsButton)
            #wxView._topBook.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.onTopPageChanged)
            pass

    def showWarning(self, title, message):
        btnStyle = wx.OK
        dlgStyle = wx.ICON_ERROR
        dlg = GMD.GenericMessageDialog(self._wxParent, message,
                                       title,
                                       btnStyle | dlgStyle)
        dlg.ShowModal()
        dlg.Destroy()

    def onTopPageChanged(self, event):
        """

        @param event:
        @type event: wx.lib.agw.flatnotebook.FlatNotebookEvent
        @return:
        """
        event.Skip()
        print 'topbook event', event, type(event), event.GetSelection()
        event.StopPropagation()

    def onPaintCompleted(self, event):
        """
        Функция, которая вызывается из отображения после окончания отрисовки wx-элементов,
        создаёт и заполняет self.__dict__['controllers'] - перечень контроллеров, отвечающих
        за заполнение соответствующих страниц. В параметры дочерним контроллером передаётся
        ссылка на инстанс текущего контроллера и ссылка на соответствующую wx-панель
        @param event:
        @type event: wx._core.PaintEvent
        @return:
        """
        print 'paint completed'
        self.__dict__['controllers'] = {}
        self.__dict__['controllers']['cs'] = clientServerPostController(self, self._mainView._clientAndServersView)
        self.__dict__['controllers']['organs'] = organsPostController(self, self._mainView._organsView)
        self.__dict__['controllers']['materials'] = {}
        self.__dict__['controllers']['materials']['isotopes'] = isotopesPostController(
            self, self._mainView._matView.isotopesPage
        )
        self.__dict__['controllers']['materials']['materials'] = materialsPostController(
            self, self._mainView._matView.materialsPage
        )
        self.handler.main.controller.deleteBusy()
        #self.__dict__['controllers']['materials']['simulation']
        #self.__dict__['controllers']['materials'] = materialsPostController(self, self._mainView._matView)

    def start(self, **kwargs):
        """
        Создаёт отображение, размечает родительское окно
        @param kwargs:
        @return:
        """
        self._mainView = configViews.mainView(self._wxParent, cParent=self)
        self._topSizer.Add(self._mainView, 0, wx.EXPAND)
        self._wxParent.SetSizerAndFit(self._topSizer)
        self._wxParent.GetSizer().Fit(self._wxParent)
        self._wxParent.Layout()
        self._curWin = self._mainView
        #self._organsController = organsPostController(self, self._mainView._organsView)
        #self._materialsController = materialsPostController(self, self._mainView._matView)
        #self.doBindings(self._mainView, 'main')
        pass

    def showOrgansConfig(self, event):
        """
        @deprecated
        @param event:
        @return:
        """
        print 'Show organs button'
        self.handler.main.controller.createBusy()
        obj = configViews.organsView(self._wxParent)
        self._wxParent.GetParent().Freeze()
        self._wxParent.Freeze()
        self._curWin.Hide()
        self._topSizer.Replace(self._curWin, obj)
        self._wxParent.Thaw()
        self._wxParent.GetParent().Thaw()
        self._curWin = obj
        self._wxParent.GetSizer().Fit(self._wxParent)
        self._wxParent.Layout()
        self.handler.informer.controller.backState(1)
        self.handler.informer.controller.setBackBackend(self.returnMainWin)

        self._curWinController = organsPostController(self, obj)
        self.handler.main.controller.deleteBusy()
        pass

    def showMaterialsConfig(self, event):
        """
        @deprecated
        @param event:
        @return:
        """
        self.handler.main.controller.createBusy()
        print 'Show materials button'
        obj = configViews.materialsView(self._wxParent)
        self._wxParent.GetParent().Freeze()
        self._wxParent.Freeze()
        self._curWin.Hide()
        self._topSizer.Replace(self._curWin, obj)
        self._wxParent.Thaw()
        self._wxParent.GetParent().Thaw()
        self._curWin = obj
        self._wxParent.GetSizer().Fit(self._wxParent)
        self._wxParent.Layout()
        self.handler.informer.controller.backState(1)
        self.handler.informer.controller.setBackBackend(self.returnMainWin)

        self._curWinController = materialsPostController(self, obj)
        self.handler.main.controller.deleteBusy()

    def returnMainWin(self, event=None):
        """
        @deprecated
        @param event:
        @return:
        """
        if 1 != self._curWinController.destroy():
            return
        del self._curWinController
        self._wxParent.Freeze()
        self._curWin.Hide()
        self._topSizer.Replace(self._curWin, self._mainView)
        self._mainView.Show()
        self._curWin.Destroy()
        self._wxParent.Thaw()
        self._curWin = self._mainView
        self._wxParent.GetSizer().Fit(self._wxParent)
        self.handler.informer.controller.backState(0)
        self.handler.informer.controller.setBackBackend(None)
        pass

    def work(self, **kwargs):
        """
        Вызывается при переключении вкладки в основном меню
        @param kwargs:
        @return:
        """
        pass


class isotopesPostController(baseController):
    def __init__(self, parent, wxParent):
        """

        @param parent:
        @type parent: configurationControllerMain
        @param wxParent:
        @type wxParent: NPPlan.view.client.main.stages.configuration.materials.isotopesList
        @return:
        """
        self._cParent = parent
        self._wxParent = wxParent
        self._wxParent.assignController(self)

        self._curItemId = None
        #self._curItem = None   # unused

        self.bindIsotopeList()
        self.populateIsotopeList()

    def bindIsotopeList(self):
        """
        Создаёт бинды для событий окна
        """
        self._wxParent.Bind(wx.EVT_TREE_SEL_CHANGED, self.itemSelected, self._wxParent.tree)
        self._wxParent.Bind(wx.EVT_BUTTON, self.doButtonPressed, self._wxParent._doButton)

    def populateIsotopeList(self):
        """
        Заполняет список изотопов из базы данных, использует wx.SetItemPyData для элементов дерева кэш
        хранилища данных
        @return:
        """
        tree = self._wxParent.tree
        tree.DeleteAllItems()
        root = tree.AddRoot(_('Isotopes'))
        for i in NPPlan.mongoConnection.isotope.find({'pid': '0'}).sort('atomicNum'):
            top = tree.AppendItem(root, '%d: %s' % (i.atomicNum, i.name))
            tree.SetItemPyData(top, i)
            #tree.SetItemPyData(top, i['_id'])
            if 0 == NPPlan.mongoConnection.isotope.find({'pid': i.symbol}).count():
                pass
            else:
                childs = tree.AppendItem(top, '%d%s %s' % (int(i.atomicMass), i.symbol, i.name))
                tree.SetItemPyData(childs, i)
                for j in NPPlan.mongoConnection.isotope.find({'pid': i.symbol}):
                    childs = tree.AppendItem(top, '%d%s %s' % (int(j.atomicMass), i.symbol, j.name))
                    tree.SetItemPyData(childs, j)

        tree.Expand(root)

    def itemSelected(self, event):
        """
        Событые, срабатывающее по выделению элемента списка изотопов
        Заполняет поля данных, берет из tree.GetItemPyData(), также заполняет self._curItemId если выбранный элемент
        является изотопом
        @param event:
        @type event: wx._controls.TreeEvent
        @return:
        """
        #print 'itemSelected', event, type(event)
        #print event.GetId(), event.GetItem(), self._wxParent.isotopesPage.tree.GetItemPyData(event.GetItem())
        #_id = self._wxParent.isotopesPage.tree.GetItemPyData(event.GetItem())
        data = self._wxParent.tree.GetItemPyData(event.GetItem())
        if data is None:
            self._curItemId = None
            self._curItem = None
            return
        #data = NPPlan.mongoConnection.isotope.find_one({'_id': _id})
        panel = self._wxParent
        self._curItemId = data['_id']
        #self._curItem = event.GetItem()   #unused
        print 'current isotope selected', self._curItemId
        panel._atomicMass.SetValue(str(data['atomicMass']))
        panel._atomicNum.SetValue(str(data['atomicNum']))
        panel._description.SetValue(data['description'])
        panel._name.SetValue(data['name'])
        panel._name2.SetValue(data['rusName'])
        panel._symbol.SetValue(data['symbol'])
        panel._geantString.SetValue(createGeantRepresentation(data, dtype='isotope'))
        panel._mcnpString.SetValue(createMcnpRepresentation(data, dtype='isotope'))
        if 'library' in data['mcnpSimData']:
            self._wxParent._mcnpLibrary.SetValue(data['mcnpSimData']['library'])
        else:
            self._wxParent._mcnpLibrary.SetValue('')
        event.Skip()
        pass

    def doButtonPressed(self, event):
        """
        Событие по нажатию кнопки do, сохраняет изменённые данные в БД
        @param event:
        @type event: wx._core.CommandEvent
        @return:
        """
        print event, type(event)
        event.Skip()
        if self._curItemId is None:
            return

        # 1. saving to database
        isotopeItem = NPPlan.mongoConnection.isotope.find_one({'_id': self._curItemId})
        isotopeItem.rusName = unicode(self._wxParent._name2.GetValue())
        isotopeItem.mcnpSimData['library'] = unicode(self._wxParent._mcnpLibrary.GetValue())
        isotopeItem.save()

        # 2. updating current tree item
        #self._wxParent.tree.SetItemPyData(self._curItem, isotope)  # приводит к вылету приложения ??


class materialsPostController(baseController):
    def __init__(self, parent, wxParent):
        """

        @param parent:
        @type parent: configurationControllerMain
        @param wxParent:
        @type wxParent: NPPlan.view.client.main.stages.configuration.materials.materialsList
        @return:
        """
        self._cParent = parent
        self._wxParent = wxParent
        self._wxParent.assignController(self)

        self._curItemId = None
        self._curCMap = []
        #self._curItem = None   # unused

        self.bindMaterialsList()
        self.populateMaterialsList()

    def bindMaterialsList(self):
        self._wxParent.list.Bind(wx.EVT_LISTBOX, self.itemSelected)
        self._wxParent.Bind(wx.EVT_BUTTON, self.previewPressed, self._wxParent._previewButton)
        self._wxParent.Bind(wx.EVT_BUTTON, self.savePressed, self._wxParent._saveButton)
        self._wxParent.Bind(wx.EVT_BUTTON, self.addIsotope2CmapPressed, self._wxParent.addIsotopeButton)
        self._wxParent.Bind(wx.EVT_BUTTON, self.addNewMatPressed, self._wxParent._addMatButton)
        pass

    def addNewMatPressed(self, event):
        """

        @param event:
        @type event: wx._core.CommandEvent
        @return:
        """
        # 0. Проверяем, было ли сохранено предыдщее изменение
        # 1. Удаляем
        self._curCMap = []
        self._curItemId = None
        self._wxParent.list.SetSelection(-1)
        self._wxParent._name.SetValue('')
        self._wxParent._rusName.SetValue('')
        self._wxParent._density.SetValue('')
        self._wxParent._densityUnits.SetSelection(0)
        self._wxParent._isotope.SetSelection(0)
        self._wxParent._proportion.SetValue('')
        self._wxParent._isotopesList.DeleteAllItems()
        self._wxParent._ctHighValue.SetValue('')
        self._wxParent._ctLowValue.SetValue('')
        self._wxParent._contourIntValue.SetValue('')
        self._wxParent._mcnpMatString.SetValue('')
        self._wxParent._mcnpCellRepresentation.SetValue('')
        self._wxParent._geantString.SetValue('')

    def addIsotope2CmapPressed(self, event):
        """

        @param event:
        @type event: wx._core.CommandEvent
        @return:
        """
        if 0 == len(self._wxParent._proportion.GetValue()):
            self._cParent.showWarning(_('Some fields are required'), _('Proportion field is empty'))
            event.Skip()
            return
            pass
        if -1 == self._wxParent._isotope.GetSelection():
            self._cParent.showWarning(_('Some fields are required'), _('Isotope field is empty'))
            event.Skip()
            return
            pass
        isotope = self._wxParent._isotope.GetClientData(self._wxParent._isotope.GetSelection())
        proportion = float(self._wxParent._proportion.GetValue()) / 100.0
        print isotope, proportion
        self._wxParent._isotopesList.Append(
            ['%d/%s' % (isotope['atomicNum'], isotope['symbol']),
             '%02.2f' % (float(proportion) * 100.0)]
        )
        self._curCMap.append((isotope, proportion,))
        #cIsotope = self._wxParent._isotopesList.GetItem(self._wxParent._isotopesList.GetItemS)
        pass

    def cmapToView(self):
        #self._wxParent._isotopesList.ClearAll()
        self._wxParent._isotopesList.DeleteAllItems()
        for i in self._curCMap:
            self._wxParent._isotopesList.Append(
                ['%d/%s' % (i[0]['atomicNum'], i[0]['symbol']),
                 '%02.2f' % (float(i[1]) * 100.0)]
            )
            pass

    def previewPressed(self, event):
        """

        @param event:
        @type event: wx._core.CommandEvent
        @return:
        """
        print event
        a = createGeantRepresentation({
            'name': self._wxParent._name.GetValue(),
            'density': float(self._wxParent._density.GetValue()),
            'densityUnits': self._wxParent._densityUnits.GetSelection(),
            'cmap': self._curCMap
        }, dtype='material')
        print a
        self._wxParent._geantString.SetValue(a)
        b = createMcnpRepresentation({'density': float(self._wxParent._density.GetValue()),
                                      'densityUnits': self._wxParent._densityUnits.GetSelection()}, dtype='cell')
        self._wxParent._mcnpCellRepresentation.SetValue(b)
        c = createMcnpRepresentation({'cmap': self._curCMap}, dtype='material')
        self._wxParent._mcnpMatString.SetValue(c)
        pass

    def savePressed(self, event):
        """

        @param event:
        @type event: wx._core.CommandEvent
        @return:
        """
        if self._curItemId is not None:
            material = NPPlan.mongoConnection.material.find_one({'_id': self._curItemId})
        else:
            material = NPPlan.mongoConnection.material()
        material.name = unicode(self._wxParent._name.GetValue())
        material.rusName = unicode(self._wxParent._rusName.GetValue())
        material.density = float(self._wxParent._density.GetValue())
        material.densityUnits = self._wxParent._densityUnits.GetSelection()
        material.cmap = self._curCMap
        material.save()
        pass

    def itemSelected(self, event):
        """

        @param event:
        @type event: wx._core.CommandEvent
        @return:
        """
        print 'matItem', type(event), event
        event.Skip()
        print self._wxParent.list.GetStringSelection()
        data = NPPlan.mongoConnection.material.find_one({'name': self._wxParent.list.GetStringSelection()})
        self._curItemId = data['_id']
        self._wxParent._name.SetValue(data['name'])
        self._wxParent._rusName.SetValue(data['rusName'])
        self._wxParent._density.SetValue('%2.5f' % data['density'])
        try:
            self._wxParent._densityUnits.SetSelection(data['densityUnits'])
        except KeyError:
            self._wxParent._densityUnits.SetSelection(0)
        self._curCMap = data['cmap']
        self.cmapToView()
        a = createGeantRepresentation({
            'name': self._wxParent._name.GetValue(),
            'density': float(self._wxParent._density.GetValue()),
            'densityUnits': self._wxParent._densityUnits.GetSelection(),
            'cmap': self._curCMap
        }, dtype='material')
        print a
        self._wxParent._geantString.SetValue(a)
        b = createMcnpRepresentation({'density': float(self._wxParent._density.GetValue()),
                                      'densityUnits': self._wxParent._densityUnits.GetSelection()}, dtype='cell')
        self._wxParent._mcnpCellRepresentation.SetValue(b)
        c = createMcnpRepresentation({'cmap': self._curCMap}, dtype='material')
        self._wxParent._mcnpMatString.SetValue(c)
        pass

    def populateMaterialsList(self):
        # 1. заполнение списка доступных изотопов
        choices = self._wxParent._isotope
        choices.Clear()
        for i in NPPlan.mongoConnection.isotope.find().sort('atomicNum'):
            iid = choices.Append('%d: %s' % (i.atomicNum, i.name), i)
            choices.SetClientData(iid, i)

        # 2. заполнение списка материалов
        self._wxParent.list.Clear()
        for i in NPPlan.mongoConnection.material.find().sort('name'):
            self._wxParent.list.Append('%s' % i.name)


class organsPostController(baseController):
    def __init__(self, parent, wxParent):
        """

        @param parent:
        @type parent: configurationControllerMain
        @param wxParent:
        @type wxParent: NPPlan.view.client.main.stages.configuration.organs.organsView
        @return:
        """
        self._cParent = parent
        self._wxParent = wxParent
        self._wxParent.assignController(self)

        self.tree = self._wxParent.tree

        self.tree.AddColumn(_('Name/rusname'))
        self.tree.AddColumn(_('Active/allowed'))
        self.tree.AddColumn(_('uid'))
        self.tree.AddColumn(_('contourData'))
        self.tree.AddColumn(_('simulationData'))
        self.tree.SetMainColumn(0)
        self.tree.SetColumnWidth(0, 175)

        self.createTreeItems()
        self._wxParent.Bind(wx.EVT_TREE_ITEM_ACTIVATED, self.itemSelected, self.tree)
        self._wxParent.Bind(wx.EVT_TREE_SEL_CHANGED, self.itemSelected, self.tree)
        self._wxParent.Bind(wx.EVT_BUTTON, self.saveEntry, self._wxParent._editButton)
        #self._wxParent.Bind(wx.EVT_PAINT, self.loadFromDatabase)
        self.loadFromDatabase()

        self.tree.Expand(self.root)
        self.tree.Expand(self.contourRoot)
        self._wxParent.clearItemsValues()

    def itemSelected(self, event):
        if event is None:
            return
        print 'itemSelected', event
        self.tree.GetItemText(event.GetItem())
        print self.tree.GetItemPyData(event.GetItem())
        self._curItemDbId = None
        item = self.tree.GetItemPyData(event.GetItem())
        if 'item' in item and 'contourRoot' == item['item']:
            self._wxParent._group.SetSelection(0)
            self._wxParent.clearItemsValues()
        elif 'item' in item and 'segmentRoot' == item['item']:
            self._wxParent._group.SetSelection(1)
            self._wxParent.clearItemsValues()
        elif hasattr(item, '_id'):
            self._curItemDbId = self.tree.GetItemPyData(event.GetItem())['_id']
            print 'parsing db entry to wx'
            print item, item.name, item.name.originalName
            self._wxParent._rusName.SetValue(unicode(item.name.originalName))
            self._wxParent._originalName.SetValue(unicode(item.name.latinName))
            self._wxParent._color.SetValue(colorFromVtkToWx(item.contourData.color, 'list'))
            self._wxParent._enabled.SetValue(item.active)
            self._wxParent._opacity.SetValue('%d' % (int(item.contourData.opacity * 100)))
            self._wxParent._uid.SetValue(unicode(item.uid))
            pass
        event.Skip()

    def saveEntry(self, event):
        if self._curItemDbId is None:
            entry = NPPlan.mongoConnection.organs()
        else:
            entry = NPPlan.mongoConnection.organs.find_one({'_id': self._curItemDbId})
        entry.name.latinName = self._wxParent._originalName.GetValue()
        entry.name.originalName = self._wxParent._rusName.GetValue()
        color = self._wxParent._color.GetValue()
        entry.contourData.color = colorFromWxToVtk(color, 'list')
        entry.contourData.opacity = float(self._wxParent._opacity.GetValue()) / 100
        entry.pid = str(self._wxParent._group.GetSelection() + 1)
        entry.active = self._wxParent._enabled.GetValue()
        entry.save()
        self.tree.DeleteAllItems()
        self.createTreeItems()
        self.loadFromDatabase()
        self.tree.Expand(self.root)
        self.tree.Expand(self.contourRoot)
        self._wxParent.clearItemsValues()
        pass

    def createTreeItems(self):
        self.root = self.tree.AddRoot(_("Organs tree root"))
        self.tree.SetItemPyData(self.root, {'item': 'root'})
        self.contourRoot = self.tree.AppendItem(self.root, _('Contouring organs'))
        self.tree.SetItemPyData(self.contourRoot, {'item': 'contourRoot'})
        self.segmentRoot = self.tree.AppendItem(self.root, _('Autosegmentation organs'))
        self.tree.SetItemPyData(self.segmentRoot, {'item': 'segmentRoot'})

    def loadFromDatabase(self, event=None):
        for i in NPPlan.mongoConnection.organs.find():
            print i, i.name.latinName, i.pid
            if 1 == int(i.pid):
                container = self.contourRoot
            elif 2 == int(i.pid):
                container = self.segmentRoot
            treeItem = self.tree.AppendItem(container, '%s/%s' % (i.name.latinName, i.name.originalName))
            self.tree.SetItemText(treeItem, _('True') if 1 == i.active else _('False'), 1)
            if i.uid is not None:
                self.tree.SetItemText(treeItem, str(i.uid), 2)
            self.tree.SetItemPyData(treeItem, i)

        pass
        if event is not None:
            event.Skip()

    def destroy(self):
        return 1


class materialsPostControllerOld(baseController):
    """
    @deprecated
    """
    def __init__(self, parent, wxParent):
        """

        @param parent:
        @type parent: configurationControllerMain
        @param wxParent:
        @type wxParent: NPPlan.view.client.main.stages.configuration.materials.materialsView
        @return:
        """
        self._cParent = parent
        self._wxParent = wxParent

        self._curCMap = []

        self.bindNbEvents()
        #self._wxParent.nb.SetSelection(0)
        #self._wxParent.nb.ChangeSelection(0)
        self.onPageChanged(None)
        if 0 == NPPlan.mongoConnection.isotope.find().count():
            self._createInitialData()
        #else:
        #    self.populateIsotopeList()
        #    self.bindIsotopeList()

            #self.populateMaterialsChoices()
            #self.populateMaterialsList()
            #self.bindMaterialsPanel()

    def destroy(self):
        return 1

    def bindNbEvents(self):
        #self._wxParent.nb.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.onPageChanged)
        self._wxParent.nb.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.onPageChanged)

    def onPageChanged(self, event):
        """

        @param event:
        @type event: wx._controls.NotebookEvent
        @return:
        """
        if event is None:
            self.populateIsotopeList()
            self.bindIsotopeList()
            return
        event.Skip()
        print 'event page changing', event, type(event)
        print event.OldSelection, event.Selection
        if 1 == event.GetSelection():
            self.unbindIsotopeList()
            self.populateMaterialsChoices()
            self.populateMaterialsList()
            self.bindMaterialsPanel()
        if 0 == event.GetSelection():
            self.unbindMaterialsPanel()
            self.populateIsotopeList()
            self.bindIsotopeList()
        event.StopPropagation()
        pass

    def _createInitialData(self):
        print NPPlan.config.appPath
        import json
        matFile = open(NPPlan.config.appPath+'/stuff/mat.json', 'r')

        u = json.load(matFile, )

        for i in u:
            isotope = NPPlan.mongoConnection.isotope()
            isotope.name = u[i][u'Name']
            isotope.symbol = i
            isotope.atomicMass = u[i][u'Atomic mass']
            isotope.atomicNum = u[i][u'Atomic no']
            isotope.pid = '0'
            isotope.save()

        #H = NPPlan.mongoConnection.isotope()
        #H.name = 'Hydrogen'
        #H.rusName = u'Водород'
        #H.symbol = 'H'
        #H.pid = '0'
        #H.atomicNum = 1
        #H.atomicMass = 1.00794
        #H.save()

        D = NPPlan.mongoConnection.isotope()
        D.name = 'Deuterium'
        D.rusName = u'Дейтерий'
        D.symbol = 'D'
        D.pid = 'H'
        D.atomicNum = 1
        D.atomicMass = 2.0141017778
        D.save()

        T = NPPlan.mongoConnection.isotope()
        T.name = 'Tritium'
        T.rusName = u'Тритий'
        T.symbol = 'T'
        T.pid = 'H'
        T.atomicNum = 1
        T.atomicMass = 3.0160492777
        T.save()

    def bindIsotopeList(self):
        self._wxParent.isotopesPage.Bind(wx.EVT_TREE_SEL_CHANGED, self.itemSelected, self._wxParent.isotopesPage.tree)

    def unbindIsotopeList(self):
        self._wxParent.isotopesPage.Unbind(wx.EVT_TREE_SEL_CHANGED, self._wxParent.isotopesPage.tree)

    def itemSelected(self, event):
        event.Skip()
        #print event.GetId(), event.GetItem(), self._wxParent.isotopesPage.tree.GetItemPyData(event.GetItem())
        #_id = self._wxParent.isotopesPage.tree.GetItemPyData(event.GetItem())
        data = self._wxParent.isotopesPage.tree.GetItemPyData(event.GetItem())
        if data is None:
            return
        #data = NPPlan.mongoConnection.isotope.find_one({'_id': _id})
        panel = self._wxParent.isotopesPage
        panel._atomicMass.SetValue(str(data['atomicMass']))
        panel._atomicNum.SetValue(str(data['atomicNum']))
        panel._description.SetValue(data['description'])
        panel._name.SetValue(data['name'])
        panel._name2.SetValue(data['rusName'])
        panel._symbol.SetValue(data['symbol'])
        print self.createGeantRepresentation(data)
        panel._geantString.SetValue(self.createGeantRepresentation(data))
        panel._mcnpString.SetValue(self.createMcnpRepresentation(data))
        pass

    def createGeantRepresentation(self, data, dtype='isotope'):
        if 'isotope' == dtype:
            return '  G4Element* el%s = new G4Element( name = "%s", \n'\
                   '                           symbol = "%s",\n' \
                   '                           z = %d.0, a = %2.3f * g/mole );' %(
                data['symbol'], data['name'], data['symbol'], data['atomicNum'], data['atomicMass'],
            )
        elif 'material' == dtype:
            s = '  G4Material* %s = new G4Material( "%s", \n' \
            '                               density = %0.3f*g/cm3,\n' \
            '                               numberofElements = %d );\n' % (
                data['name'].lower(),
                data['name'],
                data['density'],
                len(data['cmap']),
            )
            for i in data['cmap']:
                s = '%s\n%s->AddElement(el%s,%0.3f);' % (s, data['name'].lower(), i[0]['symbol'], i[1])
            return s

    def createMcnpRepresentation(self, data, dtype='isotope'):
        if 'isotope' == dtype:
            return '%d%003d' %(data['atomicNum'], int(np.round(data['atomicMass'])))
        elif 'material' == dtype:
            s = 'm%num% '
            for i in data['cmap']:
                s = '%s %d%003d -%0.6f &\n' % (s, i[0]['atomicNum'], int(np.round(i[0]['atomicMass'])), i[1])
            return s[:-2]
        elif 'cell' == dtype:
            return '%cell% %num% ' + '-%0.6f' % data['density'] + ' %topcell% u=%univ%'
            pass

    def populateIsotopeList(self):
        tree = self._wxParent.isotopesPage.tree
        tree.DeleteAllItems()
        root = tree.AddRoot(_('Isotopes'))
        for i in NPPlan.mongoConnection.isotope.find({'pid': '0'}).sort('atomicNum'):
            top = tree.AppendItem(root, '%d: %s' % (i.atomicNum, i.name))
            tree.SetItemPyData(top, i)
            #tree.SetItemPyData(top, i['_id'])
            if 0 == NPPlan.mongoConnection.isotope.find({'pid': i.symbol}).count():
                pass
            else:
                childs = tree.AppendItem(top, '%d%s %s' % (int(i.atomicMass), i.symbol, i.name))
                tree.SetItemPyData(childs, i)
                for j in NPPlan.mongoConnection.isotope.find({'pid': i.symbol}):
                    childs = tree.AppendItem(top, '%d%s %s' % (int(j.atomicMass), i.symbol, j.name))
                    tree.SetItemPyData(childs, j)
                    #tree.SetItemPyData(childs, j['_id'])

        tree.Expand(root)

    def populateMaterialsChoices(self):
        choices = self._wxParent.materialsPage._isotope
        choices.Clear()
        for i in NPPlan.mongoConnection.isotope.find().sort('atomicNum'):
            choices.Append('%d: %s' % (i.atomicNum, i.name), i)

    def bindMaterialsPanel(self):
        panel = self._wxParent.materialsPage
        panel.Bind(wx.EVT_BUTTON, self.addIsotopeToMap, panel.addIsotopeButton)
        panel.Bind(wx.EVT_BUTTON, self.previewCMap, panel._previewButton)
        panel.Bind(wx.EVT_BUTTON, self.saveCMap, panel._saveButton)

    def unbindMaterialsPanel(self):
        panel = self._wxParent.materialsPage
        panel.Unbind(wx.EVT_BUTTON, panel.addIsotopeButton)
        panel.Unbind(wx.EVT_BUTTON, panel._previewButton)
        panel.Unbind(wx.EVT_BUTTON, panel._saveButton)

    def previewCMap(self, event):
        '''
G4Material* lungexhale = new G4Material( "LungExhale",
                             density = 0.508*g/cm3,
                             numberofElements = 9 );
lungexhale->AddElement(elH,0.103);
lungexhale->AddElement(elC,0.105);
lungexhale->AddElement(elN,0.031);
lungexhale->AddElement(elO,0.749);
lungexhale->AddElement(elNa,0.002);
lungexhale->AddElement(elP,0.002);
lungexhale->AddElement(elS,0.003);
lungexhale->AddElement(elCl,0.002);
lungexhale->AddElement(elK,0.003);        '''
        panel = self._wxParent.materialsPage
        s = self.createGeantRepresentation({
            'name': panel._name.GetValue(),
            'density': float(panel._density.GetValue()),
            'cmap': self._curCMap
        }, dtype='material')
        panel._geantString.SetValue(s)
        s = self.createMcnpRepresentation({
                    'name': panel._name.GetValue(),
                    'density': float(panel._density.GetValue()),
                    'cmap': self._curCMap
                }, dtype='material')
        panel._mcnpMatString.SetValue(s)
        s = self.createMcnpRepresentation({
                    'name': panel._name.GetValue(),
                    'density': float(panel._density.GetValue()),
                    'cmap': self._curCMap
                }, dtype='cell')
        panel._mcnpCellRepresentation.SetValue(s)
        pass

    def saveCMap(self, event):
        panel = self._wxParent.materialsPage
        mat = NPPlan.mongoConnection.material()
        mat.name = panel._name.GetValue() or ''
        mat.rusName = panel._name.GetValue() or u''
        print self._curCMap
        mat.density = float(panel._density.GetValue())
        mat.cmap = self._curCMap
        mat.save()
        pass

    def showWarning(self, title, message):
        btnStyle = wx.OK
        dlgStyle = wx.ICON_ERROR
        dlg = GMD.GenericMessageDialog(self._wxParent, message,
                                       title,
                                       btnStyle | dlgStyle)
        dlg.ShowModal()
        dlg.Destroy()

    def addIsotopeToMap(self, event):
        """
        @param event:
        @type event: wx._core.CommandEvent
        @return:
        """
        if 0 == len(self._wxParent.materialsPage._proportion.GetValue()):
            self.showWarning(_('Some fields are required'), _('Proportion field is empty'))
            event.Skip()
            return
            pass
        if -1 == self._wxParent.materialsPage._isotope.GetSelection():
            self.showWarning(_('Some fields are required'), _('Isotope field is empty'))
            event.Skip()
            return
            pass
        print event, event.GetId()
        print event.GetSelection()
        print event.GetClientData()
        print event.GetClientObject()
        print event.GetString()
        isotope = self._wxParent.materialsPage._isotope.GetClientData(self._wxParent.materialsPage._isotope.GetSelection())
        print 'Chosen isotope', isotope
        proportion = float(self._wxParent.materialsPage._proportion.GetValue()) / 100.0
        print '%d/%s' % (isotope['atomicNum'], isotope['name'])
        print '%02f' % proportion
        self._wxParent.materialsPage._isotopesList.Append(
            ['%d/%s' % (isotope['atomicNum'], isotope['symbol']),
             '%02.2f' % (float(proportion) * 100.0)]
        )
        self._curCMap.append([isotope, proportion])
        pass

    def populateMaterialsList(self):
        self._wxParent.materialsPage.list.Clear()
        for i in NPPlan.mongoConnection.material.find().sort('name'):
            self._wxParent.materialsPage.list.Append('%s' % i.name)


