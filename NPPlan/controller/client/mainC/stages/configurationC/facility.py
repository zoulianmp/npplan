# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 13.09.13
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController
import wx


class facilityConfigurationController(baseController):
    def __init__(self, cParent, wxParent):
        """

        @param cParent:
        @type cParent:
        @param wxParent:
        @type wxParent: NPPlan.view.client.main.stages.configuration.facility.facilityView
        @return:
        """
        self._cParent = cParent
        self._wxParent = wxParent

        self.makeSourcesTree()
        self.doBindings()

    def doBindings(self):
        self._wxParent.Bind(wx.EVT_TREE_SEL_CHANGED, self.sourceChanged, self._wxParent.sourcesTree)

    def sourceChanged(self, event):
        item = event.GetItem()
        print event.GetItem()
        print self._wxParent.sourcesTree.GetItemText(item)
        print self._wxParent.sourcesTree.GetItemPyData(item)
        data = self._wxParent.sourcesTree.GetItemPyData(item)
        if data is not None:
            self.setData(data)
        pass

    def makeSourcesTree(self):
        a = NPPlan.mongoConnection.facility.find()
        self.root = self._wxParent.sourcesTree.AddRoot("Sources")
        l0 = {}
        for i in a:
            if 0 == i.level:
                l0[i.name] = self._wxParent.sourcesTree.AppendItem(self.root, i.rusName)
            else:
                d = self._wxParent.sourcesTree.AppendItem(l0[i.pName], i.rusName)
                self._wxParent.sourcesTree.SetItemPyData(d, i)
            print i
        #child = self._wxParent.sourcesTree.AppendItem(self.root, 'Neutron 14 MeV')
        #child2a = self._wxParent.sourcesTree.AppendItem(child, 'NG-14')
        #self._wxParent.sourcesTree.SetItemPyData(child2a, {'abc': 'abc'})
        #child2b = self._wxParent.sourcesTree.AppendItem(child, 'NG-24')
        #child2c = self._wxParent.sourcesTree.AppendItem(child, 'ING-031')

        #childx = self._wxParent.sourcesTree.AppendItem(self.root, 'Proton')
        #childx2a = self._wxParent.sourcesTree.AppendItem(childx, 'Balakin (synchrotron)')

        #childy = self._wxParent.sourcesTree.AppendItem(self.root, 'General neutron')
        #childy2a = self._wxParent.sourcesTree.AppendItem(childy, 'WWRC')
        #childy2b = self._wxParent.sourcesTree.AppendItem(childy, 'BR-10')

        #childz = self._wxParent.sourcesTree.AppendItem(self.root, 'Gamma')
        #childz2a = self._wxParent.sourcesTree.AppendItem(childz, 'General gamma source')


        pass

    def setData(self, data):
        panel = self._wxParent.sourcesData
        panel._mcnpData.SetValue('')
        panel._geantData.SetValue('')
        panel._data.SetValue('')
        panel._highEnergy.SetValue('')
        panel._lowEnergy.SetValue('')
        panel._name.SetValue(data['name'])
        panel._rusName.SetValue(data['rusName'])
        if 'n' == data['data']['mainParticleType']:
            panel._particleType.SetValue('neutron')
        elif 'p' == data['data']['mainParticleType']:
            panel._particleType.SetValue('proton')
        elif 'g' == data['data']['mainParticleType']:
            panel._particleType.SetValue('gamma')
        elif 'i' == data['data']['mainParticleType']:
            panel._particleType.SetValue('ion')
        if isinstance(data['data']['mainParticleEnergy'], list) and len(data['data']['mainParticleEnergy']) >= 2:
            low, high = data.data.mainParticleEnergy
            if 'energyDim' in data['data']['extraParticlesData']:
                panel._lowEnergy.SetValue('%.2f %s' % (low, data['data']['extraParticlesData']['energyDim']))
                panel._highEnergy.SetValue('%.2f %s' % (high, data['data']['extraParticlesData']['energyDim']))
        if 'fluence' in data['data']['extraParticlesData']:
            panel.updateDataValue('Fluence: %.3f 1/s' % data['data']['extraParticlesData']['fluence'])
        if 'markRadius' in data['data']['extraParticlesData']:
            panel.updateDataValue('Mark radius: %.2f mm' % data['data']['extraParticlesData']['markRadius'])
        if 'template' in data['mcnpData'] and data['mcnpData']['template'] is not None \
             and len(data['mcnpData']['template']) > 1:
            panel.updateMcnpDataValue('Template: %s' % data['mcnpData']['template'])
        if 'fieldSize' in data['data']['extraParticlesData']:
            panel.updateDataValue('Field size: %2.2f %s' %
                                  (data['data']['extraParticlesData']['fieldSize'],
                                   data['data']['extraParticlesData']['fieldDim'])
            )
        if 'ionType' in data['data']['extraParticlesData']:
            panel.updateDataValue('Ion type: %s' % data['data']['extraParticlesData']['ionType'])

    def work(self, *args, **kwargs):
        pass