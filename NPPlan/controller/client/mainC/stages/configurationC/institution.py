# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 06.05.14
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController
from NPPlan.model.alch.configuration.institution import institutionModel
import datetime


class institutionController(baseController):
    def __init__(self, wxParent, cParent):
        #print 'FETCHING', NPPlan.coreSession.query(institutionModel).count()
        if 0 == NPPlan.coreSession.query(institutionModel).count():
            # @todo: refactor
            m = institutionModel('',
                                 u'Федеральное государственное бюджетное учреждение «Медицинский радиологический научный центр» '
                                 u'Министерства здравоохранения Российской Федерации (ФГБУ МРНЦ Минздрава России)',
                                 u'249036, Калужская область, г.Обнинск, ул.Королева, 4',
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 '',
                                 '(48439) 9-30-25',
                                 'mrrc@mrrc.obninsk.ru',
                                 datetime.datetime.now()
                                 )
            NPPlan.coreSession.add(m)
            NPPlan.coreSession.commit()
        else:
            m = NPPlan.coreSession.query(institutionModel).all()[0]
            print m, type(m)
            wxParent.hEmk2.SetValue(m.name)
            wxParent.hEmk11.SetValue(m.phone)
            pass
    pass