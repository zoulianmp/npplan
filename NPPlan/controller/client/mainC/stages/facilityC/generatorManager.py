# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 29.07.13
@summary: 
'''

import NPPlan
from NPPlan.controller.client.mainC.abstracts import baseController
from NPPlan.controller.client.mainC.utils.generatorManager.main import generatorPanel


class generatorManagerBase(baseController):
    def __init__(self, wxParent):
        self._wxParent = wxParent

        self._generatorPanel = generatorPanel(self._wxParent)
        self._generatorPanel.Layout()
        self._generatorPanel.SetSize((800, 600))
        self._generatorPanel.initManagerPanel()