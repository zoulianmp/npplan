# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 30.07.13
@summary: 
'''

import NPPlan
import wx
from NPPlan.controller.client.mainC.abstracts import baseController
from NPPlan.view.client.main.stages.welcomeScreen import welcomeScreen as welcomeScreenForm


class main(baseController):
    def __init__(self, parent):
        self.__dict__['parent'] = parent

    def _addPanelToPanel(self, panel, newWxClass):
        panel.GetParent().Freeze()
        panel.Freeze()
        pane = newWxClass(panel)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pane, 1, wx.ALIGN_CENTER | wx.EXPAND)
        panel.SetSizer(sizer)
        panel.Layout()
        panel.Thaw()
        panel.GetParent().Thaw()
        panel.setCreated()
        return pane

    def patdata(self, panel):
        from NPPlan.controller.client.mainC.stages.planning import emptyPlanController
        if not panel.isCreated():
            planObj = emptyPlanController(wxParent=panel)
            self.handler.register('planning', planObj, panel)
            panel.setCreated()
        else:
            self.handler.planning.controller.work()
        pass

    def contouring(self, panel):
        NPPlan.config.runningConfig.curSubStage = 'contouring'
        from NPPlan.controller.client.mainC.stages.contouring import contouringController
        if not contouringController.checkRequirements():
            pass
            return
        if not panel.isCreated():
            self.handler.main.controller.createBusy()
            contObj = contouringController(panel)
            self.handler.register('contouring', contObj, panel)
            panel.setCreated()
            self.handler.main.controller.deleteBusy()
            pass
        else:
            self.handler.contouring.controller.work()

    def positioning(self, panel):
        NPPlan.config.runningConfig.curSubStage = 'positioning'
        from NPPlan.controller.client.main import getApp as getWxApp
        #from NPPlan.controller.client.main import getHandler as getHandler
        from NPPlan.controller.client.mainC.stages.positioning import positioningController
        if not positioningController.checkRequirements():
            getWxApp().showMessage('No patient', 'No patient', wx.ICON_ERROR|wx.OK)
            getWxApp().getCenter().goToPrevPage()
            return
        if not panel.isCreated():
            self.handler.main.controller.createBusy()
            panel.GetParent().Freeze()
            panel.Freeze()
            posObject = positioningController(panel)
            self.handler.register('positioning', posObject, panel)
            panel.Layout()
            panel.Thaw()
            panel.GetParent().Thaw()
            panel.setCreated()
            self.handler.main.controller.deleteBusy()
        else:
            self.handler.positioning.controller.work()

    def calculate(self, panel):
        self.handler.main.controller.createBusy()
        self.handler.main.view.Freeze()
        NPPlan.config.runningConfig.curSubStage = 'calculate'
        if not panel.isCreated():
            from NPPlan.view.client.main.stages.calculate.main import calculateView as cView
            from NPPlan.controller.client.mainC.stages.calculate import calculateController as cContr
            #v = cView(panel)
            v = self._addPanelToPanel(panel, cView)
            c = cContr(v)
            self.handler.register('calculateRunner', c, v)
            pass
        else:
            self.handler.calculateRunner.controller.work()
            pass
        self.handler.main.view.Thaw()
        self.handler.main.controller.deleteBusy()


    def results(self, panel):
        NPPlan.config.runningConfig.curSubStage = 'results'
        from NPPlan.controller.client.mainC.stages.results import resultsController
        from NPPlan.view.client.main.stages.results import resultsTopPanel
        if not resultsController.checkRequirements():
            return
        if not panel.isCreated():
            self.handler.main.controller.createBusy()
            panel.GetParent().Freeze()
            panel.Freeze()

            pane = self._addPanelToPanel(panel, resultsTopPanel)
            resObject = resultsController(pane, self)
            self.handler.register('results', resObject, pane)

            panel.Layout()
            panel.Thaw()
            panel.GetParent().Thaw()
            panel.setCreated()
            self.handler.main.controller.deleteBusy()
        else:
            self.handler.results.controller.work()
            pass
        pass

