# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 24.10.12
@summary: 
'''

import NPPlan
import tempfile
import NPPlan.model.file.dicom.rawReader as dicomRawReader
import NPPlan.model.file.dicom.thumbnail as dicomThumbnailGenerator
import NPPlan.model.file.configReader.studyInformer as studyInformer

from NPPlan.controller.client.mainC.stages.study.thumbnailHandler import thumbnailInformer

import wx

try:
    from agw import thumbnailctrl as TC
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.thumbnailctrl as TC

_panels = {}
_curStudyName = ''
thumbInited = False
thumbClass = None

def addStudy(panel):
    curStudyPanel = panel.addStudyPage(_('Study #0'), 0)

    print NPPlan.getRunningConfig()
    print NPPlan.getRunningConfig('curPlan')

    curPlan = NPPlan.getRunningConfig('curPlan')
    curPlan['studies'] = {}
    NPPlan.setRunningConfig(curPlan)

    doBindings(curStudyPanel)
    storePanel(curStudyPanel)

    initThumbnail()

def incStudies():
    curPlan = NPPlan.getRunningConfig('curPlan')
    curPlan['plan.studies'] = u'%d' %(int(curPlan['plan.studies'])+1)
    NPPlan.setRunningConfig('curPlan', curPlan)

def genLastStudyName():
    return NPPlan.getRunningConfig('curPlan.plan.name')+NPPlan.getRunningConfig('curPlan.plan.studies')

def storePanel(panel):
    global _curStudyName
    print 'DATA: %s' %NPPlan.getRunningConfig('curPlan.plan.name')
    _curStudyName = genLastStudyName()
    _panels[_curStudyName] = panel
    incStudies()

def doBindings(panel):
    # bind add folder button
    panel.Bind(wx.EVT_BUTTON, loadFromDirDialog, panel.addFolder)
    panel.thumb.Bind(TC.EVT_THUMBNAILS_SEL_CHANGED, selChanged)

    pass

def selChanged(event):
    global _panels, _curStudyName
    print _panels[_curStudyName].thumb.GetSelection()
    print _panels[_curStudyName].thumb.GetItem(_panels[_curStudyName].thumb.GetSelection())
    print _panels[_curStudyName].thumb.GetItem(_panels[_curStudyName].thumb.GetSelection()).GetFileName()
    global studyData
    obj = findInList(
        studyData[_curStudyName],
        'out',
        _panels[_curStudyName]
            .thumb
            .GetItem(_panels[_curStudyName].thumb.GetSelection())
            .GetFileName()
            .split('.')[0]
    )
    handleThumbnail(obj)
    #print _panels[_curStudyName].thumb.GetCaption()
    #print _panels[_curStudyName].thumb.GetSelectedItem(_panels[_curStudyName].thumb.GetSelection())
    #print event.GetSelection()

def loadFromDirDialog(event):
    print _panels
    dlg = wx.DirDialog(wx.GetApp().GetTopWindow(), _("Choose a directory with images:"),
                       style=wx.DD_DEFAULT_STYLE|wx.DD_DIR_MUST_EXIST)

    if dlg.ShowModal() == wx.ID_OK:
        path = parsePath(dlg.GetPath())
        loadFromDir(path)
        pass

    dlg.Destroy()
    pass

def findInList(list, field, value):
    for i in list:
        if i[field] == value:
            return i

def parsePath(path):
    if 1 == NPPlan.getOs():
        path = path.replace('\\', '/')
    if path[-1] != '/':
        path = '%s/' %path
    return path

studyData = {}
def proxyStudyData(data):
    # @todo: to db handler
    studyData[_curStudyName] = data

    print NPPlan.getRunningConfig()
    curConfig = NPPlan.getRunningConfig()


def loadFromDir(dicomPath):
    global _curStudyName, _panels
    curPlan = NPPlan.getRunningConfig('curPlan')
    tmpDir = tempfile.mkdtemp()
    print 'TMP DIR', tmpDir
    tmpDir = parsePath(tmpDir)

    curPlan['studies'][_curStudyName] = {}
    dataHandler = curPlan['studies'][_curStudyName]

    dataHandler['thumbDir'] = tmpDir
    #curPlan['thumbDir'] = 'C:\\Users\\Chernukha\\Pictures\\DICOM\\thumb\\'
    dataHandler['dicomDir'] = dicomPath
    results = dicomThumbnailGenerator.parseDir(dataHandler['dicomDir'], dataHandler['thumbDir'])
    proxyStudyData(results)
    print curPlan
    print _curStudyName
    print _panels
    print _panels[_curStudyName]
    if 1 == NPPlan.getOs():
        _panels[_curStudyName].loadThumbnails(dataHandler['thumbDir'].replace('/', '\\\\'))
    else:
        _panels[_curStudyName].loadThumbnails(dataHandler['thumbDir'])
    print curPlan
    pass

def initThumbnail():
    global _panels, _curStudyName, thumbInited, thumbClass
    if thumbInited:
        return
    studyInformer.readConfig()
    print 'from xml', studyInformer.getData()
    thumbClass = thumbnailInformer(_panels[_curStudyName].shortInfo, studyInformer.getData())
    thumbInited = True



def handleThumbnail(data):
    global thumbClass
    print 'Loading', data
    thumbClass.loadInfo(data)
    pass