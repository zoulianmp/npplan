# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 24.10.12
@summary: 
'''

import NPPlan

import dicom

class thumbnailInformer(object):
    def __init__(self, panel, fields):
        self.panel = panel
        self.fields = fields
        print 'fields,', fields
        self.panel.setItems(fields)
        self.panel.doLayout()
        self.storage = {}

    def loadInfo(self, file):
        print 'file', file
        dataset = dicom.read_file(file['inp'])
        setattr(self.panel, 'fileName', file['out'])
        for field in self.fields:
            if isinstance(field['dicom'], tuple):
                fieldData = dataset[field['dicom']].value
            else:
                fieldData = getattr(dataset, field['dicom'])
            print 'fieldData', fieldData
            setattr(self.panel, field['name'], fieldData)
        pass
