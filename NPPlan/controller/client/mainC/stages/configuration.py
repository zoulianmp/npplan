# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 01.08.13
@summary:
'''

import NPPlan
import wx
from NPPlan.controller.client.mainC.abstracts import baseController
from NPPlan.controller.client.mainC.stages.configurationC import clientServerPostController
from NPPlan.controller.client.mainC.stages.configurationC import facilityConfigurationController
from NPPlan.controller.client.mainC.stages.configurationC import institutionController

from NPPlan.view.client.main.stages.configuration import clientsAndServersView
from NPPlan.view.client.main.stages.configuration import materialsView
from NPPlan.view.client.main.stages.configuration import organsView
from NPPlan.view.client.main.stages.configuration import facilityView
from NPPlan.view.client.main.stages.configuration import insitutionView


class configuration(baseController):
    def __init__(self, parent):
        self.__dict__['parent'] = parent

    def _addPanelToPanel(self, panel, newWxClass):
        panel.GetParent().Freeze()
        panel.Freeze()
        pane = newWxClass(panel)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pane, 1, wx.ALIGN_CENTER | wx.EXPAND)
        panel.SetSizer(sizer)
        panel.Layout()
        panel.Thaw()
        panel.GetParent().Thaw()
        panel.setCreated()
        return pane

    def servers(self, panel):
        if not panel.isCreated():
            pane = self._addPanelToPanel(panel, clientsAndServersView)
            c = clientServerPostController(self, pane)
            self.handler.register('configurationServers', c, pane)
        else:
            self.handler.configurationServers.controller.work()
            pass
        pass

    def materials(self, panel):
        if not panel.isCreated():
            pane = self._addPanelToPanel(panel, materialsView)
        else:
            pass

    def organs(self, panel):
        if not panel.isCreated():
            pane = self._addPanelToPanel(panel, organsView)
        else:
            pass

    def facilities(self, panel):
        if not panel.isCreated():
            pane = self._addPanelToPanel(panel, facilityView)
            c = facilityConfigurationController(self, pane)
            #self._facility = (c, pane)
            self.handler.register('confFacility', c, pane)
        else:
            #self._facility[0].work()
            self.handler.confFacility.controller.work()
            pass

    def institution(self, panel):
        if not panel.isCreated():
            pane = self._addPanelToPanel(panel, insitutionView)
            c = institutionController(wxParent=pane, cParent=self)
            #self._institution = (c, pane)
            self.handler.register('confInstitution', c, pane)
            #pane.hEmk2.SetValue(u'Федеральное государственное бюджетное учреждение «Медицинский радиологический научный центр» '
            #                    u'Министерства здравоохранения Российской Федерации (ФГБУ МРНЦ Минздрава России)')
            pass
        else:
            pass