# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 26.09.12
@summary: 
'''

import NPPlan
import NPPlan.model.file.configReader.abstractButton as pt
import sys

_frame = None
btns = None

def init(frame):
    global btns, _frame
    _frame = frame
    pt.readConfig('patWorkButtons')
    btns = pt.getButtons('patWorkButtons')
    print btns
    frame.addButtons(btns, sys.modules[__name__])
    print 'created patWork'

def work():
    print 'working patWork'
    pass

def newPatient():
    from NPPlan.controller.client.main import getApp
    print 'starting new patient'
    getApp().runPatientsWindow(type='addPatient')

def newPlan():
    from NPPlan.controller.client.main import getApp
    getApp().createPlan(type='empty')