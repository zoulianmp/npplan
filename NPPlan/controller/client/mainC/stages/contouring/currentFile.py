# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 16.11.12
@summary: 
'''

import NPPlan

class currentFile(object):
    """
    @values:
    internal:
        self.__dict__['curName']
        self.__dict__['thumbName']
    external:
        currentFile.name
        currentFile.path
        currentFile.thumbName
        currentFile.thumbPath
    """
    def __init__(self, parent):
        self.parent = parent
        self.__dict__['window'] = float(NPPlan.config.appConfig.client.defaultwindow)
        self.__dict__['level'] = float(NPPlan.config.appConfig.client.defaultlevel)
        pass

    def cutThumbName(self, name):
        return '.'.join(name.split('.')[:-1])

    def setDicomInfo(self):
        if self.parent.informer.has_key(self.name):
            self.dicomInfo = self.parent.informer[self.name]
        else:
            self.dicomInfo = self.parent.loadDicomInfo(self.path, self.name)

    def setByThumbName(self, name):
        for i in self.parent.thumbList:
            if i == name:
                self.__dict__['curName'] = self.cutThumbName(name)
                self.__dict__['thumbName'] = name
                self.setDicomInfo()
                pass
            pass

    def getCurName(self):
        return self.__dict__['curName']

    name = property(getCurName)

    def getCurFullName(self):
        return '%s%s%s' %(self.parent.dicomDir, NPPlan.delimiter, self.name)

    path = property(getCurFullName)

    def getCurThumbName(self):
        return self.__dict__['thumbName']

    thumbName = property(getCurThumbName)

    def getCurThumbFullName(self):
        return '%s%s%s' %(self.parent.thumbDir, NPPlan.delimiter, self.thumbName)

    thumbPath = property(getCurThumbFullName)

    def setFirst(self):
        print self.parent.thumbList
        print self.parent.thumbList[0]
        self.setByThumbName(self.parent.thumbList[0])

    def getWindowLevel(self):
        return (self.__dict__['window'], self.__dict__['level'], )
    def setWindowLevel(self, windowlevel):
        """
        @param windowlevel: (window, level)
        @type windowlevel: tuple
        """
        self.__dict__['window'] = windowlevel[0]
        self.__dict__['level'] = windowlevel[1]

    def getWindow(self):
        return self.__dict__['window']
    def setWindow(self, window):
        """
        @param window: window
        @type window: int
        """
        self.__dict__['window'] = window

    def getLevel(self):
        return self.__dict__['level']
    def setLevel(self, level):
        """
        @param level: level
        @type level: int
        """
        self.__dict__['level'] = level

    windowlevel = property(getWindowLevel, setWindowLevel)
    window = property(getWindow, setWindow)
    level = property(getLevel, setLevel)