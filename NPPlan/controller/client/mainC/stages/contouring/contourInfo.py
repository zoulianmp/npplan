# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 16.11.12
@summary: 
'''

import NPPlan
from currentFile import *
from NPPlan.model.file.dicom.shortStructure import readConcreteData as shortInformer
import os

class contourInfo(object):
    dicomList = []
    thumbList = []
    informer = {}
    def __init__(self, dicomDir='', thumbDir=''):
        self._currentFile = currentFile(self)
        if dicomDir:
            self.dicomDir = dicomDir
        if thumbDir:
            self.thumbDir = thumbDir
        pass

    def loadDicomInfo(self, path, key):
        print path, key
        self.informer[key] = shortInformer(path)
        return self.informer[key]

    def getStudyName(self):
        return self.__dict__['studyName']
    def setStudyName(self, studyName):
        self.__dict__['studyName'] = studyName

    studyName = property(getStudyName, setStudyName)

    def getDicomDir(self):
        return self.__dict__['dicomDir']
    def setDicomDir(self, dicomDir):
        self.__dict__['dicomDir'] = dicomDir
        self.dicomList = os.listdir(dicomDir)

    dicomDir = property(getDicomDir, setDicomDir)

    def getThumbDir(self):
        return self.__dict__['thumbDir']
    def setThumbDir(self, thumbDir):
        self.__dict__['thumbDir'] = thumbDir
        self.thumbList = os.listdir(thumbDir)

    thumbDir = property(getThumbDir, setThumbDir)

    def getCurrentFileInfo(self):
        return self._currentFile
        pass
    def setCurrentFileInfo(self, file):
        if isinstance(file, int) and -1 == file:
            self._currentFile.setFirst()
        elif '.' in file:
            self._currentFile.setByThumbName(file)

        pass

    currentFile = property(getCurrentFileInfo, setCurrentFileInfo)

    def check(self):
        print 'self.__dict__', self.__dict__
        print 'dicomList', self.dicomList
        print 'thumbList', self.thumbList