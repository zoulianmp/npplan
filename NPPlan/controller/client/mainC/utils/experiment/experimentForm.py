# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 19.12.13
@summary: 
'''

try:
    from NPPlan.model.calc.geant.particlesReader import particlesReader as pR
except ImportError:
    from particlesReader import particlesReader as pR
import wx
import numpy as np

class MyFrame1 ( wx.Frame ):

    def __init__( self, parent, id, title, pos=wx.DefaultPosition, size=wx.Size( 500,300 ),
                  style=wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL ):
        wx.Frame.__init__ ( self, parent, id, title, pos, size, style)

        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(mainSizer)
        mainSizer.Add(epiPanel(self), 1, wx.EXPAND)

        self.Centre( wx.BOTH )

    def __del__( self ):
        pass

class epiPanel ( wx.Panel ):
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 556,372 ), style = wx.TAB_TRAVERSAL )

        gSizer1 = wx.GridSizer( 0, 2, 0, 0 )

        self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"Тип эксперимента", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText1.Wrap( -1 )
        gSizer1.Add( self.m_staticText1, 0, wx.ALL, 5 )

        m_choice1Choices = [u'Эпиндорфы, внешний воздух', u'Эпиндорфы, внешняя вода']
        self.m_choice1 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice1Choices, 0 )
        self.m_choice1.SetSelection( 0 )
        gSizer1.Add( self.m_choice1, 0, wx.ALL, 5 )

        self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"Координата, мм", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( -1 )
        gSizer1.Add( self.m_staticText2, 0, wx.ALL, 5 )

        self.m_textCtrl2 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer1.Add( self.m_textCtrl2, 0, wx.ALL, 5 )

        self.m_staticText3 = wx.StaticText( self, wx.ID_ANY, u"Доза, рад/с", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText3.Wrap( -1 )
        gSizer1.Add( self.m_staticText3, 0, wx.ALL, 5 )

        self.m_staticText4 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText4.Wrap( -1 )
        gSizer1.Add( self.m_staticText4, 0, wx.ALL, 5 )

        self.m_staticText5 = wx.StaticText( self, wx.ID_ANY, u"ЛПЭ, МэВ/мм", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText5.Wrap( -1 )
        gSizer1.Add( self.m_staticText5, 0, wx.ALL, 5 )

        self.m_staticText6 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText6.Wrap( -1 )
        gSizer1.Add( self.m_staticText6, 0, wx.ALL, 5 )

        self.m_button1 = wx.Button( self, wx.ID_ANY, u"OK", wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer1.Add( self.m_button1, 0, wx.ALL, 5 )

        self.m_button2 = wx.Button( self, wx.ID_ANY, u"Выход", wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer1.Add( self.m_button2, 0, wx.ALL, 5 )

        self.Bind(wx.EVT_BUTTON, self.onOK, self.m_button1)
        self.Bind(wx.EVT_BUTTON, self.onClose, self.m_button2)

        self.SetSizer( gSizer1 )
        self.Layout()

    def onOK(self, event):
        print self.m_choice1.GetSelection()
        if 0 == self.m_choice1.GetSelection():
            fdir = 'C:\\Temp\\protvino_chambers_exp2\\w3bb_Big_Air\\build\\'
        elif 1 == self.m_choice1.GetSelection():
            fdir = 'C:\\Temp\\protvino_chambers_exp2\\w3bb_Big_Water\\build\\'
        depositFName = fdir+r'particleDepositEnergies.out'
        kineticFName = fdir+r'particleKineticSum.out'
        countFName = fdir+r'particleLayers.out'
        rd = pR(depositFName)
        rk = pR(kineticFName)
        rc = pR(countFName)
        rMean = rk / rc
        pos = int(self.m_textCtrl2.GetValue())
        dose = np.sum(rd.getData()[pos].values()) * 1e9 / 1e6 / 2.5 * 1.602e-8
        self.m_staticText4.SetLabel(unicode(dose))
        lpe = rMean[pos-1]['C12[0.0]'] - rMean[pos]['C12[0.0]']
        self.m_staticText6.SetLabel(unicode(lpe))


    def onClose(self, event):
        self.Close()


class testApp(wx.App):
    def OnInit(self):
        self.SetAppName("epiViewer")
        return True

if __name__ == '__main__':
    app = testApp(False)
    topForm = MyFrame1(None, wx.NewId(), 'MRRC experiment data reader', size=(800, 600),
                      style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER ^ wx.MAXIMIZE_BOX,
                      pos=(10, 10))
    topForm.Show()
    app.MainLoop()