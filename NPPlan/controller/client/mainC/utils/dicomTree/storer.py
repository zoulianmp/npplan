# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 15.10.12
@summary: 
'''

import NPPlan

_storage = {}
_path = None

def add(index, data):
    _storage[index] = data

def get(index):
    if _storage.has_key(index):
        return _storage[index]
    return None

def getAll():
    return _storage

def init(path):
    _path = path

def getLine(rawData):
    return '%s, %s, %s' %(rawData.PatientsName, rawData.PatientsSex, rawData.StudyDate)

def retChoices():
    ret = []
    for i in _storage:
        ret.append({
            'text' : '%s: %s' %(i, getLine(_storage[i])),
            'index' : i,
        })
    return ret
