# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 05.10.12
@summary:
'''

import NPPlan

import NPPlan.controller.logger as log
from NPPlan.controller.client.mainC.utils.dicomTree.file import openFolder

import wx

_panel = None
def init(panel):
    global _panel
    log.log('main.utils.dicomTree', 'dicomTree Init')
    _panel = panel

    # bind open butons
    _panel.Bind(wx.EVT_BUTTON, openFolder, id=_panel.btnIds['BTN_ADD_FOLDER'])
    _panel.Bind(wx.EVT_CHOICE, choiceWork, _panel.tlcChoices)
    #_panel.Bind()
    pass

def choiceWork(event):
    # @todo: заготовка
    from NPPlan.controller.client.mainC.utils.dicomTree.storer import get as dcGet
    index = _panel.tlcChoices.GetClientData(_panel.tlcChoices.GetSelection())['index']
    data = dcGet(index)
    tlcTreeView = _panel.getTreeView()
    _panel.Freeze()
    workTree(tlcTreeView, data)
    _panel.Thaw()
    pass

def work():
    pass

def getPanel():
    global _panel
    return _panel

queue = None

def workTree(tlcTreeView, dataSet):
    global queue
    tlcTreeView.DeleteAllItems()
    root = tlcTreeView.AddRoot(text=dataSet.SOPClassUID.name)
    #import multiprocessing
    import threading
    import Queue
    queue = Queue.Queue()
    #p = multiprocessing.Process(target=recurseTreeThread, args=(root, dataSet, addTreeItem, len(dataSet)))
    #p.start()
    t = threading.Thread(target=recurseTreeThread, args=(root, dataSet, addTreeItem, len(dataSet)))
    t.start()
    tlcTreeView.SetFocus()
    #tlcTreeView.Expand(root)
    pass

def recurseTreeThread(parent, dataSet, addFunc, length):
    global queue
    for i, dataElement in enumerate(dataSet):
        print i, dataElement
        # Check and update the progress of the recursion
        # Add the data_element to the tree if not a sequence element
        if not (dataElement.VR == 'SQ'):
            wx.CallAfter(addFunc, dataElement, parent)
        # Otherwise add the sequence element to the tree
        else:
            wx.CallAfter(addFunc, dataElement, parent, needQueue=True)
            item = queue.get()
            # Enumerate for each child element of the sequence
            for i, ds in enumerate(dataElement.value):
                sq_item_description = dataElement.name.replace(" Sequence", "")
                sq_element_text = "%s %d" % (sq_item_description, i+1)
                # Add the child of the sequence to the tree
                wx.CallAfter(addFunc, dataElement, item, sq_element_text, needQueue=True)
                sq = queue.get()
                recurseTreeThread(sq, ds, addFunc, 0)
    pass

def addTreeItem(dataElement, parent, sqElementText="", needQueue=False):
    # Set the item if it is a child of a sequence element
    tlcTreeView = _panel.getTreeView()
    if not (sqElementText == ""):
        item = tlcTreeView.AppendItem(parent, text=sqElementText)
    else:
        # Account for unicode or string values
        if isinstance(dataElement.value, unicode):
             item = tlcTreeView.AppendItem(parent, text=unicode(dataElement.name))
        else:
            item = tlcTreeView.AppendItem(parent, text=str(dataElement.name))
        # Set the value if not a sequence element
        if not (dataElement.VR == 'SQ'):
            if (dataElement.name == 'Pixel Data'):
                arrayLen = 'Array of ' + str(len(dataElement.value)) + ' bytes'
                tlcTreeView.SetItemText(item, arrayLen, 1)
            elif (dataElement.name == 'Private tag data'):
                tlcTreeView.SetItemText(item, 'Private tag data', 1)
            else:
                try:
                    tlcTreeView.SetItemText(item, unicode(dataElement.value), 1)
                except UnicodeDecodeError:
                    pass
                except:
                    tlcTreeView.SetItemText(item, '%s'%dataElement.value, 1)
            # Fill in the rest of the data_element properties
        tlcTreeView.SetItemText(item, unicode(dataElement.tag), 2)
        tlcTreeView.SetItemText(item, unicode(dataElement.VM), 3)
        tlcTreeView.SetItemText(item, unicode(dataElement.VR), 4)
    if needQueue:
        queue.put(item)
    pass