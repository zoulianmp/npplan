# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 19.02.13
@summary: 
'''

from threading import *
import wx
import time
import abc

# Define notification event for thread completion
EVT_RESULT_ID = wx.NewId()

def EVT_RESULT(win, func):
    """Define Result Event."""
    win.Connect(-1, -1, EVT_RESULT_ID, func)

class ResultEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""
    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_RESULT_ID)
        self.data = data

# Thread class that executes processing
class WorkerThread(Thread):
    """Worker Thread Class."""
    def __init__(self, notify_window):
        """Init Worker Thread Class."""
        Thread.__init__(self)
        self._notify_window = notify_window
        self._want_abort = 0
        # This starts the thread running on creation, but you could
        # also make the GUI thread responsible for calling this
        self.start()

    def run(self):
        """Run Worker Thread."""
        # This is the code executing in the new thread. Simulation of
        # a long process (well, 10s here) as a simple loop - you will
        # need to structure your processing so that you periodically
        # peek at the abort variable
        #for i in range(100):
        #    time.sleep(0.1)
        #    print 'aaa rung'
        #    if self._want_abort:
                # Use a result of None to acknowledge the abort (of
                # course you can use whatever you'd like or even
                # a separate event type)
                #wx.PostEvent(self._notify_window, ResultEvent(None))
        #        print 'aaa'
        #        return
        # Here's where the result would be returned (this is an
        # example fixed result of the number 10, but it could be
        # any Python object)
        max = 100
        dlg = wx.ProgressDialog("Progress dialog example",
                               "An informative message",
                               maximum = max,
                               parent=wx.GetApp().GetTopWindow(),
                               style = wx.PD_CAN_ABORT
                                | wx.PD_APP_MODAL
                                | wx.PD_ELAPSED_TIME
                                #| wx.PD_ESTIMATED_TIME
                                | wx.PD_REMAINING_TIME
                                )

        keepGoing = True
        count = 0

        dlg.Update()

        while keepGoing and count < max:
            count += 1
            wx.MilliSleep(250)  # simulate some time-consuming thing...

            if count >= max / 2:
                (keepGoing, skip) = dlg.Update(count, "Half-time!")
            else:
                (keepGoing, skip) = dlg.Update(count)


        dlg.Destroy()

        wx.PostEvent(self._notify_window, ResultEvent(10))

    def abort(self):
        """abort worker thread."""
        # Method for use by main thread to signal an abort
        self._want_abort = 1

class baseProgress(Thread):
    __metaclass__ = abc.ABCMeta
    def __init__(self, caller=None, callback=None, autostart=True):
        self._caller = caller
        if caller is not None and isinstance(callback, str):
            self._callback = getattr(caller, callback)
        else:
            self._callback = callback
        Thread.__init__(self)
        self._keepGoing = True
        if autostart:
            self.start()

    @abc.abstractmethod
    def run(self):
        self.dlg = None
        pass

    def abort(self):
        print 'aborting'
        self._keepGoing = False
        try:
            self.dlg.EndModal(0)
            self.dlg.Close()
            self.dlg.Destroy()
        except:
            pass
        self.dlg = None

    def setCaption(self, text):
        self.dlg.setCaption(text)