# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 19.02.13
@summary: 
'''

import wx
from .base import baseProgress
from NPPlan.view.client.main.progress.gaugeDialog import TestDialog


class gaugeProgress(baseProgress):
    def __init__(self, caller=None, callback=None, autostart=True, style=None):
        baseProgress.__init__(self, caller, callback, autostart=False)
        from NPPlan.controller.client.main import getHandler
        getHandler().register('progress', self, None)

    def run(self):
        dlg = TestDialog(wx.GetApp().GetTopWindow(), -1, "Sample Dialog", size=(350, 200),
                         #style=wx.CAPTION | wx.SYSTEM_MENU | wx.THICK_FRAME,
                         style= wx.THICK_FRAME | wx.BORDER_SUNKEN, # & ~wx.CLOSE_BOX,
                         )

        self.dlg = dlg
        dlg.CenterOnScreen()
        dlg.Show()
        #dlg.MakeModal()
        #dlg.ShowModal()
        #val = dlg.ShowModal()

        while self._keepGoing:
            wx.MilliSleep(100)
            dlg.gauge.Pulse()
        #dlg.Destroy()
        #self.dlg = None
        dlg.EndModal(0)
        #dlg.IsModal()
        pass