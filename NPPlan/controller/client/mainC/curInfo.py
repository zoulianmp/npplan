# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 25.09.12
@summary: 
'''

import wx
import NPPlan
import NPPlan.controller.logger as log
from NPPlan.controller.client.mainC.abstracts import baseController

# @TODO: pre/post handlers (!important)


class curInfo(baseController):
    def __init__(self, parentController, wxView):
        """
        @access: self.handler.informer
        @param parentController: родительский контроллер
        @type parentController: NPPlan.controller.client.mainC.centerPanel.centerPanel
        @param wxView: отображение
        @type wxView: NPPlan.view.client.main.curInfo.curInfo
        @return:
        """
        #self.frame = frame
        #self.frame._controller = self
        self._parent = parentController
        self._view = wxView
        log.log('curInfo', '%s %s' % (type(self._parent), type(self._view)))
        print 'curInfo init'
        print self.handler.informer
        print self == self.handler.informer
        self.pages = []
        self.handlers = []

    def start(self):
        print 'curInfo started'
        self._view.Bind(wx.EVT_BUTTON, self.homeClick, self._view.homeButton)
        self._view.Bind(wx.EVT_BUTTON, self.backClick, self._view.backButton)
        print self.handler.informer
        self.backState(False)
        # print self == self.handler.informer.controller # true
        pass

    def homeClick(self, event=None):
        if hasattr(self, '_homeBackend') and self._homeBackend is not None:
            self._homeBackend(event)
        pass

    def backClick(self, event=None):
        if hasattr(self, '_backBackend') and self._backBackend is not None:
            self._backBackend(event)
        pass

    def backState(self, state=-1):
        if -1 == state:
            return self._view.backButton.Enabled()
        else:
            self._view.backButton.Enable(state)

    def homeState(self, state=-1):
        if -1 == state:
            return self._view.homeButton.Enabled()
        else:
            self._view.homeButton.Enable(state)

    def setText(self, text):
        self._view.infoText.SetLabel(text)

    def setBackBackend(self, backend):
        self._backBackend = backend

    def setHomeBackend(self, backend):
        self._homeBackend = backend