# -*- coding: utf8 -*-
'''
Модуль контроллера центральной панели
@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 13.09.12
@summary: 
'''

import NPPlan
import wx
import inspect
import NPPlan.model.file.configReader.stages as st
from NPPlan.model.client.screenContainer import screenContainer

from NPPlan.controller.client.mainC.curInfo import curInfo
import NPPlan.controller.client.mainC.stages.patWork as patWorkController
import NPPlan.controller.client.mainC.stages.utils as utilsController
from NPPlan.controller.client.mainC.abstracts import baseController
from NPPlan.controller.client.mainC.stages import welcome
from NPPlan.controller.client.mainC.stages import main
from NPPlan.controller.client.mainC.stages import patientAndPlans
from NPPlan.controller.client.mainC.stages import configuration
from NPPlan.controller.client.mainC.stages import facility
from NPPlan.controller.client.mainC.stages import utils

from NPPlan.view.client.main.emptyPanel import emptyPanel
from NPPlan.view.client.main.stages.abstractButton import abstractButton as abstractButtonForm
import os
try:
    from agw import labelbook as LB
except ImportError:
    import wx.lib.agw.labelbook as LB


class centerPanel(baseController):
    def __init__(self, frame, topController=None):
        """

        @param frame:
        @type frame: NPPlan.view.client.main.centerPanel.centerPanel
        @param topController:
        @type topController: NPPlan.controller.client.main.mainController
        @return:
        """
        print 'types', type(frame), type(topController)
        self.frame = frame
        self._parent = topController
        self._screens = screenContainer()
        #self.curInfoHandler = curInfo(self.frame.curInfo)
        self.curSel = 0
        self.screenNames = {}
        self.binded = False

        self.__dict__['_invoked'] = []
        self.__dict__['welcome'] = welcome.welcome(self)
        self.__dict__['main'] = main.main(self)
        #self.__dict__['planning'] =
        self.__dict__['patientAndPlans'] = patientAndPlans.patientAndPlans(self)
        self.__dict__['configuration'] = configuration.configuration(self)
        self.__dict__['facility'] = facility.facility(self)
        self.__dict__['utils'] = utils.utils(self)

        self.invokeMethods(self.__dict__['welcome'], welcome.welcome, '_welcome')
        self.invokeMethods(self.__dict__['main'], main.main, '_main')
        self.invokeMethods(self.__dict__['patientAndPlans'], patientAndPlans.patientAndPlans, '_patientAndPlans')
        self.invokeMethods(self.__dict__['configuration'], configuration.configuration, '_configuration')
        self.invokeMethods(self.__dict__['facility'], facility.facility, '_facility')
        self.invokeMethods(self.__dict__['utils'], utils.utils, '_utils')

        #NPPlan._fullHome = self._fullHome
        #print st.getStagesList()

    def invokeMethods(self, instance, obj, prependString):
        """
        Метод, исследующий открытые методы класса obj (методы, не имеющие _ первым символов) и устанавливающий
         у своего класса аттрибуты от созданных объектов instance с префиксом prependString.
        Таким образом, создаваемый объект данного класса позволяет обращаться к объектам порождаемых классов
        напрямую, минуя внутренние переменные данного класса
        @param instance: созданный класс объекта
        @type instance: instance
        @param obj: класс
        @type obj: classobj
        @param prependString: префикс имён после перегрузки
        @type prependString: str
        """
        self.__dict__['_invoked'].append((instance, obj, prependString))
        for i in inspect.getmembers(obj, predicate=inspect.ismethod):
            name = i[0]
            if name[0] == '_':
                continue
            else:
                name = name[0].upper() + name[1:]
            self.__setattr__(prependString + name, getattr(instance, i[0]))

    def homeHere(self, event, fullHome=False):
        if fullHome:
            self.frame.Freeze()
            NPPlan.config.runningConfig.curStageType = 'welcome'
            self.createCenter()
            self.frame.Thaw()
        self.frame.book.SetSelection(0)

    def goToPrevPage(self):
        self.frame.book.SetSelection(self.curSel-1)

    def createCenter(self):
        self.renderStages()
        self.frame.doLayout()

    def getWindow(self):
        return self.frame

    def getBook(self):
        return self.frame.book

    def registerInformer(self):
        #self._informer = curInfo(self, self.frame.curInfo)
        #self.handler.register('informer', self._informer, self.frame.curInfo)
        #self.handler.informer.controller.setHomeBackend(self._fullHome)
        pass
    #def getCurInfo(self):
    #    return self.curInfoHandler

    def _fullHome(self, event):
        self.homeHere(event, True)

    def lbBackMouseUp(self, event=None):
        pass

    def lbBackMouseDown(self, event=None):
        pass

    def lbForwardMouseUp(self, event=None):
        pass

    def lbForwardMouseDown(self, event=None):
        pass

    def getCurrentPage(self):
        return self.curSel

    def getCurrentScreen(self):
        return self._screens.get('%s.%s' %(NPPlan.config.runningConfig.curStageType, self.getScreenName(str(self.curSel))))

    def getScreens(self):
        return self._screens

    def getScreenName(self, id):
        print self.screenNames
        return self.screenNames[id]

    def getCurrentScreenName(self, fullName=0):
        if fullName == 1:
            return '%s.%s' %(NPPlan.config.runningConfig.curStageType, self.getScreenName(str(self.curSel)))
        return self.getScreenName(str(self.curSel))

    def renderStages(self):
        _cst = NPPlan.config.runningConfig.curStageType
        self.stages = st.getStagesForType(_cst)
        #print self.stages
        self.frame.startBook(imageList=self.createImageList())
        self.curSel = 0
        for i in self.stages:
            self.screenNames[i['order']] = i['name']
            self._screens.add('%s.%s' % (_cst, i['name']), emptyPanel(self.frame.book))
            #self._screens[int(i['order'])] = emptyPanel(self.frame.book)#, 1, i['name'])
            self.frame.book.AddPage(self._screens.get('%s.%s'%(_cst, i['name'])), i['caption'], False, int(i['order']))

        if not self.binded:
            self.frame.book.Bind(LB.EVT_IMAGENOTEBOOK_PAGE_CHANGED, self.onStageChanged)
            self.frame.book.Bind(LB.EVT_IMAGENOTEBOOK_PAGE_CHANGING, self.onStageChanging)
            self.binded = True
        self.frame.book.SetSelection(0)
        func = st.getBackend(_cst, str(0))
        print self.screenNames
        #globals()[func](self._screens.get('%s.%s'%(_cst, self.getScreenName('0'))))
        getattr(
            self,
            self._getStageFunctionName(*func))(self._screens.get('%s.%s'%(_cst, self.getScreenName('0'))))

    def _getStageFunctionName(self, func1, func2):
        return '_' + func1 + func2[0].upper() + func2[1:]

    def onStageChanging(self, event):
        """
        @type event: wx.lib.agw.labelbook.ImageNotebookEvent
        """
        print 'Stage changing', event, type(event)
        # 0. check conditions
        event.Skip()
        pass

    def onStageChanged(self, event):
        print event.GetId()
        event.StopPropagation()
        # 0. Check stage exit condition
        if self._parent.stageHasExitCondition():
            if not self._parent.checkStageCondition():
                # 0. Show message (e.g. not saved data)   <- at changing
                # 1. Check message if needed              <- handle |
                # 2. Switch to previous screen
                self.frame.book.SetSelection(event.GetOldSelection())
        # 1. Call stage after-exit function (if needed)

        # 1a. Delete or store views
        # 2. Switch to stage
        _cst = NPPlan.config.runningConfig.curStageType
        newsel = event.GetSelection()
        self.curSel = newsel
        func = st.getBackend(_cst, str(newsel))
        getattr(
            self,
            self._getStageFunctionName(*func))(self._screens.get('%s.%s'%(_cst, self.getScreenName(str(self.curSel)))))
        #globals()[func](self._screens.get('%s.%s'%(_cst, self.getScreenName(str(self.curSel)))))
        event.Skip()

    def createImageList(self):
        imageList = wx.ImageList(32, 32)
        iconPath = NPPlan.getIconPath()
        iconEmpty = iconPath + 'iconEmpty32.png'
        for i in self.stages:
            try:
                if i.has_key('iconType'):
                    if 'standart' == i['iconType']:
                        imageList.Add(wx.ArtProvider.GetBitmap(eval(i['icon']), wx.ART_OTHER, wx.Size(32, 32)))
                else:
                    if os.path.exists(os.path.join(iconPath, i['icon'])):
                        imageList.Add(wx.Bitmap(os.path.join(iconPath, i['icon']), wx.BITMAP_TYPE_PNG))
                    else:
                        imageList.Add(wx.Bitmap(iconEmpty, wx.BITMAP_TYPE_PNG))
            except KeyError:
                imageList.Add(wx.Bitmap(iconEmpty, wx.BITMAP_TYPE_PNG))

        return imageList

    def forceCallBackend(self, **kwargs):
        num = kwargs.get('num', -1)
        if num == -1:
            func = kwargs.get('func')
        else:
            func = st.getBackend(NPPlan.getProgramData()['curStageType'], num)
        screenNum = kwargs.get('screenNum', self.curSel)
        globals()[func](self._screens[screenNum])

    def shiftCurrentScreen(self, newScreenName):
        print "CURRENT SCREEN NAME", self.getCurrentScreenName()
        #oldScreen = self.getCurrentPage()
        newScreen = self._screens.get(newScreenName)
        book = self.getBook()
        book.Freeze()
        #self.getBook().GetSizer().Detach(self.getCurrentPage())
        book._mainSizer.Detach(book._windows[book.GetSelection()])
        book._windows[book.GetSelection()].Hide()
        book._mainSizer.Add(newScreen, 1, wx.EXPAND)
        book._mainSizer.Layout()
        book._windows[book.GetSelection()] = newScreen
        newScreen.Show()
        book.Thaw()
        print self.getCurrentScreenName()
        pass


def goNextStage():
    print 'going next stage'
    from NPPlan.controller.client.main import getHandler
    print getHandler().main.controller
    print getHandler().main.controller.cp

    #print getHandler().main.controller.cp.frame.book.SetSelection(1)
    print NPPlan.config.runningConfig.curStageType, NPPlan.config.runningConfig.curSubStage
    if 'main' == NPPlan.config.runningConfig.curStageType and 'patient' == NPPlan.config.runningConfig.curSubStage:
        getHandler().main.controller.cp.frame.book.SetSelection(1)
    #if 'main' == NPPlan.config.runningConfig.curStageType and 'contouring' == NPPlan.config.runningConfig.curSubStage:
    #    getHandler().main.controller.cp.frame.book.SetSelection(2)
    pass


NPPlan.goNextStage = goNextStage


def welcome_patwork(panel):
    """
    @param panel: панель
    @type panel: NPPlan.view.client.main.emptyPanel
    """
    if not panel.isCreated():
        panel.GetParent().Freeze()
        panel.Freeze()
        pan = abstractButtonForm(panel, [])
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pan, 1, wx.EXPAND)
        panel.SetSizer(sizer)
        panel.Layout()
        panel.Thaw()
        panel.GetParent().Thaw()
        panel.setCreated()
        patWorkController.init(pan)
    else:
        patWorkController.work()
        pass
    print 'blabla'


def welcome_facility(panel):
    """
    """
    from NPPlan.controller.client.main import getHandler
    from NPPlan.controller.client.mainC.stages.facility import generatorManagerBase
    if not panel.isCreated():
        panel.GetParent().Freeze()
        panel.Freeze()
        genObj = generatorManagerBase(wxParent=panel)
        getHandler().register('facilityManager', genObj, panel)
        panel.setCreated()
        panel.Thaw()
        panel.GetParent().Thaw()
    else:
        getHandler().facilityManager.controller.work()
        pass
    pass

def welcome_config(panel):
    """
    backend
    """
    print 'configuration'
    from NPPlan.controller.client.mainC.stages.configuration import configurationControllerMain
    from NPPlan.controller.client.main import getHandler
    if not panel.isCreated():
        panel.GetParent().Freeze()
        panel.Freeze()
        configObj = configurationControllerMain(wxParent=panel)
        getHandler().register('planning', configObj, panel)
        panel.setCreated()
        panel.Thaw()
        panel.GetParent().Thaw()
    else:
        getHandler().configObj.controller.work()
        pass

def welcome_utils(panel):
    """
    @param panel: панель
    @type panel: NPPlan.view.client.main.emptyPanel
    """
    if not panel.isCreated():
        panel.GetParent().Freeze()
        panel.Freeze()
        # @todo: patWork -> abstract overall
        pan = abstractButtonForm(panel, [])
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(pan, 1, wx.EXPAND)
        panel.SetSizer(sizer)
        panel.Layout()
        panel.Thaw()
        panel.GetParent().Thaw()
        panel.setCreated()
        utilsController.init(pan)
    else:
        utilsController.work()
        pass

def main_aaa_back(panel):
    '''
    заглушка
    '''
    pass

def main_patdata(panel):
    '''

    '''
    print 'PATDATA!!!'
    from NPPlan.controller.client.mainC.stages.planning import emptyPlanController
    from NPPlan.controller.client.main import getHandler

    NPPlan.config.runningConfig.curSubStage = 'patient'
    if not panel.isCreated():
        planObj = emptyPlanController(wxParent=panel)
        getHandler().register('planning', planObj, panel)
        panel.setCreated()
    else:
        getHandler().planning.controller.work()
    NPPlan.dumpRunningConfig()
    pass

def welcome_testtwopanels(panel):
    print 'Test 2 panels'
    from NPPlan.controller.client.mainC.stages.test.main import testTwoPanels
    from NPPlan.controller.client.main import getHandler as getHandler
    if not panel.isCreated():
        #atp = abstractTwoPanels(panel)
        atp = testTwoPanels(panel)
        getHandler().register('test', atp, panel)
        panel.setCreated()
    else:
        pass

def main_calculate(panel):
    """
    @param panel: панель
    @type panel: NPPlan.view.client.main.emptyPanel.emptyPanel
    """
    from NPPlan.controller.client.main import getApp as getWxApp
    from NPPlan.controller.client.mainC.stages.calculate import calculateController
    from NPPlan.controller.client.main import getHandler as getHandler
    from NPPlan.view.client.main.stages.calculate import calculateView
    if not calculateController.checkRequirements():
        getWxApp().showMessage('No patient', 'No patient', wx.ICON_ERROR|wx.OK)
        getWxApp().getCenter().goToPrevPage()
        return
    if not panel.isCreated():
        getHandler().positioningBEV.controller.looseAllHelpersFocus()
        panel.GetParent().Freeze()
        panel.Freeze()
        sizer = wx.BoxSizer(wx.VERTICAL)
        vw = calculateView(panel)
        con = calculateController(vw)
        sizer.Add(vw, 1, wx.EXPAND)
        panel.SetSizer(sizer)
        panel.Layout()
        panel.Thaw()
        panel.GetParent().Thaw()
        panel.setCreated()
        getHandler().register('calculate', con, vw)
    else:
        getHandler().calculate.controller.work()
        pass
