# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 10.09.12
@summary: 
'''

import NPPlan
import wx
try:
    from agw import buttonpanel as bp
except ImportError:
    import wx.lib.agw.buttonpanel as bp

class mainButtons():
    def __init__(self, frame, panel):
        self.frame = frame
        self.panel = panel
        self.created = False
        self.recreateButtons()

    def recreateButtons(self):
        # @todo: from stage
        #self.frame.Freeze()
        if self.created:
            sizer = self.panel.GetSizer()
            sizer.Detach(0)
            self.frame.titleBar.Hide()
            wx.CallAfter(self.frame.titleBar.Destroy)
        self.alignment = bp.BP_ALIGN_LEFT
        self.agwStyle = bp.BP_USE_GRADIENT
        self.frame.titleBar = bp.ButtonPanel(self.panel, -1, _('NPPlan'),
                                       agwStyle=self.agwStyle, alignment=self.alignment)
        self.frame.titleBar.DoLayout()
        #self.frame.SetProperties()
        self.stylization()
        self.makeButtons()
        self.frame.titleBar.DoLayout()
        self.frame.ChangeLayout()
        self.frame.titleBar.SetAlignment(bp.BP_ALIGN_RIGHT)
        #self.frame.Thaw()
        self.frame.Layout()
        self.created = True

    def stylization(self):
        bpArt = self.frame.titleBar.GetBPArt()
        bpArt.SetColour(bp.BP_TEXT_COLOUR, wx.BLACK)
        #bpArt.SetColour(bp.BP_GRADIENT_COLOUR_FROM, wx.Colour(60,11,112))
        #bpArt.SetColour(bp.BP_GRADIENT_COLOUR_TO, wx.Colour(120,23,224))
        bpArt.SetColour(bp.BP_BUTTONTEXT_COLOUR, wx.Colour(0, 0, 0))

    def makeButtons(self):
        uri = NPPlan.getProgramData()['appPath']+'/images/iconShowHideStagesBar.png'
        i = wx.Image(uri, wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        btn1Id = wx.NewId()
        btn = bp.ButtonInfo(self.frame.titleBar, btn1Id, i, kind=wx.ITEM_CHECK)
        btn.SetStatus("Toggled")
        btn.SetToggled(True)
        btn.SetText(_('Show/Hide stages bar'))
        self.frame.Bind(wx.EVT_BUTTON, self.onButton, id=btn.GetId())
        self.frame.titleBar.AddButton(btn)
        btn2 = bp.ButtonInfo(self.frame.titleBar, wx.NewId(), i, kind=wx.ITEM_NORMAL)
        #btn.SetStatus("Toggled")
        #btn.SetToggled(True)
        btn2.SetText(_('Btn2'))
        self.frame.titleBar.AddButton(btn2)

        pass

    def onButton(self, event):
        btn = event.GetId()
        #self.frame.centerPanel.hideStages()
        self.frame.centerPanel.toggleStage()



