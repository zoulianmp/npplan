# -*- coding: utf8 -*-
'''
Базовый контроллер
@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 15.11.12
@summary: 
'''

class baseController(object):
    def getHandler(self):
        """
        @return: хендлер
        @rtype: NPPlan.controller.client.mainC.abstracts.handler
        """
        if self.__dict__.has_key('_handler'):
            if self.__dict__['_handler'] is None:
                from NPPlan.controller.client.main import getHandler
                self.__dict__['_handler'] = getHandler()
        else:
            from NPPlan.controller.client.main import getHandler
            self.__dict__['_handler'] = getHandler()
        return self.__dict__['_handler']
    handler = property(getHandler)