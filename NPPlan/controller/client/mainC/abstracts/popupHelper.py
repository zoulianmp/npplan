# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 18.01.13
@summary: 
'''

import NPPlan
import os

try:
    from NPPlan.view.client.main.abstracts import popupHelperView
except ImportError:
    # @todo: на старте это не должно вызываться
    pass
import wx
import wx.lib.platebtn as platebtn

class popupHelperController(object):
    def __init__(self, wxParent, extent=(10, 10), origin=(0, 0),  hgap=2, color='BLACK', win=-1):
        """
        @param wxParent: родительское окно wx
        @type wxParent: wx.Window
        """
        self._wxParent = wxParent
        self._extent = extent
        self._win = popupHelperView(self, wxParent, color=color)
        self._winN = win

        self._buttons = {}
        self._toggleGroups = {}

        self._singleWidth = -1
        self._singleHeight = -1
        self._origin = origin
        self._hgap = hgap

        self._lastInsertId = -1

        '''def testBack(event):
            print 'backend for', event
        self.appendSingleIconButton('iconWindowExpand16.png', testBack, {})
        def testBackInGroup(btnClass):
            print 'inGroup', btnClass, btnClass._groupId, btnClass.wxId

        self.appendToggleGroup({
            '1' : {
                'icon' : 'iconWindowExpand16.png',
                'backend' : testBackInGroup,
                'params' : {}
            },
            '2' : {
                'icon' : 'smallIcon.png',
                'backend' : testBackInGroup,
                'params' : {}
            },
            '3' : {
                'icon' : 'iconDicomWlPresets16.png',
                'backend' : testBackInGroup,
                'params' : {}
            },
        }, 'g1', ['2', '3'])

        self.resize()'''
        self.active = False
        pass

    def addManyItems(self, itemsList):
        _groups = {}
        _gDefaults = {}
        _gIds = []

        print 'Adding many'
        print itemsList

        def back(ev):
            pass

        for hItem in itemsList:
            if 'toggle' == hItem['type'] and hItem['group'] is not None:
                if hItem['group'] not in _groups.keys():
                    _groups[hItem['group']] = {}
                _groups[hItem['group']][hItem['name']] = {
                    'icon' : hItem['icon'],
                    #'backend' : back,
                    'backend' : hItem['backend'],
                    'params' : hItem,
                    #'params' : {},
                }
                if 1 == int(hItem['default']):
                    try:
                        _gDefaults[hItem['group']].append(hItem['name'])
                    except KeyError:
                        _gDefaults[hItem['group']] = [hItem['name']]
                if hItem['group'] not in _gIds:
                    _gIds.append(hItem['group'])
            elif 'toggle' == hItem['type']:
                self.appendSingleToggleButton(hItem['icon'], hItem['backend'], hItem, 1 == int(hItem['default']))
            elif 'simple' == hItem['type']:
                self.appendSingleIconButton(hItem['icon'], hItem['backend'], hItem)
        #self.appendToggleGroup(_groups, _gDefaults)
        print _groups, _gDefaults, _gIds
        for gName in _gIds:
            self.appendToggleGroup(_groups[gName], gName, _gDefaults[gName])
        pass

    def getAssociatedWindowNumber(self):
        return self._winN

    def registerBackendClass(self, classInstance):
        self._backendClass = classInstance

    def resize(self):
        self._win.resize(
            len(self._buttons),
            self.getSingleWidth(),
            self.getSingleHeight(),
        )

    def getSingleWidth(self):
        if -1 != self._singleWidth:
            return self._singleWidth
        if 0 == len(self._buttons):
            # @todo: throw exception ?
            return
        firstBtn = self._buttons[self._buttons.keys()[0]]
        self._singleWidth = firstBtn.getButtonWidth()
        return self._singleWidth

    def getSingleHeight(self):
        if -1 != self._singleHeight:
            return self._singleHeight
        if 0 == len(self._buttons):
            # @todo: throw exception ?
            return
        firstBtn = self._buttons[self._buttons.keys()[0]]
        self._singleHeight = firstBtn.getButtonHeight()
        return self._singleHeight

    def getNextPos(self):
        return self._origin[0]+(self.getSingleWidth()+self._hgap)*len(self._buttons), self._origin[1]

    def appendButtonRaw(self, text, icon, type, backend, params):
        if 0 == len(self._buttons):
            pos = (0, 0)
        else:
            pos = self.getNextPos()
        btn = popupHelperButton(self, self._win, text, icon, type, pos, backend, params)
        self._buttons[btn.getWxId()] = btn
        self._lastInsertId = btn.getWxId()
        pass

    def appendSingleIconButton(self, icon, backend, params):
        self.appendButtonRaw('', icon, '', backend, params)

    def appendSingleToggleButton(self, icon, backend, params, toggled=False):
        self.appendButtonRaw('', icon, 'toggle', backend, params)
        self._buttons[self._lastInsertId].setToggleState(toggled)

    def appendToggleGroup(self, groupParams, groupId, defaults=-1):
        print 'GOT PARAMS', groupParams, groupId, defaults
        #if isinstance(groupParams, dict):
        #    groupParams = groupParams.values()
        self._toggleGroups[groupId] = []
        if isinstance(defaults, int):
            defaults = [defaults]
        #for i in range(len(groupParams)):
        for i in groupParams.iterkeys():
            #toggled = (default == i)
            toggled = i in defaults
            self.appendSingleToggleButton(
                groupParams[i]['icon'],
                groupParams[i]['backend'],
                groupParams[i]['params'],
                toggled
            )
            self._buttons[self._lastInsertId].setGroupInheritance(groupId)
            self._toggleGroups[groupId].append(self._lastInsertId)
        pass

    def toggleInGroup(self, groupId, toggleId):
        group = self._toggleGroups[groupId]
        for btnId in group:
            btnItem = self._buttons[btnId]
            if btnItem.getWxId() == toggleId:
                btnItem.setToggleState(True)
                #btnItem.backend(btnItem)
            else:
                btnItem.setToggleState(False)
                # @todo: implement lose toggle state

    def onFocus(self, params=None):
        # вычисляем положение каждый раз, так как размеры и положение родительского окна могут изменяться
        #if self.active:
        #    return
        pos = self._wxParent.GetScreenPosition()
        self._win.Position(pos, self._extent)
        self._win.Show(True)
        self.active = True

    def onFocusLost(self, params=None):
        self._win.Show(False)
        self.active = False

    associatedWindow = property(getAssociatedWindowNumber)


class popupHelperButton(object):
    def __init__(self, cParent, wxParent, text, icon, type, pos, backend, params):
        """
        @param cParent: родительский контроллер
        @type cParent: popupHelperController
        @param wxParent: родительское окно
        @type wxParent: NPPlan.view.client.main.abstracts.popupHelper.popupHelperView
        """
        self._controller = cParent
        self._parent = wxParent
        self.backend = backend
        self.params = params

        style = platebtn.PB_STYLE_DEFAULT | platebtn.PB_STYLE_SQUARE
        if 'toggle' == type:
            style = style | platebtn.PB_STYLE_TOGGLE
        elif 'dd' == type:
            style = style | platebtn.PB_STYLE_DROPARROW
        self.wxId = wx.NewId()
        if isinstance(icon, str):
            icon = wx.Bitmap(os.path.join(NPPlan.getIconPath(), icon), wx.BITMAP_TYPE_PNG)
        self.button = platebtn.PlateButton(self._parent, self.wxId, text, icon, style=style, pos=pos)
        self.button.SetWindowVariant(wx.WINDOW_VARIANT_SMALL)

        self._groupId = -1
        if 'toggle' == type:
            self._parent.Bind(wx.EVT_TOGGLEBUTTON, self.callBackend, id=self.wxId)
        elif 'dd' == type:
            self.button.Bind(platebtn.EVT_PLATEBTN_DROPARROW_PRESSED, self.callBackend, id=self.wxId)
        else:
            self._parent.Bind(wx.EVT_BUTTON, self.callBackend, id=self.wxId)

    def getWxId(self):
        return self.wxId

    def setToggleState(self, state):
        if state:
            self.button._SetState(platebtn.PLATE_PRESSED)
            self.button._pressed = True
        else:
            self.button._SetState(platebtn.PLATE_NORMAL)
            self.button._pressed = False

    def callBackend(self, event):
        print 'event called from popup', event.GetId()
        #self.backend(self)
        print self._controller._backendClass
        print self.backend
        getattr(self._controller._backendClass, self.backend)(self, event)

    def getButtonWidth(self):
        return self.button.GetBestSize().width

    def getButtonHeight(self):
        return self.button.GetBestSize().height

    def setGroupInheritance(self, groupId):
        print 'Setting inheritance'
        self._groupId = groupId
        self._parent.Unbind(wx.EVT_BUTTON, id=self.wxId)
        self._parent.Bind(wx.EVT_TOGGLEBUTTON, self.clickedInGroup, id=self.wxId)

    def clickedInGroup(self, event):
        print 'Clicked in Group'
        event.Skip()
        self._controller.toggleInGroup(self._groupId, event.GetId())
        #getattr(self._controller._backendClass, self.backend)(self, event)
        event.Skip()

    def getState(self):
        return self.button._pressed

    state = property(getState)