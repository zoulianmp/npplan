# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 13.11.12
@summary: 
'''


class handlerObject(object):
    def __init__(self, controller, view, meta={}):
        self.__dict__['_controller'] = controller
        self.__dict__['_view'] = view
        self.__dict__['_meta'] = meta

    def getController(self):
        return self.__dict__['_controller']
    controller = property(getController)

    def getView(self):
        return self.__dict__['_view']
    view = property(getView)

    def getMeta(self):
        return self.__dict__['_meta']
    meta = property(getMeta)


class handler():
    def __init__(self):
        self.__dict__['objects'] = {}
        pass

    def register(self, name, controller, view, meta={}, noLog=False):
        if not noLog:
            import NPPlan
            NPPlan.log('core.cHandler', 'Registered %s (C: %s, V: %s)' % (name, type(controller), type(view)), 2)
        self.__dict__['objects'][name] = handlerObject(controller, view, meta)
        if hasattr(controller, 'start'):
            if not noLog:
                import NPPlan
                NPPlan.log('core.cHandler', '%s starting' % name, 2)
            controller.start()

    def __getattr__(self, item):
        if item in self.__dict__['objects']:
            return self.__dict__['objects'][item]
        else:
            # @todo: throw exception
            return None

    def __contains__(self, item):
        return item in self.__dict__['objects']

