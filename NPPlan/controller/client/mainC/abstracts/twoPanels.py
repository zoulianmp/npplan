# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 2
@date: 16.11.12
@summary: 
'''

import NPPlan

import wx

try:
    from NPPlan.view.client.main.abstracts import twoPanelsView
    from NPPlan.view.client.main.abstracts import mainPanelContainer
    from NPPlan.view.client.main.abstracts import rightPanelContainer
except ImportError:
    # @todo: и этот пиздец убрать
    pass

class twoPanels(object):
    """
    @deprecated
    """
    def __init__(self, mainPanel, rightPanel):
        self.__dict__['mainPanel'] = mainPanel
        self.__dict__['rightPanel'] = rightPanel

    def doLayout(self):
        if hasattr(self.__dict__['mainPanel'], 'doLayout'):
            self.__dict__['mainPanel'].doLayout()
        if hasattr(self.__dict__['rightPanel'], 'doLayout'):
                    self.__dict__['rightPanel'].doLayout()

    def getMainPanel(self):
        return self.__dict__['mainPanel']
    def setMainPanel(self, panel):
        self.__dict__['mainPanel'].Destroy()
        self.__dict__['mainPanel'] = panel
    mainPanel = property(getMainPanel, setMainPanel)

    def getRightPanel(self):
        return self.__dict__['rightPanel']
    def setRightPanel(self, panel):
        self.__dict__['rightPanel'].Destroy()
        self.__dict__['rightPanel'] = panel
    rightPanel = property(getRightPanel, setRightPanel)

class abstractTwoPanels(object):
    def __init__(self, parent):
        """
        @param parent: родительское окно
        @type parent: wx.Panel
        """
        self.__dict__['parent'] = parent
        self.freeze()
        self.create()
        self.createMainPanel()
        self.createRightPanel()
        self.layout()
        self.thaw()

    def create(self):
        self.__dict__['child'] = twoPanelsView(self.__dict__['parent'])

    def createMainPanel(self):
        self.__dict__['mainPanel'] = mainPanelContainer(self.__dict__['child'])
        self.__dict__['child'].mainPanel = {
            'panel' : self.__dict__['mainPanel'],
            'proportion' : 8
        }

    def createRightPanel(self):
        self.__dict__['rightPanel'] = rightPanelContainer(self.__dict__['child'])
        self.__dict__['child'].rightPanel = {
            'panel' : self.__dict__['rightPanel'],
            'proportion' : 2
        }

    def layout(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.__dict__['child'], 1, wx.EXPAND)
        self.__dict__['parent'].SetSizer(sizer)
        self.__dict__['parent'].Layout()

    def freeze(self):
        self.__dict__['parent'].Freeze()
        self.__dict__['parent'].GetParent().Freeze()

    def thaw(self):
        self.__dict__['parent'].GetParent().Thaw()
        self.__dict__['parent'].Thaw()

    def getMainPanelItem(self):
        return self.__dict__['mainPanel'].item
    def setMainPanelItem(self, item):
        """
        @param item: окно
        @type item: wx.Panel
        """
        self.__dict__['mainPanel'].renderItem = item
        self.__dict__['mainPanel'].doLayout()
    mainPanel = property(getMainPanelItem, setMainPanelItem)

    def getRightPanelItem(self):
        return self.__dict__['rightPanel'].item
    def setRightPanelItem(self, item):
        self.__dict__['rightPanel'].renderItem = item
        self.__dict__['rightPanel'].doLayout()
    rightPanel = property(getRightPanelItem, setRightPanelItem)