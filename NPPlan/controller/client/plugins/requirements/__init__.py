# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 10.09.13
@summary: 
'''

import NPPlan


def testWx():
    try:
        wx = __import__('wx')
        NPPlan.log('plugin.requirements', 'wxPython %s found' % wx.VERSION_STRING, 10)
    except ImportError:
        NPPlan.log('plugin.requirements', 'wxPython not found', 10)


def testVtk():
    try:
        vtk = __import__('vtk')
        NPPlan.log('plugin.requirements', 'vtk %s found' % vtk.vtkVersion().GetVTKVersion(), 10)
    except ImportError:
        NPPlan.log('plugin.requirements', 'vtk not found', 10)


def testPymongo():
    try:
        pymongo = __import__('pymongo')
        NPPlan.log('plugin.requirements', 'pymongo %s found' % pymongo.version, 10)
    except ImportError:
        NPPlan.log('plugin.requirements', 'pymongo not found', 10)


def testMongokit():
    try:
        mongokit = __import__('mongokit')
        NPPlan.log('plugin.requirements', 'MongoKit ORM %s found' % mongokit.__version__, 10)
    except ImportError:
        NPPlan.log('plugin.requirements', 'MongoKit ORM not found', 10)


def testPydicom():
    try:
        dicom = __import__('dicom')
        NPPlan.log('plugin.requirements', 'pydicom %s found' % dicom.__version__, 10)
    except ImportError:
        NPPlan.log('plugin.requirements', 'pydicom not found', 10)


def testNumpy():
    try:
        numpy = __import__('numpy')
        NPPlan.log('plugin.requirements', 'numpy %s found' % numpy.version.full_version, 10)
    except ImportError:
        NPPlan.log('plugin.requirements', 'numpy not found', 10)


def testTwisted():
    try:
        twisted = __import__('twisted')
        NPPlan.log('plugin.requirements', 'Twisted %s found' % twisted.version, 10)
    except ImportError:
        NPPlan.log('plugin.requirements', 'Twisted not found', 10)


def testPythonValues():
    import sys
    import platform
    NPPlan.log('plugin.requirements', 'Python %s found at OS %s' % (sys.version, platform.system()), 10)


def testAll():
    testPythonValues()
    testWx()
    testVtk()
    testPymongo()
    testMongokit()
    testPydicom()
    testNumpy()
    testTwisted()


if __name__ == '__main__':
    testAll()

if __name__ == '__init__':
    testAll()