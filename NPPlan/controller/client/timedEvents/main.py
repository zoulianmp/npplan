# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 24.10.12
@summary: 
'''

import NPPlan
import wx
import NPPlan.controller.logger as log

class notifyTimer(wx.Timer):
    def __init__(self):
        wx.Timer.__init__(self)
        self.obs = observer()
        self._i = 0
        log.log('client.timedEvents.main', 'Main timer started')

    def Notify(self):
        #from NPPlan.controller.client.main import getHandler
        print 'Timer handler'
        #print getHandler().ribbon.controller.worker
        #getHandler().progress.controller.setCaption('Str: %d' %self._i)
        #self._i += 1
        #if self._i >= 3:
        #    getHandler().progress.controller.abort()
        pass

class observer(object):
    def __init__(self):
        self._values = {}

    def addValue(self, key, val):
        self._values[key] = val

    def getValue(self, key):
        return self._values[key]

    def hasValue(self, key):
        return self._values.has_key(key)