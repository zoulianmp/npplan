# -*- coding: utf8 -*-
'''
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 26.06.2012
@summary: package
'''

__all__ = [
    # основные пакеты
    'client',
    'server',

    # другие модули
    'console',
    'logger',
    'main',
]

