# -*- coding: utf8 -*-
'''
Вступительное окно (форма)
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 10.09.2012
@summary: Вступительное окно (форма)
'''
import wx
import NPPlan.controller.logger as log

# @todo: добавить информацию о процессе запуска
# @todo: возможно переделать в wx.Frame

import NPPlan
import os
import wx


class splashFrame(wx.Frame):
    def __init__(self):
        #wx.Display.ChangeMode()
        splashSize = (600, 600)
        displays = (wx.Display(i) for i in range(wx.Display.GetCount()))
        sizes = [display.GetGeometry().GetSize() for display in displays]
        #for i in displays:
        #    print i.GetName()
        #    print i.GetGeometry()
        #    print i.GetGeometry().GetSize()
        splashPos = (sizes[0][0] / 2 - splashSize[0] / 2, sizes[0][1] / 2 - splashSize[1] / 2)
        print sizes
        print splashPos
        wx.Frame.__init__(self, None, wx.ID_ANY, style=wx.CENTER_ON_SCREEN, size=splashSize, pos=splashPos)
        #self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        self.SetForegroundColour(wx.Colour(0, 0, 0))
        self.SetBackgroundColour(wx.Colour(255, 255, 255))
              
        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        # @todo: relative path to splash image
        # getIconPath not loaded yet
        self.m_bitmap1 = wx.StaticBitmap(self,
                                         wx.ID_ANY,
                                         wx.Bitmap(u"C:\\NPPlan3\\NPPlan\\images\\splash.jpg", wx.BITMAP_TYPE_ANY),
                                         #wx.Bitmap(NPPlan.getIcon('splash'), wx.BITMAP_TYPE_ANY),
                                         wx.DefaultPosition,
                                         wx.Size(512, 512),
                                         0)
        bSizer1.Add(self.m_bitmap1, 1, wx.ALL | wx.EXPAND, 5)

        self.m_staticline1 = wx.StaticLine(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        bSizer1.Add(self.m_staticline1, 0, wx.EXPAND | wx.ALL, 5)
              
        self.loadText = wx.StaticText(self, wx.ID_ANY, u"Loading: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.loadText.Wrap(-1)
        bSizer1.Add(self.loadText, 0, wx.ALL | wx.EXPAND, 5)
              
        self.SetSizer(bSizer1)
        self.Layout()
              
        self.Centre(wx.BOTH)
        #wx.FutureCall(1000, callback)
        #wx.FutureCall(2000, self.close)

    def close(self, event=0):
        self.Destroy()

    def setText(self, text):
        self.loadText.SetLabel(_('Loading: ') + unicode(text))

    def finish(self, ):
        self.loadText.SetLabel(_('Finished'))


class splashScreen(wx.SplashScreen):
    def __init__(self, img, callback):
        self.callback = callback
        log.log('splash', 'Splash screen started')
        bmp = wx.Image(img).ConvertToBitmap()
        wx.SplashScreen.__init__(self, bmp,
                                 wx.SPLASH_CENTRE_ON_SCREEN | wx.SPLASH_TIMEOUT,
                                 5000, None, -1)
        self.Bind(wx.EVT_CLOSE, self.onClose)
        # @todo: positioning
        # @todo: closing splash (probably timer broken)
        self.g2 = wx.Gauge(self, -1, 50, (110, 95), (250, 25))
        self.timer = wx.Timer(self)
        self.timer.Start(100)
        self.Bind(wx.EVT_TIMER, self.TimerHandler)
        self.fc = wx.FutureCall(2000, self.callback)

    def __del__(self):
        self.timer.Stop()

    def TimerHandler(self, event):
        self.g2.Pulse()

    def onClose(self, evt):
        evt.Skip()
        self.Hide()

        if self.fc.IsRunning():
            self.fc.Stop()
            self.callback()

if __name__ == '__main__':
    app = wx.App(False)
    f = splashFrame(None, None, None)
    f.Show()
    app.MainLoop()
