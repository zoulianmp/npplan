# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 03.02.14
@summary: 
'''


import NPPlan

import wx
import wx.lib.agw.labelbook as LB
from wx.lib.agw.fmresources import *


class imageContainer(LB.LabelContainer):
    def OnPaint(self, event):
        self._clientRect = self.GetClientRect()
        self._homeButton = None
        self._forwardButton = None
        self._backButton = None
        #pinRect = wx.Rect(clientRect.GetX() + clientRect.GetWidth() - 20, 2, 20, 20)
        #self.DrawPin(dc, pinRect, not self._bCollapsed)
        #print self.GetParent(), type(self.GetParent())
        LB.LabelContainer.OnPaint(self, event)

    def DrawPin(self, dc, pinRect, collapsed):
        LB.LabelContainer.DrawPin(self, dc, pinRect, collapsed)
        if not self._bCollapsed:
            self.draw3Buttons(dc)
        else:
            self._homeButton = None
            self._forwardButton = None
            self._backButton = None

    def draw3Buttons(self, dc):
        #print dc, self._clientRect
        pinRect = wx.Rect(self._clientRect.GetX() + 5, 2, 20, 20)
        self._homeButton = self.singleButtons(pinRect, dc, wx.ART_GO_HOME)
        pinRect = wx.Rect(self._clientRect.GetX() + 25, 2, 20, 20)
        self._backButton = self.singleButtons(pinRect, dc, wx.ART_GO_BACK)
        pinRect = wx.Rect(self._clientRect.GetX() + 45, 2, 20, 20)
        self._forwardButton = self.singleButtons(pinRect, dc, wx.ART_GO_FORWARD)

    def HitTest(self, pt):
        if self._bCollapsed:
            return LB.LabelContainer.HitTest(self, pt)
        try:
            if self._homeButton[1].Contains(pt) or \
                self._backButton[1].Contains(pt) or \
                self._forwardButton[1].Contains(pt):
                # @todo: IMG_OVER_PIN -> internal
                return -1, IMG_OVER_PIN
        except TypeError:
            return -1, IMG_OVER_PIN
            pass
        return LB.LabelContainer.HitTest(self, pt)
        pass

    def checkCaller(self):
        return hasattr(self.GetParent(), '_caller') and self.GetParent()._caller is not None

    def OnMouseLeftUp(self, event):
        if self._bCollapsed:
            LB.LabelContainer.OnMouseLeftUp(self, event)
            return
        pt = event.GetPosition()
        if self._homeButton is not None and self._homeButton[1].Contains(pt):
            if self.checkCaller():
                #print 'Call from NPPlan'
                #NPPlan._fullHome(event)
                # @todo: rework to controller
                NPPlan.cHandler.main.controller.cp._fullHome(event)
            #print 'Home Button left up'
        elif self._backButton is not None and self._backButton[1].Contains(pt):
            if self.checkCaller():
                NPPlan.cHandler.main.controller.cp.lbBackMouseUp(event)
            #print 'Back button left up'
        elif self._forwardButton is not None and self._forwardButton[1].Contains(pt):
            if self.checkCaller():
                NPPlan.cHandler.main.controller.cp.lbForwardMouseUp(event)
            #print 'Forward button left up'
        else:
            LB.LabelContainer.OnMouseLeftUp(self, event)
        pass

    def OnMouseLeftDown(self, event):
        if self._bCollapsed:
            LB.LabelContainer.OnMouseLeftDown(self, event)
            return
        pt = event.GetPosition()
        if self._homeButton is not None and self._homeButton[1].Contains(pt):
            #print 'Home Button left down'
            pass
        elif self._backButton is not None and self._backButton[1].Contains(pt):
            if self.checkCaller():
                NPPlan.cHandler.main.controller.cp.lbBackMouseDown(event)
            #print 'Back button left down'
        elif self._forwardButton is not None and self._forwardButton[1].Contains(pt):
            if self.checkCaller():
                NPPlan.cHandler.main.controller.cp.lbForwardMouseDown(event)
            #print 'Forward button left down'
        else:
            LB.LabelContainer.OnMouseLeftDown(self, event)
        pass

    def singleButtons(self, pinRect, dc, img):
        xx = pinRect.x + 2

        dc.SetBrush(wx.TRANSPARENT_BRUSH)
        dc.SetPen(wx.BLACK_PEN)
        dc.DrawRectangle(xx, pinRect.y, 16, 16)

        # Draw upper and left border with grey colour
        dc.SetPen(wx.WHITE_PEN)
        dc.DrawLine(xx, pinRect.y, xx + 16, pinRect.y)
        dc.DrawLine(xx, pinRect.y, xx, pinRect.y + 16)

        #pinBmp = wx.BitmapFromXPMData(pin_down_xpm)
        pinBmp = wx.ArtProvider.GetBitmap(img, wx.ART_TOOLBAR, (16, 16))

        pinBmp.SetMask(wx.Mask(pinBmp, wx.WHITE))

        dc.DrawBitmap(pinBmp, xx, pinRect.y, True)
        return pinBmp, pinRect
        pass
    pass


class labelBook(LB.LabelBook):
    def __init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=0, agwStyle=0, name="LabelBook", caller=None):
        LB.LabelBook.__init__(self, parent, id, pos, size, style, agwStyle, name)
        self._caller = caller
        pass

    def CreateImageContainer(self):
        """ Creates the image container class for :class:`FlatImageBook`. """

        return imageContainer(self, wx.ID_ANY, agwStyle=self.GetAGWWindowStyleFlag())

    #def SetSelection(self, page):
    #    print 'New Page own handler', page
    #    LB.LabelBook.SetSelection(self, page)
    pass

if __name__ == '__main__':
    class MyFrame(wx.Frame):

        def __init__(self, parent):

            wx.Frame.__init__(self, parent, -1, "LabelBook Demo")

            # Possible values for Tab placement are INB_TOP, INB_BOTTOM, INB_RIGHT, INB_LEFT

            notebook = labelBook(self, -1, agwStyle=LB.INB_FIT_LABELTEXT|LB.INB_LEFT|LB.INB_DRAW_SHADOW
                                                       |LB.INB_GRADIENT_BACKGROUND|LB.INB_USE_PIN_BUTTON)

            pane1 = wx.Panel(notebook)
            pane2 = wx.Panel(notebook)

            imagelist = wx.ImageList(32, 32)
            #imagelist.Add(wx.Bitmap("my_bitmap.png", wx.BITMAP_TYPE_PNG))
            notebook.AssignImageList(imagelist)

            notebook.AddPage(pane1, "Tab1", 1, 0)
            notebook.AddPage(pane2, "Tab2", 0, 0)


    # our normal wxApp-derived class, as usual

    app = wx.App(0)

    frame = MyFrame(None)
    app.SetTopWindow(frame)
    frame.Show()

    app.MainLoop()
