# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 07.03.13
@summary: 
'''

import NPPlan
import wx
import  wx.lib.mixins.listctrl  as  listmix
import wx.lib.platebtn as platebtn
import sys
import os


class TestListCtrl(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)


class listBase(wx.Panel):
    def __init__(self, parent, columns={}):
        """

        @param parent: родительский класс
        @type parent: listWithControls
        @param columns:
        @type columns: dict
        @return:
        """
        wx.Panel.__init__(self, parent)

        self.columns = columns
        self._parent = parent
        self._selected = -1
        self._data = {}

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.il = self.parseImageList()
        self.list = wx.ListCtrl(self, wx.NewId(),
                                 style=wx.LC_REPORT
                                 | wx.LC_VRULES
                                 | wx.LC_SINGLE_SEL
                                 )
        self.list.SetImageList(self.il, wx.IMAGE_LIST_SMALL)
        sizer.Add(self.list, 1, wx.EXPAND)
        self.SetSizer(sizer)

        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.onItemSelected, self.list)
        self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.onItemDeSelected, self.list)
        #self.list.Bind(wx.EVT_COMMAND_RIGHT_CLICK, self.OnRightClick)

        self.makeColumns()

    def getIconPath(self):
        if __name__ == '__main__':
            return 'C:\\NPPlan3\\NPPlan\\images\\'
        else:
            return NPPlan.getIconPath()

    def parseImageList(self):
        uid = 0
        il = wx.ImageList(16, 16)
        print self.columns
        print sorted(self.columns, key=lambda col: self.columns[col]['order'])
        for i in sorted(self.columns, key=lambda col: self.columns[col]['order']):
            data = self.columns[i]
            if 'image' in data:
                print 'image in data'
                print os.path.join(self.getIconPath(), data['image'])
                il.Add(wx.Bitmap(os.path.join(self.getIconPath(), data['image'])))
            else:
                il.Add(wx.Bitmap(os.path.join(self.getIconPath(), 'iconEmpty16.png')))
        print 'il', il, il.ImageCount
        return il
        pass

    def onItemSelected(self, event):
        """
        @param event: событие
        @type event: wx._controls.ListEvent
        @return:
        """
        #print event.m_itemIndex, self.list.GetItemData(event.m_itemIndex), self._data[event.m_itemIndex]
        self._selected = event.m_itemIndex
        self._parent.callListBackendSelected(event, self._data[event.m_itemIndex])
        self._parent.activateButtons()
        event.Skip()
        pass

    def onItemDeSelected(self, event):
        #print event
        self._parent.callListBackendUnSelected(event)
        self._parent.activateButtons(False)
        self._selected = -1
        #print self.list.SelectedItemCount()

    def getSelectedIndex(self):
        return self.list.GetNextSelected(-1)

    selectionIndex = property(getSelectedIndex)

    def makeColumns(self):
        uid = 0
        for i in sorted(self.columns, key=lambda col: self.columns[col]['order']):
            data = self.columns[i]
            if 1:
                info = wx.ListItem()
                info.m_mask = wx.LIST_MASK_TEXT | wx.LIST_MASK_IMAGE | wx.LIST_FORMAT_LEFT | wx.LIST_MASK_FORMAT
                info.m_text = data['text']
                info.m_image = uid
                info.m_format = 0
                self.list.InsertColumnInfo(uid, info)
            else:
                self.list.InsertColumn(uid, data['text'])
            self.columns[i]['_uid'] = uid
            uid += 1

        for i in self.columns:
            if 'auto' in self.columns[i] and self.columns[i]['auto']:
                self.list.SetColumnWidth(self.columns[i]['_uid'], wx.LIST_AUTOSIZE)
            elif 'size' in self.columns[i]:
                self.list.SetColumnWidth(self.columns[i]['_uid'], self.columns[i]['size'])

    def insertString(self, data):
        #index = self.list.InsertStringItem(sys.maxint, data[0], -1)
        item = wx.ListItem()
        #t = self.list.SetStringItem(index, i, '')
        bmp = wx.EmptyBitmap(16, 16, 32)
        #pixelData = wx.AlphaPixelData(bmp)
        #for pixel in pixelData:
        #    pixel.Set(data[0].red, data[0].green, data[0].blue, 255)
        self.il.Add(bmp)
        item.SetImage(self.il.ImageCount - 1)
        index = self.list.InsertImageItem(sys.maxint, self.il.ImageCount - 1)
        self._data[index] = data
        for i in range(1, len(data)):
            self.list.SetStringItem(index, i, data[i])
        return index

    def addItem(self, data):
        index = -1
        for i in sorted(self.columns, key=lambda col: self.columns[col]['order']):
            print i, data[i]
            if -1 == index:
                if isinstance(data[i], wx.Colour):
                    item = wx.ListItem()
                    bmp = wx.EmptyBitmap(16, 16, 32)
                    pixelData = wx.AlphaPixelData(bmp)
                    for pixel in pixelData:
                        pixel.Set(data[i].red, data[i].green, data[i].blue, 255)
                    self.il.Add(bmp)
                    item.SetImage(self.il.ImageCount - 1)
                    index = self.list.InsertImageItem(sys.maxint, self.il.ImageCount - 1)
                else:
                    index = self.list.InsertStringItem(sys.maxint, data[i], -1)
                # self._data[index] = data
            else:
                self.list.SetStringItem(index, self.columns[i]['order'], data[i])
        self._data[index] = data

    def deleteString(self, item):
        self.list.DeleteItem(item)

    def colorize(self, itemId, color):
        if -1 == itemId:
            itemId = self.list.GetItemCount() - 1
        item = self.list.GetItem(itemId)
        item.SetTextColour(color)
        self.list.SetItem(item)

    def setItemTextAtColumn(self, itemId, column, text=''):
        if -1 == itemId:
            itemId = self.list.GetItemCount() - 1
        self.list.SetStringItem(itemId, column, text)
        self._data[itemId][sorted(self.columns, key=lambda col: self.columns[col]['order'])[column]] = text

    def setItemColor(self, itemId, color, column=0):
        if -1 == itemId:
            itemId = self.list.GetItemCount() - 1
        item = self.list.GetItem(itemId)
        bmp = wx.EmptyBitmap(16, 16, 32)
        pixelData = wx.AlphaPixelData(bmp)
        for pixel in pixelData:
            pixel.Set(color.red, color.green, color.blue, 255)
        self.il.Add(bmp)
        item.SetImage(self.il.ImageCount - 1)
        self.list.SetItem(item)
        self._data[itemId][sorted(self.columns, key=lambda col: self.columns[col]['order'])[column]] = color


class listWithControls(wx.Panel):
    def __init__(self, parent, columns={}, controls={}, controlsPosition='up', parser=None):
        """

        @param parent: родительское окно
        @type parent: wx.Window
        @param columns: список столбцов
        @type columns: dict
        @param controls: список кнопок контроллера
        @type controls: dict
        @param controlsPosition: положение контроллера, in ['up', 'down', 'left', 'right']
        @type controlsPosition: string
        @param parser: функция-парсер события выборки
        @type parser: function
        @return:
        """
        wx.Panel.__init__(self, parent)

        self._list = {'id': wx.NewId(), 'object': None}

        self._columns = columns
        self._controls = {'data': controls, 'position': controlsPosition, 'buttons': {}, 'ids': {}}
        self._tId = 0
        self._data = {}
        self._activeOnSelect = []
        self._parser = parser

        if self._controls['position'] in ['up', 'down']:
            self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        else:
            self.mainSizer = wx.BoxSizer(wx.HORIZONTAL)

        if self._controls['position'] in ['up', 'left']:
            self.addControls()
            self.addList()
        else:
            self.addList()
            self.addControls()

        self.SetSizer(self.mainSizer)

    def GetFirstSelected(self, *args, **kwargs):
        return self._list['object'].list.GetFirstSelected(*args, **kwargs)

    def GetNextSelected(self, *args, **kwargs):
        return self._list['object'].list.GetNextSelected(*args, **kwargs)

    def activateButtons(self, state=True):
        for i in self._activeOnSelect:
            self._controls['buttons'][i].Enable(state)
            pass
        pass

    def getCurrentSelected(self):
        return self._list['object']._selected

    def setCurrentSelected(self, id=-1):
        if -1 == id:
            id = self.getItemCount() - 1
        if id > self.getItemCount() - 1:
            return
        self._list['object'].list.Select(id)

    def getColumnsLength(self):
        return len(self._columns)

    def getColumnsData(self):
        #for i in sorted(self._columns, key=lambda col: self._columns[col]['order']):
        #    print i, self._columns[i]
        return [self._columns[i] for i in sorted(self._columns, key=lambda col: self._columns[col]['order'])]

    def setParser(self, parser):
        self._parser = parser

    def colorize(self, itemId, color):
        self._list['object'].colorize(itemId, color)

    def addList(self):
        self._list['object'] = listBase(self, columns=self._columns)
        self.mainSizer.Add(self._list['object'], 1, wx.EXPAND)

    def callListBackendSelected(self, event, data):
        """
        @param event: событие
        @type event: wx._controls.ListEvent
        @param data: список значений ячеек выбранной строки в порядке следования столбцов
        @type data: list
        @return:
        """
        #print caller, data
        self._parser(event, data)
        pass

    def callListBackendUnSelected(self, event):
        """
        @param event: событие
        @type event: wx._controls.ListEvent
        """
        self._parser(event, aType='unselected')

    def addControls(self):
        if self._controls['position'] in ['up', 'down']:
            self._controls['sizer'] = wx.BoxSizer(wx.HORIZONTAL)
        else:
            self._controls['sizer'] = wx.BoxSizer(wx.VERTICAL)
        for i in self._controls['data']:
            if __name__ == '__main__':
                # did for own run, @todo: remove in real project
                bitmap = wx.Bitmap(os.path.join('C:\\NPPlan3\\NPPlan\\images\\', self._controls['data'][i]['icon']),
                                   wx.BITMAP_TYPE_PNG)
            else:
                bitmap = wx.Bitmap(os.path.join(NPPlan.getIconPath(), self._controls['data'][i]['icon']),
                                   wx.BITMAP_TYPE_PNG)
            _id = wx.NewId()
            self._controls['ids'][_id] = i
            self._controls['buttons'][i] = platebtn.PlateButton(
                self,
                _id,
                '',
                bitmap,
                style=wx.WINDOW_VARIANT_SMALL
            )
            self._controls['sizer'].Add(self._controls['buttons'][i])
            self.Bind(wx.EVT_BUTTON, self.callButtonBackend, self._controls['buttons'][i])
            if 'activation' in self._controls['data'][i] and 1 == self._controls['data'][i]['activation']:
                self._activeOnSelect.append(i)
        self.activateButtons(False)
        pass
        self.mainSizer.Add(self._controls['sizer'])

    def callButtonBackend(self, event):
        """
        @param event:
        @type event: wx._core.CommandEvent
        @return:
        """
        #print event.GetId()
        #print type(event)
        #print self._controls['ids'][event.GetId()]
        bId = self._controls['ids'][event.GetId()]
        #print self._controls['data'][bId]['backend']
        self._controls['data'][bId]['backend'](event, self._controls['data'][bId])
        event.Skip()

    def addLine(self, values=[]):
        # @todo: учитывать порядок столбцов
        self._data[self._tId] = values
        self._tId += 1
        self._list['object'].insertString(values)
        pass

    def addItem(self, data, autoselect=False):
        self._list['object'].addItem(data)
        if autoselect:
            self.setCurrentSelected(-1)

    def setItemTextAtColumn(self, itemId, column, text=''):
        self._list['object'].setItemTextAtColumn(itemId, column, text)

    def setItemColor(self, itemId, color):
        self._list['object'].setItemColor(itemId, color)

    def clear(self):
        self._list['object'].list.DeleteAllItems()
        self._data = {}

    def deleteLine(self, id):
        if -1 == id:
            id = self._tId - 1
        self._list['object'].deleteString(id)
        del self._data[id]
        self._tId -= 1

    def getItemCount(self):
        return self._list['object'].list.GetItemCount()

    def getLastItemIndex(self):
        return self.getItemCount() - 1

    selected = property(getCurrentSelected, setCurrentSelected)


def parseSelection(event=None, data=None, aType='selection'):
    """
    @param event: событие
    @type event: wx._controls.ListEvent
    @param data: список значений ячеек выбранной строки в порядке следования столбцов
    @type data: list
    @param aType: тип события, one of ['selection']
    @type aType: string
    @return:
    """
    print event, event.m_itemIndex, data, aType
    pass


class Test(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, None, 1, 'LIST TEST', size=(520, 611))
        sizer = wx.BoxSizer(wx.VERTICAL)

        self.window = listWithControls(self,
                               columns={
                                   '1': {'text': 'data', 'size': 300, 'order': 1, 'image': 'iconBack16.png'},
                                   '2': {'text': 'id', 'size': 100, 'order': 0},
                                   '3': {'text': 'id2', 'order': 2, 'image': 'iconForward16.png'},
                               }, controls={
                                   'add': {'text': 'Add', 'backend': self.backAdd, 'icon': 'iconPlus32.png'},
                                   'del': {'text': 'Delete', 'backend': self.backDel, 'icon': 'iconMinus32.png',
                                           'activation': 1},
                                },
                               controlsPosition='up',
                               parser=parseSelection,
                               )
        sizer.Add(self.window, 1, wx.EXPAND)
        self.SetSizer(sizer)

        self.start()

    def backAdd(self, event, data):
        print event, data
        self.window._list['object'].insertString(['1!2!3', '456', '789'])
        print self.window.selected
        pass

    def backDel(self, event, data):
        print event, data
        #print event.m_itemIndex
        #id = self.window._list['object']._selected
        if 0 == self.window._list['object'].list.GetSelectedItemCount():
            print 'No selection'
            return
        else:
            id = self.window._list['object'].list.GetNextSelected(-1)
            print id
            self.window.deleteLine(id)
        pass

    def start(self):
        self.window.addLine(['123', '456', '789'])
        self.window.addLine(['223', '45__6', '!789'])
        self.window.addLine(['333', '45___6', '!!789'])

        self.window.colorize(-1, wx.Color(255, 0, 255))

        self.window.addItem({'2': wx.Color(255, 0, 255), '1': 'blabla', '3': 'blabla3'})
        self.window.addItem({'2': 'ppp', '1': 'blabla', '3': 'blabla3'})

        self.window.setItemTextAtColumn(-1, 0, '')
        self.window.setItemColor(-1, wx.Color(255, 255, 0))

        print self.window.getItemCount()

        self.window.setCurrentSelected(-1)

        #self.window.deleteLine(1)

        pass


if __name__ == '__main__':
    app = wx.PySimpleApp()
    rtw = Test(app)
    rtw.Show()
    app.MainLoop()
    pass
