# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 24.12.12
@summary: 
'''

import NPPlan

from vtk.wx.wxVTKRenderWindowInteractor import *
import wx
from helper import popupWindowHelper as windowHelper


class vtkBaseWindow(wx.ScrolledWindow):
    def __init__(self, parent):
        wx.ScrolledWindow.__init__(self, parent, wx.ID_ANY)

        sizer = wx.BoxSizer(wx.VERTICAL)

        self.__dict__['vtkWindow'] = wxVTKRenderWindowInteractor(self, wx.NewId(), name='viewer')
        sizer.Add(self.__dict__['vtkWindow'], 1, wx.EXPAND)
        self.SetSizer(sizer)

        self.controller = None

        self.popup = None

        #self.Bind(wx.EVT_MOTION, self.showHelper)

    def showHelper(self, wN, event, params={}):
        #if self.popup is None:
        #    win = windowHelper(self, wx.SIMPLE_BORDER, wN, params=self.__dict__['helperParams'])
        #    pos = self.GetScreenPosition()
        #    print pos
        #    win.Position(pos, (10, 10))
        #    self.popup = win
        #if not self.popup.IsShown():
        #    self.popup.Show()
        event.Skip()
        #if self.popup.firstTime:
        #    return self.popup

    def hideHelper(self, event):
        #print 'hide helper called'
        #if self.popup is None:
        #    return
        #self.popup.Show(False)
        event.Skip()

    def appendHelper(self, params):
        self.__dict__['helperParams'] = params
        pass

    def getDC(self):
        return wx.ClientDC(self.__dict__['vtkWindow'])

    def getVtkWindow(self):
        return self.__dict__['vtkWindow']

    vtkWindow = property(getVtkWindow)
    vtkDC = property(getDC)

