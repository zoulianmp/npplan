# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 21.12.12
@summary: 
'''

import NPPlan
import wx
import abc
try:
    from agw import foldpanelbar as fpb
except ImportError:
    import wx.lib.agw.foldpanelbar as fpb


class foldPanelBase(wx.Panel):
    __metaclass__ = abc.ABCMeta

    def __init__(self, parent, **kwargs):
        wx.Panel.__init__(self, parent, -1)

        self._controller = kwargs.get('cParent', None)

        self.createControls(**kwargs)
        self.GetSizer().Fit(self)
        self.GetSizer().SetSizeHints(self)
        self.GetSizer().Layout()

    @abc.abstractmethod
    def createControls(self, **kwargs):
        pass


class rightFoldPanelBase(wx.Panel):
    """
    Базовый абстрактный класс правой foldbar-панели
    @todo: implement separator
    """
    # @todo: implement separator
    def __init__(self, parent):
        """
        Конструктор
        @param parent: родительское окно
        @type parent: wx.Panel
        """
        wx.Panel.__init__(self, parent)
        self.__dict__['parent'] = parent
        self.__dict__['panels'] = {}
        self.makeContent()

    def makeContent(self):
        """
        Создаёт первичную панель в конструкторе
        """
        self._pnl = fpb.FoldPanelBar(self, -1, wx.DefaultPosition, wx.Size(-1,-1), agwStyle=0)

        Images = wx.ImageList(16,16)
        Images.Add(GetExpandedIconBitmap())
        Images.Add(GetCollapsedIconBitmap())
        self.__dict__['images'] = Images

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self._pnl, 1, wx.ALL|wx.EXPAND)
        self.SetSizer(sizer)

    def addPanel(self, id, baseClass, text='Panel', size=(-1, 250), **kwargs):
        """
        Добавляет панель
        @param id: идентификатор панели
        @type id: string
        @param baseClass: базовый класс
        @type baseClass: proxy of wx.Panel
        @param text: текст для отображения
        @type text: string
        @param size: размеры окна класса
        @type size: tuple
        """
        # @todo: implement visible flag
        item = {
            'container': self._pnl.AddFoldPanel(text, collapsed=False, foldIcons=self.__dict__['images']),
            'size' : size,
            'visible' : True,
            'text' : text,
        }
        item['view'] = baseClass(item['container'], **kwargs)
        self._pnl.AddFoldPanelWindow(item['container'], item['view'], fpb.FPB_ALIGN_WIDTH, 2, 20)
        self.__dict__['panels'][id] = item

    def doLayout(self):
        """
        Переделывает макет окна
        """
        self._pnl.Layout()
        self.Layout()
        for i in self.__dict__['panels']:
            if self.__dict__['panels'][i]['visible']:
                self.__dict__['panels'][i]['view'].Layout()
                self.__dict__['panels'][i]['view'].SetSize(self.__dict__['panels'][i]['size'])
        self._pnl.RedisplayFoldPanelItems()

    def collapseAll(self):
        """
        Сворачивает все видимые панели
        """
        for i in self.__dict__['panels']:
            if self.__dict__['panels'][i]['visible']:
                self.__dict__['panels'][i]['container'].Collapse()

    def collapseById(self, id):
        """
        Сворачивает панель по id
        @todo: handle exception if panel is not visible
        """
        self.__dict__['panels'][id]['container'].Collapse()

    def hasPanel(self, id):
        """
        Возвращает true, если панель с таким id есть в списке панелей
        @return: флаг существования
        @rtype: bool
        """
        return self.__dict__['panels'].has_key(id)

    def setVisibleList(self, panels):
        """
        Устанавливает флаг видимости в True у всех панелей, имеющих id, переданные в 1м аргументе
        @param panels: список панелей для смены флага видимости
        @type panels: list
        """
        for i in panels:
            self.__dict__['panels'][i]['visible'] = True

    def clearAll(self, flag=True):
        """
        Очищает текущий объект foldpanel. Если передано True, то устанавливает у всех панелей флаг видимости в False
        @param flag: установить ли флаг видимости для панелей
        @type flag: bool
        """
        # @todo: проверить на утечку памяти
        if flag:
            for i in self.__dict__['panels']:
                self.__dict__['panels'][i]['visible'] = False
        self._pnl.Destroy()
        self._pnl = fpb.FoldPanelBar(self, -1, wx.DefaultPosition, wx.Size(-1,-1), agwStyle=0)
        self.resize()

    def resize(self):
        """
        Пересобирает окно
        """
        # @todo: проверить на утечку памяти
        self.GetSizer().Detach(0)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self._pnl, 1, wx.ALL|wx.EXPAND)
        self.SetSizer(sizer)

    def getViewForId(self, id):
        """
        Возвращает экземпляр базового класса этой панели (@see addPanel)
        @param id: идентфикатор
        @type id: string
        @return: окно
        @rtype: wx.Panel
        """
        # @todo: handle exception if panel is not visible
        return self.__dict__['panels'][id]['view']

def GetCollapsedIconData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x8eIDAT8\x8d\xa5\x93-n\xe4@\x10\x85?g\x03\n6lh)\xc4\xd2\x12\xc3\x81\
\xd6\xa2I\x90\x154\xb9\x81\x8f1G\xc8\x11\x16\x86\xcd\xa0\x99F\xb3A\x91\xa1\
\xc9J&\x96L"5lX\xcc\x0bl\xf7v\xb2\x7fZ\xa5\x98\xebU\xbdz\xf5\\\x9deW\x9f\xf8\
H\\\xbfO|{y\x9dT\x15P\x04\x01\x01UPUD\x84\xdb/7YZ\x9f\xa5\n\xce\x97aRU\x8a\
\xdc`\xacA\x00\x04P\xf0!0\xf6\x81\xa0\xf0p\xff9\xfb\x85\xe0|\x19&T)K\x8b\x18\
\xf9\xa3\xe4\xbe\xf3\x8c^#\xc9\xd5\n\xa8*\xc5?\x9a\x01\x8a\xd2b\r\x1cN\xc3\
\x14\t\xce\x97a\xb2F0Ks\xd58\xaa\xc6\xc5\xa6\xf7\xdfya\xe7\xbdR\x13M2\xf9\
\xf9qKQ\x1fi\xf6-\x00~T\xfac\x1dq#\x82,\xe5q\x05\x91D\xba@\xefj\xba1\xf0\xdc\
zzW\xcff&\xb8,\x89\xa8@Q\xd6\xaaf\xdfRm,\xee\xb1BDxr#\xae\xf5|\xddo\xd6\xe2H\
\x18\x15\x84\xa0q@]\xe54\x8d\xa3\xedf\x05M\xe3\xd8Uy\xc4\x15\x8d\xf5\xd7\x8b\
~\x82\x0fh\x0e"\xb0\xad,\xee\xb8c\xbb\x18\xe7\x8e;6\xa5\x89\x04\xde\xff\x1c\
\x16\xef\xe0p\xfa>\x19\x11\xca\x8d\x8d\xe0\x93\x1b\x01\xd8m\xf3(;x\xa5\xef=\
\xb7w\xf3\x1d$\x7f\xc1\xe0\xbd\xa7\xeb\xa0(,"Kc\x12\xc1+\xfd\xe8\tI\xee\xed)\
\xbf\xbcN\xc1{D\x04k\x05#\x12\xfd\xf2a\xde[\x81\x87\xbb\xdf\x9cr\x1a\x87\xd3\
0)\xba>\x83\xd5\xb97o\xe0\xaf\x04\xff\x13?\x00\xd2\xfb\xa9`z\xac\x80w\x00\
\x00\x00\x00IEND\xaeB`\x82'

def GetCollapsedIconBitmap():
    return wx.BitmapFromImage(GetCollapsedIconImage())

def GetCollapsedIconImage():
    import cStringIO
    stream = cStringIO.StringIO(GetCollapsedIconData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def GetExpandedIconData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x9fIDAT8\x8d\x95\x93\xa1\x8e\xdc0\x14EO\xb2\xc4\xd0\xd2\x12\xb7(mI\
\xa4%V\xd1lQT4[4-\x9a\xfe\xc1\xc2|\xc6\xc2~BY\x83:A3E\xd3\xa0*\xa4\xd2\x90H!\
\x95\x0c\r\r\x1fK\x81g\xb2\x99\x84\xb4\x0fY\xd6\xbb\xc7\xf7>=\'Iz\xc3\xbcv\
\xfbn\xb8\x9c\x15 \xe7\xf3\xc7\x0fw\xc9\xbc7\x99\x03\x0e\xfbn0\x99F+\x85R\
\x80RH\x10\x82\x08\xde\x05\x1ef\x90+\xc0\xe1\xd8\ryn\xd0Z-\\A\xb4\xd2\xf7\
\x9e\xfbwoF\xc8\x088\x1c\xbbae\xb3\xe8y&\x9a\xdf\xf5\xbd\xe7\xfem\x84\xa4\
\x97\xccYf\x16\x8d\xdb\xb2a]\xfeX\x18\xc9s\xc3\xe1\x18\xe7\x94\x12cb\xcc\xb5\
\xfa\xb1l8\xf5\x01\xe7\x84\xc7\xb2Y@\xb2\xcc0\x02\xb4\x9a\x88%\xbe\xdc\xb4\
\x9e\xb6Zs\xaa74\xadg[6\x88<\xb7]\xc6\x14\x1dL\x86\xe6\x83\xa0\x81\xba\xda\
\x10\x02x/\xd4\xd5\x06\r\x840!\x9c\x1fM\x92\xf4\x86\x9f\xbf\xfe\x0c\xd6\x9ae\
\xd6u\x8d \xf4\xf5\x165\x9b\x8f\x04\xe1\xc5\xcb\xdb$\x05\x90\xa97@\x04lQas\
\xcd*7\x14\xdb\x9aY\xcb\xb8\\\xe9E\x10|\xbc\xf2^\xb0E\x85\xc95_\x9f\n\xaa/\
\x05\x10\x81\xce\xc9\xa8\xf6><G\xd8\xed\xbbA)X\xd9\x0c\x01\x9a\xc6Q\x14\xd9h\
[\x04\xda\xd6c\xadFkE\xf0\xc2\xab\xd7\xb7\xc9\x08\x00\xf8\xf6\xbd\x1b\x8cQ\
\xd8|\xb9\x0f\xd3\x9a\x8a\xc7\x08\x00\x9f?\xdd%\xde\x07\xda\x93\xc3{\x19C\
\x8a\x9c\x03\x0b8\x17\xe8\x9d\xbf\x02.>\x13\xc0n\xff{PJ\xc5\xfdP\x11""<\xbc\
\xff\x87\xdf\xf8\xbf\xf5\x17FF\xaf\x8f\x8b\xd3\xe6K\x00\x00\x00\x00IEND\xaeB\
`\x82'

def GetExpandedIconBitmap():
    return wx.BitmapFromImage(GetExpandedIconImage())

def GetExpandedIconImage():
    import cStringIO
    stream = cStringIO.StringIO(GetExpandedIconData())
    return wx.ImageFromStream(stream)