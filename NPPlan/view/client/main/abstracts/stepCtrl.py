# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.03.13
@summary: 
'''

import wx


class spinStepCtrl(wx.SpinCtrl):
    def __init__(self, *args, **kwargs):
        wx.SpinCtrl.__init__(self, *args, **kwargs)
        self.step = 1.5
        self.Bind(wx.EVT_SPIN_UP, self.OnUp)
        self.Bind(wx.EVT_SPIN_DOWN, self.OnDown)

    def OnUp(self, event):
        self.SetValue(self.GetValue() + self.step)

    def OnDown(self, event):
        self.SetValue(self.GetValue() - self.step)

# @todo: implement float spinCtrl with step lock button