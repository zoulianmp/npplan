# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 15.03.13
@summary: 
'''

import NPPlan
import wx
import abc
import os
import wx.lib.platebtn as platebtn


class txtBtnCtrlBase(wx.TextCtrl):
    def __init__(self, *args, **kwargs):
        super(txtBtnCtrlBase, self).__init__(*args, **kwargs)
        self._locked = False
        #wx.TextCtrl.__init__(self, *args, **kwargs)

    def isLocked(self):
        return self._locked

    def locked(self, value=True):
        self._locked = value


class txtBtnCtrlViewBase(object):
    #__metaclass__ = abc.ABCMeta

    def __init__(self, *args, **kwargs):
        pass
        print 'txtBtnCtrlViewBase', kwargs.get('size')
        self._textSize = kwargs.get('size', (-1, -1))
        self._textValue = kwargs.get('value', '')
        # not working in this type of inheritance
        #self._lockPreBackend = kwargs.get('preBackend', None)
        #self._lockPostBackend = kwargs.get('postBackend', None)
        #self._autofocus = kwargs.get('autofocus', False)
        self._lockPreBackend = None
        self._lockPostBackend = None
        super(txtBtnCtrlViewBase, self).__init__(*args, **kwargs)
        self._createInterior()
        self._layoutInterior()
        self._doBindings()

    #@abc.abstractmethod
    def _createInterior(self):
        self._text = self._createTextCtrl()
        self._button = self._createButton()
        pass

    #@abc.abstractmethod
    def _layoutInterior(self):
        pass

    def _createTextCtrl(self):
        return self

    def _createButton(self):
        return self

    def _doBindings(self):
        self._button.Bind(wx.EVT_BUTTON, self._buttonClicked)

    def _buttonClicked(self, event):
        event.Skip()
        if self._lockPreBackend is not None:
            self._lockPreBackend(event=event, state=self._text.isLocked(), text=self._text.GetValue())
        self._text.locked(not self._text.isLocked())
        self._text.Enable(not self._text.isLocked())
        if self._text.isLocked():
            self._button.SetBitmap(self.lockIcon)
        else:
            self._button.SetBitmap(self.unlockIcon)
        if self._lockPostBackend is not None:
            self._lockPostBackend(event=event, state=self._text.isLocked(), text=self._text.GetValue())
        if not self._text.isLocked():
            self._text.SetFocus()
        event.Skip()
        pass

    def Enable(self, value):
        if value:
            self._text.locked(False)
            self._text.Enable(True)
            self._button.SetBitmap(self.unlockIcon)
        else:
            self._text.locked(True)
            self._text.Enable(False)
            self._button.SetBitmap(self.lockIcon)

    def GetValue(self):
        if self._text == self:
            return super(txtBtnCtrlViewBase, self).GetValue()
        else:
            return self._text.GetValue()

    def IsEmpty(self):
        if self._text == self:
            return super(txtBtnCtrlViewBase, self).IsEmpty()
        else:
            return self._text.IsEmpty()

    def SetValue(self, value):
        if self._text == self:
            super(txtBtnCtrlViewBase, self).SetValue(value)
        else:
            self._text.SetValue(value)

    def IsLocked(self):
        return self._text.isLocked()


class txtBtnCtrlClass(txtBtnCtrlViewBase, wx.Panel):
    def _createTextCtrl(self):
        style = 0
        self._textWxId = wx.NewId()
        return txtBtnCtrlBase(self, id=self._textWxId, style=style, size=self._textSize, value=self._textValue)

    def assignLockBackend(self, postBackend=None, preBackend=None):
        self._lockPreBackend = preBackend
        self._lockPostBackend = postBackend

    def _createButton(self):
        #NPPlan.ge
        unlockIconPath = os.path.join(NPPlan.getIconPath(), 'iconUnlock16.png')
        lockIconPath = os.path.join(NPPlan.getIconPath(), 'iconLock16.png')
        self.lockIcon = wx.Bitmap(lockIconPath, wx.BITMAP_TYPE_PNG)
        self.unlockIcon = wx.Bitmap(unlockIconPath, wx.BITMAP_TYPE_PNG)
        #print unlockIconPath, lockIconPath
        self._btnWxId = wx.NewId()
        btn = platebtn.PlateButton(
            self,
            self._btnWxId,
            '',
            self.unlockIcon,
            style=wx.WINDOW_VARIANT_SMALL | platebtn.PB_STYLE_SQUARE | platebtn.PB_STYLE_NOBG
        )
        btn.SetPressColor(wx.WHITE)
        btn.SetBackgroundColour(wx.WHITE)
        btn.SetForegroundColour(wx.WHITE)
        btn.SetOwnBackgroundColour(wx.WHITE)
        btn.SetOwnForegroundColour(wx.WHITE)
        return btn

        pass

    def _layoutInterior(self):
        panelSizer = wx.BoxSizer(wx.HORIZONTAL)
        panelSizer.Add(self._text, flag=wx.EXPAND, proportion=1)
        panelSizer.Add(self._button, 0, wx.LEFT | wx.CENTER, 2)
        self.SetSizerAndFit(panelSizer)


class Test(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, None, 1, 'OWN TEXT CONTROL WITH BUTTONS TEST', size=(520, 611))
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetBackgroundColour(wx.Color(255, 255, 255))

        self.ctrl = txtBtnCtrlClass(self)
        self.ctrl.assignLockBackend(postBackend=self.test)
        print self.ctrl.IsLocked()
        self.ctrl.SetValue('123')
        self.ctrl.Enable(False)
        print self.ctrl.IsLocked()
        self.ctrl2 = txtBtnCtrlClass(self)

        sizer.Add(self.ctrl, 0, wx.TOP|wx.LEFT, 8)
        sizer.Add(self.ctrl2, 0, wx.TOP|wx.LEFT, 8)
        self.SetSizer(sizer)

    def test(self, event=None, state=None, text=None):
        print event, state, text
        print self.ctrl.GetValue()


if __name__ == '__main__':
    app = wx.PySimpleApp()
    rtw = Test(app)
    rtw.Show()
    app.MainLoop()
    pass