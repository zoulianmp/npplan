# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 04.02.14
@summary:
@todo:
1. Формирование колонок по передаваемым параметрами
2. Проброс функции бекенда для кнопок добавления/удаления/сохранения
3. Проброс функции бекенда для выделения элемента
4. Баг с "залезанием" на vtk-окно при прокрутке (возможно убрать прокрутку в конкретном случае)
5. (Возможно) реализовать свою DataViewModel
'''

import wx
import wx.dataview as dv
import os
import wx.lib.platebtn as platebtn
from NPPlan.model.plan.positioning import wxDVListModel, planListModel
import NPPlan


class listPanel(wx.Panel):
    def __init__(self, parent, id=wx.ID_ANY, columns=[]):
        """

        @param parent:
        @type parent: listWithControls
        @param id:
        @param columns:
        @return:
        """
        wx.Panel.__init__(self, parent, id)
        self._parent = parent

        self.dvc = dvc = dv.DataViewListCtrl(self)

        #data = dict()
        #data[0] = planListModel(0, None, "123", (22, 150, 27), "p", "aaa\bccc\nnn", "aaa\bccc\nnn")
        #data[1] = planListModel(0, None, "456", (180, 12, 54), "p", "aaa\bccc\nnn", "aaa\bccc\nnn")

        #self.model = wxDVListModel(data.values())

        #self.dvc.AssociateModel(self.model)

        #c0 = self.dvc.AppendTextColumn("ID", 0, width=25)
        self.makeColumns()
        #c0 = self.dvc.AppendBitmapColumn(_("Color"), 0, width=25)
        #c0.Reorderable = False
        #c0.Sortable = False
        #c0.MinWidth = 25
        #self.dvc.AppendTextColumn(_("Description"),  1, width=170)#, mode=dv.DATAVIEW_CELL_EDITABLE)
        #self.dvc.AppendTextColumn(_("Source type"),  2, width=170)
        #self.dvc.AppendTextColumn(_("Source position"),  3, width=170)
        #self.dvc.AppendTextColumn(_("Target position"),  4, width=170)

        self.dvc.SetRowHeight(100)

        #bmp = wx.EmptyBitmap(16, 16, 32)
        #pixelData = wx.AlphaPixelData(bmp)
        #data = wx.Colour(220, 160, 215)
        #for pixel in pixelData:
        #    pixel.Set(data.red, data.green, data.blue, 255)
        #dvc.AppendItem((0, "456", bmp, "p", "aaa\nbccc\nnn", "aaa\nbccc\nnn"))


        self.Sizer = wx.BoxSizer()
        self.Sizer.Add(dvc, 1, wx.EXPAND)

        self.Bind(dv.EVT_DATAVIEW_SELECTION_CHANGED, self.onItemSelected, self.dvc)

    def checkColumns(self):
        # 1. Check if 'order' key exists
        # 2. Check if 'type' key exists
        columns = self._parent._columns
        jo = max(columns, key=lambda q: columns[q]['order'] if 'order' in columns[q] else -1)
        jo = columns[jo]['order']
        for i in columns:
            if 'order' not in columns[i]:
                #print i, columns[i]
                jo += 1
                columns[i]['order'] = jo
            if 'type' not in columns[i]:
                columns[i]['type'] = 'string'
            if 'size' not in columns[i]:
                columns[i]['size'] = 20

    def makeColumns(self):
        columns = self._parent._columns
        self.checkColumns()
        try:
            iconPath = NPPlan.getIconPath()
        except TypeError:
            iconPath = "C:\\NPPlan3\\NPPlan\\images"
        iconEmpty = os.path.join(iconPath, 'iconEmpty32.png')
        for i in sorted(columns, key=lambda q: columns[q]['order']):
            if 'image' in columns[i]:
                icon = os.path.join(iconPath, columns[i]['image'])
                icon = wx.Bitmap(icon)
            else:
                icon = wx.Bitmap(iconEmpty)
            title = columns[i]['text']
            #if 'string' == columns[i]['type']:
            if 'bitmap' == columns[i]['type']:
                #print title, type(title)
                q = self.dvc.AppendBitmapColumn(title, columns[i]['order'], width=columns[i]['size'])
                q.SetBitmap(icon)
            else:
                q = self.dvc.AppendTextColumn(title, columns[i]['order'], width=columns[i]['size'])
                q.SetBitmap(icon)
            if 'sortable' in columns[i] and columns[i]['sortable']:
                q.Sortable = True
            else:
                q.Sortable = False
            if 'reorderable' in columns[i] and columns[i]['reorderable']:
                q.Reorderable = True
            else:
                q.Reorderable = False
            if 'resizeable' in columns[i] and not columns[i]['resizeable']:
                q.Resizeable = False
            else:
                q.Resizeable = True
                pass
        #dv.DataViewColumn


            print i, columns[i]
        pass

    def appendItem(self, dataTuple):
        self.dvc.AppendItem(dataTuple)

    def onItemSelected(self, event):
        """
        @param event: событие
        @type event: wx.dataview.DataViewEvent
        @return:
        """
        #print event.m_itemIndex, self.list.GetItemData(event.m_itemIndex), self._data[event.m_itemIndex]
        #self._selected = event.m_itemIndex
        #self._parent.callListBackendSelected(event, self._data[event.m_itemIndex])
        #self._parent.activateButtons()
        #self.GetParent().callListBackendSelected(event, self.dvc.GetModel().data[event.GetItem().GetID()-1])
        print event, type(event)
        #self.GetParent().activateButtons()
        self.GetParent().callListBackendSelected(event)
        event.Skip()
        pass

    def testActivate(self, event):
        print event, type(event), "HIGH LEVEL ACTIVATE"
        print event.GetValue(), event.GetModel(), event.GetDataObject(), event.GetItem()
        print event.GetItem().GetID()
        print self.dvc.GetModel().data[event.GetItem().GetID()-1]


class listWithControls(wx.Panel):
    def __init__(self, parent, columns={}, controls={}, controlsPosition='up', parser=None):
        """

        @param parent: родительское окно
        @type parent: wx.Window
        @param columns: список столбцов
        @type columns: dict
        @param controls: список кнопок контроллера
        @type controls: dict
        @param controlsPosition: положение контроллера, in ['up', 'down', 'left', 'right']
        @type controlsPosition: string
        @param parser: функция-парсер события выборки
        @type parser: function
        @return:
        """
        wx.Panel.__init__(self, parent)
        self._columns = columns
        self._list = {}
        self._controls = {'data': controls, 'position': controlsPosition, 'buttons': {}, 'ids': {}}
        self._parser = parser

        if self._controls['position'] in ['up', 'down']:
            self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        else:
            self.mainSizer = wx.BoxSizer(wx.HORIZONTAL)

        if self._controls['position'] in ['up', 'left']:
            self.addControls()
            self.addList()
        else:
            self.addList()
            self.addControls()

        self.SetSizer(self.mainSizer)

    def addItem(self, dataDict):
        self._list['dvc'].AppendItem(dataDict)
        pass

    def callButtonBackend(self, event):
        """
        @param event:
        @type event: wx._core.CommandEvent
        @return:
        """
        bId = self._controls['ids'][event.GetId()]
        self._controls['data'][bId]['backend'](event, self._controls['data'][bId])
        event.Skip()

    def addList(self):
        self._list['object'] = listPanel(self, wx.ID_ANY)
        self._list['dvc'] = self._list['object'].dvc
        self.mainSizer.Add(self._list['object'], 1, wx.EXPAND)
        pass

    def addControls(self):
        if self._controls['position'] in ['up', 'down']:
            self._controls['sizer'] = wx.BoxSizer(wx.HORIZONTAL)
        else:
            self._controls['sizer'] = wx.BoxSizer(wx.VERTICAL)
        for i in self._controls['data']:
            if __name__ == '__main__':
                # did for own run, @todo: remove in real project
                bitmap = wx.Bitmap(os.path.join('C:\\NPPlan3\\NPPlan\\images\\', self._controls['data'][i]['icon']),
                                   wx.BITMAP_TYPE_PNG)
            else:
                bitmap = wx.Bitmap(os.path.join(NPPlan.getIconPath(), self._controls['data'][i]['icon']),
                                   wx.BITMAP_TYPE_PNG)
            _id = wx.NewId()
            self._controls['ids'][_id] = i
            self._controls['buttons'][i] = platebtn.PlateButton(
                self,
                _id,
                '',
                bitmap,
                style=wx.WINDOW_VARIANT_SMALL
            )
            self._controls['sizer'].Add(self._controls['buttons'][i])
            self.Bind(wx.EVT_BUTTON, self.callButtonBackend, self._controls['buttons'][i])
            #if 'activation' in self._controls['data'][i] and 1 == self._controls['data'][i]['activation']:
            #    self._activeOnSelect.append(i)
        #self.activateButtons(False)
        pass
        self.mainSizer.Add(self._controls['sizer'])

    def callListBackendSelected(self, event, data=None):
        """
        @param event: событие
        @type event: wx.dataview.DataViewEvent
        @param data: список значений ячеек выбранной строки в порядке следования столбцов @deprecated
        @type data: list
        @return:
        """
        #print caller, data
        self._parser(event, data)
        pass

    def setParser(self, parser):
        self._parser = parser


if __name__ == '__main__':
    class MyFrame(wx.Frame):
        def __init__(self, parent):
            wx.Frame.__init__(self, parent, -1, "DVListWithControls test")
            sizer = wx.BoxSizer(wx.VERTICAL)
            #notebook = listPanel(self, -1)
            notebook = listWithControls(self, controls={
                                               'add': {'text': _('Add beam'), 'backend': self.addBeamClicked,
                                                       'icon': 'iconPlus32.png'},
                                               'del': {'text': _('Delete beam'), 'backend': self.delBeamClicked,
                                                       'icon': 'iconMinus32.png', 'activation': 1},
                                               'save': {'text': _('Save beam'), 'backend': self.saveBeamClicked,
                                                        'icon': 'iconSave32.png', 'activation': 1},
                                           },
                                        columns={
                                            'name': {'text': _('Name'), 'order': 1, 'image': 'iconText16.png'},
                                            'color': {'text': _('Color'), 'order': 0, 'size': 30, 'type': 'bitmap', 'image': 'iconColors16.png', 'resizeable': 0},
                                            'target': {'text': _('Target'), 'image': 'iconTarget16.png', 'reorderable': True},
                                            'source': {'text': _('Source'), 'order': 3, 'image': 'iconStar16.png', 'reorderable': True}
                                        },
                                        controlsPosition='down'
                                        )
            sizer.Add(notebook, 1, wx.ALL | wx.EXPAND)
            self.SetSizer(sizer)

        def addBeamClicked(self, event):
            pass
        def delBeamClicked(self, event):
            pass
        def saveBeamClicked(self, event):
            pass

    app = wx.App(0)

    frame = MyFrame(None)
    app.SetTopWindow(frame)
    frame.Show()

    app.MainLoop()