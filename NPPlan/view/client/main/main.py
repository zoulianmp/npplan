# -*- coding: utf8 -*-
'''
Модуль с классом отображения главного окна программы. Наследуется от myApp(wx.App). Позволяет динамически изменять размеры элементов окна в случае изменений, см. @see: main.changeLayout
@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 10.09.12
@summary: Модуль с классом отображения главного окна программы.
'''

import NPPlan
import wx
from NPPlan.view.client.main.centerPanel import centerPanel
try:
    from agw import ribbon as RB
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.ribbon as RB

class main(wx.Frame):
    """
    Класс отображения главного окна программы. Создаёт объекты отображения для agw.RibbonBar и NPPlan.view.client.main.centerPanel и помещает их как элементы окна.
    Динамически отслеживает их размеры. Расширяется на всю доступную область окна
    """
    def __init__(self, parent, title):
        """
        Конструктор
        @param parent: родительское окно
        @type parent: wx.Window
        @param title: заголовок
        @type title: string
        """
        size = wx.DisplaySize()
        wx.Frame.__init__(self, parent, -1, title, size = (size[0], size[1]), pos=(0, 0),
                          style=wx.DEFAULT_FRAME_STYLE | wx.NO_FULL_REPAINT_ON_RESIZE |wx.EXPAND)

        self.tbIcon = None

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.mainPanel = wx.Panel(self, -1)

        self.ribbon = RB.RibbonBar(self.mainPanel, wx.NewId())

        # test
        # @todo: to centerPanel
        self.centerPanel = centerPanel(self.mainPanel)

        self.mainPanel.SetSizer(sizer)

        self.Bind(wx.EVT_CLOSE, self.onCloseWindow)

    def changeLayout(self):
        """
        Динамически изменяет разметку (Layout) окна
        @todo: проверить утечку памяти при пересоздании
        """
        self.Freeze()

        vSizer = wx.BoxSizer(wx.VERTICAL)

        self.mainPanel.SetSizer(vSizer)

        #vSizer.Add(self.titleBar, 0, wx.EXPAND)
        vSizer.Add(self.ribbon, 0, wx.EXPAND)
        #vSizer.Add(self.topMenu, 0, wx.EXPAND)
        #vSizer.Add((1, 1))
        vSizer.Add(self.centerPanel, 1, wx.EXPAND|wx.ALL, 5)

        vSizer.Layout()
        self.mainPanel.Layout()
        self.Thaw()



    def mSetIcon(self, icon, tbIcon):
        """
        Устанавливает иконку для окна
        @param icon: иконка в заголовке
        @type icon: wx.Icon
        @param tbIcon: тулбар
        @type tbIcon: NPPlan.view.client.taskBarIcon.tbIcon (wx.TaskBarIcon)
        """
        self.SetIcon(icon)
        self.tbIcon = tbIcon

    def onCloseWindow(self, event):
        """
        Уничтожение окна
        @todo: перенести в контроллер
        @param event: event
        @type event: wx.PyEvent
        """
        if self.tbIcon is not None:
            self.tbIcon.Destroy()
        self.Destroy()

