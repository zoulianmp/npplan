# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 11.09.12
@summary: 
'''

import NPPlan
import wx
import wx.lib.platebtn as platebtn
from wx.lib.ticker import Ticker


class curInfo(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.NewId(), style=wx.BORDER_SUNKEN)

        self.SetBackgroundColour(wx.Colour(250, 250, 250))

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        homeIcon = ''
        self.homeButton = platebtn.PlateButton(
            self,
            wx.ID_ANY,
            '',
            wx.Bitmap(NPPlan.getIconPath() + 'iconHome2Button32.png', wx.BITMAP_TYPE_PNG),
            style=wx.WINDOW_VARIANT_SMALL
        )

        self.backButton = platebtn.PlateButton(
            self,
            wx.ID_ANY,
            '',
            wx.Bitmap(NPPlan.getIconPath() + 'iconBack32.png', wx.BITMAP_TYPE_PNG),
            style=wx.WINDOW_VARIANT_SMALL
        )
        #self.backButton.Enable(False)

        for i in [self.homeButton, self.backButton]:
            i.SetBackgroundColour(wx.Colour(250, 250, 250))
            i.SetPressColor(wx.Colour(250, 250, 250))
            i.SetForegroundColour(wx.Colour(250, 250, 250))
        sizer.Add(self.homeButton)
        sizer.Add(self.backButton)

        caption = 'test'
        self.infoText = wx.StaticText(self, -1, caption, size=(1000, -1))
        font = wx.Font(18, wx.SWISS, wx.NORMAL, wx.NORMAL)
        self.infoText.SetFont(font)
        sizer.Add(self.infoText, 0, wx.CENTER | wx.EXPAND, 3)

        #self.ticker = Ticker(self)
        #self.ticker.SetText('test')
        #sizer.Add(self.ticker, 0, wx.CENTER | wx.EXPAND, 3)

        self.versionText = wx.StaticText(self, -1, "NPPlan ver. %s" % NPPlan.VERSION, pos=(1800, 0))
        isDebug = 'ON' if 1 == int(NPPlan.config.appConfig.logger.debug) else 'OFF'
        self.debugText = wx.StaticText(self, -1, 'Debug mode: %s' % isDebug, pos=(1800, 15))
        #sizer.Add(self.versionText, 0, wx.RIGHT | wx.EXPAND | wx.ALIGN_RIGHT, 3)

        self.SetSizer(sizer)
