# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 06.12.12
@summary: 
'''

import NPPlan
from main import calculateBase


class calculateMcnp(calculateBase):
    def __init__(self, parent, choiceStatusExplorerChoices=[]):
        calculateBase.__init__(self, parent, choiceStatusExplorerChoices, headerText=_('MCNP calculation'))