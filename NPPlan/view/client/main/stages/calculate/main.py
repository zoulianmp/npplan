# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 06.12.12
@summary: 
'''

import NPPlan
import wx
try:
    from agw import flatnotebook as fnb
except ImportError:
    import wx.lib.agw.flatnotebook as fnb
from wx.lib.agw import ultimatelistctrl as ULC
import wx.lib.platebtn as platebtn
import sys


class calculateView(wx.Panel):
    def __init__(self, parent):
        """
        @param parent: родительское окно
        @type parent: NPPlan.view.client.main.emptyPanel.emptyPanel
        """
        wx.Panel.__init__(self, parent, wx.NewId())
        bookStyle = fnb.FNB_NODRAG | fnb.FNB_VC8 | fnb.FNB_NO_X_BUTTON | fnb.FNB_SMART_TABS
        self.book = fnb.FlatNotebook(self, wx.ID_ANY, agwStyle=bookStyle)
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.mainSizer.Add(self.book, 1, wx.EXPAND)
        self.SetSizer(self.mainSizer)

        self.book.Bind(fnb.EVT_FLATNOTEBOOK_PAGE_CHANGING, self.onPageChanging)
        self.book.Bind(fnb.EVT_FLATNOTEBOOK_PAGE_CHANGED, self.onPageChanged)

    def addPage(self, pageObject, name):
        """
        @param pageObject: страница
        @type pageObject: wx.Window
        @param name: имя страницы
        @type name: string or unicode
        """
        self.Freeze()
        self.book.AddPage(pageObject, name)
        self.Thaw()

    def onPageChanging(self, event):
        """
        @param event: событие
        @type event: wx.Event
        """
        event.StopPropagation()
        event.Skip()

    def onPageChanged(self, event):
        """
        @param event: событие
        @type event: wx.Event
        """
        event.StopPropagation()
        event.Skip()


class calculateBase(wx.Panel):
    def __init__(self, parent, choiceStatusExplorerChoices=[], headerText='Calculation'):
        wx.Panel.__init__ (self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(629, 399),
                           style=wx.TAB_TRAVERSAL)

        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        header = wx.StaticText(self, wx.ID_ANY, headerText)
        bSizer1.Add(header)

        sbSizer1 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Calculation task"), wx.VERTICAL)

        gSizer1 = wx.GridSizer(0, 2, 8, 8)

        #self._btnCreateTaskClient = wx.Button(self, wx.ID_ANY, u"Create task", wx.DefaultPosition, wx.DefaultSize, 0)
        createTaskBmp = wx.Bitmap(NPPlan.getIconPath() + 'iconPlay32.png')
        bstyle = platebtn.PB_STYLE_DEFAULT
        self._btnCreateTaskClient = platebtn.PlateButton(self, wx.ID_ANY, _('Create task'), createTaskBmp, style=bstyle)
        self._btnCreateTaskClient.SetOwnBackgroundColour(wx.Colour(255, 255, 255))
        self._checkCreateTaskClientBackground = wx.CheckBox(self, -1, _("Run in background"))
        createTaskClientSizer = wx.BoxSizer(wx.VERTICAL)
        createTaskClientSizer.Add(self._btnCreateTaskClient)
        createTaskClientSizer.Add(self._checkCreateTaskClientBackground)
        gSizer1.Add(createTaskClientSizer, 0, wx.ALL, 5)

        labelCreateTaskClient = wx.StaticText(self, wx.ID_ANY, u"Create calculation task at client", wx.DefaultPosition, wx.DefaultSize, 0)
        labelCreateTaskClient.Wrap(-1)
        gSizer1.Add(labelCreateTaskClient, 0, wx.ALL|wx.EXPAND, 5)

        self._btnCreateTaskServer = wx.Button(self, wx.ID_ANY, u"Create task on server", wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer1.Add(self._btnCreateTaskServer, 0, wx.ALL, 5)

        labelCreateTaskServer = wx.StaticText(self, wx.ID_ANY, u"Append task to server calculation maker queue", wx.DefaultPosition, wx.DefaultSize, 0)
        labelCreateTaskServer.Wrap(-1)
        gSizer1.Add(labelCreateTaskServer, 0, wx.ALL|wx.EXPAND, 5)

        self._btnAppendTask = wx.Button(self, wx.ID_ANY, u"Append task", wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer1.Add(self._btnAppendTask, 0, wx.ALL, 5)

        self.m_staticText2 = wx.StaticText(self, wx.ID_ANY,
                                           u"Append task to server calculation queue\nMCNP task queue folder: "
                                           u"%s" % NPPlan.config.appConfig.mcnp.taskdir, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        gSizer1.Add(self.m_staticText2, 0, wx.ALL|wx.EXPAND, 5)

        self._btnDoWork = wx.Button(self, wx.ID_ANY, u"One-click button", wx.DefaultPosition, wx.DefaultSize, 0)
        gSizer1.Add(self._btnDoWork, 0, wx.ALL, 5)

        self.m_staticText3 = wx.StaticText(self, wx.ID_ANY, u"Do all stuff in one click", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText3.Wrap(-1)
        gSizer1.Add(self.m_staticText3, 0, wx.ALL|wx.EXPAND, 5)

        sbSizer1.Add(gSizer1, 1, wx.EXPAND, 5)

        bSizer1.Add(sbSizer1, 1, wx.EXPAND, 5)

        sbSizer2 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Calculation status"), wx.VERTICAL)

        _choiceStatusExplorerChoices = choiceStatusExplorerChoices
        self._choiceStatusExplorer = wx.Choice(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize,
                                               _choiceStatusExplorerChoices, 0)
        self._choiceStatusExplorer.SetSelection(0)
        sbSizer2.Add(self._choiceStatusExplorer, 0, wx.ALL, 5)

        #self._statusList = wx.ListCtrl(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1,-1), wx.LC_ICON)
        self._statusList = ULC.UltimateListCtrl(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1,-1), wx.LC_REPORT,
                                                agwStyle=ULC.ULC_LIST
                                                         | ULC.ULC_VRULES
                                                         | ULC.ULC_HRULES
                                                         | ULC.ULC_HEADER_IN_ALL_VIEWS
                                                         | wx.LC_REPORT
        )
        sbSizer2.Add(self._statusList, 1, wx.EXPAND, 5)

        bSizer1.Add(sbSizer2, 1, wx.EXPAND, 5)

        self.SetSizer(bSizer1)
        self.Layout()

    def switchCreateLocalState(self, state):
        self._createLocalState = state
        if state:
            pauseTaskBmp = wx.Bitmap(NPPlan.getIconPath() + 'iconPause32.png')
            self._btnCreateTaskClient.SetBitmap(pauseTaskBmp)
        else:
            createTaskBmp = wx.Bitmap(NPPlan.getIconPath() + 'iconPlay32.png')
            self._btnCreateTaskClient.SetBitmap(createTaskBmp)

    def createListColumns(self, columns):
        self._statusList.Freeze()
        for i in range(len(columns)):
            columnData = columns[i]
            info = ULC.UltimateListItem()
            info._format = wx.LIST_FORMAT_RIGHT
            info._mask = wx.LIST_MASK_TEXT | wx.LIST_MASK_IMAGE | wx.LIST_MASK_FORMAT | ULC.ULC_MASK_FONT
            info._image = []
            info._text = columnData['text']

            self._statusList.InsertColumnInfo(i, info)

        for i in range(len(columns)):
            columnData = columns[i]
            if 'size' not in columnData or 'auto' == columnData['size']:
                self._statusList.SetColumnWidth(i, wx.LIST_AUTOSIZE)
            else:
                self._statusList.SetColumnWidth(i, columnData['size'])


        #index = self._statusList.InsertImageStringItem(0, '123', [], it_kind=0)
        #self._statusList.SetStringItem(index, 1, '345', it_kind=1)

        self._statusList.Thaw()
        self._statusList.Update()
        pass

    def appendTask(self, params):
        index = self._statusList.InsertImageStringItem(sys.maxint, params['name'], [], it_kind=0)
        self._statusList.SetStringItem(index, 1, params['cid'], it_kind=0)
        self._statusList.SetStringItem(index, 2, params['status'], it_kind=0)
        return index
        pass

    def __del__(self):
        pass
