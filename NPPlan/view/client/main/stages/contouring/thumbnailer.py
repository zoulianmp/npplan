# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 30.10.12
@summary: 
'''

import NPPlan
import wx
from NPPlan.controller.client.mainC.abstracts import popupHelperController
try:
    from agw import thumbnailctrl as TC
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.thumbnailctrl as TC
from NPPlan.view.client.main.abstracts.thumbnailer import thumbnailerWithControls
from NPPlan.view.client.main.abstracts.thumbnailer import dicomScrolledThumbnail


class thumbnailer(wx.Panel):
    def __init__(self, parent, buttons, data, selBackend):
        wx.Panel.__init__(self, parent, wx.ID_ANY)
        # @todo: implement from informer through controller
        thumbSizer = wx.BoxSizer(wx.HORIZONTAL)
        #self.thumb = TC.ThumbnailCtrl(self, -1, imagehandler=TC.PILImageHandler)
        self.thumb = thumbnailerWithControls(self, buttons, data, selBackend)
        #self.thumb.setData(data)
        #self.thumb.setBackend(selBackend)
        #self.thumb = dicomScrolledThumbnail(self, -1)
        #self.thumb.ShowFileNames()
        #self.thumb.ShowDir("C:\\Users\\Chernukha\\Pictures\\DICOM\\thumb\\")
        self.thumb.SetMinSize((400, 400))
        #self.thumb.SetThumbOutline(TC.THUMB_OUTLINE_FULL)
        #items = self.thumb._scrolled._items
        #for i in items:
        #    print i.GetFileName()
        #    i.SetCaption('123')
        #    i.SetFileName('123')
        thumbSizer.Add(self.thumb, 10, wx.EXPAND|wx.ALL)
        #thumbSizer.Add(wx.StaticText(self, wx.NewId(), 'ssss'))
        self.SetSizer(thumbSizer)

