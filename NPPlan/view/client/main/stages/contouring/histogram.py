# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 06.12.12
@summary: 
'''

import NPPlan
import wx
from vtk.wx.wxVTKRenderWindowInteractor import *

class histogramView(wx.Panel):
    def __init__(self, parent):
        """
        @param parent: родительское окно
        @type parent: wx.lib.agw.foldpanelbar.FoldPanelItem
        """
        wx.Panel.__init__(self, parent, wx.ID_ANY)
        sizer = wx.BoxSizer(wx.VERTICAL)

        self.bmp = wx.StaticBitmap(self, -1)
        sizer.Add(self.bmp)
        self.SetSizer(sizer)

    def setBitmap(self, bmp):
        """
        @param bmp: рисунок
        @type bmp: wx.Bitmap
        """
        self.bmp.SetBitmap(bmp)
        pass

