# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 30.10.12
@summary: 
'''

import NPPlan

import wx
from renderWindow import renderWindow, renderWindowOld
#from thumbnailer import thumbnailer
from NPPlan.view.client.main.abstracts.thumbnailer import thumbnailerWithControls


class contourWin(wx.Panel):
    def __init__(self, parent, thumbnailControls={}, thumbnailData={}, thumbnailBackend={}, parentController=None):
        wx.Panel.__init__(self, parent, wx.ID_ANY)

        self.vtkHandler = None
        self._parentController = parentController

        lrSizer = wx.BoxSizer(wx.HORIZONTAL)

        vrSizer = wx.BoxSizer(wx.VERTICAL)

        self.renderWindow = renderWindow(self, parentController=parentController)
        self.l1Window = renderWindowOld(self)
        self.l2Window = renderWindowOld(self)

        lrInnerSizer = wx.BoxSizer(wx.HORIZONTAL)
        vrInnerSizer = wx.BoxSizer(wx.VERTICAL)

        vrInnerSizer.Add(self.l1Window, 1, wx.EXPAND)
        vrInnerSizer.Add(self.l2Window, 1, wx.EXPAND)

        lrInnerSizer.Add(self.renderWindow, 5, wx.EXPAND)
        lrInnerSizer.Add(vrInnerSizer, 2, wx.EXPAND)

        #vrSizer.Add(self.renderWindow, 8, wx.EXPAND)
        vrSizer.Add(lrInnerSizer, 8, wx.EXPAND)
        #lrSizer.Add(wx.StaticText(self, -1, 'test'))

        #self.thumbnailer = thumbnailer(self, buttons=thumbnailControls,
        #                                                   data=thumbnailData,
        #                                                   selBackend=thumbnailBackend)
        self.thumbnailer = thumbnailerWithControls(self,
                                                   buttons=thumbnailControls,
                                                   data=thumbnailData,
                                                   selBackend=thumbnailBackend
        )
        #self.thumbnailer = thumbnailerWithControls(wxParent=self,
        #                                           buttons=thumbnailControls,
        #                                           data=thumbnailData,
        #                                           selBackend=thumbnailBackend
        #)
        vrSizer.Add(self.thumbnailer, 2, wx.EXPAND | wx.ALL)

        lrSizer.Add(vrSizer, 1, wx.EXPAND)

        self.SetSizer(lrSizer)

        #wx.FutureCall(2000, self.setW)

    def setInfoText(self, value):
        pass
        #self._infoText.SetLabel(value)

    def getVtkWindow(self):
        return self.renderWindow.getVtkWindow()

    def getThumbnailer(self):
        return self.thumbnailer.thumb

    thumb = property(getThumbnailer)

    def setW(self):
        print 'event'
        #self.vtkHandler.setWindowLevel(20, 30)
        self.vtkHandler.windowlevel = (20, 30)
        self.vtkHandler.setImage('C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\84269566')
        self.vtkHandler.update()
