# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2012
@version: 1
@date: 03.10.2012
@summary:
'''

import NPPlan

import wx
from NPPlan.view.client.main.stages.patient.personal import personal as personalData
from NPPlan.view.client.main.stages.patient.medical import medical as medicalData
from NPPlan.view.client.main.stages.patient.plan import plan as planData
from NPPlan.view.client.main.stages.patient.study import study as studyData
try:
    from agw import flatnotebook as fnb
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.flatnotebook as fnb

class addPatientPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.NewId(), style=wx.BORDER_SUNKEN)

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)

        bookStyle = fnb.FNB_NODRAG | fnb.FNB_VC8 | fnb.FNB_NO_X_BUTTON | fnb.FNB_SMART_TABS
        self.book = fnb.FlatNotebook(self, wx.ID_ANY, agwStyle=bookStyle)

        self.startPages()

        self.mainSizer.Add(self.book, 1, wx.EXPAND)

        self.SetSizer(self.mainSizer)

        self.book.Bind(fnb.EVT_FLATNOTEBOOK_PAGE_CHANGING, self.onPageChanging)
        self.book.Bind(fnb.EVT_FLATNOTEBOOK_PAGE_CHANGED, self.onPageChanged)
        self.Layout()
        self.Refresh()
        self.GetParent().Layout()
        self.GetParent().Refresh()
        self.GetParent().GetParent().Layout()
        self.GetParent().GetParent().Refresh()
        self.GetParent().GetParent().GetParent().Layout()
        self.GetParent().GetParent().GetParent().Refresh()

    def startPages(self):
        self.Freeze()
        self.personal = personalData(self.book)
        self.book.AddPage(self.personal, _('Personal data'), True)

        self.medical = medicalData(self.book)
        self.book.AddPage(self.medical, _('Medical data'), False)

        self.plan = planData(self.book)
        self.book.AddPage(self.plan, _('Plan data'), False)
        self.Thaw()

    def addStudyPage(self, name, data):
        self.Freeze()
        study = studyData(self.book)
        self.book.AddPage(study, name, False)
        self.book.GetPageCount()
        self.book.SetSelection(self.book.GetPageCount()-1)
        self.Thaw()
        return study

    def onPageChanging(self, event):
        event.StopPropagation()
        print 'add', event.GetId()
        event.Skip()

    def setFields(self, type, fields):
        print type, fields
        if 'personal' == type:
            self.personal.setFields(fields)
        elif 'medical' == type:
            self.medical.setFields(fields)
        pass

    def getPlanPanel(self):
        return self.plan

    def onPageChanged(self, event):
        event.StopPropagation()
        event.Skip()

