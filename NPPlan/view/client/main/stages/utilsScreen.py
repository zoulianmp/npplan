# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 26.09.12
@summary: 
'''

import NPPlan
import wx
import  wx.lib.buttons as buttons
import NPPlan.controller.client.mainC.stages.patWork as patWork

# @todo: перевести в абстрактный фрейм

class patWorkScreen(wx.Panel):
    def __init__(self, parent, buttons):
        wx.Panel.__init__(self, parent, wx.NewId())#, style=wx.BORDER_SUNKEN)

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.backends = {}

        self.SetSizerAndFit(self.mainSizer)
        self.Layout()

    def clearButtons(self):
        #self.mainSizer.DeleteWindows()
        self.mainSizer.Clear()
        #self.mainSizer.Detach()

    def addButtons(self, buttonsList):
        self.clearButtons()
        iconPath = NPPlan.getIconPath()
        font = wx.Font(15, wx.SWISS, wx.NORMAL, wx.FONTWEIGHT_BOLD)
        for btn in buttonsList:
            sz = wx.BoxSizer(wx.HORIZONTAL)
            btnId = wx.NewId()
            b = buttons.GenBitmapButton(self, btnId, wx.Bitmap(iconPath+btn['icon'], wx.BITMAP_TYPE_PNG), style=wx.BORDER_NONE)
            self.backends[btnId] = btn['backend']

            szDescr = wx.BoxSizer(wx.VERTICAL)
            text = wx.StaticText(self, -1, btn['name'])
            text.SetFont(font)
            descr = wx.StaticText(self, -1, btn['description'])
            szDescr.Add(text)
            szDescr.Add(descr)

            self.Bind(wx.EVT_BUTTON, self.onButton, b)

            sz.Add(b)
            sz.Add(szDescr, 0, wx.ALIGN_CENTER_VERTICAL)
            self.mainSizer.Add(sz)
        self.SetSizerAndFit(self.mainSizer)
        self.Layout()

    def onButton(self, event):
        backend = getattr(patWork, self.backends[event.GetId()])
        #res = backend()
        wx.CallAfter(backend)
        event.Skip()