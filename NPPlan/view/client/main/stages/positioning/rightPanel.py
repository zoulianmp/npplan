# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 14.12.12
@summary: 
'''

import NPPlan
import wx
import  wx.lib.colourselect as  csel
import wx.lib.platebtn as platebtn
#from NPPlan.view.client.main.abstracts.list import listWithControls
from NPPlan.view.client.main.abstracts.DVListWithControls import listWithControls
from NPPlan.view.client.main.abstracts import txtBtnCtrlClass
import  wx.lib.scrolledpanel as scrolled
import abc
import  wx.lib.colourselect as  csel
from NPPlan.view.client.main.abstracts.stepCtrl import spinStepCtrl
from NPPlan.view.client.main.abstracts.rightFoldPanel import foldPanelBase
try:
    from agw import foldpanelbar as fpb
except ImportError:
    import wx.lib.agw.foldpanelbar as fpb
try:
    from agw import floatspin as FS
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.floatspin as FS
import wx.lib.agw.supertooltip as STT
from wx.lib.wordwrap import wordwrap
from NPPlan.view.client.main.abstracts import GetExpandedIconBitmap, GetCollapsedIconBitmap


class rightPanelBase(wx.Panel):
    def makeSize(self):
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self._pnl, 0, wx.ALL | wx.EXPAND, 0)
        self.SetSizer(sizer)
        self.Layout()
        self.GetSizer().Layout()
        print 'OWN SIZE', self.GetSize()
        print self.GetParent().GetSize()
        print self.GetParent(), type(self.GetParent())
        self._pnl.SetMinSize((300, 1000))
        self.Bind(wx.EVT_SCROLL, self.onScroll)

        self.Layout()

    def onScroll(self, event):
        event.Skip()

    def createFoldPanel(self):
        self._pnl = fpb.FoldPanelBar(self, -1, wx.DefaultPosition, wx.Size(-1, -1))
        Images = wx.ImageList(16, 16)
        Images.Add(GetExpandedIconBitmap())
        Images.Add(GetCollapsedIconBitmap())

    def appendNewItem(self, name='', collapsed=False):
        item = self._pnl.AddFoldPanel(name, collapsed=collapsed,)
        #self._pnl.AddFoldPanelWindow(item, wx.StaticText(item, -1, "Adjust The First Colour"),
        #                             fpb.FPB_ALIGN_WIDTH, 5, 20)
        #self.t2 = wx.StaticText(item, -1, 'ttt')
        #self._pnl.AddFoldPanelWindow(item, self.t2,
        #                             fpb.FPB_ALIGN_WIDTH, 5, 20)
        return item
        #self.makeSize()


class rightPanelThreeDim(rightPanelBase, wx.Panel):
    def __init__(self, parent, **kwargs):
        wx.Panel.__init__(self, parent, -1)


class rightPanelBev(rightPanelBase, wx.ScrolledWindow):
    def __init__(self, parent, **kwargs):
        #wx.Panel.__init__(self, parent, -1)
        #scrolled.ScrolledPanel.__init__(self, parent, -1)
        wx.ScrolledWindow.__init__(self, parent, wx.ID_ANY)
        #self.SetAutoLayout(1)
        #self.SetVirtualSize((self.GetSize()[0], 2000))
        self.SetScrollRate(20, 20)
        self.Bind(wx.EVT_SCROLL, self.onScroll)
        #self.SetupScrolling()

    def onScroll(self, event):
        event.Skip()


class viewerSettings(foldPanelBase):
    def createControls(self, **kwargs):
        color = (255, 255, 0)
        b = csel.ColourSelect(self, -1, '', color, size=wx.DefaultSize)
        sizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        sizer.AddMany([
            wx.StaticText(self, -1, _('Background')),
            b
        ])
        outline = wx.CheckBox(self, -1, "")
        sizer.AddMany([
            wx.StaticText(self, -1, _('Hide outline')),
            outline
        ])
        self.SetSizer(sizer)
        self._bgPicker = b
        self._outlineCheck = outline


class viewerClickSettings(foldPanelBase):
    #def __init__(self, parent, **kwargs):
    #    wx.Panel.__init__(self, parent, -1)
    def createControls(self, **kwargs):
        print kwargs.get('choices')
        choices = kwargs.get('choices')
        self.leftX = wx.Choice(self, -1, (100, 50), choices=choices)
        self.leftY = wx.Choice(self, -1, (100, 50), choices=choices)
        self.rightX = wx.Choice(self, -1, (100, 50), choices=choices)
        self.rightY = wx.Choice(self, -1, (100, 50), choices=choices)

        sizer = wx.FlexGridSizer(cols=2, hgap=8, vgap=10)
        sizer.AddMany([
            wx.StaticText(self, -1, 'Left X'), self.leftX,
            wx.StaticText(self, -1, 'Left Y'), self.leftY,
            wx.StaticText(self, -1, 'Right X'), self.rightX,
            wx.StaticText(self, -1, 'Right Y'), self.rightY,
        ])
        self.SetSizer(sizer)


class volumeMapperView(foldPanelBase):
    #def __init__(self, parent, **kwargs):
        #wx.Panel.__init__(self, parent, -1)

    def createControls(self, **kwargs):
        self.items = {}
        volumesList = kwargs.get('volumesList')
        sizer = wx.BoxSizer(wx.VERTICAL)
        for i in volumesList:
            self.items[i] = wx.CheckBox(self, -1, str(i))
            self.items[i].SetValue(True)
            sizer.Add(self.items[i])
        self.SetSizer(sizer)


class bevPosition(foldPanelBase):
    #def __init__(self, parent, **kwargs):
    #    wx.Panel.__init__(self, parent, -1)

    def createControls(self, **kwargs):
        # @todo: implement min/max values for each slice
        sizer = wx.BoxSizer(wx.VERTICAL)
        fgSizer = wx.FlexGridSizer(cols=3, hgap=10, vgap=2)

        axialLabel = wx.StaticText(self, -1, _('Axial position'))
        #self._axialText = wx.TextCtrl(self, -1, '')
        #self._axialText = spinStepCtrl(self, -1, '')
        self._axialText = FS.FloatSpin(self, -1, min_val=-10, max_val=1000,
                                       increment=1.5, agwStyle=FS.FS_RIGHT)
        self._axialText.SetDigits(2)
        #self._axialText.Set`
        self._axialStep = txtBtnCtrlClass(self, id=-1, size=(40, -1))
        self._axialStep.Enable(False)
        self._axialStep.assignLockBackend(postBackend=self.setAxialStep)
        self._axialStep.SetValue(str(1.5))
        self._axialText.step = 1.5

        coronalLabel = wx.StaticText(self, -1, _('Coronal position'))
        #self._coronalText = wx.TextCtrl(self, -1, '')
        self._coronalText = FS.FloatSpin(self, -1, min_val=-10, max_val=1000,
                                         increment=1.5, agwStyle=FS.FS_RIGHT)
        self._coronalText.SetDigits(2)
        self._coronalStep = txtBtnCtrlClass(self, id=-1, size=(40, -1))
        self._coronalStep.Enable(False)
        self._coronalStep.SetValue(str(1.5))
        self._coronalStep.assignLockBackend(postBackend=self.setCoronalStep)

        sagittalLabel = wx.StaticText(self, -1, _('Sagittal position'))
        #self._sagittalText = wx.TextCtrl(self, -1, '')
        self._sagittalText = FS.FloatSpin(self, -1, min_val=-10, max_val=1000,
                                          increment=1.5, agwStyle=FS.FS_RIGHT)
        self._sagittalText.SetDigits(2)
        self._sagittalStep = txtBtnCtrlClass(self, id=-1, size=(40, -1))
        self._sagittalStep.Enable(False)
        self._sagittalStep.SetValue(str(1.5))
        self._sagittalStep.assignLockBackend(postBackend=self.setSagittalStep)

        descr = wx.StaticText(self, -1, _('Position'))
        step = wx.StaticText(self, -1, _('Step'))

        fgSizer.AddMany([
            (1,1), descr, step,
            axialLabel, self._axialText, self._axialStep,
            coronalLabel, self._coronalText, self._coronalStep,
            sagittalLabel, self._sagittalText, self._sagittalStep,
        ])

        self._btn = platebtn.PlateButton(self, wx.ID_ANY, _('Change position'), None, style=platebtn.PB_STYLE_NOBG)
        sizer.Add(fgSizer)
        sizer.Add(self._btn)
        self.SetSizer(sizer)

    def setAxialStep(self, *args, **kwargs):
        self._axialText.SetIncrement(float(self._axialStep.GetValue()))

    def setCoronalStep(self, *args, **kwargs):
        self._coronalText.SetIncrement(float(self._coronalStep.GetValue()))

    def setSagittalStep(self, *args, **kwargs):
        self._sagittalText.SetIncrement(float(self._sagittalStep.GetValue()))

    def focusMainWindow(self, event=None, state=None, text=None):
        from NPPlan.controller.client.main import getHandler
        getHandler().positioning.view.SetFocus()

    def setAxial(self, value):
        self._axialText.SetValue(value)

    def getAxial(self):
        return float(self._axialText.GetValue())

    axial = property(getAxial, setAxial)

    def setCoronal(self, value):
        self._coronalText.SetValue(value)

    def getCoronal(self):
        return float(self._coronalText.GetValue())

    coronal = property(getCoronal, setCoronal)

    def setSagittal(self, value):
        self._sagittalText.SetValue(value)

    def getSagittal(self):
        return float(self._sagittalText.GetValue())

    sagittal = property(getSagittal, setSagittal)

    def getStep(self, caller=None):
        return float(getattr(self, '_' + caller.lower() + 'Step').GetValue())


class bevInformer(foldPanelBase):
    #def __init__(self, parent, **kwargs):
    #    wx.ScrolledWindow.__init__(self, parent, -1)
    def createControls(self, **kwargs):
        # @todo: columns via controller
        columns = kwargs.get('columns')
        controls = kwargs.get('controls')
        controlsPosition = kwargs.get('controlsPosition')
        facilities = kwargs.get('facilities', None)
        self.listWC = listWithControls(
            self,
            columns=columns,
            controls=controls,
            controlsPosition=controlsPosition,
        )
        self.listWC.SetMinSize((-1, 350))
        self.pnlDescription = wx.Panel(self, style=wx.TAB_TRAVERSAL | wx.BORDER_NONE, pos=(0, 350))
        self.pnlTarget = wx.Panel(self, style=wx.TAB_TRAVERSAL | wx.BORDER_NONE, pos=(0, 550))
        self.pnlSizes = wx.Panel(self, style=wx.TAB_TRAVERSAL | wx.BORDER_NONE, pos=(0, 750))
        self.pnlFacility = wx.Panel(self, style=wx.TAB_TRAVERSAL | wx.BORDER_NONE, pos=(0, 850))
        sizer = wx.BoxSizer(wx.VERTICAL)
        for i in [self.listWC, self.pnlDescription, self.pnlTarget, self.pnlSizes, self.pnlFacility]:
            sizer.Add(i, 0, wx.EXPAND)
        #sizer.Add(self.listWC, 0, wx.ALL | wx.EXPAND)
        #sizer.Add(self.pnlDescription)
        #sizer.Add(self.pnlTarget)#, 1, wx.ALL | wx.EXPAND)
        #sizer.Add(self.pnlSizes)
        #sizer.Add(self.pnlFacility)#, 1, wx.ALL | wx.EXPAND)
        self.SetSizer(sizer)

        #self._controller = None
        self.renderDescription()
        self.renderTargetParams()
        self.renderSizesParams()
        self.renderFacilityParams(facilities, [])
        tgPos = self.pnlTarget.GetPosition()
        tgSz = self.pnlTarget.GetSize()

        #self.pnlSizes.SetPosition((1, tgPos[1] + tgSz[1] + 2))
        #szPos = self.pnlSizes.GetPosition()
        #szSz = self.pnlSizes.GetSize()
        #self.pnlFacility.SetPosition((1, szPos[1] + szSz[1] + 2))
        #width = self.GetParent().GetSize()[0]
        #print 'panel bev informer size', self.GetSize()
        #print 'panel bev informer size', self.GetParent().GetSize()
        #print 'panel bev informer size', self.GetParent().GetParent().GetSize()
        #self.listWC.SetSize((width, 1000))
        #self.doAlignment()

        #self.SetVirtualSize((300, 1200))
        #self.SetScrollRate(20, 20)

    def doAlignment(self):
        _start = self.pnlDescription.GetPosition()[1] + self.pnlDescription.GetSize()[1]
        _inv = self.pnlDescription
        for i in [self.pnlTarget, self.pnlSizes, self.pnlFacility]:
            i.SetPosition((0, 2 + _start))
            _start += i.GetSize()[1]
        pass

    def renderDescription(self):
        box = wx.StaticBox(self.pnlDescription, -1, _('Description'))
        bsizer = wx.StaticBoxSizer(box, wx.VERTICAL)
        trSizer = wx.BoxSizer(wx.HORIZONTAL)
        #t = wx.StaticText(self.pnlDescription, -1, _('Description'))
        self.description = wx.TextCtrl(self.pnlDescription, -1, '', size=(250, 25))
        self.cSel = csel.ColourSelect(self.pnlDescription, -1, size=(30, 26))
        trSizer.AddMany([self.description, self.cSel])
        #bsizer.Add(self.description, 0, wx.TOP | wx.LEFT, 4)
        bsizer.Add(trSizer, 0, wx.TOP | wx.LEFT, 4)
        border = wx.BoxSizer()
        border.Add(bsizer, 1, wx.EXPAND | wx.ALL, 1)
        self.pnlDescription.SetSizerAndFit(border)

    def renderTargetParams(self):
        box = wx.StaticBox(self.pnlTarget, -1, _('Center target params'))
        bsizer = wx.StaticBoxSizer(box, wx.VERTICAL)

        t = wx.StaticText(self.pnlTarget, -1, _('World orthogonal coords'))
        bsizer.Add(t, 0, wx.TOP | wx.LEFT, 10)

        lrXYZSizer = wx.BoxSizer(wx.HORIZONTAL)
        sX = wx.StaticText(self.pnlTarget, -1, 'X: ')
        sY = wx.StaticText(self.pnlTarget, -1, 'Y: ')
        sZ = wx.StaticText(self.pnlTarget, -1, 'Z: ')
        self.fX = txtBtnCtrlClass(self.pnlTarget, id=-1, size=(40, -1))
        self.fY = txtBtnCtrlClass(self.pnlTarget, id=-1, size=(40, -1))
        self.fZ = txtBtnCtrlClass(self.pnlTarget, id=-1, size=(40, -1))
        lrXYZSizer.AddMany([sX, self.fX,
                            sY, self.fY,
                            sZ, self.fZ])
        bsizer.Add(lrXYZSizer, 0, wx.BOTTOM | wx.LEFT, 2)

        t2 = wx.StaticText(self.pnlTarget, -1, _('World spherical coords'))
        bsizer.Add(t2, 0, wx.TOP | wx.LEFT, 10)

        lrSphereSizer = wx.BoxSizer(wx.HORIZONTAL)
        sR = wx.StaticText(self.pnlTarget, -1, 'R: ')
        sFi = wx.StaticText(self.pnlTarget, -1, 'Fi: ')
        sTheta = wx.StaticText(self.pnlTarget, -1, 'Theta: ')
        self.fR = txtBtnCtrlClass(self.pnlTarget, id=-1, size=(40, -1))
        self.fFi = txtBtnCtrlClass(self.pnlTarget, id=-1, size=(40, -1))
        self.fTheta = txtBtnCtrlClass(self.pnlTarget, id=-1, size=(40, -1))
        lrSphereSizer.AddMany([sR, self.fR,
                              sFi, self.fFi,
                              sTheta, self.fTheta])
        bsizer.Add(lrSphereSizer, 0, wx.BOTTOM | wx.LEFT, 2)

        border = wx.BoxSizer()
        border.Add(bsizer, 1, wx.EXPAND | wx.ALL, 1)
        self.pnlTarget.SetSizerAndFit(border)

    def renderSizesParams(self):
        box = wx.StaticBox(self.pnlSizes, -1, _('Collimator params'))

        bsizer = wx.StaticBoxSizer(box, wx.VERTICAL)

        shapeSizeSizer = wx.BoxSizer(wx.HORIZONTAL)
        shT = wx.StaticText(self.pnlSizes, -1, _('Shape'))
        shapes = []
        self.cShape = wx.Choice(self.pnlSizes, -1, size=(100, -1), choices = shapes)
        siT = wx.StaticText(self.pnlSizes, -1, _('Size'))
        self.cSize = wx.Choice(self.pnlSizes, -1, size=(100, -1))
        shapeSizeSizer.AddMany([shT, self.cShape, siT, self.cSize])
        #t = wx.StaticText(self.pnlSizes, -1, _('Choose collimator size'))
        bsizer.Add(shapeSizeSizer, 0, wx.CENTER, 1)

        bsizer.Add(wx.StaticText(self.pnlSizes, -1, _('Collimator center params')), 0, wx.CENTER, 1)

        colmatPositionSizer = wx.FlexGridSizer(cols=4, hgap=2, vgap=2)
        cX = wx.StaticText(self.pnlSizes, -1, 'X')
        cY = wx.StaticText(self.pnlSizes, -1, 'Y')
        cZ = wx.StaticText(self.pnlSizes, -1, 'Z')
        self.cX = txtBtnCtrlClass(self.pnlSizes, id=-1, size=(40, -1))
        self.cY = txtBtnCtrlClass(self.pnlSizes, id=-1, size=(40, -1))
        self.cZ = txtBtnCtrlClass(self.pnlSizes, id=-1, size=(40, -1))

        cA1 = wx.StaticText(self.pnlSizes, -1, 'Angle 1')
        cA2 = wx.StaticText(self.pnlSizes, -1, 'Angle 2')
        cA3 = wx.StaticText(self.pnlSizes, -1, 'Angle 3')
        self.cA1 = txtBtnCtrlClass(self.pnlSizes, id=-1, size=(40, -1))
        self.cA2 = txtBtnCtrlClass(self.pnlSizes, id=-1, size=(40, -1))
        self.cA3 = txtBtnCtrlClass(self.pnlSizes, id=-1, size=(40, -1))

        colmatPositionSizer.AddMany([
            cX, self.cX, cA1, self.cA1,
            cY, self.cY, cA2, self.cA2,
            cZ, self.cZ, cA3, self.cA3
        ])
        bsizer.Add(colmatPositionSizer, 0, wx.CENTER, 1)

        ssdSizer = wx.BoxSizer(wx.HORIZONTAL)
        ssdT = wx.StaticText(self.pnlSizes, -1, _('SSD (mm)'))
        #ssdTD = wx.StaticText(self.pnlSizes, -1, _('mm'))
        stdT = wx.StaticText(self.pnlSizes, -1, _('STD (mm)'))
        #stdTD = wx.StaticText(self.pnlSizes, -1, _('mm'))
        self.ssd = txtBtnCtrlClass(self.pnlSizes, id=-1, size=(40, -1))
        self.ssd.SetValue('100')
        self.std = txtBtnCtrlClass(self.pnlSizes, id=-1, size=(40, -1))
        self.ssd.Enable(False)
        self.std.Enable(False)
        ssdSizer.AddMany([ssdT, self.ssd, stdT, self.std])
        bsizer.Add(ssdSizer, 0, wx.CENTER, 1)

        border = wx.BoxSizer()
        border.Add(bsizer, 1, wx.EXPAND | wx.ALL, 1)
        self.pnlSizes.SetSizerAndFit(border)
        pass

    def renderFacilityParams(self, facilities=[], collimators=[], positioners=[], ):
        box = wx.StaticBox(self.pnlFacility, -1, _('Facility params'))
        bsizer = wx.StaticBoxSizer(box, wx.VERTICAL)

        fgSizer = wx.FlexGridSizer(cols=3, hgap=10, vgap=3)
        sampleList = ['123', '1234', '5345']
        t = wx.StaticText(self.pnlFacility, -1, _('Beam facility'), (15, 50), (75, -1))
        t2 = wx.StaticText(self.pnlFacility, -1, _('Collimator'), (15, 50), (75, -1))
        t3 = wx.StaticText(self.pnlFacility, -1, _('Position facility'), (15, 50), (75, -1))
        self.choiceFacility = wx.Choice(self.pnlFacility, -1, (100, 50), choices=facilities)
        bstyle = wx.WINDOW_VARIANT_SMALL | platebtn.PB_STYLE_DEFAULT
        hBtn1 = platebtn.PlateButton(
            self.pnlFacility,
            wx.ID_ANY,
            "",
            wx.Bitmap(NPPlan.getIconPath()+'iconHelpSmall16.png'),
            style=bstyle
        )
        self.tip1 = STT.SuperToolTip('123')
        self.tip1.ApplyStyle('Office 2007 Blue')
        self.tip1.SetTarget(hBtn1)
        self.tip1.SetDrawHeaderLine(True)
        print 'STT', STT.GetStyleKeys()
        hBtn2 = platebtn.PlateButton(
            self.pnlFacility,
            wx.ID_ANY,
            "",
            wx.Bitmap(NPPlan.getIconPath() + 'iconHelpSmall16.png'),
            style=bstyle
        )
        self.ch2 = wx.Choice(self.pnlFacility, -1, (100, 50), choices=collimators)
        self.ch3 = wx.Choice(self.pnlFacility, -1, (100, 50), choices=positioners)
        hBtn3 = platebtn.PlateButton(
            self.pnlFacility,
            wx.ID_ANY,
            "",
            wx.Bitmap(NPPlan.getIconPath() + 'iconHelpSmall16.png'),
            style=bstyle
        )
        fgSizer.AddMany([
            hBtn1, t, self.choiceFacility,
            hBtn2, t2, self.ch2,
            hBtn3, t3, self.ch3
        ])

        bsizer.Add(fgSizer, 0, wx.LEFT | wx.TOP, 4)

        border = wx.BoxSizer()
        border.Add(bsizer, 1, wx.EXPAND | wx.ALL, 1)
        self.pnlFacility.SetSizerAndFit(border)
        pass

    def updateTip1(self, data, img):
        """

        @param data:
        @type data: NPPlan.model.mongo.kitmodels.facilities.facility
        @return:
        """
        print 'Updating tip1', data, type(data)
        self.tip1.SetMessage(wordwrap(data.description, 250, wx.ClientDC(self)))

        self.tip1.SetHeader(data.rusName)
        # @todo: check image existance and show it at header
        self.tip1.SetBodyImage(img)
        #self.tip1.SetBodyImage(wx.Bitmap(r'C:\Users\Chernukha\Downloads\ng-14.jpg', wx.BITMAP_TYPE_ANY).ConvertToImage().Scale(100, 100, wx.IMAGE_QUALITY_HIGH).ConvertToBitmap())
        #self.tip1.SetHeaderBitmap(wx.Bitmap(r'C:\Users\Chernukha\Downloads\ng-14.jpg', wx.BITMAP_TYPE_ANY).ConvertToImage().Resize((100, 100), (0, 0)).ConvertToBitmap())
        #self.tip1.SetHeaderBitmap(wx.Bitmap(r'C:\Users\Chernukha\Downloads\ng-14.jpg', wx.BITMAP_TYPE_ANY).ConvertToImage().Scale(100, 100, wx.IMAGE_QUALITY_HIGH).ConvertToBitmap())
        #wx.Bitmap.ConvertToImage()
        #wx.Image.Resize()
        #wx.Image.Scale()


    def assignController(self, controller):
        """
        @param controller:
        @type controller: NPPlan.controller.client.mainC.stages.positioning.bev.wxListController
        @return:
        """
        self._controller = controller
        #self.listWC.setParser(controller.selectionParser)
        pass

    def addBeam(self, *args, **kwargs):
        """
        proxy for self._controller.addBeamClick
        @param args:
        @param kwargs:
        @return:
        """
        #from NPPlan.controller.client.main import getHandler
        self._controller.addBeamClicked(*args, **kwargs)
        ##print getHandler().rightPanelBev
        #getHandler().rightPanelBev.controller.addBeamClicked(*args, **kwargs)
        pass

    def delBeam(self, *args, **kwargs):
        self._controller.delBeamClicked(*args, **kwargs)
        pass


#class rightPanelView(rightFoldPanelBase):
#    def __init__(self, parent, **kwargs):
#        rightFoldPanelBase.__init__(self, parent)

#class rightPanelView(wx.Panel):
#    def __init__(self, *args, **kwargs):
#        wx.Panel.__init__(self, *args, **kwargs)
