# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 12.11.12
@summary: 
'''

import NPPlan

from vtk.wx.wxVTKRenderWindowInteractor import *
import wx

class viewerVtkBase(wx.ScrolledWindow):
    def __init__(self, parent):
        wx.ScrolledWindow.__init__(self, parent, wx.ID_ANY)

        sizer = wx.BoxSizer(wx.VERTICAL)

        self.__dict__['vtkWindow'] = wxVTKRenderWindowInteractor(self, wx.NewId(), name='viewer')
        sizer.Add(self.__dict__['vtkWindow'], 1, wx.EXPAND)
        self.SetSizer(sizer)

        self.controller = None

    def getDC(self):
        return wx.ClientDC(self.__dict__['vtkWindow'])

    def getVtkWindow(self):
        return self.__dict__['vtkWindow']

    vtkWindow = property(getVtkWindow)
    vtkDC = property(getDC)


