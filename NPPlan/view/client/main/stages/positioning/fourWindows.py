# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 24.12.12
@summary: 
'''

import NPPlan

import wx
try:
    from agw import fourwaysplitter as FWS
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.fourwaysplitter as FWS

from projectionVtkWindows import projectionWindow
from threeDimWindow import threeDimWindow


class fourWindowsView(wx.Panel):
    def __init__(self, pController=None, config={}):
        parent = pController._panel
        wx.Panel.__init__(self, parent, wx.NewId())

        #splitter = fourWindowsBase(self)
        splitter = FWS.FourWaySplitter(self, agwStyle=wx.SP_LIVE_UPDATE)
        self.splitter = splitter

        self._controller = pController

        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer.Add(splitter, 1, wx.EXPAND)
        self.SetSizer(sizer)

        self.wins = {}
        for win in config:
            if 'threeDim' != win:
                self.wins[config[win]['position']] = projectionWindow(self, win, config[win]['caption'], config[win]['captionColor'])
            else:
                self.wins[config[win]['position']] = threeDimWindow(self)

        splitter.AppendWindow(self.wins[0])
        splitter.AppendWindow(self.wins[1])
        splitter.AppendWindow(self.wins[2])
        splitter.AppendWindow(self.wins[3])

    def mouseWheel(self, event):
        """
        @param event: событие
        @type event: wx.MouseEvent
        """
        event.Skip()
        print event.GetPositionTuple()
        print 'MOUSEWHEEL', event, event.GetWheelRotation()
        return
        if event.GetWheelRotation() > 0:
            self._controller.mWheel('up', self.getWindowNumber(event.GetPosition()), event)
        else:
            self._controller.mWheel('down', self.getWindowNumber(event.GetPosition()), event)
        event.Skip()

    def assertController(self, controller):
        self._controller = controller

    def getWindowNumber(self, xypos, caller='out'):
        x, y = xypos
        if caller == 'out':
            xtl, ytl = self.splitter.GetWindow(0).GetScreenPosition()
            xtls, ytls = self.splitter.GetWindow(0).GetSize()
            xtr, ytr = self.splitter.GetWindow(1).GetScreenPosition()
            xtrs, ytrs = self.splitter.GetWindow(1).GetSize()
            xbl, ybl = self.splitter.GetWindow(2).GetScreenPosition()
            xbls, ybls = self.splitter.GetWindow(2).GetSize()
            xbr, ybr = self.splitter.GetWindow(3).GetScreenPosition()
            xbrs, ybrs = self.splitter.GetWindow(3).GetSize()
        elif caller == 'self':
            xtl, ytl = self.splitter.GetWindow(0).GetPosition()
            xtls, ytls = self.splitter.GetWindow(0).GetSize()
            xtr, ytr = self.splitter.GetWindow(1).GetPosition()
            xtrs, ytrs = self.splitter.GetWindow(1).GetSize()
            xbl, ybl = self.splitter.GetWindow(2).GetPosition()
            xbls, ybls = self.splitter.GetWindow(2).GetSize()
            xbr, ybr = self.splitter.GetWindow(3).GetPosition()
            xbrs, ybrs = self.splitter.GetWindow(3).GetSize()
        #print xtl, ytl
        #print xtr, ytr
        #print xbl, ybl
        #print xbr, ybr
        if (x > xtl and x < xtr) and (y > ytl and y < ybl and y < ybl + ybls):
        #if x > xtl and y > ytl and x < xtr and y < ybl:
            return 0
        elif (x > xtr and x < xtr + xtrs) and (y > ytr and y < ybr):
        #elif x > xtr and y > ytr and y < ybl:
            return 1
        #elif x > xbr and y > ybr and y > ybl:
        elif (x > xbl and x < xbr) and (y > ybl and y < ybl + ybls):
            return 2
        elif (x > xbr and x < xbr + xbrs) and (y > ybr and y < ybr + ybrs):
            return 3
        else:
            return -1

    def rightClicked(self, event=0):
        """
        @param event: событие
        @type event: wx.MouseEvent
        """
        #print 'from fourway splitter'
        #self.CaptureMouse()
        print 'rightclick', event.GetPosition()
        print self.getWindowNumber(event.GetPosition(), 'self')
        if -1 == self._controller._curExpand:
            self._controller.showPopup(self.getWindowNumber(event.GetPosition(), 'self'), event.GetPosition())
        else:
            self._controller.showPopup(self._controller._curExpand, event.GetPosition(), True)
        event.Skip()

    def getLeftUp(self):
        return self.wins[0]

    def getRightUp(self):
        return self.wins[1]

    def getLeftDown(self):
        return self.wins[2]

    def getRightDown(self):
        return self.wins[3]

    leftUp = property(getLeftUp)
    rightUp = property(getRightUp)
    leftUp = property(getLeftUp)
    leftDown = property(getLeftDown)

    def getByPosition(self, pos):
        return self.wins[pos]

    def doLayout(self):
        for i in range(0, 3):
            self.wins[i].doLayout()

    def leftDown(self, event):
        event.Skip()
        self._controller.handleLeft('down', self.getWindowNumber(event.GetPosition(), 'self'), event)
        event.Skip()

    def leftUp(self, event):
        event.Skip()
        self._controller.handleLeft('up', self.getWindowNumber(event.GetPosition(), 'self'), event)
        event.Skip()

    def motion(self, event):
        event.Skip()
        print 'motion', event.GetPosition()
        wN = self.getWindowNumber(event.GetPosition(), 'self')
        if -1 == wN:
            return
        self._controller.handleLeft('motion', wN, event)
        self._controller.handleHelper(wN, self.wins[wN], event)
        #self.wins[wN].showHelper(event)
        event.Skip()
        pass

    def lostFocus(self, event, winNumber):
        self.wins[winNumber].hideHelper(event)
        pass

    def appendHelpers(self, params):
        for i in range(0, 4):
            self.wins[i].appendHelper(params)
            #self.wins[i].appendHelper(params[i])
        pass

