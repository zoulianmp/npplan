# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 15.11.12
@summary: 
'''

import NPPlan

import wx
try:
    from agw import flatnotebook as fnb
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.flatnotebook as fnb

class positioningTopView(wx.Panel):
    def __init__(self, parent, id=wx.ID_ANY):
        wx.Panel.__init__(self, parent)
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)

        bookStyle = fnb.FNB_NODRAG | fnb.FNB_VC8 | fnb.FNB_NO_X_BUTTON | fnb.FNB_SMART_TABS
        self.book = fnb.FlatNotebook(self, wx.ID_ANY, agwStyle=bookStyle)

        self.startPages()

        self.mainSizer.Add(self.book, 1, wx.EXPAND)

        self.SetSizer(self.mainSizer)

        self.Layout()
        self.Refresh()
        self.GetParent().Layout()
        self.GetParent().Refresh()
        self.GetParent().GetParent().Layout()
        self.GetParent().GetParent().Refresh()
        self.GetParent().GetParent().GetParent().Layout()
        self.GetParent().GetParent().GetParent().Refresh()

    def startPages(self):
        pass

    def addPage(self, pageObject, name):
        self.Freeze()
        self.book.AddPage(pageObject, name)
        self.Thaw()

    def getCurrentPage(self):
        return self.book.GetCurrentPage()