# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 12.11.12
@summary: 
'''
# @deprecated
import NPPlan
import wx

class rightPan(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.ID_ANY)

    def setRenderItem(self, item):
        self.item = item
    def getRenderItem(self):
        return self.item
    renderItem = property(getRenderItem, setRenderItem)

    def doLayout(self):
        if self.item is None:
            return
        if self.GetSizer() is not None:
            self.GetSizer().DeleteWindows()
            self.GetSizer().Detach(0)
        sizer = wx.BoxSizer(wx.VERTICAL)
        #print 'PANEL SIZE'
        #print self.GetSize()
        self.item.SetSize(self.GetSize())
        sizer.Add(self.item, 1, wx.EXPAND)
        self.SetSizer(sizer)