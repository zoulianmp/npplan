# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 13.11.12
@summary: 
'''

import NPPlan
import wx
import  wx.lib.colourselect as  csel
try:
    from agw import pycollapsiblepane as PCP
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.pycollapsiblepane as PCP

class viewerRightPan(wx.ScrolledWindow):
    def __init__(self, parent):
        wx.ScrolledWindow.__init__(self, parent, wx.ID_ANY)

        self.cpSet = PCP.PyCollapsiblePane(self,
                                           label=_('Viewer settings'),
                                           agwStyle=PCP.CP_GTK_EXPANDER
        )

        self.cpGray = PCP.PyCollapsiblePane(self,
                                           label=_('Grayscale settings'),
                                           agwStyle=PCP.CP_GTK_EXPANDER
        )

        self.settings = {}
        self.grayscale = {}
        self.renderSettingsPane(self.cpSet.GetPane())
        self.renderGrayscalePane(self.cpGray.GetPane())
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.cpSet, 0, wx.ALL|wx.EXPAND)
        sizer.Add(self.cpGray, 0, wx.ALL|wx.EXPAND)
        self.SetSizer(sizer)
        self.Layout()
        self.GetParent().Layout()

    def renderSettingsPane(self, pane):
        label = 'aaa'
        color = (255, 255, 0)
        b = csel.ColourSelect(pane, -1, '', color, size = wx.DefaultSize)
        sizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        sizer.AddMany([
            wx.StaticText(pane, -1, _('Background')),
            b
        ])
        outline = wx.CheckBox(pane, -1, "")
        sizer.AddMany([
            wx.StaticText(pane, -1, _('Hide outline')),
            outline
        ])
        pane.SetSizer(sizer)
        self.settings['bgPicker'] = b
        self.settings['outlineCheck'] = outline
        #b.Bind(csel.EVT_COLOURSELECT, self.onSelectColor)

    def renderGrayscalePane(self, pane):
        sizer = wx.FlexGridSizer(cols=2, hgap=5, vgap=5)
        sl0 = wx.Slider(pane, -1, value=1, minValue=1, maxValue=3)
        sl1 = wx.Slider(pane, -1, value=-300, minValue=-1024, maxValue=2875)
        sl2 = wx.Slider(pane, -1, value=300, minValue=-1024, maxValue=2875)
        sl3 = wx.Slider(pane, -1, value=60, minValue=-360, maxValue=360)
        sizer.AddMany([
            wx.StaticText(pane, -1, _('First')),
            sl0
        ])
        sizer.AddMany([
            wx.StaticText(pane, -1, _('Left')),
            sl1
        ])
        sizer.AddMany([
            wx.StaticText(pane, -1, _('Right')),
            sl2
        ])
        sizer.AddMany([
            wx.StaticText(pane, -1, _('angle')),
            sl3
        ])
        self.grayscale['sl0'] = sl0
        self.grayscale['sl1'] = sl1
        self.grayscale['sl2'] = sl2
        self.grayscale['sl3'] = sl3
        pane.SetSizer(sizer)

    def onSelectColor(self, event):
        print event.GetValue()
