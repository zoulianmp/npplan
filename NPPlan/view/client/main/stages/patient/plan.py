# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 19.10.12
@summary: 
'''

import NPPlan
import sys

import wx
import wx.lib.mixins.listctrl as listmix
import wx.lib.platebtn as platebtn
import wx.richtext as rt
from NPPlan.view.client.main.makeFieldsContent import makeFieldsContent

class planListCtrl(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin):
    def __init__(self, parent, ID, pos=wx.DefaultPosition,
                 size=wx.DefaultSize, style=0):
        wx.ListCtrl.__init__(self, parent, ID, pos, size, style)
        listmix.ListCtrlAutoWidthMixin.__init__(self)


class plan(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.NewId(), style=wx.TAB_TRAVERSAL)

        self.SetBackgroundColour(wx.WHITE)

        self.fields = {}

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)

        topSz = wx.BoxSizer(wx.HORIZONTAL)
        self.showDoctors = wx.CheckBox(self, -1, _('Show only this doctor'))
        self.showToday = wx.CheckBox(self, -1, _('Show only today\'s events'))
        topSz.AddMany([
            self.showDoctors,
            self.showToday
        ])
        self.mainSizer.Add(topSz, 0, wx.EXPAND)

        self.plansListId = wx.NewId()
        self.plansList = planListCtrl(self, self.plansListId,
                                         style=wx.LC_REPORT
                                         #| wx.BORDER_NONE
                                         | wx.LC_EDIT_LABELS
                                         | wx.LC_SORT_ASCENDING
                                         | wx.LC_SINGLE_SEL
                                         )
        self.mainSizer.Add(self.plansList, 10, wx.EXPAND)

        btnSz = wx.BoxSizer(wx.HORIZONTAL)
        self.addPlanBtn = platebtn.PlateButton(
            self,
            wx.ID_ANY,
            _('Add plan'),
            wx.Bitmap(NPPlan.getIconPath()+'iconPlus32.png', wx.BITMAP_TYPE_PNG),
            style=wx.WINDOW_VARIANT_SMALL
        )
        self.editPlanBtn = platebtn.PlateButton(
            self,
            wx.ID_ANY,
            _('Edit plan'),
            wx.Bitmap(NPPlan.getIconPath()+'iconEdit32.png', wx.BITMAP_TYPE_PNG),
            style=wx.WINDOW_VARIANT_SMALL
        )
        self.delPlanBtn = platebtn.PlateButton(
            self,
            wx.ID_ANY,
            _('Delete plan'),
            wx.Bitmap(NPPlan.getIconPath()+'iconCross32.png', wx.BITMAP_TYPE_PNG),
            style=wx.WINDOW_VARIANT_SMALL
        )
        self.addStudyBtn = platebtn.PlateButton(
            self,
            wx.ID_ANY,
            _('Add study'),
            wx.Bitmap(NPPlan.getIconPath()+'iconAddStudy32.png', wx.BITMAP_TYPE_PNG),
            style=wx.WINDOW_VARIANT_SMALL
        )
        btnSz.Add(self.addPlanBtn)
        btnSz.Add(self.editPlanBtn)
        btnSz.Add(self.delPlanBtn)
        btnSz.Add(self.addStudyBtn)
        self.addStudyBtn.Enable(0)
        self.mainSizer.Add(btnSz)

        curPlanAllSz = wx.BoxSizer(wx.HORIZONTAL)
        curPlanLeftSz = wx.FlexGridSizer(cols=2, vgap=3, hgap=10)
        # @todo: from config via controller
        self.renderPlanFields(self, curPlanLeftSz,
                    fields = [
                        {
                            'name' : 'plan.status',
                            'caption' : 'Status:',
                            'type' : 'select',
                            'values' : {
                                'active' : 'Active',
                                'finished' : 'Finished',
                                'restricted' : 'Closed, view only'
                            }
                        },
                        {
                            'name' : 'plan.name',
                            'caption' : 'Name:',
                            'type' : 'text',
                        },
                        {
                            'name' : 'plan.startDate',
                            'caption' : 'Start date',
                            'type' : 'date',
                        },
                        {
                            'name' : 'plan.type',
                            'caption' : 'Plan type',
                            'type' : 'select',
                            'values' : {
                                'sel' : 'test'
                            }
                        }
                    ]
        )
        curPlanAllSz.Add(curPlanLeftSz, 0, wx.EXPAND)
        rightPanel = wx.Panel(self, wx.NewId(), style=wx.BORDER_SUNKEN)
        #rightPanel = wx.StaticBox(self, -1, _('Plan information'))
        curPlanRightSz = wx.FlexGridSizer(cols=2, vgap=3, hgap=10)
        self.renderPlanFields(rightPanel, curPlanRightSz, fields=[
            {
                'name' : 'plan.studies',
                'caption' : 'Plan information',
                'text' : '',
                'type' : 'staticText',
                'captionFontSize' : 14
            },
            {
                'name' : 'plan.studies',
                'caption' : 'Number of studies',
                'text' : '0',
                'type' : 'staticText',
            },
            {
                'name' : 'plan.treatmentStartDate',
                'caption' : 'Treatment start date',
                'type' : 'date'
            },
            {
                'name' : 'plan.treatmentStopDate',
                'caption' : 'Treatment stop date',
                'type' : 'date'
            },
        ])
        rightPanel.SetSizer(curPlanRightSz)
        curPlanAllSz.Add(rightPanel, 0, wx.EXPAND)
        self.mainSizer.Add(curPlanAllSz, 14, wx.EXPAND)

        self.mainSizer.Add(wx.StaticText(self, wx.ID_ANY, _('Plan notes:')))
        self.fields['plan.notes'] = wx.TextCtrl(self, -1, "",
                                size=(600, -1), style=wx.TE_MULTILINE|wx.TE_RICH2)
        self.mainSizer.Add(self.fields['plan.notes'], 20, wx.EXPAND)

        self.SetSizer(self.mainSizer)

    def renderPlanFields(self, panel, sizer, fields=None):
        self.fields = makeFieldsContent(panel, sizer, fields, self.fields)

    def addListItem(self, fields):
        print fields
        self.plansList.Append(fields)

    def collect(self):
        print 'collect'
        ret = {}
        for i in self.fields:
            try:
                # TextCtrl
                ret[i] = self.fields[i].GetText()
            except AttributeError:
                try:
                    # Label
                    ret[i] = self.fields[i].GetLabel()
                except AttributeError:
                    try:
                        # Choice
                        ret[i] = self.fields[i].GetStringSelection()
                    except AttributeError:
                        # DatePicker
                        ret[i] = self.fields[i].GetDate()
            print i
        return ret

    def GetListCtrl(self):
        return self.plansList
