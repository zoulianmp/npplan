# -*- coding: utf8 -*-
'''

@author:
@copyright: MRRC Obninsk 2012
@version: 1
@date: 24.10.12
@summary:
'''

import NPPlan
import wx

class studyShortInfo(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.ID_ANY, style=wx.BORDER_NONE|wx.EXPAND)
        self.__dict__['items'] = []
        self.__dict__['values'] = {}

    def addItem(self, item):
        self.items.append(item)

    def setItems(self, items):
        self.__dict__['items'] = items

    def doLayout(self):
        self.GetParent().Freeze()
        try:
            self.GetSizer().Destroy()
        except AttributeError:
            pass
        sizer = wx.FlexGridSizer(cols=2, hgap=6, vgap=1)

        title = wx.StaticText(self, wx.ID_ANY, _('Slice short information'))
        title.SetFont(wx.Font(14, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, underline=True))
        sizer.AddMany([
            title,
            (1,1)
        ])

        sizer.Add(wx.StaticText(self, wx.ID_ANY, _('File name')))
        self.__dict__['values']['fileName'] = wx.StaticText(self, wx.ID_ANY, '')
        sizer.Add(self.__dict__['values']['fileName'])

        for i in self.__dict__['items']:
            caption = wx.StaticText(self, wx.ID_ANY, i['ruCaption'])
            self.__dict__['values'][i['name']] = wx.StaticText(self, wx.ID_ANY, '')
            sizer.AddMany([
                caption,
                self.__dict__['values'][i['name']],
            ])

        self.SetSizerAndFit(sizer)
        self.GetParent().Thaw()

    def reLayout(self):
        self.Freeze()
        self.SetSizerAndFit(self.GetSizer())
        self.Thaw()

    def setText(self, item):
        if isinstance(item, list):
            for i in item:
                self.__dict__['values'][i['name']].SetLabel(i['text'])
        else:
            self.__dict__['values'][item['name']].SetLabel(item['text'])

    def __setattr__(self, key, value):
        try:
            self.__dict__['values'][key].SetLabel(str(value))
        except KeyError:
            pass
        self.reLayout()

    def __getattr__(self, item):
        try:
            self.__dict__['values'][item].GetLabel()
        except KeyError:
            pass


