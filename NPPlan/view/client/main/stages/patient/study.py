# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 19.10.12
@summary: 
'''

import NPPlan
import wx

from NPPlan.view.client.main.dragdropopen import fileDropPanel
import wx.lib.platebtn as platebtn
from NPPlan.view.client.main.stages.patient.studyShortInfo import studyShortInfo
#from NPPlan.view.client.elements.thumbnailGrid.thumbnailGrid import thumbnailGrid

try:
    from agw import thumbnailctrl as TC
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.thumbnailctrl as TC

class study(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.NewId())
        mainSizer = wx.BoxSizer(wx.VERTICAL)

        inpSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.fileDropPanel = fileDropPanel(self)
        inpSizer.Add(self.fileDropPanel)

        btnSizer = wx.BoxSizer(wx.VERTICAL)
        self.clearBtn = platebtn.PlateButton(
            self,
            wx.ID_ANY,
            _('Clear'),
            wx.Bitmap(NPPlan.getIconPath()+'iconCross32.png', wx.BITMAP_TYPE_PNG),
            style=wx.WINDOW_VARIANT_SMALL
        )
        btnSizer.Add(self.clearBtn)
        addTypesPanel = wx.Panel(self, style=wx.BORDER_SUNKEN)
        addTypesSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetBackgroundColour(wx.WHITE)
        addTypesPanel.SetBackgroundColour(wx.WHITE)
        self.addFolder = platebtn.PlateButton(
            addTypesPanel,
            wx.ID_ANY,
            _('Import folder'),
            wx.Bitmap(NPPlan.getIconPath()+'iconDicomFolder32.png', wx.BITMAP_TYPE_PNG),
            style=wx.WINDOW_VARIANT_SMALL
        )
        self.addCd = platebtn.PlateButton(
            addTypesPanel,
            wx.ID_ANY,
            _('Import cd'),
            wx.Bitmap(NPPlan.getIconPath()+'iconDicomCd32.png', wx.BITMAP_TYPE_PNG),
            style=wx.WINDOW_VARIANT_SMALL
        )
        self.addNetwork = platebtn.PlateButton(
            addTypesPanel,
            wx.ID_ANY,
            _('Import network'),
            wx.Bitmap(NPPlan.getIconPath()+'iconDicomNetwork32.png', wx.BITMAP_TYPE_PNG),
            style=wx.WINDOW_VARIANT_SMALL
        )
        addTypesSizer.AddMany([
            self.addFolder,
            self.addCd,
            self.addNetwork
        ])
        addTypesPanel.SetSizer(addTypesSizer)
        btnSizer.Add(wx.StaticText(self, wx.ID_ANY, _('Drag files or choose source:')))
        btnSizer.Add(addTypesPanel)
        inpSizer.Add(btnSizer)

        # @todo: bind to controller
        self.Bind(wx.EVT_BUTTON, self.testBtn, self.addNetwork)

        mainSizer.Add(inpSizer, 1, wx.EXPAND)

        thumbSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.thumb = TC.ThumbnailCtrl(self, -1, imagehandler=TC.PILImageHandler)
        self.thumb.ShowFileNames()
        self.thumb.ShowDir(NPPlan.getIconPath())
        self.thumb.SetMinSize((400, 400))
        self.thumb.SetThumbOutline(TC.THUMB_OUTLINE_FULL)
        thumbSizer.Add(self.thumb, 10, wx.EXPAND|wx.ALL)
        self.shortInfo = studyShortInfo(self)
        thumbSizer.Add(self.shortInfo, 5, wx.EXPAND)
        mainSizer.Add(thumbSizer, 1, wx.EXPAND)

        self.SetSizerAndFit(mainSizer)
        self.Layout()
        mainSizer.Layout()

    def loadThumbnails(self, dir):
        print 'Loading thumbs: %s' %dir
        self.thumb.ShowDir(dir)

    def testBtn(self, event):
        print 'Set dir'
        self.thumb.ShowDir('C:\\Users\\Chernukha\\Pictures\\DICOM\\thumb\\')