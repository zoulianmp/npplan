# -*- coding: utf8 -*-
'''
Вид главного окна конфигурации
@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 10.06.13
@summary: 
'''

import wx
try:
    from agw import flatnotebook as fnb
except ImportError:
    import wx.lib.agw.flatnotebook as fnb
from materials import materialsView
from organs import organsView
from cs import *


class mainView(wx.Panel):
    def __init__(self, parent, cParent=None):
        """
        Создаёт контрол wx.lib.agw.flatnotebook.FlatNotebook, добавляет в него окна управления конфигурациями
        клиент-сервера, органов и материалов
        @param parent:
        @type parent: wx.Panel
        @param cParent:
        @type cParent: NPPlan.controller.client.mainC.stages.configuration.controller.configurationControllerMain
        @return:
        @todo: add log viewer
        """
        wx.Panel.__init__(self, parent)

        sizer = wx.FlexGridSizer(cols=2, hgap=8, vgap=8)
        self._topBook = fnb.FlatNotebook(self, wx.ID_ANY, agwStyle=fnb.FNB_NO_X_BUTTON | fnb.FNB_NODRAG | fnb.FNB_VC8)
        self._controller = cParent
        #self._organsButton = wx.Button(self, wx.NewId(), _('Organs and contours configuration'))
        #organsLabel = wx.StaticText(self, wx.NewId(), _('Organd label description'))
        #self._materialsButton = wx.Button(self, wx.NewId(), _('Configure simulation data'))
        #materialsLabel = wx.StaticText(self, wx.NewId(), _('Simulation grid and materials'))
        self._clientAndServersView = clientsAndServersView(self._topBook)
        self._topBook.AddPage(self._clientAndServersView, _('Clients and servers'))
        self._organsView = organsView(self._topBook)
        self._topBook.AddPage(self._organsView, _('Organs and contours'))
        self._matView = materialsView(self._topBook)
        self._topBook.AddPage(self._matView, _('Isotopes and simulation materials'))
        sizer.Add(self._topBook)
        self.SetSizer(sizer)
        self.Layout()
        self.Bind(wx.EVT_PAINT, self.onPaint)
        self.Bind(fnb.EVT_FLATNOTEBOOK_PAGE_CHANGED, self.onPageChanged)

    def onPaint(self, event):
        """
        Функция, которая вызывается по окончанию отрисовки wx-элементов, передаёт управление в
        контроллер
        @param event:
        @type event: wx._core.PaintEvent
        @return:
        """
        event.Skip()
        #self._controller.onPaintCompleted(event)
        event.Skip()

    def onPageChanged(self, event):
        """
        @param event:
        @type event: wx.lib.agw.flatnotebook.FlatNotebookEvent
        @return:
        """
        #self._controller.onTopPageChanged(event)
        pass