# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 11.09.13
@summary: 
'''

import NPPlan
import wx
import  wx.gizmos   as  gizmos


class facilityView(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self._controller = None

        bSizer1 = wx.BoxSizer( wx.VERTICAL )
        
        sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Устройства контроля" ), wx.VERTICAL )
        
        bSizer2 = wx.BoxSizer( wx.HORIZONTAL )
        
        m_listBox1Choices = []
        self.m_listBox1 = wx.ListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_listBox1Choices, 0 )
        bSizer2.Add( self.m_listBox1, 1, wx.ALL|wx.EXPAND, 5 )
        
        bSizer3 = wx.BoxSizer( wx.VERTICAL )
        
        self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText1.Wrap( -1 )
        bSizer3.Add( self.m_staticText1, 0, wx.ALL, 5 )
        
        self.m_bitmap1 = wx.StaticBitmap( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer3.Add( self.m_bitmap1, 3, wx.ALL|wx.EXPAND, 5 )
        
        bSizer4 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.m_button1 = wx.Button( self, wx.ID_ANY, u"MyButton", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer4.Add( self.m_button1, 0, wx.ALL, 5 )
        
        self.m_button2 = wx.Button( self, wx.ID_ANY, u"MyButton", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer4.Add( self.m_button2, 0, wx.ALL, 5 )
        
        
        bSizer3.Add( bSizer4, 1, wx.EXPAND, 5 )
        
        
        bSizer2.Add( bSizer3, 1, wx.EXPAND, 5 )
        
        
        sbSizer3.Add( bSizer2, 1, wx.EXPAND, 5 )
        
        
        bSizer1.Add( sbSizer3, 1, wx.EXPAND, 5 )
        
        sbSizer4 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Источники излучения" ), wx.VERTICAL )

        # @todo: same panel

        self.sourcesTree = wx.TreeCtrl(self, -1, style =
                                        wx.TR_DEFAULT_STYLE
                                        #| wx.TR_HAS_BUTTONS
                                        #| wx.TR_TWIST_BUTTONS
                                        | wx.TR_ROW_LINES
                                        | wx.TR_HIDE_ROOT
                                        | wx.TR_COLUMN_LINES
                                        #| wx.TR_NO_LINES
                                        | wx.TR_FULL_ROW_HIGHLIGHT,
                                   )

        #self.tree.SetMinSize((800, -1))
        bSizerSources = wx.BoxSizer( wx.HORIZONTAL )
        bSizerSources.Add(self.sourcesTree, 1, wx.ALL|wx.EXPAND, 5)

        self.sourcesData = sourceDataPanel(self)
        bSizerSources.Add(self.sourcesData, 1, wx.ALL|wx.EXPAND, 5)

        sbSizer4.Add(bSizerSources, 1, wx.EXPAND, 5)

        #self.sourcesTree.AddColumn("Sources")
        #self.sourcesTree.SetMainColumn(0)
        #self.sourcesTree.SetColumnWidth(0, 400)


        bSizer1.Add( sbSizer4, 1, wx.EXPAND, 5 )
        
        sbSizer5 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Системы позиционирования" ), wx.VERTICAL )
        # @todo: same panel
        
        bSizer1.Add( sbSizer5, 1, wx.EXPAND, 5 )
        
        
        self.SetSizer( bSizer1 )
        self.Layout()


class sourceDataPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        fgSizer1 = wx.FlexGridSizer( 0, 4, 0, 0 )
        fgSizer1.SetFlexibleDirection( wx.BOTH )
        fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_staticText1 = wx.StaticText( self, wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText1.Wrap( -1 )
        fgSizer1.Add( self.m_staticText1, 0, wx.ALL, 5 )
        
        self._name = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( self._name, 0, wx.ALL, 5 )
        
        self.m_staticText2 = wx.StaticText( self, wx.ID_ANY, u"Russian name", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText2.Wrap( -1 )
        fgSizer1.Add( self.m_staticText2, 0, wx.ALL, 5 )
        
        self._rusName = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, (200, -1), 0 )
        fgSizer1.Add( self._rusName, 0, wx.ALL, 5 )
        
        self.m_staticText6 = wx.StaticText( self, wx.ID_ANY, u"Particle type", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText6.Wrap( -1 )
        fgSizer1.Add( self.m_staticText6, 0, wx.ALL, 5 )
        
        self._particleType = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( self._particleType, 0, wx.ALL, 5 )
        
        
        fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        self.m_staticText3 = wx.StaticText( self, wx.ID_ANY, u"Energy low limit, ", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText3.Wrap( -1 )
        fgSizer1.Add( self.m_staticText3, 0, wx.ALL, 5 )
        
        self._lowEnergy = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( self._lowEnergy, 0, wx.ALL, 5 )
        
        self.m_staticText4 = wx.StaticText( self, wx.ID_ANY, u"Energy high limit, ", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText4.Wrap( -1 )
        fgSizer1.Add( self.m_staticText4, 0, wx.ALL, 5 )
        
        self._highEnergy = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        fgSizer1.Add( self._highEnergy, 0, wx.ALL, 5 )
        
        self.m_staticText7 = wx.StaticText( self, wx.ID_ANY, u"Additional information", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7.Wrap( -1 )
        fgSizer1.Add( self.m_staticText7, 0, wx.ALL, 5 )
        
        self._data = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, (200, 50), wx.TE_MULTILINE )
        fgSizer1.Add( self._data, 0, wx.ALL, 5 )
        
        
        fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        fgSizer1.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        self.m_staticText8 = wx.StaticText( self, wx.ID_ANY, u"MCNP Info", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText8.Wrap( -1 )
        fgSizer1.Add( self.m_staticText8, 0, wx.ALL, 5 )
        
        self._mcnpData = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, (200, 50), wx.TE_MULTILINE )
        fgSizer1.Add( self._mcnpData, 0, wx.ALL, 5 )
        
        self.m_staticText9 = wx.StaticText( self, wx.ID_ANY, u"Geant info", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )
        fgSizer1.Add( self.m_staticText9, 0, wx.ALL, 5 )
        
        self._geantData = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, (200, 50), wx.TE_MULTILINE )
        fgSizer1.Add( self._geantData, 0, wx.ALL, 5 )
        
        
        self.SetSizer( fgSizer1 )
        self.Layout()

    def setData(self, data):
        pass

    def updateDataValue(self, moreData):
        # @todo: refactor
        oldData = self._data.GetValue()
        if len(oldData) > 1:
            self._data.SetValue("%s\n%s" % (oldData, moreData))
        else:
            self._data.SetValue(moreData)

    def updateMcnpDataValue(self, moreData):
        oldData = self._mcnpData.GetValue()
        if len(oldData) > 1:
            self._mcnpData.SetValue("%s\n%s" % (oldData, moreData))
        else:
            self._mcnpData.SetValue(moreData)

    def updateGeantDataValue(self, moreData):
        oldData = self._geantData.GetValue()
        if len(oldData) > 1:
            self._geantData.SetValue("%s\n%s" % (oldData, moreData))
        else:
            self._geantData.SetValue(moreData)