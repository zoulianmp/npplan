# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.06.13
@summary: 
'''

import NPPlan
import wx
import wx.richtext


class clientsAndServersView (wx.Panel):
    def __init__ (self, parent):
        wx.Panel.__init__ (self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(1038,773),
                           style=wx.TAB_TRAVERSAL, name=u"clientsAndServersView")

        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        sbSizer2 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"NPPlan server"), wx.VERTICAL)

        bSizer7 = wx.BoxSizer(wx.HORIZONTAL)

        fgSizer1 = wx.FlexGridSizer(0, 2, 8, 8)
        fgSizer1.SetFlexibleDirection(wx.BOTH)
        fgSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText6 = wx.StaticText(self, wx.ID_ANY, u"Host", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText6.Wrap(-1)
        fgSizer1.Add(self.m_staticText6, 0, wx.ALL, 5)

        self.npplanHost = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(350,-1), 0)
        fgSizer1.Add(self.npplanHost, 0, wx.ALL, 5)

        self.m_staticText7 = wx.StaticText(self, wx.ID_ANY, u"PBInfo Port", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText7.Wrap(-1)
        fgSizer1.Add(self.m_staticText7, 0, wx.ALL, 5)

        self.npplanPbPort = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer1.Add(self.npplanPbPort, 0, wx.ALL, 5)

        self.m_staticText8 = wx.StaticText(self, wx.ID_ANY, u"Informer port", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText8.Wrap(-1)
        fgSizer1.Add(self.m_staticText8, 0, wx.ALL, 5)

        self.npplanInfoPort = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer1.Add(self.npplanInfoPort, 0, wx.ALL, 5)

        self.m_staticText10 = wx.StaticText(self, wx.ID_ANY, u"Secure port", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText10.Wrap(-1)
        fgSizer1.Add(self.m_staticText10, 0, wx.ALL, 5)

        self.m_textCtrl7 = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer1.Add(self.m_textCtrl7, 0, wx.ALL, 5)

        self.m_staticText11 = wx.StaticText(self, wx.ID_ANY, u"Use secure connection if available", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText11.Wrap(-1)
        fgSizer1.Add(self.m_staticText11, 0, wx.ALL, 5)

        self.m_checkBox4 = wx.CheckBox(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox4.SetValue(True)
        fgSizer1.Add(self.m_checkBox4, 0, wx.ALL, 5)

        self.m_staticText12 = wx.StaticText(self, wx.ID_ANY, u"NAT", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText12.Wrap(-1)
        fgSizer1.Add(self.m_staticText12, 0, wx.ALL, 5)

        self.m_checkBox5 = wx.CheckBox(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer1.Add(self.m_checkBox5, 0, wx.ALL, 5)

        self.m_staticText14 = wx.StaticText(self, wx.ID_ANY, u"Allow incoming requests", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText14.Wrap(-1)
        fgSizer1.Add(self.m_staticText14, 0, wx.ALL, 5)

        self.m_checkBox7 = wx.CheckBox(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox7.SetValue(True)
        fgSizer1.Add(self.m_checkBox7, 0, wx.ALL, 5)

        self.m_staticText17 = wx.StaticText(self, wx.ID_ANY, u"Keep alive", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText17.Wrap(-1)
        fgSizer1.Add(self.m_staticText17, 0, wx.ALL, 5)

        self.m_checkBox9 = wx.CheckBox(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_checkBox9.SetValue(True)
        fgSizer1.Add(self.m_checkBox9, 0, wx.ALL, 5)

        self.planTestConnection = wx.Button(self, wx.ID_ANY, u"Test", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer1.Add(self.planTestConnection, 0, wx.ALL, 5)

        self.planSave = wx.Button(self, wx.ID_ANY, u"Save", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer1.Add(self.planSave, 0, wx.ALL, 5)


        bSizer7.Add(fgSizer1, 5, wx.EXPAND, 5)

        bSizer8 = wx.BoxSizer(wx.VERTICAL)

        self.m_staticText18 = wx.StaticText(self, wx.ID_ANY, u"Server console", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText18.Wrap(-1)
        bSizer8.Add(self.m_staticText18, 0, wx.ALL, 5)

        self.npplanServerConsole = wx.richtext.RichTextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0|wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER|wx.WANTS_CHARS)
        bSizer8.Add(self.npplanServerConsole, 1, wx.EXPAND | wx.ALL, 5)


        bSizer7.Add(bSizer8, 2, wx.EXPAND, 5)


        sbSizer2.Add(bSizer7, 1, wx.EXPAND, 5)


        bSizer1.Add(sbSizer2, 1, wx.EXPAND, 5)

        sbSizer3 = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY, u"Mongo database"), wx.VERTICAL)

        bSizer2 = wx.BoxSizer(wx.VERTICAL)

        bSizer4 = wx.BoxSizer(wx.HORIZONTAL)

        self.configMongoAuto = wx.CheckBox(self, wx.ID_ANY, u"Auto configuration", wx.DefaultPosition, wx.DefaultSize, 0)
        self.configMongoAuto.SetValue(True)
        bSizer4.Add(self.configMongoAuto, 0, wx.ALL, 5)

        self.configMongoManualOnAutoFail = wx.CheckBox(self, wx.ID_ANY, u"Use manual if auto failed", wx.DefaultPosition, wx.DefaultSize, 0)
        self.configMongoManualOnAutoFail.SetValue(True)
        bSizer4.Add(self.configMongoManualOnAutoFail, 0, wx.ALL, 5)


        bSizer2.Add(bSizer4, 0, wx.EXPAND, 5)

        bSizer5 = wx.BoxSizer(wx.HORIZONTAL)

        fgSizer2 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer2.SetFlexibleDirection(wx.BOTH)
        fgSizer2.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText2 = wx.StaticText(self, wx.ID_ANY, u"Host", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        fgSizer2.Add(self.m_staticText2, 0, wx.ALL, 5)

        self.mongoHost = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(350,-1), 0)
        fgSizer2.Add(self.mongoHost, 0, wx.ALL, 5)

        self.m_staticText3 = wx.StaticText(self, wx.ID_ANY, u"Port", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText3.Wrap(-1)
        fgSizer2.Add(self.m_staticText3, 0, wx.ALL, 5)

        self.mongoPort = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2.Add(self.mongoPort, 0, wx.ALL, 5)

        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, u"Database", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        fgSizer2.Add(self.m_staticText4, 0, wx.ALL, 5)

        self.mongoDatabase = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(250,-1), 0)
        fgSizer2.Add(self.mongoDatabase, 0, wx.ALL, 5)

        self.mongoTestConnection = wx.Button(self, wx.ID_ANY, u"Test connection", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2.Add(self.mongoTestConnection, 0, wx.ALL, 5)

        self.mongoSave = wx.Button(self, wx.ID_ANY, u"Save", wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2.Add(self.mongoSave, 0, wx.ALL, 5)

        self.m_staticText9 = wx.StaticText(self, wx.ID_ANY, u"Status", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText9.Wrap(-1)
        fgSizer2.Add(self.m_staticText9, 0, wx.ALL, 5)

        self.mongoStateBitmap = wx.StaticBitmap(self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0)
        fgSizer2.Add(self.mongoStateBitmap, 0, wx.ALL, 5)


        bSizer5.Add(fgSizer2, 2, wx.EXPAND, 5)

        bSizer6 = wx.BoxSizer(wx.VERTICAL)

        self.m_staticText5 = wx.StaticText(self, wx.ID_ANY, u"Collections", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText5.Wrap(-1)
        bSizer6.Add(self.m_staticText5, 0, wx.ALL, 5)

        #self.mongoCollectionsList = wx.ListCtrl(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_ICON)
        self.mongoCollectionsList = wx.ListBox(self, wx.ID_ANY, style=wx.LB_SINGLE)
        bSizer6.Add(self.mongoCollectionsList, 1, wx.ALL|wx.EXPAND, 5)


        bSizer5.Add(bSizer6, 1, wx.EXPAND, 5)


        bSizer2.Add(bSizer5, 1, wx.EXPAND, 5)


        sbSizer3.Add(bSizer2, 1, wx.EXPAND, 5)


        bSizer1.Add(sbSizer3, 1, wx.EXPAND, 5)


        self.SetSizer(bSizer1)
        self.Layout()

    def addConsoleLine(self, line):
        cur = self.npplanServerConsole.GetValue()
        self.npplanServerConsole.SetValue(cur + "\n> " + line)
        self.npplanServerConsole.ScrollLines(cur.count("\n") + 1)

    def __del__(self):
        pass


