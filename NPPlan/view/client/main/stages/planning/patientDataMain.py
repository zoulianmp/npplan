# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 28.04.14
@summary: 
'''

import NPPlan
import wx
from wx import xrc

class patiendDataMainView(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, style = wx.TAB_TRAVERSAL )
        
        
        bSizer11 = wx.BoxSizer( wx.HORIZONTAL )
        
        sbSizer10 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Общая информация" ), wx.VERTICAL )
        
        bSizer3 = wx.BoxSizer( wx.HORIZONTAL )
        
        fgSizer1 = wx.FlexGridSizer( 0, 2, 4, 12 )
        fgSizer1.SetFlexibleDirection( wx.BOTH )
        fgSizer1.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
        
        self.m_staticText6 = wx.StaticText( self, wx.ID_ANY, u"Фамилия", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText6.Wrap( -1 )
        fgSizer1.Add( self.m_staticText6, 0, wx.ALL, 5 )
        
        self.dSurname = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        fgSizer1.Add( self.dSurname, 0, wx.ALL, 5 )
        
        self.m_staticText7 = wx.StaticText( self, wx.ID_ANY, u"Имя", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText7.Wrap( -1 )
        fgSizer1.Add( self.m_staticText7, 0, wx.ALL, 5 )
        
        self.dName = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        fgSizer1.Add( self.dName, 0, wx.ALL, 5 )
        
        self.m_staticText8 = wx.StaticText( self, wx.ID_ANY, u"Отчество", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText8.Wrap( -1 )
        fgSizer1.Add( self.m_staticText8, 0, wx.ALL, 5 )
        
        self.dAltername = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        fgSizer1.Add( self.dAltername, 0, wx.ALL, 5 )
        
        self.m_staticText9 = wx.StaticText( self, wx.ID_ANY, u"Псевдоним", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )
        fgSizer1.Add( self.m_staticText9, 0, wx.ALL, 5 )
        
        self.dAlias = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        fgSizer1.Add( self.dAlias, 0, wx.ALL, 5 )
        
        self.m_staticText10 = wx.StaticText( self, wx.ID_ANY, u"Пол", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText10.Wrap( -1 )
        fgSizer1.Add( self.m_staticText10, 0, wx.ALL, 5 )
        
        dGenderChoices = []
        self.dGender = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 300,-1 ), dGenderChoices, 0 )
        self.dGender.SetSelection( 0 )
        fgSizer1.Add( self.dGender, 0, wx.ALL, 5 )
        
        self.m_staticText11 = wx.StaticText( self, wx.ID_ANY, u"Дата рождения", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText11.Wrap( -1 )
        fgSizer1.Add( self.m_staticText11, 0, wx.ALL, 5 )
        
        self.dBirthDate = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        fgSizer1.Add( self.dBirthDate, 0, wx.ALL, 5 )
        
        self.m_staticText12 = wx.StaticText( self, wx.ID_ANY, u"Место рождения", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText12.Wrap( -1 )
        fgSizer1.Add( self.m_staticText12, 0, wx.ALL, 5 )
        
        self.dBirthPlace = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        fgSizer1.Add( self.dBirthPlace, 0, wx.ALL, 5 )
        
        self.m_staticText13 = wx.StaticText( self, wx.ID_ANY, u"Семейное положение", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText13.Wrap( -1 )
        fgSizer1.Add( self.m_staticText13, 0, wx.ALL, 5 )
        
        dSpouseChoices = []
        self.dSpouse = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 300,-1 ), dSpouseChoices, 0 )
        self.dSpouse.SetSelection( 0 )
        fgSizer1.Add( self.dSpouse, 0, wx.ALL, 5 )
        
        self.dServiceTitle = wx.StaticText( self, wx.ID_ANY, u"Служебные отметки", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.dServiceTitle.Wrap( -1 )
        fgSizer1.Add( self.dServiceTitle, 0, wx.ALL, 5 )
        
        self.dService = wx.richtext.RichTextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,200 ), 0|wx.VSCROLL|wx.HSCROLL|wx.NO_BORDER|wx.WANTS_CHARS )
        fgSizer1.Add( self.dService, 1, wx.EXPAND |wx.ALL, 5 )
        
        
        bSizer3.Add( fgSizer1, 0, wx.EXPAND, 5 )
        
        
        sbSizer10.Add( bSizer3, 1, wx.EXPAND, 5 )
        
        
        bSizer11.Add( sbSizer10, 1, wx.EXPAND, 5 )
        
        sbSizer15 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Информация о проживании" ), wx.VERTICAL )
        
        gSizer3 = wx.GridSizer( 0, 2, 0, 0 )
        
        self.m_staticText16 = wx.StaticText( self, wx.ID_ANY, u"Улица, дом, корпус, квартира", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText16.Wrap( -1 )
        gSizer3.Add( self.m_staticText16, 0, wx.ALL, 5 )
        
        self.dAdressLine = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        gSizer3.Add( self.dAdressLine, 0, wx.ALL, 5 )
        
        self.m_staticText17 = wx.StaticText( self, wx.ID_ANY, u"Город", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText17.Wrap( -1 )
        gSizer3.Add( self.m_staticText17, 0, wx.ALL, 5 )
        
        self.dCity = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        gSizer3.Add( self.dCity, 0, wx.ALL, 5 )
        
        self.m_staticText18 = wx.StaticText( self, wx.ID_ANY, u"Область", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText18.Wrap( -1 )
        gSizer3.Add( self.m_staticText18, 0, wx.ALL, 5 )
        
        self.dRegion = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        gSizer3.Add( self.dRegion, 0, wx.ALL, 5 )
        
        self.m_staticText19 = wx.StaticText( self, wx.ID_ANY, u"Почтовый индекс", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText19.Wrap( -1 )
        gSizer3.Add( self.m_staticText19, 0, wx.ALL, 5 )
        
        self.dPostalCode = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        gSizer3.Add( self.dPostalCode, 0, wx.ALL, 5 )
        
        self.m_staticText20 = wx.StaticText( self, wx.ID_ANY, u"Страна", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText20.Wrap( -1 )
        gSizer3.Add( self.m_staticText20, 0, wx.ALL, 5 )
        
        self.dCountry = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        gSizer3.Add( self.dCountry, 0, wx.ALL, 5 )
        
        self.m_staticText21 = wx.StaticText( self, wx.ID_ANY, u"Телефон", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText21.Wrap( -1 )
        gSizer3.Add( self.m_staticText21, 0, wx.ALL, 5 )
        
        self.dTelephone = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        gSizer3.Add( self.dTelephone, 0, wx.ALL, 5 )
        
        self.m_staticText22 = wx.StaticText( self, wx.ID_ANY, u"E-mail", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText22.Wrap( -1 )
        gSizer3.Add( self.m_staticText22, 0, wx.ALL, 5 )
        
        self.dEmail = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 300,-1 ), 0 )
        gSizer3.Add( self.dEmail, 0, wx.ALL, 5 )
        
        
        gSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        gSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        gSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        gSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        gSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        gSizer3.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
        
        
        sbSizer15.Add( gSizer3, 0, wx.EXPAND, 5 )
        
        
        bSizer11.Add( sbSizer15, 1, wx.EXPAND, 5 )
        
        sbSizer18 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Дополнительно" ), wx.VERTICAL )
        
        bSizer5 = wx.BoxSizer( wx.VERTICAL )
        
        bSizer6 = wx.BoxSizer( wx.VERTICAL )
        
        self.dPhoto = wx.StaticBitmap( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.Size( 150,150 ), 0 )
        bSizer6.Add( self.dPhoto, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
        
        self.dAddPhoto = wx.Button( self, wx.ID_ANY, u"Добавить фото", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
        bSizer6.Add( self.dAddPhoto, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
        
        
        bSizer5.Add( bSizer6, 1, wx.EXPAND, 5 )
        
        bSizer9 = wx.BoxSizer( wx.VERTICAL )
        
        
        bSizer5.Add( bSizer9, 1, wx.EXPAND, 5 )
        
        
        sbSizer18.Add( bSizer5, 1, wx.EXPAND, 5 )
        
        
        bSizer11.Add( sbSizer18, 1, wx.EXPAND, 5 )
        
        
        self.SetSizer( bSizer11 )
        self.Layout()
