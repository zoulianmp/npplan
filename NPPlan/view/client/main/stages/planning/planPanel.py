# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.05.13
@summary: 
'''

import NPPlan
import wx
import wx.gizmos as gizmos
from NPPlan.view.client.main.abstracts.thumbnailer import thumbnailerWithControls
import wx.lib.platebtn as platebtn
try:
    from agw import thumbnailctrl as TC
except ImportError:
    import wx.lib.agw.thumbnailctrl as TC


class planPanelView(wx.Panel):
    def __init__(self, parent, initData={}):
        wx.Panel.__init__(self, parent)

        sz = wx.BoxSizer(wx.VERTICAL)

        lrSz = wx.BoxSizer(wx.HORIZONTAL)
        dataSz = wx.FlexGridSizer(cols=2, hgap=8, vgap=10)
        patLabel = wx.StaticText(self, wx.NewId(), _('Patient'))
        self._patField = wx.TextCtrl(self, wx.NewId(), size=(400, -1))
        studyLabel = wx.StaticText(self, wx.NewId(), _('Study'))
        self._studyField = wx.TextCtrl(self, wx.NewId(), size=(400, -1))
        facLabel = wx.StaticText(self, wx.NewId(), _('Facility'))
        self._facField = wx.TextCtrl(self, wx.NewId(), size=(400, -1))
        dateLabel = wx.StaticText(self, wx.NewId(), _('Date time'))
        self._dateField = wx.TextCtrl(self, wx.NewId(), size=(400, -1))
        folderLabel = wx.StaticText(self, wx.NewId(), _('Date time'))
        self._folderField = wx.TextCtrl(self, wx.NewId(), size=(320, -1))
        self._folderButton = wx.Button(self, wx.NewId(), _('Open dataset'))
        folderSz = wx.BoxSizer(wx.HORIZONTAL)
        folderSz.AddMany([self._folderField, self._folderButton])
        planDescriptionLabel = wx.StaticText(self, wx.NewId(), _('Plan description'))
        self._planDescriptionField = wx.TextCtrl(self, wx.NewId(), size=(400, -1))
        dataSz.AddMany([patLabel, self._patField,
                        studyLabel, self._studyField,
                        facLabel, self._facField,
                        dateLabel, self._dateField,
                        folderLabel, folderSz,
                        (1, 1), (1, 1),
                        planDescriptionLabel, self._planDescriptionField,
                        ])

        self.tree = gizmos.TreeListCtrl(self, -1, style =
                                        wx.TR_DEFAULT_STYLE
                                        #| wx.TR_HAS_BUTTONS
                                        #| wx.TR_TWIST_BUTTONS
                                        #| wx.TR_ROW_LINES
                                        #| wx.TR_COLUMN_LINES
                                        | wx.TR_NO_LINES
                                        | wx.TR_FULL_ROW_HIGHLIGHT,
                                   )
        self.tree.AddColumn(_("Series ID"))
        self.tree.AddColumn(_("Series length"))
        self.tree.AddColumn(_("Series description"))
        self.tree.SetMainColumn(0)
        self.tree.SetColumnWidth(0, 175)
        self.tree.SetSize((600, 400))

        btnSz = wx.BoxSizer(wx.VERTICAL)
        self._goBtn = platebtn.PlateButton(self, wx.ID_ANY, _('Start planning'), style=platebtn.PB_STYLE_DEFAULT)
        btnSz.Add(self._goBtn, flag=wx.CENTER | wx.EXPAND)

        lrSz.Add(dataSz, 5, wx.EXPAND)
        lrSz.Add(self.tree, 5, wx.EXPAND)
        lrSz.Add(btnSz, 1, wx.EXPAND | wx.CENTER)

        self.thumbnailer = emptyReplacement(self)
        #self.informer = emptyReplacement(self)

        #lrbSz = wx.BoxSizer(wx.HORIZONTAL)

        #lrbSz.Add(self.thumbnailer, 6, wx.EXPAND)
        #lrbSz.Add(self.informer, 2, wx.EXPAND)

        sz.Add(lrSz, 1, wx.EXPAND)
        sz.Add(self.thumbnailer, 4, wx.EXPAND)
        self.SetSizer(sz)

    def setThumbnails(self, buttons, data, backend):
        print 'ST', buttons, data, backend
        newThumb = thumbnailerWithControls(self, buttons, data, backend)
        self.GetSizer().Replace(self.thumbnailer, newThumb)
        self.thumbnailer.Destroy()
        del self.thumbnailer
        self.thumbnailer = newThumb
        self.SendSizeEvent()
        self.thumbnailer.Bind(TC.EVT_THUMBNAILS_SEL_CHANGED, backend)


class emptyReplacement(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)










