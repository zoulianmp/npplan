# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 28.04.14
@summary: 
'''

import wx

class patiendDataAdditionalView ( wx.Panel ):
    def __init__( self, parent ):
        wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, style = wx.TAB_TRAVERSAL )
        
        bSizer15 = wx.BoxSizer( wx.HORIZONTAL )
        
        sbSizer19 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Паспортные данные" ), wx.VERTICAL )
        
        bSizer16 = wx.BoxSizer( wx.VERTICAL )
        
        bSizer17 = wx.BoxSizer( wx.HORIZONTAL )
        
        self.m_staticText26 = wx.StaticText( self, wx.ID_ANY, u"Серия", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText26.Wrap( -1 )
        bSizer17.Add( self.m_staticText26, 0, wx.ALL, 5 )
        
        self.m_textCtrl23 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer17.Add( self.m_textCtrl23, 0, wx.ALL, 5 )
        
        self.m_staticText27 = wx.StaticText( self, wx.ID_ANY, u"Номер", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText27.Wrap( -1 )
        bSizer17.Add( self.m_staticText27, 0, wx.ALL, 5 )
        
        self.m_textCtrl24 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
        bSizer17.Add( self.m_textCtrl24, 0, wx.ALL, 5 )
        
        
        bSizer16.Add( bSizer17, 0, wx.EXPAND, 5 )
        
        gSizer5 = wx.GridSizer( 0, 2, 0, 0 )
        
        self.m_staticText28 = wx.StaticText( self, wx.ID_ANY, u"Выдан", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText28.Wrap( -1 )
        gSizer5.Add( self.m_staticText28, 0, wx.ALL, 5 )
        
        self.m_textCtrl25 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 250,-1 ), 0 )
        gSizer5.Add( self.m_textCtrl25, 0, wx.ALL, 5 )
        
        self.m_staticText29 = wx.StaticText( self, wx.ID_ANY, u"Код подразделения", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText29.Wrap( -1 )
        gSizer5.Add( self.m_staticText29, 0, wx.ALL, 5 )
        
        self.m_textCtrl26 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer5.Add( self.m_textCtrl26, 0, wx.ALL, 5 )
        
        self.m_staticText30 = wx.StaticText( self, wx.ID_ANY, u"Дата выдачи", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText30.Wrap( -1 )
        gSizer5.Add( self.m_staticText30, 0, wx.ALL, 5 )
        
        self.m_textCtrl27 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 250,-1 ), 0 )
        gSizer5.Add( self.m_textCtrl27, 0, wx.ALL, 5 )
        
        self.m_staticText31 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText31.Wrap( -1 )
        gSizer5.Add( self.m_staticText31, 0, wx.ALL, 5 )
        
        self.m_textCtrl28 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer5.Add( self.m_textCtrl28, 0, wx.ALL, 5 )
        
        self.m_staticText32 = wx.StaticText( self, wx.ID_ANY, u"MyLabel", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText32.Wrap( -1 )
        gSizer5.Add( self.m_staticText32, 0, wx.ALL, 5 )
        
        self.m_textCtrl29 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer5.Add( self.m_textCtrl29, 0, wx.ALL, 5 )
        
        
        bSizer16.Add( gSizer5, 1, wx.EXPAND, 5 )
        
        
        sbSizer19.Add( bSizer16, 0, wx.EXPAND, 5 )
        
        
        bSizer15.Add( sbSizer19, 1, wx.EXPAND, 5 )
        
        sbSizer20 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Дополнительные данные" ), wx.VERTICAL )
        
        gSizer6 = wx.GridSizer( 0, 2, 0, 0 )
        
        self.m_staticText33 = wx.StaticText( self, wx.ID_ANY, u"СНИЛС", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText33.Wrap( -1 )
        gSizer6.Add( self.m_staticText33, 0, wx.ALL, 5 )
        
        self.m_textCtrl30 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer6.Add( self.m_textCtrl30, 0, wx.ALL, 5 )
        
        self.m_staticText34 = wx.StaticText( self, wx.ID_ANY, u"ИНН", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText34.Wrap( -1 )
        gSizer6.Add( self.m_staticText34, 0, wx.ALL, 5 )
        
        self.m_textCtrl31 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        gSizer6.Add( self.m_textCtrl31, 0, wx.ALL, 5 )
        
        
        sbSizer20.Add( gSizer6, 0, wx.EXPAND, 5 )
        
        
        bSizer15.Add( sbSizer20, 1, wx.EXPAND, 5 )
        
        
        self.SetSizer( bSizer15 )
        self.Layout()
	
