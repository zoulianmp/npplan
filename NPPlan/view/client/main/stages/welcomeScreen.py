# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 13.09.12
@summary: 
'''

import NPPlan
import wx


class welcomeScreen(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        bSizer1 = wx.BoxSizer(wx.VERTICAL)

        bSizer2 = wx.BoxSizer(wx.HORIZONTAL)

        self.m_staticText2 = wx.StaticText(self, wx.ID_ANY,
                                            _("Welcome to NPPlan treatment planning and \n"
                                              "control system ver. 3.0\n(c) MRRC 2010-2014"),
                                            wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        self.m_staticText2.SetFont(wx.Font( 18, 74, 90, 92, False, wx.EmptyString))

        bSizer2.Add(self.m_staticText2, 0, wx.ALIGN_CENTER | wx.ALL, 5)

        bSizer1.Add(bSizer2, 1, wx.ALIGN_CENTER, 5)

        self.SetSizer(bSizer1)
        self.Layout()