# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 05.10.12
@summary: 
'''

import NPPlan

import wx
import wx.lib.buttons as buttons
from treeCtrl import DICOMTree

class dicomTree(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.NewId())
        iconPath = NPPlan.getIconPath()
        self._controller = None

        self.btnIds = {
            'BTN_ADD_FOLDER' : wx.NewId(),
            'BTN_ADD_NETWORK' : wx.NewId(),
            'BTN_ADD_CD' : wx.NewId(),
        }
        self.btnTexts = {
            'BTN_ADD_FOLDER' : _('Import folder'),
            'BTN_ADD_NETWORK' :_('Import network'),
            'BTN_ADD_CD' : _('Import CD'),

        }
        self.btnBitmaps = {
            'BTN_ADD_FOLDER' : '%s%s'%(iconPath, 'iconDicomFolder32.png'),
            'BTN_ADD_NETWORK' : '%s%s'%(iconPath, 'iconDicomNetwork32.png'),
            'BTN_ADD_CD' : '%s%s'%(iconPath, 'iconDicomCd32.png'),
        }

        btnBox = wx.StaticBox(self, -1, _('Choose source'))
        btnSizer = wx.StaticBoxSizer(btnBox, wx.HORIZONTAL)
        for i in self.btnIds.keys():
            btn = buttons.GenBitmapTextButton(self, self.btnIds[i], label=self.btnTexts[i], bitmap=wx.Bitmap(self.btnBitmaps[i], wx.BITMAP_TYPE_PNG))
            btnSizer.Add(btn)

        self.tlcChoices = wx.Choice(self, wx.NewId())
        #self.tlcChoices.Append()

        self.tlcTreeView = DICOMTree(self, wx.NewId())
        #self.tlcTreeView.SetSize((640, 400))
        self.tlcTreeView.DeleteAllItems()

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(btnSizer)
        sizer.Add(self.tlcChoices, 0, wx.EXPAND)
        sizer.Add(self.tlcTreeView, 10, wx.EXPAND|wx.ALIGN_CENTER)
        self.SetSizerAndFit(sizer)
        self.Layout()

    def runDialog(self, callback):
        dlg = wx.DirDialog(self, _("utils.dicomTree.Choose a directory:"),
                          style=wx.DD_DEFAULT_STYLE
                           )

        if dlg.ShowModal() == wx.ID_OK:
            #self.log.WriteText('You selected: %s\n' % dlg.GetPath())
            callback(dlg.GetPath())
            pass

        dlg.Destroy()

    def addChoices(self, choices):
        self.tlcChoices.Clear()
        for i in choices:
            self.tlcChoices.Append(i['text'], i)


    def getTreeView(self):
        return self.tlcTreeView