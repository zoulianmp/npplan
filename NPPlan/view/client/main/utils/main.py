# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 30.04.14
@summary: 
'''

import NPPlan
import wx


class utilsView(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self._wxEbtViewer = wx.CommandLinkButton(self, -1,
                                   _("EBT Viewer"),
                                   _("""\
                                   Run wxEbtViewer utility for 1D and 3D dose distribution obtained
                                   from EBT-films during Protvino experiment
                                    ."""),
        )
        self._wxEbtViewer.SetSize((400, 250))
        self._dicomViewer = wx.CommandLinkButton(self, -1,
                                   _("DICOM Viewer"),
                                   _("""\
                                   Run DICOM tree viewer
                                    ."""),
        )
        self._dicomViewer.SetSize((400, 250))
        sizer.Add(self._wxEbtViewer, 0, wx.ALIGN_CENTER | wx.ALL, 4)
        sizer.Add(self._dicomViewer, 0, wx.ALIGN_CENTER | wx.ALL, 4)
        self.SetSizer(sizer)
        pass

