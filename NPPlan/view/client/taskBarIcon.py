# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 10.09.12
@summary: 
'''

import wx
from NPPlan.view.client import *

class taskBar(wx.TaskBarIcon):
    def __init__(self, frame, icon, tooltip):
        super(taskBar, self).__init__()
        self.frame = frame
        self.SetIcon(icon, tooltip)
        self.Bind(wx.EVT_TASKBAR_LEFT_DOWN, self.onLeftDown)

    def onLeftDown(self, event):
        print 'aaa'

    def CreatePopupMenu(self):
        # @todo: формировать меню динамическим способом из контроллера
        menu = wx.Menu()
        createMenuItem(menu, 'Say Hello', self.onHello)
        menu.AppendSeparator()
        createMenuItem(menu, 'Exit', self.onExit)
        return menu

    def onHello(self, event):
        print 'wrapper'

    def onExit(self, event):
        # @todo: использовать общую точку выхода в контроллере
        wx.CallAfter(self.Destroy)
        wx.CallAfter(wx.GetApp().GetTopWindow().Destroy())