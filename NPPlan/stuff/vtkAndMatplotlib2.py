# -*- coding: utf8 -*-
'''
small example
based on original code: http://sda.iu.edu/matplot.html  (mplvtk)

base: vtk

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 03.03.14
@summary: 
'''


import vtk
import matplotlib
import pylab

#create a Sphere
sphereSource = vtk.vtkSphereSource()
sphereSource.SetCenter(0.0, 0.0, 0.0)
sphereSource.SetRadius(0.5)

#create a mapper
sphereMapper = vtk.vtkPolyDataMapper()
sphereMapper.SetInputConnection(sphereSource.GetOutputPort())

#create an actor
sphereActor = vtk.vtkActor()
sphereActor.SetMapper(sphereMapper)

#a renderer and render window
renderer = vtk.vtkRenderer()
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)

#an interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)

#add the actors to the scene
renderer.AddActor(sphereActor)
renderer.SetBackground(.1,.2,.3) # Background dark blue

transform = vtk.vtkTransform()
transform.Translate(1.0, 0.0, 0.0)

renderer.ResetCamera()
renderWindow.OffScreenRenderingOn()
renderWindow.Render()

vtkRGB = vtk.vtkUnsignedCharArray()

im = pylab.zeros((512,512,3))

def vtkImage():
    renderWindow.Render()
    renderWindow.GetPixelData(0,0,511,511, 1,vtkRGB)
#  vtkRGB.Squeeze()
    idx=0
    for iy in range(512-1,-1,-1):
        for ix in range(512):
            im[iy,ix,0] = vtkRGB.GetValue(idx) / 255.
            im[iy,ix,1] = vtkRGB.GetValue(idx+1) / 255.
            im[iy,ix,2] = vtkRGB.GetValue(idx+2) / 255.
            idx += 3

vtkImage()
print im
pylab.imshow(im, None)
pylab.show()

# begin mouse interaction
#renderWindowInteractor.Start()