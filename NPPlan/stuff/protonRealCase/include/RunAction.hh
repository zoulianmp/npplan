#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include <map>

struct SlayerRA
{
	G4double depEnergy, depEnergy2, depEnergyError, dose, dose2, doseError;
	G4int nEvents;
};

class G4Run;
class RunAction : public G4UserRunAction
{
  public:
    RunAction();
    virtual ~RunAction();

    virtual void BeginOfRunAction(const G4Run* run);
    virtual void   EndOfRunAction(const G4Run* run);

    void addData(SlayerRA***, G4int, G4int, G4int, G4double, G4double);

    //void addAllDep(G4int, G4int, G4int, G4double, G4double);
    void addAllDep(G4int, G4int, G4int, G4double, G4double, G4double, G4double, G4int);

    void addParticleDep(G4String, // pName
      G4int, G4int, G4int,  // pos
      G4double, G4double,   // ene, ene2
      G4double, G4double,   // dose, dose2
      G4int                 // NoE
      );

    void addParticleDep(SlayerRA ***, // target
      G4int, G4int, G4int,  // pos
      G4double, G4double,   // ene, ene2
      G4double, G4double,   // dose, dose2
      G4int                 // NoE
      );

    void dumpDep(SlayerRA ***, G4String);

  private:
    void prepareError(SlayerRA ***target);
    SlayerRA*** makeZero();
    G4int voxelQuantity;
    SlayerRA ***depAll;
    std::map<G4String, SlayerRA***> depFParticlesRA;
    SlayerRA ***depProtons;
    void setupSummator();
    void zeroSummator();
};

#endif