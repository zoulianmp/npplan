#ifndef DetectorSD_h
#define DetectorSD_h 1

#include "G4VSensitiveDetector.hh"
#include <map>
class G4Step;
class RunAction;
//class DetectorSDMessenger;

struct Slayer
{
	G4double depEnergy, depEnergy2;
  G4double depEnergyError;
  G4double dose, dose2;
  G4double doseError;
	G4int nEvents;
};

class DetectorSD: public G4VSensitiveDetector 
{
  public:
    DetectorSD(G4String, G4ThreeVector, G4int);
    ~DetectorSD();
    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);

    G4String outFileNameAll;
    G4String outFileNameProtons;
    G4int voxelQuantity;
    G4double voxelSizeX;
    G4double voxelSizeY;
    G4double voxelSizeZ;

    // by definition
    //void addParticle2Store(G4ParticleDefinition*);
    // by string
    bool addParticle2Store(G4String);
    void addParticlesData(G4String, G4int, G4int, G4int, G4double, G4double);
    // by Z A
    //void addParticle2Store(G4int, G4int);

  private:

    Slayer ***depAll;
    std::map<G4String, Slayer***> depFParticles;
    //std::map<G4ParticleDefinition*, Slayer***> depFParticles;
    Slayer ***depProtons;

    RunAction* runAction;
    //void addData(target, ix, iy, iz, ene, dose);
    void addData(Slayer***, G4int, G4int, G4int, G4double, G4double);
    void setupSummator();
    void zeroSummator();
    void prepareError(Slayer***);
    std::ofstream fileoutPart;
};


#endif