#include "RunAction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
//#include "G4VUserDetectorConstruction.hh"
#include "DetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

RunAction::RunAction()
 : G4UserRunAction()
{ 
  G4RunManager::GetRunManager()->SetPrintProgress(10000);
  //G4RunManager::GetRunManager()->GetCurrentEvent();
  //G4RunManager::GetRunManager()->GetNumberOfEventsToBeProcessed();

  //G4RunManager::GetRunManager()->SetRandomNumberStorePerEvent
  //voxelQuantity = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->voxelQuantity;

  //setupSummator();
  //voxelQuantity = DetectorConstruction::voxelQuantity;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{

}

void RunAction::setupSummator() {
  depAll = new SlayerRA**[voxelQuantity];
  depProtons = new SlayerRA**[voxelQuantity];
  for(int ix = 0; ix < voxelQuantity; ix++) {
    depAll[ix] = new SlayerRA*[voxelQuantity];
    depProtons[ix] = new SlayerRA*[voxelQuantity];
    for(int iy = 0; iy < voxelQuantity; iy++) {
      depAll[ix][iy] = new SlayerRA[voxelQuantity];
      depProtons[ix][iy] = new SlayerRA[voxelQuantity];
    }
  }
  zeroSummator();
}

void RunAction::zeroSummator() {
   for(int ix = 0; ix < voxelQuantity; ix++) {
    for(int iy = 0; iy < voxelQuantity; iy++) {
      for(int iz = 0; iz < voxelQuantity; iz++) {
        depAll[ix][iy][iz].depEnergy = 0;
        depAll[ix][iy][iz].depEnergy2 = 0;
        depAll[ix][iy][iz].depEnergyError = 0;
        depAll[ix][iy][iz].nEvents = 0;
        depAll[ix][iy][iz].dose = 0;
        depAll[ix][iy][iz].dose2 = 0;
        depAll[ix][iy][iz].doseError = 0;

        depProtons[ix][iy][iz].depEnergy = 0;
        depProtons[ix][iy][iz].depEnergy2 = 0;
        depProtons[ix][iy][iz].depEnergyError = 0;
        depProtons[ix][iy][iz].nEvents = 0;
        depProtons[ix][iy][iz].dose = 0;
        depProtons[ix][iy][iz].dose2 = 0;
        depProtons[ix][iy][iz].doseError = 0;
      }
    }
  }
}

void RunAction::addParticleDep(G4String pName, // pName
      G4int x, G4int y, G4int z,  // pos
      G4double ene, G4double ene2,   // ene, ene2
      G4double dose, G4double dose2,   // dose, dose2
      G4int NoE                // NoE
      ) {
  if (depFParticlesRA.count(pName) == 0) {
    SlayerRA ***t;
    t = makeZero();
    addParticleDep(t, x, y, z, ene, ene2, dose, dose2, NoE);
    depFParticlesRA.insert(std::pair<G4String, SlayerRA***>(pName, t));
  } else {
    addParticleDep(depFParticlesRA.find(pName)->second, x, y, z, ene, ene2, dose, dose2, NoE);
  }
}

void RunAction::addParticleDep(SlayerRA ***target,
      G4int x, G4int y, G4int z,  // pos
      G4double ene, G4double ene2,   // ene, ene2
      G4double dose, G4double dose2,   // dose, dose2
      G4int NoE                // NoE
      ) {
  target[x][y][z].depEnergy += ene;
  target[x][y][z].depEnergy2 += ene2;
  target[x][y][z].dose += dose;
  target[x][y][z].dose2 += dose2;
  target[x][y][z].nEvents += NoE;
}

SlayerRA*** RunAction::makeZero() {
  SlayerRA ***t = new SlayerRA**[voxelQuantity];
  for(int ix = 0; ix < voxelQuantity; ix++) {
    t[ix] = new SlayerRA*[voxelQuantity];
    for(int iy = 0; iy < voxelQuantity; iy++) {
      t[ix][iy] = new SlayerRA[voxelQuantity];
      for(int iz = 0; iz < voxelQuantity; iz++) {
        t[ix][iy][iz].depEnergy = 0.0;
        t[ix][iy][iz].depEnergy2 = 0.0;
        t[ix][iy][iz].depEnergyError = 0.0;
        t[ix][iy][iz].dose = 0.0;
        t[ix][iy][iz].dose2 = 0.0;
        t[ix][iy][iz].doseError = 0.0;
        t[ix][iy][iz].nEvents = 0;
      }
    }
  }

  return t;
}

void RunAction::prepareError(SlayerRA ***target) {
  G4int n;
  G4double dd, d2;
  G4double ddDo, d2Do;
  G4double v, vDo;
  for(int ix = 0; ix < voxelQuantity; ix++) {
    for(int iy = 0; iy < voxelQuantity; iy++) {
      for(int iz = 0; iz < voxelQuantity; iz++) {
        n=target[ix][iy][iz].nEvents;
        dd=target[ix][iy][iz].depEnergy*target[ix][iy][iz].depEnergy;
			  d2=target[ix][iy][iz].depEnergy2;
        ddDo=target[ix][iy][iz].dose*target[ix][iy][iz].dose;
			  d2Do=target[ix][iy][iz].dose2;

        v=n*d2-dd;
        vDo = n*d2Do-ddDo;

        if (n>1) {
          target[ix][iy][iz].depEnergyError=1.0*std::sqrt(v/(n-1));
          target[ix][iy][iz].doseError = 1.0*std::sqrt(vDo/(n-1));
        }
      }
    }
  }
}

void RunAction::addData(SlayerRA*** target, G4int x, G4int y, G4int z, G4double ene, G4double dose) {
  target[x][y][z].nEvents += 1;
  target[x][y][z].depEnergy += ene;
  target[x][y][z].depEnergy2 += ene*ene;
  target[x][y][z].dose += dose;
  target[x][y][z].dose2 += dose*dose;
}

void RunAction::addAllDep(G4int x, G4int y, G4int z, G4double ene, G4double ene2,
                          G4double dose, G4double dose2, G4int NoE) {
  //addData(depAll, x, y, z, ene, dose);
  addParticleDep(depAll, x, y, z, ene, ene2, dose, dose2, NoE);
}

void RunAction::BeginOfRunAction(const G4Run*)
{ 
  voxelQuantity = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->voxelQuantity;
  setupSummator();  
}

void RunAction::dumpDep(SlayerRA ***target, G4String filename) {
  prepareError(target);

  std::ofstream fileoutPart;
  //G4String outFileNameAll(filename);
  fileoutPart.open(filename);
  fileoutPart<<"x "<<" \t";
  fileoutPart<<"y "<<" \t";
  fileoutPart<<"z "<<" \t";
  fileoutPart<<"Dep energy, MeV "<<" \t";
  fileoutPart<<"Energy error, MeV "<<" \t";
  fileoutPart<<"Dose, Gy "<<" \t";
  fileoutPart<<"Dose error, Gy "<<" \t";
  fileoutPart<<"Events ";
  fileoutPart<<" \n";
  //fileoutPart<<"\n";
  fileoutPart<<std::defaultfloat;

  G4double sumEn, sumDo, sumNoE;

  sumEn = sumDo = sumNoE = 0;
  for(int ix = 0; ix < voxelQuantity; ix++) {
    for(int iy = 0; iy < voxelQuantity; iy++) {
      for(int iz = 0; iz < voxelQuantity; iz++) {
        fileoutPart<<ix<<" \t";
        fileoutPart<<iy<<" \t";
        fileoutPart<<iz<<" \t";

        fileoutPart<<G4BestUnit(target[ix][iy][iz].depEnergy, "Energy")<<" \t";
        fileoutPart<<G4BestUnit(target[ix][iy][iz].depEnergyError, "Energy")<<" \t";
        fileoutPart<<G4BestUnit(target[ix][iy][iz].dose, "Dose")<<" \t";
        fileoutPart<<G4BestUnit(target[ix][iy][iz].doseError, "Dose")<<" \t";
        fileoutPart<<target[ix][iy][iz].nEvents<<" \n";
        sumEn += target[ix][iy][iz].depEnergy;
        sumDo += target[ix][iy][iz].dose;
        sumNoE += target[ix][iy][iz].nEvents;
      }
    }
  }
  fileoutPart<<"Summary: \t";
  fileoutPart<<G4BestUnit(sumEn, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(sumDo, "Dose")<<" \t";
  fileoutPart<<sumNoE<<" \n";
  fileoutPart.close();
}

void RunAction::EndOfRunAction(const G4Run* )
{
  dumpDep(depAll, "RAAllDep.out");
  for (std::map<G4String, SlayerRA***>::iterator it = depFParticlesRA.begin();
				it != depFParticlesRA.end();
				++it) {
      dumpDep(it->second, G4String("RA")+it->first+G4String("Dep.out"));
  }
}