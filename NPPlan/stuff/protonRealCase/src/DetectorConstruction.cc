/*
 * Base file for geant projects
 * Detector construction implementation
 * @author: mrxak
 * (c) MRRC, 2013
*/

#include "globals.hh"
#include "DetectorConstruction.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4PVParameterised.hh"
#include "G4PVDivision.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4PSCellFlux.hh"
#include "G4PSTrackCounter.hh"
#include "G4PSTrackLength.hh"
#include "G4PSDoseDeposit.hh"
#include "G4SDManager.hh"
#include "DetectorSD.hh"
#include "G4ProductionCuts.hh"

#include "PhantomParameterisation.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

//class DetectorMessenger;

//#define hasBone 1

DetectorConstruction::DetectorConstruction()

{
  //fMessenger = new DetectorMessenger(this);
}

DetectorConstruction::~DetectorConstruction() {

}


G4VPhysicalVolume* DetectorConstruction::Construct() {
  G4NistManager* nistManager = G4NistManager::Instance();
  nistManager->FindOrBuildMaterial("G4_AIR");
  nistManager->FindOrBuildMaterial("G4_WATER");

  G4Material* air  = G4Material::GetMaterial("G4_AIR");
  G4Material* water  = G4Material::GetMaterial("G4_WATER");
  G4Material *WATER = G4NistManager::Instance()->FindOrBuildMaterial("G4_WATER");

  G4double worldXDimension = 0.8*m;
  G4double worldYDimension = 0.8*m;
  G4double worldZDimension = 0.8*m;

  G4Box *worldBox = new G4Box( "WorldSolid",
                             worldXDimension,
                             worldYDimension,
                             worldZDimension );

  G4LogicalVolume *worldLogic = new G4LogicalVolume( worldBox,
                                       air,
                                       "WorldLogical",
                                       0, 0, 0 );

  fPBox = new G4PVPlacement( 0,
                                    G4ThreeVector(0,0,0),
                                    "World",
                                    worldLogic,
                                    0,
                                    false,
                                    0 );

	/*G4Region *regVolOut;
	regVolOut = new G4Region("WaterPhantomR");
	G4ProductionCuts* cutsOut = new G4ProductionCuts;
	cutsOut->SetProductionCut(5*cm);
	regVolOut->SetProductionCuts(cutsOut);
  worldLogic->SetRegion(regVolOut);
  regVolOut->AddRootLogicalVolume(worldLogic);*/

	G4Box *fullWaterBox = new G4Box("bigWaterBox", 10*cm, 8*cm, 12*cm);
  G4LogicalVolume *fullWaterLV = new G4LogicalVolume(fullWaterBox, WATER, "fullWaterLV", 0, 0, 0);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), "fullWaterPV", fullWaterLV, fPBox, false, 0);

  G4double fVoxelHalfDimX, fVoxelHalfDimY, fVoxelHalfDimZ;
  //fVoxelHalfDimX = fVoxelHalfDimY = fVoxelHalfDimZ = 0.5 * mm;
  // @todo: move voxels parameters to runfile
  fVoxelHalfDimX = fVoxelHalfDimY = fVoxelHalfDimZ = 2.5 * mm;
  G4double fNVoxelX, fNVoxelY, fNVoxelZ;
  fNVoxelX = fNVoxelY = fNVoxelZ = 20;

	G4Box *fullWaterPhantomBox = new G4Box("fullWaterPhantomBox", fVoxelHalfDimX * fNVoxelX,
    fVoxelHalfDimY * fNVoxelY, 
    fVoxelHalfDimZ * fNVoxelZ);
	G4LogicalVolume *fullWaterPhantomLV = new G4LogicalVolume(fullWaterPhantomBox, WATER, "fullWaterPhantomLV", 0, 0, 0);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), fullWaterPhantomLV, "fullWaterPhantomPV", fullWaterLV, false, 0);
//new G4PVPlacement(0, G4ThreeVector(0, 0, 0), "fullWaterPhantomPV", fullWaterPhantomLV, fullWaterLV, false, 0);
  /**/

	G4Region *regVol;
	regVol= new G4Region("WaterPhantomR");
	G4ProductionCuts* cuts = new G4ProductionCuts;
	cuts->SetProductionCut(0.5*mm);
	regVol->SetProductionCuts(cuts);
  fullWaterPhantomLV->SetRegion(regVol);
  regVol->AddRootLogicalVolume(fullWaterPhantomLV);

    G4String yRepName("RepY");
    G4VSolid* solYRep = new G4Box(yRepName,fNVoxelX*fVoxelHalfDimX,
                                  fVoxelHalfDimY,
                  fNVoxelZ*fVoxelHalfDimZ);
    G4LogicalVolume* logYRep = new G4LogicalVolume(solYRep,WATER,yRepName);
    new G4PVReplica(yRepName,logYRep,fullWaterPhantomLV,kYAxis,
    fNVoxelY,fVoxelHalfDimY*2.); 


    //--- X Slice
    G4String xRepName("RepX");
    G4VSolid* solXRep = new G4Box(xRepName,fVoxelHalfDimX,fVoxelHalfDimY,
                                  fNVoxelZ*fVoxelHalfDimZ);
    G4LogicalVolume* logXRep = new G4LogicalVolume(solXRep,WATER,xRepName);
    new G4PVReplica(xRepName,logXRep,logYRep,kXAxis,fNVoxelX,fVoxelHalfDimX*2.);
    
    //----- Voxel solid and logical volumes
    //--- Z Slice
    G4VSolid* solVoxel = new G4Box("phantom",fVoxelHalfDimX,
    fVoxelHalfDimY,fVoxelHalfDimZ);
    G4LogicalVolume* logicVoxel = new G4LogicalVolume(solVoxel,WATER,"phantom");


    //
    // Parameterisation for transformation of voxels.
    //  (voxel size is fixed in this example.
    //    e.g. nested parameterisation handles material 
    //    and transfomation of voxels.)
    G4ThreeVector voxelSize(fVoxelHalfDimX,fVoxelHalfDimY,fVoxelHalfDimZ);
    PhantomParameterisation* param =
    new PhantomParameterisation(voxelSize, fNVoxelX, fNVoxelY, fNVoxelZ );
    
    new G4PVParameterised("phantom",    // their name
                          logicVoxel, // their logical volume
                          logXRep,      // Mother logical volume
                          kZAxis,       // Are placed along this axis
                          //kUndefined,
                          // Are placed along this axis
                          fNVoxelZ,      // Number of cells
                          param);       // Parameterisation.

                          
     

    //param->SetMaterialIndices( fMateIDs );
    //param->SetNoVoxel( fNVoxelX, fNVoxelY, fNVoxelZ );

  /*G4Box *bx = new G4Box("bx", 0.5*mm, 10*cm, 10*cm);
  G4Box *by = new G4Box("by", 0.5*mm, 0.5*mm, 10*cm);
  G4Box *bz = new G4Box("bz", 0.5*mm, 0.5*mm, 0.5*mm);

  G4LogicalVolume *bxLV = new G4LogicalVolume(bx, WATER, "bxLV", 0, 0, 0);
  G4LogicalVolume *byLV = new G4LogicalVolume(by, WATER, "bxLV", 0, 0, 0);
  G4LogicalVolume *bzLV = new G4LogicalVolume(bz, WATER, "bxLV", 0, 0, 0);

    new G4PVReplica("lrX", bxLV, 
                 fullWaterPhantomLV, kXAxis, 200, 1*mm);

    new G4PVReplica("lrY", byLV, 
                 bxLV, kYAxis, 200, 1*mm);

    new G4PVReplica("lrZ", bzLV, 
                 byLV, kZAxis, 200, 1*mm);
                 */
    

  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSD* sd = new DetectorSD("zz", voxelSize, fNVoxelX);  // @todo: 3D voxel counter
  sd->addParticle2Store("gamma");
  sd->addParticle2Store("proton");
  sd->addParticle2Store("0proton");
  sd->addParticle2Store("alpha");
  sdMan->AddNewDetector(sd);
  logicVoxel->SetSensitiveDetector(sd);/**/

  voxelQuantity = fNVoxelX;
  //DetectorConstruction::voxelQuantity = fNVoxelX;
  
  return fPBox;
}
