#include "EventAction.hh"

#include "G4Event.hh"
#include "G4UnitsTable.hh"

EventAction::EventAction()
:G4UserEventAction()
{

}

EventAction::~EventAction()
{
}

void EventAction::BeginOfEventAction(const G4Event* evt)
{
  //G4cout<<evt->GetEventID()<<G4endl;
  // @todo: rework
  G4cout<<"Progress: "
    <<100.0*static_cast<float>(evt->GetEventID())/static_cast<float>(G4RunManager::GetRunManager()->GetNumberOfEventsToBeProcessed())<<"%\r";
}

void EventAction::EndOfEventAction(const G4Event*)
{
}



