import vtk

dicompath = "C:\\Users\\Chernukha\\Pictures\\DICOM\\01111140\\"

reader = vtk.vtkDICOMImageReader()
reader.SetDirectoryName(dicompath)
reader.Update()

shrink = vtk.vtkImageShrink3D()
shrink.SetShrinkFactors(4, 4, 1)
shrink.SetInput(reader.GetOutput())

skinExtractor = vtk.vtkContourFilter()
skinExtractor.SetInput(shrink.GetOutput())
#skinExtractor.GenerateValues(self.sk1, self.skL, self.skR)
skinExtractor.SetValue(1, -300)
skinExtractor.ComputeGradientsOn()
skinNormals = vtk.vtkPolyDataNormals()
skinNormals.SetInput(skinExtractor.GetOutput())
skinNormals.SetFeatureAngle(60.0)
skinMapper = vtk.vtkPolyDataMapper()
skinMapper.SetInput(skinNormals.GetOutput())
skinMapper.ScalarVisibilityOff()
skin = vtk.vtkActor()
skin.SetMapper(skinMapper)

writer = vtk.vtkSTLWriter()
writer.SetInputConnection(skinNormals.GetOutputPort())
writer.SetFileName('test.stl')
writer.SetFileTypeToBinary()
writer.Write()

# vtkSmartPointer<vtkSTLReader> reader =
#    vtkSmartPointer<vtkSTLReader>::New();
#  reader->SetFileName(filename.c_str());
#  reader->Update();
readers = vtk.vtkSTLReader()
readers.SetFileName('test.stl')
readers.Update()

raw_input()
