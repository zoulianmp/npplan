#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include <map>
#include <vector>
#include <ctime>


struct SlayerRA
{
	G4double depEnergy, depEnergy2, depEnergyError, dose, dose2, doseError;
	G4int nEvents;
  G4double cellFlux, cellFlux2;
  SlayerRA();
};

typedef std::map<G4String, SlayerRA*> parte;
typedef std::pair<G4String, G4String> ppr;

class G4Run;
class RunAction : public G4UserRunAction
{
  public:
    RunAction();
    RunAction(bool);
    virtual ~RunAction();

    virtual void BeginOfRunAction(const G4Run* run);
    virtual void   EndOfRunAction(const G4Run* run);
    void addDetData(G4String, G4double, G4double, G4double, G4double, G4int, G4double);
    bool checkHasBestSolution();
    void addDetParticleData(G4String, G4String, G4double, G4double, G4double, G4double, G4int, G4double);
  private:

    bool useContinue;

    std::map<G4String, SlayerRA> depToDets;
    SlayerRA makeZero(SlayerRA);

    SlayerRA prepareError(SlayerRA target);
    void prepareErrorS(SlayerRA *target);
    void dumpDep(SlayerRA target, G4String filename);
    void dumpDep(G4String particle, SlayerRA *target, G4String filename);

    void dumpStruct2File(SlayerRA, G4String);
    void dumpStruct2File(SlayerRA*, G4String);
    SlayerRA readStructFromFile(SlayerRA, G4String);
    void readStructParticlesFromFile(G4String, G4String);
    void dumpList(std::vector<ppr>);
    void readList();


    G4int previousRuns;
    void dumpNoE();
    void readNoE();

    std::map<ppr, SlayerRA*> dets2Energies;

	  clock_t beginAt;
	  clock_t finishedAt;
};

#endif