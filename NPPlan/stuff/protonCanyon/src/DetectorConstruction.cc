#include "DetectorConstruction.hh"

#include "DetectorSDSingle.hh"
#include "DetectorSDSinglePart.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Orb.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4RotationMatrix.hh"
#include "G4NistManager.hh"
#include "globals.hh"
#include "G4VisAttributes.hh" 
#include "G4SDManager.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

DetectorConstruction::DetectorConstruction() {}

DetectorConstruction::~DetectorConstruction() {}

// ������ ��� ����������� ������ �������
#define SIZE(x) sizeof(x)/sizeof(*x)


G4VPhysicalVolume* DetectorConstruction::Construct()
{
    G4NistManager* nistMan = G4NistManager::Instance();
  G4Material* air = nistMan->FindOrBuildMaterial("G4_AIR");
  //G4Material* plex = nistMan->FindOrBuildMaterial("G4_PLEXIGLASS");
  //G4Material* polystyrene = nistMan->FindOrBuildMaterial("G4_POLYSTYRENE");
  //G4Material* cells = nistMan->FindOrBuildMaterial("G4_MUSCLE_SKELETAL_ICRP");
  G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
  G4Material* concrete = nistMan->FindOrBuildMaterial("G4_CONCRETE");
  //G4Material* Fe2O3 = nistMan->FindOrBuildMaterial("G4_FERRIC_OXIDE");
  //G4Material* Al2O3 = nistMan->FindOrBuildMaterial("G4_ALUMINUM_OXIDE");
  G4Material* tissue = nistMan->FindOrBuildMaterial("G4_TISSUE_SOFT_ICRP");
  
  
 G4Element* elH
= new G4Element("Hydrogen", "H" , 1., 1.01*g/mole);
 G4Element* elC
= new G4Element("Carbon", "C" , 6., 12.01*g/mole);
 G4Element* elN
= new G4Element("Nitrogen", "N" , 7., 14.01*g/mole);
 G4Element* elO
= new G4Element("Oxygen", "O" , 8., 16.00*g/mole);
 G4Element* elLi
= new G4Element("Lithium", "Li" , 3., 6.94*g/mole);
 G4Element* elCl
= new G4Element("Chlorine", "Cl" , 17., 35.45*g/mole);
 G4Element* elK
= new G4Element("Potassium", "K", 19., 39.10*g/mole);
 G4Element* elBr
= new G4Element("Bromine", "Br", 35., 79.90*g/mole);
 G4Element* elCa 
= new G4Element("Calcium", "Ca", 20.0, 40.078*g/mole);
  G4Element* elFe 
= new G4Element("Iron", "Fe", 26, 56.845*g/mole);
 G4Element* elAl
= new G4Element("Aluminium", "Al", 13, 26.98*g/mole);
  G4Element* elSi 
= new G4Element("Silicium", "Si", 14, 28.086*g/mole);

/*G4Material* lime = new G4Material("Lime", 2.211*g/cm3, 3);
  lime->AddElement(elCa, 0.4);
  lime->AddElement(elC, 0.12);
  lime->AddElement(elO, 0.48);
  
G4Material* sand = new G4Material("Sand", 2.648*g/cm3, 2);
  sand->AddElement(elSi, 0.467);
  sand->AddElement(elO, 0.533);

G4Material* brick = new G4Material("Brick", 2.1*g/cm3, 5);
  brick->AddMaterial(sand, 0.51);
  brick->AddMaterial(Al2O3, 0.3);
  brick->AddMaterial(water, 0.11);
  brick->AddMaterial(lime, 0.03);
  brick->AddMaterial(Fe2O3, 0.05);
*/

  G4double world_x_length = 20*m;
  G4double world_y_length = 12*m;
  G4double world_z_length = 20*m;
  
  G4double canyon_concrete_x_length = 11.33*m;
  G4double canyon_concrete_y_length = 8.3*m;
  G4double canyon_concrete_z_length = 14.255*m;

  G4double canyon_air_x_length = canyon_concrete_x_length - 2*m;
  G4double canyon_air_y_length = 6.3*m;
  G4double canyon_air_z_length = canyon_concrete_z_length - 2*m;

  G4double door_x_length = 1*m;
  G4double door_y_length = 2*m;
  G4double door_z_length = 1.21*m;

  G4double door_x_place = -0.5*(canyon_concrete_x_length - door_x_length);
  G4double door_y_place = -0.5*(canyon_air_y_length - door_y_length);
  G4double door_z_place = 0.5*(canyon_air_z_length - door_z_length);

  //G4double concrete_z_length = 0.5*m;
  //G4double air_detector_z_length = 0.25*m;
  //G4double air_detector_z = -(0.5*concrete_z_length + 0.5*air_detector_z_length);
  
  G4Box* world_box = new G4Box("world", 0.5*world_x_length, 0.5*world_y_length, 0.5*world_z_length);
  G4LogicalVolume* world_log = new G4LogicalVolume(world_box, air, "world");
  G4VPhysicalVolume* world_phys = new G4PVPlacement(0, G4ThreeVector(), world_log, "world", 0, false, 0);

  G4Box* container = new G4Box("container", 0.5*world_x_length, 0.5*world_y_length, 0.5*world_z_length);
  G4LogicalVolume* container_log = new G4LogicalVolume(container, air, "container");
  G4VPhysicalVolume* container_phys = new G4PVPlacement(0, G4ThreeVector(), container_log, "container", world_log, false, 0);
  
  G4Box* canyon_concrete_box = new G4Box("concrete", 0.5*canyon_concrete_x_length, 0.5*canyon_concrete_y_length, 0.5*canyon_concrete_z_length);
  G4LogicalVolume* canyon_concrete_log = new G4LogicalVolume(canyon_concrete_box, concrete, "concrete");// !
  G4VPhysicalVolume* canyon_concrete_phys = new G4PVPlacement(0, G4ThreeVector(0,0,0), canyon_concrete_log, "concrete", container_log, false, 0);

  G4Box* canyon_air_box = new G4Box("canyons_air", 0.5*canyon_air_x_length, 0.5*canyon_air_y_length, 0.5*canyon_air_z_length);
  G4LogicalVolume* canyon_air_log = new G4LogicalVolume(canyon_air_box, air, "air");
  G4VPhysicalVolume* canyon_air_phys = new G4PVPlacement(0, G4ThreeVector(0*m,0,0), canyon_air_log, "air", canyon_concrete_log, false, 0);
  /*
  G4Box* shield_wall2 = new G4Box("shield_wall2", 0.5*m, 1*m, 1.225*m);
  G4LogicalVolume* shield_wall2_log = new G4LogicalVolume(shield_wall2, concrete, "shield_wall2");
  G4VPhysicalVolume* shield_wall2_phys = new G4PVPlacement(0, G4ThreeVector(-(canyon_air_x_length*0.5-2.4*m), door_y_place, canyon_air_z_length*0.5-1.225*m), shield_wall2_log, "shield_wall2", canyon_air_log, false, 0);
  */
  /*G4Box* phantom = new G4Box("phantom", 8*cm, 12*cm, 10*cm);
  G4LogicalVolume* phantom_log = new G4LogicalVolume(phantom, water, "phantom");
  G4VPhysicalVolume* phantom_phys = new G4PVPlacement(0, G4ThreeVector(1.4525*m,-1.65*m,4.1175*m), phantom_log, "phantom", canyon_air_log, false, 0);
  */
  

  G4double p0x, p0y, p0z;
  p0x = 1.4525*m;
  p0y = -1.65*m;
  p0z = 4.1175*m;

  G4Tubs* phantom = new G4Tubs("phantom", 0, 5*cm, 20*cm, 0*deg, 360*deg);
  G4LogicalVolume* phantom_log = new G4LogicalVolume(phantom, tissue, "phantom");
  G4VPhysicalVolume* phantom_phys = new G4PVPlacement(0, G4ThreeVector(1.4525*m,-1.65*m,4.1175*m), phantom_log, "phantom", canyon_air_log, false, 0);

/*  G4Orb* phB = new G4Orb("phB", 1*m);
  G4LogicalVolume* phB_log = new G4LogicalVolume(phB, tissue, "phB");
  new G4PVPlacement(0, G4ThreeVector(1.4525*m,-1.65*m,4.1175*m), phB_log, "phB", canyon_air_log, false, 0);*/

  G4Box* door = new G4Box("door", 0.5*door_x_length, 0.5*door_y_length, 0.5*door_z_length);
  G4LogicalVolume* door_log = new G4LogicalVolume(door, air, "door");
  G4VPhysicalVolume* door_phys = new G4PVPlacement(0, G4ThreeVector(door_x_place, door_y_place, door_z_place), door_log, "door", canyon_concrete_log, false, 0);
  
  //detectors
  
  G4double detector_size = 10*cm;
   
   //detector za stenoy
  G4Box* detector1 = new G4Box("detector1", 0.5*detector_size, 0.5*detector_size, 0.5*detector_size);
  G4LogicalVolume* detector1_log = new G4LogicalVolume(detector1, tissue, "detector1");
  G4VPhysicalVolume* detector1_phys = new G4PVPlacement(0, G4ThreeVector(1.4525*m,-1.65*m,(7.1275*m+0.5*detector_size)), detector1_log, "detector1", container_log, false, 0);
  
  /*
  //detector za dveryu
  G4Box* detector1 = new G4Box("detector1", 0.5*detector_size, 0.5*detector_size, 0.5*detector_size);
  G4LogicalVolume* detector1_log = new G4LogicalVolume(detector1, water, "detector1");
  //G4VPhysicalVolume* detector1_phys = new G4PVPlacement(0, G4ThreeVector(1.4525*m,-1.65*m,7.1775*m), detector1_log, "detector1", container_log, false, 0);
  G4VPhysicalVolume* detector1_phys = new G4PVPlacement(0, G4ThreeVector(door_x_place-0.55*m, door_y_place, door_z_place), detector1_log, "detector1", container_log, false, 0);
  */
  
  //detector 1m ot objecta
  
  G4Box* detector2 = new G4Box("detector2", 0.5*detector_size, 0.5*detector_size, 0.5*detector_size);
  G4LogicalVolume* detector2_log = new G4LogicalVolume(detector2, tissue, "detector2");
  G4VPhysicalVolume* detector2_phys = new G4PVPlacement(0, G4ThreeVector(1.4525*m,-1.65*m,5.1175*m), detector2_log, "detector2", canyon_air_log, false, 0); 

  G4Box* detector3 = new G4Box("detector3", 0.5*detector_size, 0.5*detector_size, 0.5*detector_size);
  G4LogicalVolume* detector3_log = new G4LogicalVolume(detector3, tissue, "detector3");
  G4VPhysicalVolume* detector3_phys = new G4PVPlacement(0, G4ThreeVector(2.1596*m,-1.65*m,4.8246*m), detector3_log, "detector3", canyon_air_log, false, 0);
  
  G4Box* detector4 = new G4Box("detector4", 0.5*detector_size, 0.5*detector_size, 0.5*detector_size);
  G4LogicalVolume* detector4_log = new G4LogicalVolume(detector4, tissue, "detector4");
  //G4VPhysicalVolume* detector4_phys = new G4PVPlacement(0, G4ThreeVector(2.4525*m,-1.65*m,4.1175*m), detector4_log, "detector4", canyon_air_log, false, 0);
  G4VPhysicalVolume* detector4_phys = new G4PVPlacement(0, G4ThreeVector(0.5*canyon_concrete_x_length+0.5*detector_size,-1.65*m,4.1175*m), detector4_log, "detector4", container_log, false, 0);
  
  G4Box* detector5 = new G4Box("detector5", 0.5*detector_size, 0.5*detector_size, 0.5*detector_size);
  G4LogicalVolume* detector5_log = new G4LogicalVolume(detector5, tissue, "detector5");

  G4double xdt = 0.7454*m;
  G4double ydt = -1.65*m;
  G4double zdt = 4.8246*m;
  G4VPhysicalVolume* detector5_phys = new G4PVPlacement(0, G4ThreeVector(xdt, ydt, zdt), detector5_log, "detector5", canyon_air_log, false, 0);

  G4LogicalVolume* detector5a_log = new G4LogicalVolume(detector5, tissue, "detector5a");
  xdt = p0x - 0.7*m;
  ydt = p0y - 0.7*m;
  zdt = 4.1175*m;
  G4VPhysicalVolume* detector5a_phys = new G4PVPlacement(0, G4ThreeVector(xdt, ydt, zdt), detector5a_log, "detector5a", canyon_air_log, false, 0);

  G4LogicalVolume* detector5b_log = new G4LogicalVolume(detector5, tissue, "detector5b");
  xdt = p0x + 0.7*m;
  ydt = p0y - 0.7*m;
  zdt = 4.1175*m;
  new G4PVPlacement(0, G4ThreeVector(xdt, ydt, zdt), detector5b_log, "detector5b", canyon_air_log, false, 0);

  G4LogicalVolume* detector5c_log = new G4LogicalVolume(detector5, tissue, "detector5c");
  xdt = p0x - 0.7*m;
  ydt = p0y + 0.7*m;
  zdt = 4.1175*m;
  new G4PVPlacement(0, G4ThreeVector(xdt, ydt, zdt), detector5c_log, "detector5c", canyon_air_log, false, 0);

  G4LogicalVolume* detector5d_log = new G4LogicalVolume(detector5, tissue, "detector5d");
  xdt = p0x + 0.7*m;
  ydt = p0y + 0.7*m;
  zdt = 4.1175*m;
  new G4PVPlacement(0, G4ThreeVector(xdt, ydt, zdt), detector5d_log, "detector5d", canyon_air_log, false, 0);

  G4LogicalVolume* detector5e_log = new G4LogicalVolume(detector5, tissue, "detector5e");
  xdt = p0x + 1*m;
  ydt = p0y;
  zdt = 4.1175*m;
  new G4PVPlacement(0, G4ThreeVector(xdt, ydt, zdt), detector5e_log, "detector5e", canyon_air_log, false, 0);

  G4LogicalVolume* detector5f_log = new G4LogicalVolume(detector5, tissue, "detector5f");
  xdt = p0x;
  ydt = p0y + 1*m;
  zdt = 4.1175*m;
  new G4PVPlacement(0, G4ThreeVector(xdt, ydt, zdt), detector5f_log, "detector5f", canyon_air_log, false, 0);

  G4LogicalVolume* detector5g_log = new G4LogicalVolume(detector5, tissue, "detector5g");
  xdt = p0x - 1*m;
  ydt = p0y;
  zdt = 4.1175*m;
  new G4PVPlacement(0, G4ThreeVector(xdt, ydt, zdt), detector5g_log, "detector5g", canyon_air_log, false, 0);

  G4LogicalVolume* detector5h_log = new G4LogicalVolume(detector5, tissue, "detector5e");
  xdt = p0x;
  ydt = p0y - 1*m;
  zdt = 4.1175*m;
  new G4PVPlacement(0, G4ThreeVector(xdt, ydt, zdt), detector5h_log, "detector5f", canyon_air_log, false, 0);
  
  G4Box* detector6 = new G4Box("detector6", 0.5*detector_size, 0.5*detector_size, 0.5*detector_size);
  G4LogicalVolume* detector6_log = new G4LogicalVolume(detector6, tissue, "detector6");
  G4VPhysicalVolume* detector6_phys = new G4PVPlacement(0, G4ThreeVector(0.4525*m,-1.65*m,4.1175*m), detector6_log, "detector6", canyon_air_log, false, 0);

  G4LogicalVolume* detector6a_log = new G4LogicalVolume(detector6, tissue, "detector6a");
  G4VPhysicalVolume* detector6a_phys = new G4PVPlacement(0, G4ThreeVector(2.4525*m,-1.65*m,4.1175*m), detector6_log, "detector6a", canyon_air_log, false, 0);

  G4LogicalVolume* detectorTop_log = new G4LogicalVolume(detector6, tissue, "detectorTop");

  xdt = 1.4525 * m;
  ydt = 0.5*canyon_concrete_y_length + detector_size / 2;
  zdt = 4.1175 * m;
  new G4PVPlacement(0, G4ThreeVector(xdt, ydt, zdt), detectorTop_log, "detectorTop", container_log, false, 0);

  qOfDets = 0;

  /**/addDetParted(detector1_log, "det1_fwWall");
  /**/addDetParted(detector2_log, "det2_forward");/**/
  addDetParted(detector3_log, "det3tl");
  addDetParted(detector4_log, "det4_leftWall");
  addDetParted(detector5_log, "det5tr");
  addDetParted(detector6_log, "det6rt");
  addDetParted(detector6a_log, "det6lt");
  
  addDetParted(detector5a_log, "det5mm");
  addDetParted(detector5b_log, "det5pm");
  addDetParted(detector5c_log, "det5mp");
  addDetParted(detector5d_log, "det5pp");
  addDetParted(detector5e_log, "det5x");
  addDetParted(detector5f_log, "det5y");
  addDetParted(detector5g_log, "det5nx");
  addDetParted(detector5h_log, "det5ny");/**/

  addDetParted(detectorTop_log, "detTop");

  addDetParted(phantom_log, "phantom");

  /*DetectorSD1* detectorSD1 = new DetectorSD1("DetectorSD1");
  G4SDManager* sdMan1 = G4SDManager::GetSDMpointer();
  sdMan1->AddNewDetector(detectorSD1);
  detector1_log->SetSensitiveDetector(detectorSD1);

  DetectorSD2* detectorSD2 = new DetectorSD2("DetectorSD2");
  G4SDManager* sdMan2 = G4SDManager::GetSDMpointer();
  sdMan2->AddNewDetector(detectorSD2);
  detector2_log->SetSensitiveDetector(detectorSD2);
  
  DetectorSD3* detectorSD3 = new DetectorSD3("DetectorSD3");
  G4SDManager* sdMan3 = G4SDManager::GetSDMpointer();
  sdMan3->AddNewDetector(detectorSD3);
  detector3_log->SetSensitiveDetector(detectorSD3);
  
  DetectorSD4* detectorSD4 = new DetectorSD4("DetectorSD4");
  G4SDManager* sdMan4 = G4SDManager::GetSDMpointer();
  sdMan4->AddNewDetector(detectorSD4);
  detector4_log->SetSensitiveDetector(detectorSD4);
  
  DetectorSD5* detectorSD5 = new DetectorSD5("DetectorSD5");
  G4SDManager* sdMan5 = G4SDManager::GetSDMpointer();
  sdMan5->AddNewDetector(detectorSD5);
  detector5_log->SetSensitiveDetector(detectorSD5);
  
  DetectorSD6* detectorSD6 = new DetectorSD6("DetectorSD6");
  G4SDManager* sdMan6 = G4SDManager::GetSDMpointer();
  sdMan6->AddNewDetector(detectorSD6);
  detector6_log->SetSensitiveDetector(detectorSD6);

 //world_log->SetVisAttributes(G4VisAttributes::Invisible);

 canyon_concrete_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Gray()));
 //detector1_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Magenta()));
 //detector2_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Cyan()));
 phantom_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Magenta()));
 //shield_wall2_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Gray()));

  

  /*addDet(detectorTop_log, "oTop1");
  addDet(detectorLt_log, "oLeft1");
  addDet(detectorRt_log, "oRight1");
  addDet(detectorRt2_log, "oRight2");
  addDet(detectorFw_log, "oForward1");
  addDet(detectorFw2_log, "oForward2");*/

  G4cout<<"Detectors "<<qOfDets<<G4endl;
  
  /*
  G4Box* detector2 = new G4Box("detector2", 0.5*detector_size, 0.5*detector_size, 0.5*detector_size);
  G4LogicalVolume* detector2_log = new G4LogicalVolume(detector2, air, "detector2");
  G4VPhysicalVolume* detector2_phys = new G4PVPlacement(0, G4ThreeVector(door_x_place-0.55*m, door_y_place, door_z_place+0.4*m), detector2_log, "detector2", canyon_air_log, false, 0);
  */
  /*DetectorSD1* detectorSD1 = new DetectorSD1("DetectorSD1");
  G4SDManager* sdMan1 = G4SDManager::GetSDMpointer();
  sdMan1->AddNewDetector(detectorSD1);
  phantom_log->SetSensitiveDetector(detectorSD1);

  DetectorSD2* detectorSD2 = new DetectorSD2("DetectorSD2");
  G4SDManager* sdMan2 = G4SDManager::GetSDMpointer();
  sdMan2->AddNewDetector(detectorSD2);
  detector2_log->SetSensitiveDetector(detectorSD2);
  
  DetectorSD3* detectorSD3 = new DetectorSD3("DetectorSD3");
  G4SDManager* sdMan3 = G4SDManager::GetSDMpointer();
  sdMan3->AddNewDetector(detectorSD3);
  detector3_log->SetSensitiveDetector(detectorSD3);
  
  DetectorSD4* detectorSD4 = new DetectorSD4("DetectorSD4");
  G4SDManager* sdMan4 = G4SDManager::GetSDMpointer();
  sdMan4->AddNewDetector(detectorSD4);
  detector4_log->SetSensitiveDetector(detectorSD4);
  
  DetectorSD5* detectorSD5 = new DetectorSD5("DetectorSD5");
  G4SDManager* sdMan5 = G4SDManager::GetSDMpointer();
  sdMan5->AddNewDetector(detectorSD5);
  detector5_log->SetSensitiveDetector(detectorSD5);
  
  DetectorSD6* detectorSD6 = new DetectorSD6("DetectorSD6");
  G4SDManager* sdMan6 = G4SDManager::GetSDMpointer();
  sdMan6->AddNewDetector(detectorSD6);
  detector6_log->SetSensitiveDetector(detectorSD6);*/

 //world_log->SetVisAttributes(G4VisAttributes::Invisible);


 canyon_concrete_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Gray()));
 //detector1_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Magenta()));
 //detector2_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Cyan()));
 phantom_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Magenta()));
 //shield_wall2_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Gray()));

  return world_phys;

}

void DetectorConstruction::addDet(G4LogicalVolume* vol, G4String name) {
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSDSingle* det = new DetectorSDSingle(name, vol->GetSolid()->GetCubicVolume());
  sdMan->AddNewDetector(det);
  vol->SetSensitiveDetector(det);
  qOfDets++;
  lOfDets.push_back(name);
}

void DetectorConstruction::addDetParted(G4LogicalVolume* vol, G4String name) {
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSDSinglePart* det = new DetectorSDSinglePart(name, vol->GetSolid()->GetCubicVolume());
  det->addParticle("proton");
  det->addParticle("alpha");
  det->addParticle("e-");
  det->addParticle("neutronHE", "neutron", 20, 500);
  det->addParticle("neutronLE", "neutron", 0, 20);
  sdMan->AddNewDetector(det);
  vol->SetSensitiveDetector(det);
  qOfDets++;
  lOfDets.push_back(name);
}

void DetectorConstruction::addDet(G4LogicalVolume* vol, G4String name, G4double volume) {
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSDSingle* det;
  if (0.0 == volume) {
    det = new DetectorSDSingle(name);
  } else {
    det = new DetectorSDSingle(name, volume);
  }
  sdMan->AddNewDetector(det);
  vol->SetSensitiveDetector(det);
  qOfDets++;
  lOfDets.push_back(name);
}