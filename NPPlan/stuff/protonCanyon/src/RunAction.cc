#include "RunAction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
//#include "G4VUserDetectorConstruction.hh"
#include "DetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include <ctime>

SlayerRA::SlayerRA() : depEnergy(0.0), depEnergy2(0.0), depEnergyError(0.0),
  dose(0.0), dose2(0.0), doseError(0.0),
  cellFlux(0.0), cellFlux2(0.0),
  nEvents(0)
{
}

RunAction::RunAction()
  : useContinue(false), G4UserRunAction(), previousRuns(0)
{ 
  G4RunManager::GetRunManager()->SetPrintProgress(10000);
}

RunAction::RunAction(bool _useContinue)
 : useContinue(_useContinue), G4UserRunAction(), previousRuns(0)
{ 
  G4cout<<"  Run with continue"<<G4endl;
  G4RunManager::GetRunManager()->SetPrintProgress(10000);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{

}

SlayerRA RunAction::makeZero(SlayerRA o) {
  o.depEnergy = o.depEnergy2 = o.depEnergyError = 0.0;
  o.dose = o.dose2 = o.doseError = 0.0;
  o.cellFlux = o.cellFlux2 = 0.0;
  o.nEvents = 0;
  return o;
}

void RunAction::addDetData(G4String detName, G4double en, G4double en2, G4double dose, G4double dose2, G4int nEvent, G4double cellFlux) {
  //G4cout<<"Got RA "<<detName<<" det "<<en<<G4endl;
  SlayerRA q;
  q = depToDets.find(detName)->second;
  q.depEnergy += en;
  q.depEnergy2 += en2;
  q.dose += dose;
  q.dose2 += dose2;
  q.nEvents += nEvent;
  q.cellFlux += cellFlux;
  depToDets.find(detName)->second = q;
}

void RunAction::addDetParticleData(G4String detName, G4String particle, G4double en, G4double en2, G4double dose, G4double dose2, G4int nEvent, G4double cellFlux) {
  //G4cout<<"Got RA "<<detName<<" det "<<en<<G4endl;
  // 1. find detector
  ppr pairIndex(detName, particle);
  if (0 == dets2Energies.count(pairIndex)) {
    SlayerRA *data = new SlayerRA();
    data->depEnergy = en;
    data->depEnergy2 = en2;
    data->dose = dose;
    data->dose2 = dose2;
    data->nEvents = nEvent;
    data->cellFlux = cellFlux;
    dets2Energies.insert(std::pair<ppr, SlayerRA*>(pairIndex, data));
  } else {
    dets2Energies.find(pairIndex)->second->depEnergy += en;
    dets2Energies.find(pairIndex)->second->depEnergy2 += en2;
    dets2Energies.find(pairIndex)->second->dose += dose;
    dets2Energies.find(pairIndex)->second->dose2 += dose2;
    dets2Energies.find(pairIndex)->second->nEvents += nEvent;
    dets2Energies.find(pairIndex)->second->cellFlux += cellFlux;
  }

}

SlayerRA RunAction::prepareError(SlayerRA target) {
  G4int n;
  G4double v, vDo;
  v = target.nEvents*target.depEnergy2 - target.depEnergy*target.depEnergy;
  vDo = target.nEvents*target.dose2 - target.dose*target.dose;
  n = target.nEvents;
  if (n>1) {
    target.depEnergyError = 1.0*std::sqrt(v/(n-1));
    target.doseError = 1.0*std::sqrt(vDo/(n-1));
  }
  return target;
}

void RunAction::prepareErrorS(SlayerRA *target) {
  G4int n;
  G4double v, vDo;
  v = target->nEvents*target->depEnergy2 - target->depEnergy*target->depEnergy;
  vDo = target->nEvents*target->dose2 - target->dose*target->dose;
  n = target->nEvents;
  if (n>1) {
    target->depEnergyError = 1.0*std::sqrt(v/(n-1));
    target->doseError = 1.0*std::sqrt(vDo/(n-1));
  }
}

void RunAction::BeginOfRunAction(const G4Run*)
{ 
  G4cout<<"DetectorsRA "<<((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->qOfDets<<G4endl;
  for(G4int i = 0; i < ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->qOfDets; i++) {
    SlayerRA t;
    t = makeZero(t);
    G4String detName = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->lOfDets[i];
    if (useContinue) {
      t = readStructFromFile(t, detName+".dump");
    }
    depToDets.insert(std::pair<G4String, SlayerRA>(detName, t));
  }

  G4cout<<"USE CONTINUE: "<<useContinue<<G4endl;
  if (useContinue) {
    readNoE();
    readList();
  }

  beginAt = clock();
}


void RunAction::EndOfRunAction(const G4Run* )
{
  for (std::map<G4String, SlayerRA>::iterator it = depToDets.begin();
				it != depToDets.end();
				++it)
  {
    dumpDep(it->second, it->first+".out");
    dumpStruct2File(it->second, it->first+".dump");
  }

  if (useContinue)
    previousRuns += (G4int)G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEvent();
  else
    previousRuns = (G4int)G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEvent();
  dumpNoE();

  std::vector<ppr> ll;

  for (std::map<ppr, SlayerRA*>::iterator it = dets2Energies.begin();
    it != dets2Energies.end();
    ++it)
  {
    dumpDep(it->first.second, it->second, it->first.first+".out");
    dumpStruct2File(it->second, it->first.first+"."+it->first.second);

    ll.push_back(it->first);
  }
  //dumpList(ll);
  std::ofstream fileoutPart;
  fileoutPart.open("particles.list", std::ofstream::out | std::ofstream::trunc);
  //fileoutPart<<"Particle "<<particle<<G4endl;
  fileoutPart<<ll.size()<<G4endl;
  for (int i = 0; i < ll.size(); i++) {
    fileoutPart<<ll[i].first<<G4endl;
    fileoutPart<<ll[i].second<<G4endl;
  }
  fileoutPart.close();

  finishedAt = clock();
  double elapsedSecs = double(finishedAt - beginAt) / CLOCKS_PER_SEC;
  fileoutPart.open("runTimes.log", std::ofstream::out | std::ofstream::app);
  fileoutPart<<"Run "<<(G4int)G4RunManager::GetRunManager()->GetCurrentRun()->GetRunID()<<" time elapsed: "<<elapsedSecs<<G4endl;
  fileoutPart.close();
}

bool RunAction::checkHasBestSolution() {
  G4cout<<"Checking is best?"<<G4endl;
  bool hasBest = true;
  for (std::map<G4String, SlayerRA>::iterator it = depToDets.begin();
				it != depToDets.end();
				++it)
  {
    it->second = prepareError(it->second);
    if ((0 == it->second.nEvents) || ((1 == it->second.nEvents))) {
      G4cout<<"No entry particles at "<<it->first<<G4endl;
      hasBest = false;
      break;
    } else {
      G4int re = (int)(it->second.doseError / it->second.dose * 100);
      G4cout<<"Relative error at "<<it->first<<" is "<<re<<G4endl;
      if (re > 10) {
        hasBest = false;
        break;
      }
    }
  }

  return hasBest;
}

void RunAction::dumpStruct2File(SlayerRA target, G4String filename) {
  std::ofstream fileoutPart;
  fileoutPart.open(filename, std::ofstream::out | std::ofstream::trunc);
  fileoutPart<<target.depEnergy / MeV<<G4endl;
  fileoutPart<<target.depEnergy2 / MeV<<G4endl;
  fileoutPart<<target.dose / MeV<<G4endl;
  fileoutPart<<target.dose2 / MeV<<G4endl;
  fileoutPart<<target.nEvents<<G4endl;
  fileoutPart<<target.cellFlux / (1 / cm2)<<G4endl;
  fileoutPart.close();
}

void RunAction::dumpStruct2File(SlayerRA *target, G4String filename) {
  std::ofstream fileoutPart;
  fileoutPart.open(filename, std::ofstream::out | std::ofstream::trunc);
  fileoutPart<<target->depEnergy / MeV<<G4endl;
  fileoutPart<<target->depEnergy2 / MeV<<G4endl;
  fileoutPart<<target->dose / MeV<<G4endl;
  fileoutPart<<target->dose2 / MeV<<G4endl;
  fileoutPart<<target->nEvents<<G4endl;
  fileoutPart<<target->cellFlux / (1 / cm2)<<G4endl;
  fileoutPart.close();
}

SlayerRA RunAction::readStructFromFile(SlayerRA target, G4String filename) {
  std::ifstream fileoutPart;
  fileoutPart.open(filename, std::ifstream::in);
  fileoutPart>>target.depEnergy;
  fileoutPart>>target.depEnergy2;
  fileoutPart>>target.dose;
  fileoutPart>>target.dose2;
  fileoutPart>>target.nEvents;
  fileoutPart>>target.cellFlux;
  target.depEnergy *= MeV;
  target.depEnergy2 *= MeV;
  target.dose *= MeV;
  target.dose2 *= MeV;
  target.cellFlux *= (1 / cm2);
  fileoutPart.close();

  return target;
}

void RunAction::readStructParticlesFromFile(G4String det, G4String particle) {
  // 1. split "detector.particle" filename format
  /*addDetParticleData(det, particle, 0.0, 0.0,
      0.0, 0.0, 0, 0.0);*/
  // 2. add empty data with standart addDetParticleData
  std::ifstream fileoutPart;
  fileoutPart.open(det+"."+particle, std::ifstream::in);  
  G4double ene, ene2, d, d2, ne, cf;
  fileoutPart>>ene;
  fileoutPart>>ene2;
  fileoutPart>>d;
  fileoutPart>>d2;
  fileoutPart>>ne;
  fileoutPart>>cf;
  ene *= MeV;
  ene2 *= MeV;
  d *= MeV;
  d2 *= MeV;
  cf *= (1/cm2);
  fileoutPart.close();
  addDetParticleData(det, particle, ene, ene2,
      d, d2, ne, cf);
}

void RunAction::readList() {
  std::ifstream fileoutPart;
  fileoutPart.open("particles.list", std::ifstream::in);
  G4int szl;
  G4String det;
  G4String particle;
  fileoutPart>>szl;
  for (int i = 0; i < szl; i++) {
    fileoutPart>>det;
    fileoutPart>>particle;
    readStructParticlesFromFile(det, particle);
  }
}

void dumpList(std::vector<ppr> ll) {

}

void RunAction::dumpDep(G4String particle, SlayerRA *target, G4String filename) {
  prepareErrorS(target);
  std::ofstream fileoutPart;
  fileoutPart.open(filename, std::ofstream::app);
  fileoutPart<<"Particle "<<particle<<G4endl;

  G4double divider;
  if (!useContinue)
    divider = G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEvent();
  else 
    divider = G4double(previousRuns);

  fileoutPart<<G4BestUnit(target->depEnergy, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target->depEnergyError, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target->dose, "Dose")<<" \t";
  fileoutPart<<target->dose / (MeV /g)<<" \t";
  fileoutPart<<G4BestUnit(target->doseError, "Dose")<<" \t";
  fileoutPart<<target->cellFlux / (1 / cm2)<<" \t";
  fileoutPart<<target->nEvents<<" \t";
//  fileoutPart<<relativeError<<"%";
  fileoutPart<<" \n";

  fileoutPart<<G4BestUnit(target->depEnergy / divider, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target->depEnergyError, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target->dose / divider, "Dose")<<" \t";
  fileoutPart<<target->dose / divider / (MeV /g)<<" \t";
  fileoutPart<<G4BestUnit(target->doseError, "Dose")<<" \t";
  fileoutPart<<target->cellFlux / divider / (1 / cm2)<<" \t";
  fileoutPart<<target->nEvents<<" \t";
  fileoutPart<<" \n";

  fileoutPart.close();
}

void RunAction::dumpDep(SlayerRA target, G4String filename) {
  target = prepareError(target);

  std::ofstream fileoutPart;
  fileoutPart.open(filename);
  fileoutPart<<"Dep energy, MeV "<<" \t";
  fileoutPart<<"Energy error, MeV "<<" \t";
  fileoutPart<<"Dose, Gy "<<" \t";
  fileoutPart<<"Dose, MeV / g "<<" \t";
  fileoutPart<<"Dose error, Gy "<<" \t";
  fileoutPart<<"Cell flux, 1/cm2 "<<" \t";
  fileoutPart<<"Cell flux2, 1/cm2"<<" \t";
  fileoutPart<<"Events "<<" \t";
  fileoutPart<<"Dose error, percents";
  fileoutPart<<" \n";
  //fileoutPart<<"\n";
  fileoutPart<<std::defaultfloat;

  G4double v, vDo, n;
  v = target.nEvents*target.depEnergy2 - target.depEnergy*target.depEnergy;
  vDo = target.nEvents*target.dose2 - target.dose*target.dose;
  n = target.nEvents;
  if (n>1) {
    target.depEnergyError = 1.0*std::sqrt(v/(n-1));
    target.doseError = 1.0*std::sqrt(vDo/(n-1));
  }

  G4double divider;
  if (!useContinue)
    divider = G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEvent();
  else 
    divider = G4double(previousRuns);
  //G4cout<<"Divider: "<<divider<<G4endl;

  G4float relativeError;
  if (0.0 != target.doseError)
    relativeError = (int)(target.doseError / target.dose * 100) + 0.001*((int)(target.doseError / target.dose * 100000) % 1000);
  else
    relativeError = 100.0;

  if (relativeError < 10.0) {
    G4cout<<filename<<" has relative error < 10%"<<G4endl;
  } else {
    G4cout<<filename<<" has relative error > 10%"<<G4endl;
  }
  

  fileoutPart<<G4BestUnit(target.depEnergy, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target.depEnergyError, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target.dose, "Dose")<<" \t";
  fileoutPart<<target.dose / (MeV /g)<<" \t";
  fileoutPart<<G4BestUnit(target.doseError, "Dose")<<" \t";
  fileoutPart<<target.cellFlux / (1 / cm2)<<" \t";
  fileoutPart<<target.cellFlux2 / (1 / cm2)<<" \t";
  fileoutPart<<target.nEvents<<" \t";
  fileoutPart<<relativeError<<"%";
  fileoutPart<<" \n";

  fileoutPart<<G4BestUnit(target.depEnergy / divider, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target.depEnergyError, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target.dose / divider, "Dose")<<" \t";
  fileoutPart<<target.dose / divider / (MeV /g)<<" \t";
  fileoutPart<<G4BestUnit(target.doseError, "Dose")<<" \t";
  fileoutPart<<target.cellFlux / divider / (1 / cm2)<<" \t";
  fileoutPart<<target.cellFlux2 / divider / (1 / cm2)<<" \t";
  fileoutPart<<target.nEvents<<" \t";
  fileoutPart<<relativeError<<"%";
  fileoutPart<<" \n";

  fileoutPart.close();
}


void RunAction::dumpNoE() {
  std::ofstream fileoutPart;
  fileoutPart.open("_core.dump", std::ofstream::out | std::ofstream::trunc);
  fileoutPart<<previousRuns<<G4endl;
  fileoutPart.close();  
}
void RunAction::readNoE() {
  std::ifstream fileoutPart;
  fileoutPart.open("_core.dump", std::ifstream::in);
  fileoutPart>>previousRuns;
  fileoutPart.close();
}
