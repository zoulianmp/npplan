import os
import matplotlib
import wx
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import wx.lib.agw.pybusyinfo as PBI
import wx.lib.agw.genericmessagedialog as GMD
import os

def readDataFile(fName):
    x = []
    y = []
    z = []
    v = []
    for line in file(fName):
        splLine = line.split()
        x.append(int(splLine[0]))
        y.append(int(splLine[1]))
        z.append(int(splLine[2]))
        v.append(float(splLine[3]))
    print np.array(x), np.array(y), np.array(z), np.array(v)
    return len(x), np.array(x), np.array(y), np.array(z), np.array(v)

def parseX(x, y, z, v, eq):
    y = y[np.where(x==eq)]
    z = z[np.where(x==eq)]
    v = v[np.where(x==eq)]
    x = x[np.where(x==eq)]
    return x, y, z, v

def parseY(x, y, z, v, eq):
    z = z[np.where(y==eq)]
    v = v[np.where(y==eq)]
    x = x[np.where(y==eq)]
    y = y[np.where(y==eq)]
    return x, y, z, v

def parseZ(x, y, z, v, eq):
    y = y[np.where(z==eq)]
    v = v[np.where(z==eq)]
    x = x[np.where(z==eq)]
    z = z[np.where(z==eq)]
    return x, y, z, v


class MainFrame(wx.Frame):
    """ The main frame of the application
    """
    title = '3d viewer'

    def __init__(self):
        wx.Frame.__init__(self, None, -1, self.title)
        pnl = mainPanel(self)
        sizer = wx.BoxSizer()
        sizer.Add(pnl, 1, wx.EXPAND | wx.ALL)
        self.SetSizer(sizer)
        #self._data = readDataFile(r'C:\Temp\ebt\cubes_out\cube1z.txt')
        #self._data = None
        #self.createMainPanel()


class mainPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, wx.NewId())
        self._data = None
        self.createMainPanel()
        #self.drawFigure()

    def createMainPanel(self):
        self.panel = wx.Panel(self, -1, style=wx.TAB_TRAVERSAL)
        self.dpi = 250
        self.fig = Figure((5.0, 4.0), dpi=self.dpi)
        self.canvas = FigCanvas(self.panel, -1, self.fig)
        self.axes = self.fig.add_subplot(111, projection='3d')
        self.axes.grid(False)
        self.axes.set_xlim3d(-1, 290)
        self.axes.set_ylim3d(-1, 320)
        self.axes.set_zlim3d(0, 21)
        #self.axes.hold(False)
        self.toolbar = NavigationToolbar(self.canvas)

        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox.Add(self.toolbar, 0, wx.EXPAND)
        self.vbox.AddSpacer(10)

        self.panel.SetSizer(self.vbox)
        self.vbox.Fit(self)

        self.hbox = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox.Add(self.panel, 3, wx.EXPAND | wx.ALL)
        self.createButtons()
        self.hbox.Add(self.btnSizer, 1, wx.EXPAND | wx.ALL)
        self.SetSizer(self.hbox)
        #self.SetSizer(self.hbox)


    def createButtons(self):
        self.btnSizer = wx.FlexGridSizer(cols=3)

        # file fields
        self._fileButton = wx.Button(self, -1, "Open")
        self._fileField = wx.TextCtrl(self, -1, "")
        self._forceAuto = wx.CheckBox(self, -1, "Auto draw")
        #self._forceAuto.SetValue(1)
        self.btnSizer.AddMany([
            self._fileButton, self._fileField, self._forceAuto
        ])
        self.Bind(wx.EVT_BUTTON, self.openFile, self._fileButton)

        # position fields
        xLimText = wx.StaticText(self, -1, "x lim ")
        yLimText = wx.StaticText(self, -1, "y lim ")
        zLimText = wx.StaticText(self, -1, "z lim ")
        useEachXText = wx.StaticText(self, -1, "use each ")
        useEachYText = wx.StaticText(self, -1, "use each ")
        useEachZText = wx.StaticText(self, -1, "use each ")
        self._xsLimField = wx.TextCtrl(self, -1, "0")
        self._ysLimField = wx.TextCtrl(self, -1, "0")
        self._zsLimField = wx.TextCtrl(self, -1, "0")
        self._xeLimField = wx.TextCtrl(self, -1, "289")
        self._yeLimField = wx.TextCtrl(self, -1, "319")
        self._zeLimField = wx.TextCtrl(self, -1, "21")
        self._xUseField = wx.TextCtrl(self, -1, "10")
        self._yUseField = wx.TextCtrl(self, -1, "10")
        self._zUseField = wx.TextCtrl(self, -1, "1")
        self.btnSizer.AddMany([
            xLimText, self._xsLimField, self._xeLimField,
            (1, 1), useEachXText, self._xUseField,
            yLimText, self._ysLimField, self._yeLimField,
            (1, 1), useEachYText, self._yUseField,
            zLimText, self._zsLimField, self._zeLimField,
            (1, 1), useEachZText, self._zUseField,
        ])

        # dose limit fields
        doseLim = wx.StaticText(self, -1, "Dose lim ")
        self._dosesField = wx.TextCtrl(self, -1, "0")
        self._doseeField = wx.TextCtrl(self, -1, "50")
        self.btnSizer.AddMany([
            doseLim, self._dosesField, self._doseeField,
        ])

        # cut fields
        self._useCut = wx.CheckBox(self, -1, "Use cut")
        self._useCut.SetValue(0)
        self.Bind(wx.EVT_CHECKBOX, self.useCutChecker, self._useCut)
        _xCutChoice = wx.StaticText(self, -1, "x cutter lim ")
        _yCutChoice = wx.StaticText(self, -1, "y cutter lim ")
        _zCutChoice = wx.StaticText(self, -1, "z cutter lim ")
        self._xCutsLimField = wx.TextCtrl(self, -1, str(288/2))
        self._yCutsLimField = wx.TextCtrl(self, -1, "0")
        self._zCutsLimField = wx.TextCtrl(self, -1, "8")
        self._xCuteLimField = wx.TextCtrl(self, -1, "289")
        self._yCuteLimField = wx.TextCtrl(self, -1, str(319/2))
        self._zCuteLimField = wx.TextCtrl(self, -1, "21")
        self._xCutsLimField.Disable()
        self._yCutsLimField.Disable()
        self._zCutsLimField.Disable()
        self._xCuteLimField.Disable()
        self._yCuteLimField.Disable()
        self._zCuteLimField.Disable()
        self.btnSizer.AddMany([
            self._useCut, (1, 1), (1, 1),
            _xCutChoice, self._xCutsLimField, self._xCuteLimField,
            _yCutChoice, self._yCutsLimField, self._yCuteLimField,
            _zCutChoice, self._zCutsLimField, self._zCuteLimField,
        ])

        b = wx.Button(self, -1, "Redraw 3D")
        self.Bind(wx.EVT_BUTTON, self.onRedrawClick, b)
        self.btnSizer.AddMany([
            b, (1, 1), (1, 1)
        ])

        _x1d = wx.StaticText(self, -1, "x cutter lim ")
        _y1d = wx.StaticText(self, -1, "y cutter lim ")
        _z1d = wx.StaticText(self, -1, "z cutter lim ")
        self._x1dField = wx.TextCtrl(self, -1, "-1")
        self._y1dField = wx.TextCtrl(self, -1, "-1")
        self._z1dField = wx.TextCtrl(self, -1, "-1")

        self.btnSizer.AddMany([
            _x1d, self._x1dField, (1, 1),
            _y1d, self._y1dField, (1, 1),
            _z1d, self._z1dField, (1, 1),
        ])

        b2 = wx.Button(self, -1, "Redraw 1D")
        self.Bind(wx.EVT_BUTTON, self.onRedraw1DClick, b2)
        self.btnSizer.AddMany([
            b2, (1, 1), (1, 1)
        ])

        pass

    def openFile(self, event):
                # dialog is set up to change the current working directory to the path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(),
            defaultFile="",
            wildcard="Text (.txt)|*.txt|Out file (.out)|*.out|All files (*.*)|*.*",
            style=wx.OPEN | wx.CHANGE_DIR
            )
        path = ""
        # Show the dialog and retrieve the user response. If it is the OK response,
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            # This returns a Python list of files that were selected.
            paths = dlg.GetPaths()

            path = paths[0]
            #print path
            spPath = os.path.split(path)
            self._fileField.SetValue(spPath[1])
        dlg.Destroy()
        busy = PBI.PyBusyInfo("Reading data", parent=None, title="Please wait",)
        wx.Yield()
        if path != "":
            self._data = readDataFile(path)
        del busy
        if self._forceAuto.IsChecked():
            wx.CallLater(200, self.onRedrawClick)
        pass

    def useCutChecker(self, event):
        if event.IsChecked():
            self._xCutsLimField.Enable()
            self._yCutsLimField.Enable()
            self._zCutsLimField.Enable()
            self._xCuteLimField.Enable()
            self._yCuteLimField.Enable()
            self._zCuteLimField.Enable()
        else:
            self._xCutsLimField.Disable()
            self._yCutsLimField.Disable()
            self._zCutsLimField.Disable()
            self._xCuteLimField.Disable()
            self._yCuteLimField.Disable()
            self._zCuteLimField.Disable()
        pass

    def onRedraw1DClick(self, event=None):
        busy = PBI.PyBusyInfo("Redrawing", parent=None, title="Please wait",)
        wx.Yield()
        self.draw1DFigure()
        del busy
        pass

    def onRedrawClick(self, event=None):
        if self._data is None:
            dlg = GMD.GenericMessageDialog(self, "No input data",
                                           "Error",
                                           wx.ICON_ERROR | wx.OK)
            dlg.ShowModal()
            dlg.Destroy()
            return
        busy = PBI.PyBusyInfo("Redrawing", parent=None, title="Please wait",)
        wx.Yield()
        self.drawFigure()
        del busy
        pass

    def draw1DFigure(self):
        self.fig.clf(True)   # in order to clear title
        self.axes = self.fig.add_subplot(111)
        xdd = int(self._x1dField.GetValue())
        ydd = int(self._y1dField.GetValue())
        zdd = int(self._z1dField.GetValue())
        if xdd == -1 and ydd == -1 and zdd == 1:
            return

        x = self._data[1].copy()
        y = self._data[2].copy()
        z = self._data[3].copy()
        v = self._data[4].copy()

        if xdd == -1:
            x, y, z, v = parseY(x, y, z, v, ydd)
            x, y, z, v = parseZ(x, y, z, v, zdd)

            self.fig.suptitle("Along x, y = %d, z = %d" %(ydd, zdd))
            self.axes.bar(x, v)
        if ydd == -1:
            x, y, z, v = parseX(x, y, z, v, xdd)
            x, y, z, v = parseZ(x, y, z, v, zdd)

            self.fig.suptitle("Along y, x = %d, z = %d" %(xdd, zdd))
            self.axes.bar(y, v)

        if zdd == -1:
            x, y, z, v = parseY(x, y, z, v, ydd)
            x, y, z, v = parseX(x, y, z, v, xdd)

            self.fig.suptitle("Along z, x = %d, y = %d" %(xdd, ydd))
            self.axes.bar(z, v)
            pass
        self.canvas.draw()
        pass

    def drawFigure(self):
        """ Redraws the figure
        """
        self.fig.clf(True)   # in order to clear title
        self.axes = self.fig.add_subplot(111, projection='3d')
        #self.axes.cla()
        self.axes.mouse_init(rotate_btn=1, zoom_btn=3)
        self.axes.grid(False)
        self.axes.set_xlim3d(-1, 290)
        self.axes.set_ylim3d(-1, 320)
        self.axes.set_zlim3d(0, 21)
        #self.axes.clear()
        #self.axes.grid(False)
        #self.fig.cla()
        #self.axes.clear()

        #self.canvas.draw()
        #self.fig.


        quantity = self._data[0]
        ax = []
        ay = []
        az = []
        ac = []
        useEachX = int(self._xUseField.GetValue())
        useEachY = int(self._yUseField.GetValue())
        useEachZ = int(self._zUseField.GetValue())
        useCut = self._useCut.IsChecked()
        cutXLow = int(self._xCutsLimField.GetValue())
        cutYLow = int(self._yCutsLimField.GetValue())
        cutZLow = int(self._zCutsLimField.GetValue())
        cutXHigh = int(self._xCuteLimField.GetValue())
        cutYHigh = int(self._yCuteLimField.GetValue())
        cutZHigh = int(self._zCuteLimField.GetValue())
        enLow = float(self._dosesField.GetValue())
        enHigh = float(self._doseeField.GetValue())

        XLow = int(self._xsLimField.GetValue())
        YLow = int(self._ysLimField.GetValue())
        ZLow = int(self._zsLimField.GetValue())
        XHigh = int(self._xeLimField.GetValue())
        YHigh = int(self._yeLimField.GetValue())
        ZHigh = int(self._zeLimField.GetValue())

        for i in range(quantity):
            x = int(self._data[1][i])
            y = int(self._data[2][i])
            z = int(self._data[3][i])
            c = float(self._data[4][i])

            if x % useEachX != 0:
                continue
            if y % useEachY != 0:
                continue
            if useEachZ != 1 and z % useEachZ != 0:
                continue
            if useCut:
                if cutXLow <= x <= cutXHigh and cutYLow <= y <= cutYHigh and cutZLow <= z <= cutZHigh:
                    continue
            if not (enLow <= c <= enHigh):
                continue
            if not (XLow <= x <= XHigh and YLow <= y <= YHigh and ZLow <= z <= ZHigh):
                continue
                #if x>(288/2) and y<(318/2-50) and z > 8:
                #    continue

            ax.append(x)
            ay.append(y)
            az.append(z)
            ac.append(c)

            pass
        #x, y, z, v = readDataFile(r'C:\Temp\ebt\cubes_out\cube1z.txt')
        print 'Read file completed'
        ax = np.array(ax)
        ay = np.array(ay)
        az = np.array(az)
        av = np.array(ac)
        ac = av / np.max(av)
        print ax.shape, ay.shape, az.shape, ac.shape
        print np.max(av), "at", np.where(av==np.max(av))
        #_x = self._data[1]
        #_y = self._data[2]
        #_z = self._data[3]
        #_v = self._data[4]
        xmax = self._data[1][np.where(self._data[4]==np.max(self._data[4]))]
        ymax = self._data[2][np.where(self._data[4]==np.max(self._data[4]))]
        zmax = self._data[3][np.where(self._data[4]==np.max(self._data[4]))]
        self.fig.suptitle("Maximum dose D(%d, %d, %d) = %2.2f (current %2.2f) " %(xmax, ymax, zmax, np.max(self._data[4]), np.max(av)))
        #self.axes.set_title("Maximum dose D(%d, %d, %d) = %2.2f (current %2.2f) " %(xmax, ymax, zmax, np.max(self._data[4]), np.max(av)))

        #self.axes.scatter(fx, fy, fz, c=fc)
        self.axes.scatter(ax, ay, az, c=ac, cmap=plt.jet())
        #plt.colorbar(p)
        self.canvas.draw()
        pass


if __name__ == '__main__':
    app = wx.PySimpleApp()
    app.frame = MainFrame()
    app.frame.Show()
    app.MainLoop()
    #x, y, z, v = readDataFile(r'D:\dev\data\cubes_out\cube1.txt')
    #print np.where(x>100), v[np.where(x>100)]


