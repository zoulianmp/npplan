# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 21.05.13
@summary: 
'''

import os
import dicom
import shutil

dirName = r'C:\Users\Chernukha\Pictures\DICOM\11070516\12390000'
moveDir = r'C:\Users\Chernukha\Pictures\DICOM\11070516'

for i in os.listdir(dirName):
  d = dicom.read_file(os.path.join(dirName, i))
  sv = str(d[(0x0008, 0x103e)].value)
  if not os.path.exists(os.path.join(moveDir, sv)):
    os.mkdir(os.path.join(moveDir, sv))
  shutil.copy( os.path.join(dirName, i),
                 os.path.join(moveDir, sv, i) )

  print i, d[(0x0008, 0x103e)]