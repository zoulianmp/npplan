# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 14.02.13
@summary: 
'''

import pp
import pickle
import time
import numpy as np
import itertools
import scipy
from scipy.spatial.distance import cdist

global minSourcesQuantity
minSourcesQuantity = 1
global maxSourcesQuantity
maxSourcesQuantity = 40
#maxSourcesQuantity = 20

global size
# весь объём, 5х5х5 см == 125 см^3
size = (5.0, 5.0, 5.0)

global singleSize
# объём одного источника
singleSize = (0.1, 0.1, 0.1)
#singleSize = (0.5, 0.5, 0.5)

allSize = size[0]/singleSize[0]*size[1]/singleSize[1]*size[2]/singleSize[2]

#print allSize

global grid
grid = np.array([0.0]*int(allSize))
#print grid.shape
grid = np.reshape(grid, (size[0]/singleSize[0], size[1]/singleSize[1], size[2]/singleSize[2]))
#print grid.shape

def resetGrid():
    global grid
    global restrictedCells
    del grid
    grid = np.array([0.0]*int(allSize))
    grid = np.reshape(grid, (size[0]/singleSize[0], size[1]/singleSize[1], size[2]/singleSize[2]))
    #restrictedCells = []

def calculateGridNumberBySingleCoord(singleCoord, singleSize):
    return np.round(singleCoord[0]/singleSize[0]), np.round(singleCoord[1]/singleSize[1]), np.round(singleCoord[2]/singleSize[2])

def makeSingleCoordGromGridNumber(gridNumber, singleSize):
    return gridNumber[0]*singleSize[0], gridNumber[1]*singleSize[1], gridNumber[2]*singleSize[2]

def setDoseInGridFromSingleSource(grid, singleCoords, singleSize):
    gridNumber = calculateGridNumberBySingleCoord(singleCoords, singleSize)
    shape = grid.shape()

    pass

global restrictedCells
restrictedCells = []

def calcFromSingle(grid, gridCoords, singleSize, doses, spheresRanges):
    x = gridCoords[0]
    y = gridCoords[1]
    z = gridCoords[2]

    ownCellCoords = makeSingleCoordGromGridNumber((x, y, z), singleSize)

    grid[x, y, z] += doses[0]
    for i in range(grid.shape[0]):
        for j in range(grid.shape[1]):
            for k in range(grid.shape[2]):
                targetCellCoords = makeSingleCoordGromGridNumber((i, j, k), singleSize)

                dose = func(pos=ownCellCoords, target=targetCellCoords, spheresRanges=spheresRanges, doses=doses)

                grid[i, j, k] += dose

    return grid
#print calculateGridNumberBySingleCoord((2, 0.4, 3))
#print makeSingleCoordGromGridNumber(calculateGridNumberBySingleCoord((2, 0.4, 3)))

def makeSpheresRanges():
    return np.arange(0.5, 5, 0.5)
'''
def initDoses():
    return np.array([
    9.67542E-01,
    1.66081E-01,
    6.37196E-02,
    3.35865E-02,
    2.05607E-02,
    1.37057E-02,
    9.60868E-03,
    6.92466E-03,
    5.01335E-03,
    ])'''
def initDoses():
    return np.array([
    4.29E+01,
    7.36E+00,
    2.82E+00,
    1.49E+00,
    9.10E-01,
    6.08E-01,
    4.26E-01,
    3.05E-01,
    2.21E-01,
    ])


global spheresRanges
spheresRanges = makeSpheresRanges()

global doses
doses = initDoses()
#print doses
#print len(doses)

def retVolumeBetweenSpheres(rad1, rad2):
    return 4*np.pi*rad2**3/3 - 4*np.pi*rad1**3/3

def func(pos=(0, 0, 0), target=(0, 0, 0), spheresRanges=[], doses=[]):
    found = -1
    dist = myDist(pos, target)
    if dist < spheresRanges[0]:
        return doses[0]
    for i in range(1, len(doses)):
        # print dist, i, spheresRanges[i]
        if spheresRanges[i-1] < dist < spheresRanges[i]:
            found = i
        pass
    #print 'FOUND: ', found
    if found == -1:
        return 0.0
    return doses[found]
    pass

def myDist(p1=(0.0, 0.0, 0.0), p2=(1.0, 1.0, 1.0)):
    return np.sqrt( (p1[0]-p2[0])**2 + (p1[1]-p2[1])**2 + (p1[2]-p2[2])**2)

def printGrid(grid):
    for i in range(grid.shape[0]):
        s = ''
        for j in range(grid.shape[1]):
            s = '%s %s' %(s, grid[i, j])
        print s

def gridSourceIterator(grid,):
    #global restrictedCells
    for i in range(grid.shape[0]):
        for j in range(grid.shape[1]):
            for k in range(grid.shape[2]):
                #if (i, j, k) not in restrictedCells:
                    #restrictedCells.append((i, j, k))
                yield (i, j, k)
                    #restrictedCells.remove((i, j, k))

def checkGrid(grid,):
    if (grid>=95).all() and (grid<=140).all():
        printGrid(savedGrid)
        print restrictedCells
        for i in restrictedCells:
            print makeSingleCoordGromGridNumber(i, singleSize)
        return True


def calcAllSingle(allSize, size, singleSize, doses, spheresRanges):
    grid = np.array([0.0]*int(allSize))
    grid = np.reshape(grid, (size[0]/singleSize[0], size[1]/singleSize[1], size[2]/singleSize[2]))

    savedSources = {}
    for cellSource in gridSourceIterator(grid):
        savedSources[cellSource] = calcFromSingle(grid, cellSource, singleSize, doses, spheresRanges)
        grid = np.array([0.0]*int(allSize))
        grid = np.reshape(grid, (size[0]/singleSize[0], size[1]/singleSize[1], size[2]/singleSize[2]))
    return savedSources

def calcAllPP():
    ppservers = ()

    ncpus = 6
    job_server = pp.Server(ncpus, ppservers=ppservers)

    start = time.time()

    job1 = job_server.submit(
        calcAllSingle,
        (allSize, size, singleSize, doses, spheresRanges),
        (
            gridSourceIterator,
            myDist,
            func,
            retVolumeBetweenSpheres,
            calcFromSingle,
            makeSingleCoordGromGridNumber,
            setDoseInGridFromSingleSource,
            calculateGridNumberBySingleCoord
            ),
        ("numpy as np","scipy"))

    # Retrieves the result calculated by job1
    # The value of job1() is the same as sum_primes(100)
    # If the job has not been finished yet, execution will wait here until result is available
    result = job1()

    print time.time()-start

    #f = open('gridData', 'w')
    #pickle.dump(result, f)
    #f.close()
    return result

def check(grid):
    #return True
    if (grid>140).any():
        return False
    if (grid<60).any():
        return False
    return True


def calcIntersections(gridData, minQuantity, maxQuantity, grid):
    f = open('gridDataValidKeys', 'w')
    valid = []
    lList = {}
    start = time.time()
    for i in range(minQuantity, maxQuantity):
        for keys in itertools.combinations(gridData.keys(), i):
            #print keys
            grid0 = np.copy(grid)
            for key in keys:
                grid0 += gridData[key]
            #lList[keys] = grid0
            #print grid0
            if check(grid0):
#                print keys
#                valid.append(keys)
                f.write('%s\n' %str(keys))
    print 'Intersections', time.time()-start
    f.close()
    return valid
    pass

def calcIntersectionsPP(data, minSourcesQuantity, maxSourcesQuantity):
    resetGrid()
    ppservers = ()

    ncpus = 6
    job_server = pp.Server(ncpus, ppservers=ppservers)

    start = time.time()

    job1 = job_server.submit(
        calcIntersections,
        (data, minSourcesQuantity, maxSourcesQuantity, grid),
        (
            check, 
        ),
        ("itertools", "numpy as np", )
    )

    print time.time()-start

    result = job1()
    print result

    f = open('gridValidData', 'w')
    pickle.dump(result, f)
    f.close()
    return result



if __name__ == '__main__':
    result = calcAllPP()
    #f = open('gridData', 'r')
    #data = pickle.load(f)
    #for i in data:
    #    data[i] = np.array(data[i])

    #print data
    #print result
#    valid = calcIntersections(data, 1, 5, grid)
    valid = calcIntersections(result, minSourcesQuantity, maxSourcesQuantity, grid)

    #calcIntersections(result, 1, 3)
    #calcIntersectionsPP(data, minSourcesQuantity, maxSourcesQuantity)
    #calcIntersectionsPP(result, minSourcesQuantity, maxSourcesQuantity)
    #print data == result
    #print data == result
    pass

