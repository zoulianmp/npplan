# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 14.04.14
@summary: 
'''

import re
#mob = re.search('\d', 'xdtwkeltjwlkejt7wthwk89lk')


def splitParticle(particleName):
    q = re.search('\d', particleName)
    if q is not None:
        return q.start()
    return None
    pass


filename = r'C:\Temp\geantProjects\Hadr03_carbon_plet_wt_build\Release_nokill\carbonWaterA.log'

stF = False
rls = []

for line in open(filename):
    if 'generated particles:' in line:
        stF = True
        continue
    if 'Momentum balance:' in line:
        stF = False
    if len(line) < 2:
        continue
    if stF:
        #print len(line)
        #print line
        rls.append(line)
    pass

print rls
lda = {}
allSum = 0

for ld in rls:
    lineData = ld.split()
    print lineData
    print splitParticle(lineData[0])
    part = lineData[0][:splitParticle(lineData[0])]
    if ':' in part:
        part = part[:-1]
    allSum += int(lineData[1])
    if part in lda:
        lda[part]['count'] += int(lineData[1])
        lda[part]['cc'].append(int(lineData[1]))
        lda[part]['spectrum'].append(float(lineData[4]))
    else:
        lda[part] = {
            'cc': [int(lineData[1])],
            'count': int(lineData[1]),
            'spectrum': [float(lineData[4])]
        }

for i in lda:
    lda[i]['percent'] = float(lda[i]['count']) / allSum
import pprint

pprint.pprint(lda)
#lda['K'] = {'count': 0}
#lda['Ar'] = {'count': 0}

for i in lda:
    #print ('%0.15f' % lda[i]['percent']).replace('.', ',')
    #print i, lda[i]['count'], lda[i]['percent']
    #print i
    print lda[i]['count']

print allSum