# -*- coding: utf8 -*-
#!/usr/bin/env python
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 05.09.13
@summary: 
'''



# This example reads a volume dataset, extracts two isosurfaces that
# represent the skin and bone, and then displays them.

import vtk
from vtk.util.misc import vtkGetDataRoot
#VTK_DATA_ROOT = vtkGetDataRoot()
#VTK_DATA_ROOT = 'f:\\VTKDATA'
VTK_DATA_ROOT = 'C:\\Users\\Public\\VTKData'

# Create the renderer, the render window, and the interactor. The
# renderer draws into the render window, the interactor enables mouse-
# and keyboard-based interaction with the scene.
aRenderer = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(aRenderer)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# The following reader is used to read a series of 2D slices (images)
# that compose the volume. The slice dimensions are set, and the
# pixel spacing. The data Endianness must also be specified. The reader
# usese the FilePrefix in combination with the slice number to construct
# filenames using the format FilePrefix.%d. (In this case the FilePrefix
# is the root name of the file: quarter.)
v16 = vtk.vtkVolume16Reader()
v16.SetDataDimensions(64, 64)
v16.SetDataByteOrderToLittleEndian()
v16.SetFilePrefix(VTK_DATA_ROOT + "/Data/headsq/quarter")
v16.SetImageRange(1, 93)
v16.SetDataSpacing(3.2, 3.2, 1.5)

# fix 05.09.2013 started
obliqueElements3 = [
    -0.18893742443639305, -0.9352972578934333, -0.29920175304359226, 0,
    0.9379375565914191, -0.26211588383052414, 0.22708677499813312, 0,
    -0.29081916990228146, -0.23772737078586345, 0.9267739247500336, 0,
    0, 0, 0, 1
]

v16.Update()

#extent = reader.GetOutputInformation(0).Get(vtk.vtkStreamingDemandDrivenPipeline.WHOLE_EXTENT())
extent = v16.GetOutput().GetExtent()
spacing = v16.GetOutput().GetSpacing()
origin = v16.GetOutput().GetOrigin()

print extent, spacing, origin

center = [0, 0, 0]
center[0] = origin[0] + spacing[0] * 0.5 * (extent[0] + extent[1])
center[1] = origin[1] + spacing[1] * 0.5 * (extent[2] + extent[3])
center[2] = origin[2] + spacing[2] * 0.5 * (extent[4] + extent[5])

resliceAxes = vtk.vtkMatrix4x4()
#resliceAxes.DeepCopy(sagittalElements)
resliceAxes.DeepCopy(obliqueElements3)
resliceAxes.Invert()  # ??

resliceAxes.SetElement(0, 3, center[0])
#resliceAxes.SetElement(1, 3, center[1])
resliceAxes.SetElement(1, 3, 15)
resliceAxes.SetElement(2, 3, center[2])

reslice = vtk.vtkImageReslice()
reslice.SetInputConnection(v16.GetOutputPort())
reslice.SetOutputDimensionality(3)
reslice.SetResliceAxes(resliceAxes)
reslice.SetInterpolationModeToLinear()
reslice.Update()
# fix 05.09.2013 end 1

# An isosurface, or contour value of 500 is known to correspond to the
# skin of the patient. Once generated, a vtkPolyDataNormals filter is
# is used to create normals for smooth surface shading during rendering.
# The triangle stripper is used to create triangle strips from the
# isosurface these render much faster on may systems.
skinExtractor = vtk.vtkContourFilter()
skinExtractor.SetInput(reslice.GetOutput())  # fix 05.09.2013
skinExtractor.SetValue(0, 500)
skinNormals = vtk.vtkPolyDataNormals()
skinNormals.SetInput(skinExtractor.GetOutput())
skinNormals.SetFeatureAngle(60.0)
skinStripper = vtk.vtkStripper()
skinStripper.SetInput(skinNormals.GetOutput())
skinMapper = vtk.vtkPolyDataMapper()
skinMapper.SetInput(skinStripper.GetOutput())
skinMapper.ScalarVisibilityOff()
skin = vtk.vtkActor()
skin.SetMapper(skinMapper)
skin.GetProperty().SetDiffuseColor(1, .49, .25)
skin.GetProperty().SetSpecular(.3)
skin.GetProperty().SetSpecularPower(20)

# An isosurface, or contour value of 1150 is known to correspond to the
# skin of the patient. Once generated, a vtkPolyDataNormals filter is
# is used to create normals for smooth surface shading during rendering.
# The triangle stripper is used to create triangle strips from the
# isosurface these render much faster on may systems.
boneExtractor = vtk.vtkContourFilter()
boneExtractor.SetInput(v16.GetOutput())
boneExtractor.SetValue(0, 1150)
boneNormals = vtk.vtkPolyDataNormals()
boneNormals.SetInput(boneExtractor.GetOutput())
boneNormals.SetFeatureAngle(60.0)
boneStripper = vtk.vtkStripper()
boneStripper.SetInput(boneNormals.GetOutput())
boneMapper = vtk.vtkPolyDataMapper()
boneMapper.SetInput(boneStripper.GetOutput())
boneMapper.ScalarVisibilityOff()
bone = vtk.vtkActor()
bone.SetMapper(boneMapper)
bone.GetProperty().SetDiffuseColor(1, 1, .9412)

# An outline provides context around the data.
outlineData = vtk.vtkOutlineFilter()
#outlineData.SetInput(skinNormals.GetOutput())   # fix 05.09.2013
outlineData.SetInput(reslice.GetOutput())   # fix 05.09.2013
mapOutline = vtk.vtkPolyDataMapper()
mapOutline.SetInput(outlineData.GetOutput())
outline = vtk.vtkActor()
outline.SetMapper(mapOutline)
outline.GetProperty().SetColor(0, 0, 0)
print outlineData
print mapOutline
print outline

outlineData2 = vtk.vtkOutlineFilter()
outlineData2.SetInput(v16.GetOutput())   # fix 05.09.2013
mapOutline2 = vtk.vtkPolyDataMapper()
mapOutline2.SetInput(outlineData2.GetOutput())
outline2 = vtk.vtkActor()
outline2.SetMapper(mapOutline2)
outline2.GetProperty().SetColor(0, 0, 0)

# It is convenient to create an initial view of the data. The FocalPoint
# and Position form a vector direction. Later on (ResetCamera() method)
# this vector is used to position the camera to look at the data in
# this direction.
aCamera = vtk.vtkCamera()
aCamera.SetViewUp(0, 0, -1)
aCamera.SetPosition(0, 1, 0)
aCamera.SetFocalPoint(0, 0, 0)
aCamera.ComputeViewPlaneNormal()

# Actors are added to the renderer. An initial camera view is created.
# The Dolly() method moves the camera towards the FocalPoint,
# thereby enlarging the image.
aRenderer.AddActor(outline)
aRenderer.AddActor(outline2)
aRenderer.AddActor(skin)
aRenderer.AddActor(bone)
aRenderer.SetActiveCamera(aCamera)
aRenderer.ResetCamera()
aCamera.Dolly(1.5)

# Set a background color for the renderer and set the size of the
# render window (expressed in pixels).
aRenderer.SetBackground(1, 1, 1)
renWin.SetSize(640, 480)

# Note that when camera movement occurs (as it does in the Dolly()
# method), the clipping planes often need adjusting. Clipping planes
# consist of two planes: near and far along the view direction. The
# near plane clips out objects in front of the plane the far plane
# clips out objects behind the plane. This way only what is drawn
# between the planes is actually rendered.
aRenderer.ResetCameraClippingRange()

# Interact with the data.
iren.Initialize()
renWin.Render()
iren.Start()
