/*
 * Base file for geant projects
 * Detector construction definition
 * @author: mrxak
 * (c) MRRC, 2013
*/

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4NistManager.hh"
#include "G4ThreeVector.hh"
//#include parametrised

class G4Material;
class G4Box;
class G4LogicalVolume;
//class G4ThreeVector;

class DetectorMessenger;

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    DetectorConstruction();
    ~DetectorConstruction();

    G4VPhysicalVolume* physWorld;

    G4VPhysicalVolume* Construct();
  protected:
    void defineMaterials(void);
    void setupDetectors(G4LogicalVolume*, G4double, G4String);
  private:
    G4double worldSizeXYZ;
    G4Material *fBone;
    G4Material *fTissue;
};

#endif