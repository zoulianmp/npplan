# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 20.01.14
@summary: 
'''

# @todo: abstract this
import os
import shutil

releaseDir = r'C:\Temp\geantProjects\testSyncProtons_build\Stable3'
releaseFile = 'testSyncProtons.exe'
releasePath = os.path.join(releaseDir, releaseFile)

nonChangeable = []
for i in os.listdir(releaseDir):
    if i != releaseFile:
        nonChangeable.append(os.path.join(releaseDir, i))

targetDir = r'C:\Temp\geantProjects\testSyncProtons_build\expSeries2'
nps = 1e7
#nps = 100
energyString = "MeV"
particleData = ("proton", )


class singleData(object):
    def __init__(self,
                 targetSeriesDir,
                 eId,
                 energy=5.4,
                 expType=0,
                 ):
        global nps
        #if os.path.exists(targetInstanceDir)
        self._cDir = _cDir = os.path.join(targetSeriesDir, eId)
        if not os.path.exists(_cDir):
            os.mkdir(_cDir)

        f = open(os.path.join(_cDir, 'run.mac'), 'w')
        f.write("/run/verbose 1\n")
        f.write("/event/verbose 0\n")
        f.write("/tracking/verbose 0\n")
        f.write("/process/list\n")
        #f.write("/gun/particle ion\n")
        #f.write("/gun/ion 6 12\n")
        f.write("/gun/particle %s\n" % particleData[0])
        if 3 == len(particleData):
            f.write("/gun/ion %d %d\n" % (particleData[1], particleData[2]))
        f.write("/gun/energy %f %s\n" % (energy, energyString))
        f.write("/run/beamOn %d\n" % nps)

        shutil.copy(releasePath, _cDir)
        for i in nonChangeable:
            shutil.copy(i, _cDir)

    def getDir(self):
        return self._cDir


class singleSeries(object):
    def __init__(self, seriesDir, energy=5.4, expType=1):
        self._cDir = _cDir = os.path.join(targetDir, seriesDir)
        os.mkdir(_cDir)

        self._f = open(os.path.join(_cDir, 'runAll.bat'), 'w')
        self._dName = 0

        if isinstance(energy, list):
            for i in energy:
                self.appendAndWrite(i, expType)
        elif isinstance(expType, list):
            for i in expType:
                self.appendAndWrite(energy, i)
        self._f.close()
        pass

    def appendAndWrite(self, energy=5.4, expType=1):
        c = singleData(self._cDir, 'exp%d' % self._dName,
               energy, expType)
        self._f.write("cd %s\n" % c.getDir())
        self._f.write("%s -m run.mac -ph %d > run.log\n" % (releaseFile, expType))
        self._dName += 1

    def getDir(self):
        return self._cDir

if __name__ == '__main__':
    print nonChangeable

    #os.mkdir(os.path.join(targetDir, 'test1'))
    #singleSeries('test1')
    f = open(os.path.join(targetDir, 'runAll.bat'), 'w')
    s = singleSeries('et1',
                 energy=[50, 100, 150, 200, 250],
                 expType=-1)
    f.write("start %s\n" % os.path.join(s.getDir(), 'runAll.bat'))
    s = singleSeries('et2',
                     energy=[50, 100, 150, 200, 250],
                 expType=-2)
    f.write("start %s\n" % os.path.join(s.getDir(), 'runAll.bat'))
    s = singleSeries('et3',
                     energy=[50, 100, 150, 200, 250],
                 expType=-3)
    f.write("start %s\n" % os.path.join(s.getDir(), 'runAll.bat'))

    f.close()