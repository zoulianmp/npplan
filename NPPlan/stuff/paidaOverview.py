# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 13.12.13
@summary: 
'''

from paida import *

### In AIDA system, an analysisFactory creates some treeFactories,
### and a treeFactory creates some trees.
analysisFactory = IAnalysisFactory.create()
treeFactory = analysisFactory.createTreeFactory()
#tree = treeFactory.create()
#tree = treeFactory.l
### The tree with zipped XML format and allowing to create new r/w mode file.
#tree = treeFactory.create('exampleBasic.aida', 'xml', False, True, 'compress=yes')
tree = treeFactory.create(r'C:\Temp\geantProjects\Hadr03_build\Release_FTFP\neutron.xml', 'xml', True, False)#, 'compress=yes')

### Creating a histogram.
histogramFactory = analysisFactory.createHistogramFactory(tree)
h1d = histogramFactory.createHistogram1D('name', 'title', 20, 0.0, 20.0)
h1d = tree.find("1")


### Plotting the histogram.
### analysisFactory -> plotterFactory -> plotter -> region(s)
plotterFactory = analysisFactory.createPlotterFactory()
plotter = plotterFactory.create('PAIDA Plotter')
region = plotter.createRegion()
region.plot(h1d)

### Save plots to a file.
#plotter.writeToFile('sampleBasic.ps')

### Wait.
dummy = raw_input('Hit any key.')