#ifndef StackingAction_H
#define StackingAction_H 1

#include "G4UserStackingAction.hh"
#include "globals.hh"
class G4Track;

class StackingAction: public G4UserStackingAction
{
  public:
    StackingAction();
    ~StackingAction();

  public:
    void PrepareNewEvent();
    G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);
    
    G4int emitedPhotons;
};

#endif

