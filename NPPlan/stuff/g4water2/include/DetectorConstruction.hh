#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1

#include "G4VUserDetectorConstruction.hh"

#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4NistManager.hh"
#include "G4SDManager.hh"
#include <fstream> 

class DetectorConstruction: public G4VUserDetectorConstruction
{                 
  public:
    DetectorConstruction();
    ~DetectorConstruction();
    
	G4int getExpSeriesLength(void);
	void  getExpContainerData(void);
	void  getExpType(void);
	void  getBigBoxData(void);

    G4VPhysicalVolume* Construct();
    
    G4int nLayers;          // ����� ���� � ��������� �������
  private:
	G4LogicalVolume* addVoxelsLayer(G4LogicalVolume*, G4ThreeVector, G4String, G4Material*, G4double, G4double, G4double, G4int, G4SDManager*);
	G4LogicalVolume* addZVoxelsLayer(G4LogicalVolume*, G4ThreeVector, G4String, G4Material*, G4double, G4double, G4double, G4int, G4SDManager*);
	G4LogicalVolume* addVoxelsLayer(G4LogicalVolume*, G4ThreeVector, G4String, G4Material*, G4double, G4double, G4double, G4int, G4SDManager*, G4bool);
	G4LogicalVolume* addExpContainer(G4LogicalVolume*, G4double, G4String, G4SDManager*, G4bool);
	void addExpBigContainer(G4LogicalVolume*, G4SDManager*);
	void addExpBigContainerNormed(G4LogicalVolume*, G4SDManager*, G4double);
	G4LogicalVolume* addExpSeriesContainers(G4LogicalVolume*, G4SDManager*, G4int, G4double, G4String);
	G4NistManager* nistMan;

	G4double plexLastLayerPosition;

	G4double bigBoxSzX;
	G4double bigBoxSzY;
	G4double bigBoxSzZ;

	G4int expLen;			// ���������� ����������������� ������
	G4double expBigBoxX;    // ������ ������������������ ����� �� X
	G4double expBigBoxY;    // ������ ������������������ ����� �� Y
	G4double expBigBoxZ;    // ������ ������������������ ����� �� Z
	G4double expFrontForL;  // ������� �������� ������ ������������������ �����
	G4double expFrontBacL;  // ������� ������ ������ ������������������ �����
	G4double expBigBoxInnerZ; // (�����������) ���������� �������
	G4double airGap;        // ������� ��������� ��������� ����� �������
	G4int expType;          // ��� ������������, 1 - ��������� ���� ��� �������, 2 - ��� ����� ���� � ����� � ������� ����� ����� ����
	G4int glassShiftLength; // ���������� ���� ������ ����� �������
	G4int voxelsNumber;     // ����� ���� ��� ���������� ���������� � �����
	G4double photoWidth;	// ������� ��������� ����� 2 ������� (��� expType == 2)
	G4int outterMaterialType; // ������� ��������, 0 - ����, 1 - ������
};

#endif

