#ifndef DetectorSDSimple_h
#define DetectorSDSimple_h 1

#include "G4VSensitiveDetector.hh"
class G4Step;
class RunAction;

class DetectorSDSimple: public G4VSensitiveDetector 
{
  public:
    DetectorSDSimple(G4String, G4int);
	DetectorSDSimple(G4String, G4int,G4int);
    ~DetectorSDSimple();

    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);
    
  private:
	G4String sName;
    RunAction* runAction;
	G4int sVc;
	// summary dep
	G4double *edep;
	G4int *eCount;

	// carbon dep
	G4double *edepCarbon;
	G4int *eCountCarbon;

	// proton+deuteron+triton dep
	G4double *edepProton;
	G4int *eCountProton;

	// alpha dep
	G4double *edepAlpha;
	G4int *eCountAlpha;

	std::ofstream fileoutPart;
	std::ofstream fileoutCPart;
	std::ofstream fileoutPartCarbon;
	std::ofstream fileoutCPartCarbon;
	std::ofstream fileoutPartProton;
	std::ofstream fileoutCPartProton;
	std::ofstream fileoutPartAlpha;
	std::ofstream fileoutCPartAlpha;
	G4int targetCopyLayerPosition;
};

#endif
