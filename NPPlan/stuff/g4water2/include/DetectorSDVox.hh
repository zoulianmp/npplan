#ifndef DetectorSDVox_h
#define DetectorSDVox_h 1
#include "G4VSensitiveDetector.hh"
#include <map>

class G4Step;
class RunAction;
class DetectorSDVox: public G4VSensitiveDetector 
{
	public:
		DetectorSDVox(G4String, G4int);
		~DetectorSDVox();

		void Initialize(G4HCofThisEvent*);
		G4bool ProcessHits(G4Step*, G4TouchableHistory*);
		void EndOfEvent(G4HCofThisEvent*);
	private:
		RunAction* runAction;
		G4String detName;
		G4int nLayers;

		G4double **mat;
		G4int **matC;
		G4double enSum;
		std::ofstream fileoutPart;
		std::ofstream fileoutCPart;

		void _createStorage(void);
};
#endif