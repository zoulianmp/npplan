#ifndef DetectorSDGlass_h
#define DetectorSDGlass_h 1
#include "G4VSensitiveDetector.hh"
class G4Step;
class RunAction;

//std::multimap<std::string, int> mParticlesMap;
//std::vector<std::string> mParticlesList;
//int jCounter;
#include <map>
#include <string>

class DetectorSDGlass: public G4VSensitiveDetector 
{
  public:
    DetectorSDGlass(G4String);
    ~DetectorSDGlass();

    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);
	std::multimap<std::string, G4double> ParticlesAppenderAnyDouble(std::multimap<std::string, G4double>, std::string, G4double);
	std::multimap<std::string, unsigned long> ParticlesAppenderAnyInt(std::multimap<std::string, unsigned long>, std::string, unsigned long);
	G4double GetValueDouble(std::multimap<std::string, G4double> obj, std::string pName);
	unsigned long GetValueInt(std::multimap<std::string, unsigned long> obj, std::string pName);
    
  private:
    RunAction* runAction;
    G4int nLayers;
    G4double* detEnergy;
	std::string fname;
	std::vector<std::string> mParticlesList;
	std::multimap<std::string, G4double> mParticlesNums;
	std::multimap<std::string, G4double> mParticlesEnergies;
	std::multimap<std::string, G4double> mParticlesKinetic;
};

#endif