#include "DetectorConstruction.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "PhysicsList.hh"
#include "StackingAction.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#include "Randomize.hh"
#include <ctime>

int main(int argc, char** argv)
{
 // CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
 CLHEP::HepRandom::setTheSeed(time(NULL));
 //CLHEP::HepRandom::setTheSeed(time(0)+getpid());
  G4RunManager* runManager = new G4RunManager;


G4VModularPhysicsList* physList = new PhysicsList;
//physList -> SetDefaultCutValue(5*mm);


  runManager->SetUserInitialization(physList);

  runManager->SetUserInitialization(new DetectorConstruction);
  runManager->SetUserAction(new PrimaryGeneratorAction);

  runManager->SetUserAction(new RunAction);
  runManager->SetUserAction(new EventAction);
  runManager->SetUserAction(new StackingAction);

  runManager->Initialize();


#ifdef G4VIS_USE
  // Initialize visualization
  G4VisManager* visManager = new G4VisExecutive;
  // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
  // G4VisManager* visManager = new G4VisExecutive("Quiet");
  visManager->Initialize();
#endif

  // Get the pointer to the User Interface manager
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  if (argc!=1) {
    // batch mode
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UImanager->ApplyCommand(command+fileName);
  }
  else {
    // interactive mode : define UI session
#ifdef G4UI_USE
    G4UIExecutive* ui = new G4UIExecutive(argc, argv);
  #ifdef G4VIS_USE
    UImanager->ApplyCommand("/control/execute vis.mac"); 
  #else
    UImanager->ApplyCommand("/control/execute init.mac"); 
  #endif
    ui->SessionStart();
    delete ui;
#endif
  }
 
#ifdef G4VIS_USE
  delete visManager;
#endif
  delete runManager;
  
  return 0;
}
