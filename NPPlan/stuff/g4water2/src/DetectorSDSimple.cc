#include "DetectorSDSimple.hh"
#include "RunAction.hh"
#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4Step.hh"


DetectorSDSimple::DetectorSDSimple(G4String name, G4int vC): G4VSensitiveDetector(name)
{
	sName = name;
	runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
	sVc = vC;
	edep = new G4double[sVc];
	eCount = new G4int[sVc];
	edepCarbon = new G4double[sVc];
	eCountCarbon = new G4int[sVc];
	edepProton = new G4double[sVc];
	eCountProton = new G4int[sVc];
	edepAlpha = new G4double[sVc];
	eCountAlpha = new G4int[sVc];
	for (int i = 0; i < sVc; i++) {
		edep[i] = 0.0;
		eCount[i] = 0;
		edepProton[i] = 0.0;
		eCountProton[i] = 0;
		edepCarbon[i] = 0.0;
		eCountCarbon[i] = 0;
		edepAlpha[i] = 0.0;
		eCountAlpha[i] = 0;
	}
	targetCopyLayerPosition = 0;
}

DetectorSDSimple::DetectorSDSimple(G4String name, G4int vC, G4int tc): G4VSensitiveDetector(name)
{
	sName = name;
	runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
	sVc = vC;
	edep = new G4double[sVc];
	eCount = new G4int[sVc];
	edepCarbon = new G4double[sVc];
	eCountCarbon = new G4int[sVc];
	edepProton = new G4double[sVc];
	eCountProton = new G4int[sVc];
	edepAlpha = new G4double[sVc];
	eCountAlpha = new G4int[sVc];
	for (int i = 0; i < sVc; i++) {
		edep[i] = 0.0;
		eCount[i] = 0;
		edepProton[i] = 0.0;
		eCountProton[i] = 0;
		edepCarbon[i] = 0.0;
		eCountCarbon[i] = 0;
		edepAlpha[i] = 0.0;
		eCountAlpha[i] = 0;
	}
	targetCopyLayerPosition = tc;
}

DetectorSDSimple::~DetectorSDSimple()
{
}

void DetectorSDSimple::Initialize(G4HCofThisEvent*)
{
	G4String fnamePart = "edep_all_"+sName+".out";
	G4String fnameCPart = "c_all_"+sName+".out";
	G4String fnamePartCarbon = "edep_carbon_"+sName+".out";
	G4String fnameCPartCarbon = "c_carbon_"+sName+".out";
	G4String fnamePartProton = "edep_proton_"+sName+".out";
	G4String fnameCPartProton = "c_proton_"+sName+".out";
	G4String fnamePartAlpha = "edep_alpha_"+sName+".out";
	G4String fnameCPartAlpha = "c_alpha_"+sName+".out";
	fileoutPart.open(fnamePart);
	fileoutCPart.open(fnameCPart);
	fileoutPartCarbon.open(fnamePartCarbon);
	fileoutCPartCarbon.open(fnameCPartCarbon);
	fileoutPartProton.open(fnamePartProton);
	fileoutCPartProton.open(fnameCPartProton);
	fileoutPartAlpha.open(fnamePartAlpha);
	fileoutCPartAlpha.open(fnameCPartAlpha);
}

G4bool DetectorSDSimple::ProcessHits(G4Step* step, G4TouchableHistory*)
{
	G4StepPoint* prePoint = step->GetPreStepPoint();
	G4TouchableHandle touchable = prePoint->GetTouchableHandle();
	G4String name = step->GetTrack()->GetDefinition()->GetParticleName();

	G4int cLayer = touchable->GetCopyNumber(targetCopyLayerPosition);

	edep[cLayer] += step->GetTotalEnergyDeposit();
	eCount[cLayer] += 1;

	if (name=="C12[0.0]") {
		edepCarbon[cLayer] += step->GetTotalEnergyDeposit();
		eCountCarbon[cLayer] += 1;
	}

	if (name=="alpha") {
		edepAlpha[cLayer] += step->GetTotalEnergyDeposit();
		eCountAlpha[cLayer] += 1;
	}

	if ((name=="proton") || (name=="deuteron") || (name=="triton")) {
		edepProton[cLayer] += step->GetTotalEnergyDeposit();
		eCountProton[cLayer] += 1;
	}

	/*G4cout<<" "<<layer0
		<<" "<<layer1
		<<" "<<layer2
		<<" "<<layer3
		<<"  energy: "<<step->GetTotalEnergyDeposit()
		<<G4endl;*/
	return true;
}

void DetectorSDSimple::EndOfEvent(G4HCofThisEvent*)
{
	G4double sum;
	sum = 0.0;
	for (int i = 0; i < sVc; i++) {
		fileoutPart<<"Layer "<<i<<": "<<edep[i]<<G4endl;
		fileoutCPart<<"Layer "<<i<<": "<<eCount[i]<<G4endl;
		fileoutPartCarbon<<"Layer "<<i<<": "<<edepCarbon[i]<<G4endl;
		fileoutCPartCarbon<<"Layer "<<i<<": "<<eCountCarbon[i]<<G4endl;
		fileoutPartProton<<"Layer "<<i<<": "<<edepProton[i]<<G4endl;
		fileoutCPartProton<<"Layer "<<i<<": "<<eCountProton[i]<<G4endl;
		fileoutPartAlpha<<"Layer "<<i<<": "<<edepAlpha[i]<<G4endl;
		fileoutCPartAlpha<<"Layer "<<i<<": "<<eCountAlpha[i]<<G4endl;
	}
	fileoutPart.close();
	fileoutCPart.close();
	fileoutPartCarbon.close();
	fileoutCPartCarbon.close();
	fileoutPartProton.close();
	fileoutCPartProton.close();
	fileoutPartAlpha.close();
	fileoutCPartAlpha.close();
}

