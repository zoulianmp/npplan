//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: ChamberParameterisation.cc 69899 2013-05-17 10:05:33Z gcosmo $
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "ChamberParameterisation.hh"

#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Box.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/*
ChamberParameterisation::ChamberParameterisation(  
        G4int    NoChambers, 
        G4double startZ,          //  Z of center of first 
        G4double spacingZ,        //  Z spacing of centers
        G4double widthChamber, 
        G4double lengthInitial, 
        G4double lengthFinal )
{
   fNoChambers =  NoChambers; 
   fStartZ     =  startZ; 
   fHalfWidth  =  widthChamber*0.5;
   fSpacing    =  spacingZ;
   fHalfLengthFirst = 0.5 * lengthInitial; 
   // fHalfLengthLast = lengthFinal;
   if( NoChambers > 0 ){
      fHalfLengthIncr =  0.5 * (lengthFinal-lengthInitial)/NoChambers;
      if (spacingZ < widthChamber) {
       G4Exception("ChamberParameterisation::ChamberParameterisation()",
                   "InvalidSetup", FatalException,
                   "Width>Spacing");
      }
   }
   
}*/

ChamberParameterisation::ChamberParameterisation(G4int _noChambers, 
		  G4double _bigBoxSzZ,
		  G4double _expBigBoxInnerZ,
		  G4double _szX,
		  G4double _szY,
		  G4double _szZ,
		  G4int _vtype
		  )
{
	NoChambers = _noChambers;
	bigBoxSzZ = _bigBoxSzZ;
	expBigBoxInnerZ = _expBigBoxInnerZ;
	szX = _szX;
	szY = _szY;
	szZ = _szZ;
	vType = _vtype;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ChamberParameterisation::~ChamberParameterisation()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ChamberParameterisation::ComputeTransformation
(const G4int copyNo, G4VPhysicalVolume* physVol) const
{
//G4double      Zposition= fStartZ + (copyNo+1) * fSpacing;
	G4double      pos;
	if (1 == vType) {
		pos = -1.0 * (bigBoxSzZ / 2) + 1.5*(expBigBoxInnerZ)*copyNo;// + expBigBoxInnerZ / 2;
	} else if (3 != vType) {
//		pos = -1.0 * (bigBoxSzZ / 2) + expBigBoxInnerZ*copyNo + expBigBoxInnerZ / 2 + expBigBoxInnerZ / 2 + 1.2*mm;
		pos = -1.0 * (bigBoxSzZ / 2) + (szZ / 2) + szZ*copyNo;
	} else {
		if (copyNo == 0) {
			pos = 2;
		} else if (copyNo == 1) {
			pos = 8;
		} else if (copyNo == 2) {
			pos = 15.5;
		} else {
			pos = 15.5 + 7.5 + (copyNo-3)*1.5;
		}
		pos *= cm;
		pos += -0.5*(szZ*NoChambers);
	}
//G4cout<<"pos: "<<pos<<G4endl;
  G4ThreeVector origin(0,0,pos);
  physVol->SetTranslation(origin);
  physVol->SetRotation(0);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ChamberParameterisation::ComputeDimensions
(G4Box& trackerChamber, const G4int copyNo, const G4VPhysicalVolume*) const
{
  //G4double  halfLength= fHalfLengthFirst + copyNo * fHalfLengthIncr;
  //trackerChamber.SetXHalfLength(halfLength);
  //trackerChamber.SetYHalfLength(halfLength);
  //trackerChamber.SetZHalfLength(fHalfWidth);
  trackerChamber.SetXHalfLength(szX / 2);
  trackerChamber.SetYHalfLength(szY / 2);
  trackerChamber.SetZHalfLength(szZ / 2);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
