#include "StackingAction.hh"

#include "G4Track.hh"

StackingAction::StackingAction() {}

StackingAction::~StackingAction() {}

void StackingAction::PrepareNewEvent()
{
  emitedPhotons = 0;
}

G4ClassificationOfNewTrack StackingAction::ClassifyNewTrack(const G4Track* track)
{
  G4String particleName = track->GetDefinition()->GetParticleName();

  if (particleName == "opticalphoton") {
    emitedPhotons++;

    // уничтожаем фотон
    return fKill;
  }
  
  return fWaiting;
}
