#include "DetectorSDGlass.hh"
#include "RunAction.hh"
#include "DetectorConstruction.hh"

#include <map>
#include <string>
#include <vector>
#include "G4RunManager.hh"
#include "G4Step.hh"

DetectorSDGlass::DetectorSDGlass(G4String name): G4VSensitiveDetector(name)
{
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();

  mParticlesEnergies.clear();
  mParticlesNums.clear();
  mParticlesKinetic.clear();
  mParticlesList.clear();

  fname = (std::string)name;
}

DetectorSDGlass::~DetectorSDGlass()
{
  delete [] detEnergy;
}

void DetectorSDGlass::Initialize(G4HCofThisEvent*)
{
	mParticlesEnergies.clear();
	mParticlesNums.clear();
	mParticlesKinetic.clear();
	mParticlesList.clear();
}

std::multimap<std::string, G4double> DetectorSDGlass::ParticlesAppenderAnyDouble(std::multimap<std::string, G4double> obj, std::string pName, G4double value) {
	if (obj.count(pName) == 0) {
		obj.insert(std::pair<std::string, G4double>(pName, value));
	} else {
		obj.find(pName)->second += value;
	}
	return obj;
}

std::multimap<std::string, unsigned long> DetectorSDGlass::ParticlesAppenderAnyInt(std::multimap<std::string, unsigned long> obj, std::string pName, unsigned long value) {
	if (obj.count(pName) == 0) {
		obj.insert(std::pair<std::string, G4double>(pName, value));
	} else {
		obj.find(pName)->second += value;
	}
	return obj;
}

G4double DetectorSDGlass::GetValueDouble(std::multimap<std::string, G4double> obj, std::string pName) {
	if (obj.count(pName) == 0) {
		return 0.0;
	} else {
		return obj.find(pName)->second;
	}
}

unsigned long DetectorSDGlass::GetValueInt(std::multimap<std::string, unsigned long> obj, std::string pName) {
	if (obj.count(pName) == 0) {
		return 0;
	} else {
		return obj.find(pName)->second;
	}
}

G4bool DetectorSDGlass::ProcessHits(G4Step* step, G4TouchableHistory*)
{
  G4StepPoint* prePoint = step->GetPreStepPoint();
  G4TouchableHandle touchable = prePoint->GetTouchableHandle();
  G4String name = step->GetTrack()->GetDefinition()->GetParticleName();
  G4int parentTrackId = step->GetTrack()->GetParentID();
  G4double edep = step->GetTotalEnergyDeposit();

  //step->GetDeltaEnergy
  G4cout<<step->GetPreStepPoint()->GetKineticEnergy()<<" "<<
	  step->GetPostStepPoint()->GetKineticEnergy()<<" "<<
	  step->GetPostStepPoint()->GetKineticEnergy() - step->GetPreStepPoint()->GetKineticEnergy()<<G4endl;
  G4cout<<edep<<G4endl;

  if(std::find(mParticlesList.begin(), mParticlesList.end(), name) != mParticlesList.end()) {
    
  } else {
	  mParticlesList.push_back(name);
  }

  mParticlesEnergies = ParticlesAppenderAnyDouble(mParticlesEnergies, (std::string)name, edep);
  mParticlesKinetic = ParticlesAppenderAnyDouble(mParticlesKinetic, (std::string)name, prePoint->GetKineticEnergy());
  mParticlesNums = ParticlesAppenderAnyDouble(mParticlesNums, (std::string)name, 1.0);

  return true;
}

void DetectorSDGlass::EndOfEvent(G4HCofThisEvent*)
{
	for (std::vector<std::string>::const_iterator it = mParticlesList.begin(); it != mParticlesList.end(); ++it) {
		//G4cout<<"Particle: "<< *it << "Kinetic sum " << GetValueDouble(mParticlesKinetic, *it) <<
//			"Dep " << GetValueDouble(mParticlesEnergies, *it)<<
			//"Num " << GetValueDouble(mParticlesNums, *it)<<G4endl;
		runAction->GlassDataAppender(*it, 
			GetValueDouble(mParticlesKinetic, *it),
			GetValueDouble(mParticlesEnergies, *it),
			GetValueDouble(mParticlesNums, *it)
			);
//cout << *it << ' ';
	   //G4cout<<*it<<": "<<mParticlesMap.count((*it))<<G4endl;
	   //runAction->ParticlesMapAppender((*it), mParticlesMap.count((*it)));
	   //(*it)
	}
}