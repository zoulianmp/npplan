#include "EventAction.hh"
#include "RunAction.hh"
#include "StackingAction.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"

EventAction::EventAction() {}

EventAction::~EventAction() {}

void EventAction::EndOfEventAction(const G4Event* event)
{
  // в конце каждого события
  RunAction* runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
  StackingAction* stackingAction = (StackingAction*) G4RunManager::GetRunManager()->GetUserStackingAction();
  
  // набираем гистограмму испущенных фотонов
  runAction->FillEmitedHist(stackingAction->emitedPhotons);
  
  // отображаем прогресс моделирования
  runAction->DisplayProgress(event->GetEventID());
}

