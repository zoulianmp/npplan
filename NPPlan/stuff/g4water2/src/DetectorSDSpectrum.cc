#include "DetectorSDSpectrum.hh"
#include "RunAction.hh"
#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4Step.hh"

#include <map>

DetectorSDSpectrum::DetectorSDSpectrum(G4String name): G4VSensitiveDetector(name)
{
	runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
	lowLimit = 0;
	highLimit = 100;
	spStep = 1;
	detName = name;
	_createStorage();
	useSide = 0;
}

DetectorSDSpectrum::DetectorSDSpectrum(G4String name, G4double _lowLimit, G4double _highLimit, G4double _spStep): G4VSensitiveDetector(name)
{
	runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
	lowLimit = _lowLimit;
	highLimit = _highLimit;
	spStep = _spStep;
	detName = name;
	_createStorage();
	useSide = 0;
}

DetectorSDSpectrum::DetectorSDSpectrum(G4String name, G4double _lowLimit, G4double _highLimit, G4double _spStep, G4int _useSide): G4VSensitiveDetector(name)
{
	runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
	lowLimit = _lowLimit;
	highLimit = _highLimit;
	spStep = _spStep;
	detName = name;
	_createStorage();
	useSide = _useSide;
}

void DetectorSDSpectrum::_createStorage(void) 
{
	// �������� ������ ��� ������ ��� �������� ������
	// �� �������
	G4double gapsNum = (highLimit - lowLimit) / spStep;
	mParticle2Energy = new std::multimap<G4String, G4double>[(int)gapsNum];
	G4cout << "gaps num" << gapsNum << G4endl;

	fnamePart = "detector_" + detName + ".out";
}

DetectorSDSpectrum::~DetectorSDSpectrum()
{

}

void DetectorSDSpectrum::Initialize(G4HCofThisEvent*)
{
	flushNow = true;
}

G4bool DetectorSDSpectrum::ProcessHits(G4Step* step, G4TouchableHistory*)
{
	G4String pName = step->GetTrack()->GetDefinition()->GetParticleName();
	G4double kineticEnergy;
	if (0 == useSide) {
		kineticEnergy = step->GetPreStepPoint()->GetKineticEnergy();
	} else {
		kineticEnergy = step->GetPostStepPoint()->GetKineticEnergy();
	}

	//if (1 == step->GetTrack()->GetTrackID()) {
//		flushNow = true;
		//fileoutPart.open(fnamePart);
	//}

	if ((kineticEnergy > highLimit) || (kineticEnergy < lowLimit)) {
		return true;
	}

	G4int gapNum = (G4int) (kineticEnergy / spStep);

	
	if (mParticle2Energy[gapNum].count(pName) == 0) {
		mParticle2Energy[gapNum].insert(std::pair<std::string, unsigned long>(pName, 1));
	} else {
		mParticle2Energy[gapNum].find(pName)->second += 1;
	}
	
	//G4cout << "Processing spectrum hits " << gapNum << " " << pName << " " << kineticEnergy << G4endl;
	
	return true;
}

void DetectorSDSpectrum::EndOfEvent(G4HCofThisEvent* l)
{
	// @todo: remove save after each step (??)

	if (flushNow == true) {
		fileoutPart.open(fnamePart);
		fileoutPart<< "------------------- Layer " << detName << " -------------------" <<G4endl;
		for (long i = 0; i < (highLimit - lowLimit) / spStep; i++) 
		{
			fileoutPart<<"Gap "<<lowLimit + i * spStep<<" - "<<lowLimit + (i+1) * spStep<<G4endl;
			for (std::multimap<G4String, G4double>::iterator it = mParticle2Energy[i].begin();
				it != mParticle2Energy[i].end();
				++it)
			{
				fileoutPart<< it->first <<" " << it->second << G4endl;
			}
			fileoutPart<<G4endl<<G4endl;
		}
		fileoutPart<<"------------------- END OF " << detName <<" -------------------"<<G4endl;

		fileoutPart.close();
	}
}