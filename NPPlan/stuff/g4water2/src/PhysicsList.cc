#include "PhysicsList.hh"
#include "G4IonBinaryCascadePhysics.hh"
#include "HadronPhysicsQGSP_BIC_HP.hh"
#include "G4HadronElasticPhysicsHP.hh"
#include "G4DecayPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"
#include "G4EmLivermorePhysics.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh" 
#include "G4ParticleDefinition.hh"
#include "G4HadronInelasticQBBC.hh"
#include "G4IonPhysics.hh"
#include "G4StoppingPhysics.hh"
#include "G4NeutronTrackingCut.hh"
#include "G4EmExtraPhysics.hh"
#include "HadronPhysicsQGSP_BIC.hh"
#include "G4HadronElasticPhysics.hh"
#include "G4HadronElasticPhysicsHP.hh"
#include "G4IonBinaryCascadePhysics.hh"
//#include "globals.hh"
PhysicsList::PhysicsList(): G4VModularPhysicsList()
 {
   //defaultCutValue = 1.*mm;
	 defaultCutValue = 2.*cm;
   SetVerboseLevel(1);
   /*
   RegisterPhysics(new G4IonBinaryCascadePhysics());
   //RegisterPhysics(new HadronPhysicsQGSP_BIC_HP());
   RegisterPhysics(new G4HadronElasticPhysicsHP());
   RegisterPhysics(new G4DecayPhysics());
   RegisterPhysics(new G4RadioactiveDecayPhysics());
   RegisterPhysics(new G4EmLivermorePhysics());*/

   RegisterPhysics(new G4EmLivermorePhysics());
   RegisterPhysics( new G4HadronInelasticQBBC());  
   RegisterPhysics( new G4IonPhysics());
   RegisterPhysics( new G4StoppingPhysics());
   RegisterPhysics( new G4NeutronTrackingCut());
   RegisterPhysics( new G4EmExtraPhysics());
   RegisterPhysics( new HadronPhysicsQGSP_BIC());
   RegisterPhysics( new G4HadronElasticPhysics());
   //RegisterPhysics( new G4IonBinaryCascadePhysics());
  }

  
PhysicsList::~PhysicsList()
{}

void PhysicsList::SetCuts()
  {
	  //SetParticleCuts();
    SetCutsWithDefault();
    
  }
