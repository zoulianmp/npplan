#include "DetectorSD.hh"
#include "RunAction.hh"
#include "DetectorConstruction.hh"

#include <map>
#include <string>
#include <vector>
#include "G4RunManager.hh"
#include "G4Step.hh"

/**/
//std::multimap<std::string, int> mParticlesMap;
std::vector<std::string> mParticlesList;
//int jCounter = 0;

DetectorSD::DetectorSD(G4String name): G4VSensitiveDetector(name)
{
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
  sName = name;

  // узнаем количество слоев в детекторе
  nLayers = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->nLayers;
  detEnergy = new G4double[nLayers];
  mParticlesEnergiesLayers = new std::multimap<std::string, G4double>[nLayers];
  mParticlesMapLayers = new std::multimap<std::string, unsigned long>[nLayers];
  mParticlesKineticLayers = new std::multimap<std::string, G4double>[nLayers];
  mParticlesSourceLayers = new std::multimap<std::string, G4double>[nLayers];
  mParticlesSourceNumLayers = new std::multimap<std::string, G4double>[nLayers];
  mParticlesMap.clear();
}

DetectorSD::~DetectorSD()
{
  delete [] detEnergy;
}

void DetectorSD::Initialize(G4HCofThisEvent*)
{
  // в начале события сбрасываем энергию поглощенную детектором
  for (unsigned long i = 0; i < nLayers; ++i) {
    detEnergy[i] = 0;
	mParticlesMapLayers[i].clear();
	mParticlesEnergiesLayers[i].clear();
	mParticlesKineticLayers[i].clear();
	mParticlesSourceLayers[i].clear();
  }
  mParticlesList.clear();
  mParticlesMap.clear();
  //jCounter = 0;
}

void DetectorSD::ParticlesAppender(std::string pName, unsigned long value) {
	if (mParticlesMap.count(pName) == 0) {
		mParticlesMap.insert(std::pair<std::string, unsigned long>(pName, value));
	} else {
		mParticlesMap.find(pName)->second += value;
	}
}

void DetectorSD::ParticlesAppenderLayer(std::string pName, unsigned long value, unsigned long layer) {
	if (mParticlesMapLayers[layer].count(pName) == 0) {
		mParticlesMapLayers[layer].insert(std::pair<std::string, unsigned long>(pName, value));
	} else {
		mParticlesMapLayers[layer].find(pName)->second += value;
	}
}

void DetectorSD::ParticlesAppenderEnergyLayer(std::string pName, G4double value, unsigned long layer) {
	if (mParticlesEnergiesLayers[layer].count(pName) == 0) {
		mParticlesEnergiesLayers[layer].insert(std::pair<std::string, G4double>(pName, value));
	} else {
		mParticlesEnergiesLayers[layer].find(pName)->second += value;
	}
}

void DetectorSD::ParticlesAppenderKineticLayer(std::string pName, G4double value, unsigned long layer) {
	if (mParticlesEnergiesLayers[layer].count(pName) == 0) {
		mParticlesEnergiesLayers[layer].insert(std::pair<std::string, G4double>(pName, value));
	} else {
		mParticlesEnergiesLayers[layer].find(pName)->second += value;
	}
}

void DetectorSD::ParticlesAppenderAnyDouble(std::multimap<std::string, G4double> *obj, std::string pName, G4double value , unsigned long layer) {
	if (obj[layer].count(pName) == 0) {
		obj[layer].insert(std::pair<std::string, G4double>(pName, value));
	} else {
		obj[layer].find(pName)->second += value;
	}
	//return obj
}

G4bool DetectorSD::ProcessHits(G4Step* step, G4TouchableHistory*)
{
  // номер слоя
  G4StepPoint* prePoint = step->GetPreStepPoint();
  G4TouchableHandle touchable = prePoint->GetTouchableHandle();
  G4String name = step->GetTrack()->GetDefinition()->GetParticleName();
  G4int parentTrackId = step->GetTrack()->GetParentID();

  //if (parentTrackId == 0) {
//	  G4cout<<parentTrackId<<
		  //" "<<step->GetTrack()->GetTrackID()<<
		  //" "<<step->GetTrack()->GetKineticEnergy()<<G4endl;
  //}

//G4cout<<"vol nextvol "<<step->GetTrack()->GetVolume()<<" "<<G4cout<<step->GetTrack()->GetNextVolume()<<G4endl;
//  G4cout<<"volname nextvolname "<<step->GetTrack()->GetVolume()->GetName()<<" "<<G4cout<<step->GetTrack()->GetNextVolume()->GetName()<<G4endl;
//  G4cout<<"step number "<<step->GetTrack()->GetCurrentStepNumber()<<G4endl;
//G4cout<<step->GetTrack()->GetMaterial()<<" "<<G4cout<<step->GetTrack()->GetNextMaterial()<<G4endl;
//  G4cout<<"track_id track_pid track_length "<<step->GetTrack()->GetTrackID()<<" "<<step->GetTrack()->GetParentID()<<" "<<step->GetTrack()->GetTrackLength()<<G4endl;
  //G4cout<<"particle: "<<name<<G4endl;
  //G4cout<<mParticlesMap.size()<<G4endl;
  //mParticlesMap.insert(std::pair<std::string, int>((std::string)name, jCounter++));
  //ParticlesAppender((std::string)name, 1);

  if(std::find(mParticlesList.begin(), mParticlesList.end(), name) != mParticlesList.end()) {
    
  } else {
	  mParticlesList.push_back(name);
  }
  G4int layer = touchable->GetCopyNumber(1);
  ParticlesAppenderLayer((std::string)name, 1, (unsigned long)layer);
  
  // получаем величину оставленной энергии
  G4double edep = step->GetTotalEnergyDeposit();
  // и сохраняем ее в соответствующей слою ячейке
  detEnergy[layer] += edep;
  /*
  G4cout<<step->GetPreStepPoint()->GetKineticEnergy()<<" "<<
	  step->GetPostStepPoint()->GetKineticEnergy()<<" "<<
	  step->GetPostStepPoint()->GetKineticEnergy() - step->GetPreStepPoint()->GetKineticEnergy()<<G4endl;
  G4cout<<edep<<G4endl;*/

  //mParticlesEnergiesLayers[layer].insert(std::pair<std::string, G4double>((std::string)name, edep));
  mParticlesKineticLayers[layer].insert(std::pair<std::string, G4double>((std::string)name, prePoint->GetKineticEnergy()));
  ParticlesAppenderEnergyLayer((std::string)name, edep, layer);
  //if (0 == parentTrackId) {
//	  ParticlesAppenderAnyDouble(mParticlesSourceLayers, (std::string)name, edep, layer);
//	  ParticlesAppenderAnyDouble(mParticlesSourceNumLayers, (std::string)name, 1.0, layer);
 // }

  /*
  G4cout<<"particle: "<<name<<" layer: "<<layer<<" energy dep: "<<edep<<" non ion energy dep:"<<step->GetNonIonizingEnergyDeposit()
	  <<" delta energy"<<step->GetDeltaEnergy()
	  <<"!  point data, total: "<<prePoint->GetTotalEnergy() * MeV<<" kinetic: "<<prePoint->GetKineticEnergy() * MeV<<G4endl;
  */
 // for (int i = 0; i < nLayers; i++)
 // G4double layEnergy = detEnergy[layer];

  return true;
}

void DetectorSD::EndOfEvent(G4HCofThisEvent*)
{
  // сохраняем энергию накопленную за событие в детекторе
  // в гистограмму
  G4double totalEnergy = 0;
  for (unsigned long i = 0; i < nLayers; ++i)
    totalEnergy += detEnergy[i];
  
  runAction->FillEnergyHist(totalEnergy);
  runAction->FillEnergyProfile(detEnergy);
  runAction->LayerOut(detEnergy);
  /*G4cout<<mParticlesMap.count("neutron")<<G4endl;
  for (std::multimap<std::string, int>::iterator it = mParticlesMap.begin();
       it != mParticlesMap.end();
       ++it)
   {
       G4cout << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
   }*/
   //for (std::vector<std::string>::const_iterator it = mParticlesList.begin(); it != mParticlesList.end(); ++it) {
//cout << *it << ' ';
	   //G4cout<<*it<<": "<<mParticlesMap.count((*it))<<G4endl;
	   //runAction->ParticlesMapAppender((*it), mParticlesMap.count((*it)));
	   //(*it)
   //}
  /*
   for (std::multimap<std::string, int>::iterator it = mParticlesMap.begin();
       it != mParticlesMap.end();
       ++it)
   {
       //G4cout << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
	   runAction->ParticlesMapAppender(it->first, it->second);
   }
   */
   for (unsigned long i = 0; i < nLayers; ++i) {
	   for (std::multimap<std::string, unsigned long>::iterator it = mParticlesMapLayers[i].begin();
		   it != mParticlesMapLayers[i].end();
		   ++it)
	   {
		   //G4cout << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
		   runAction->ParticlesMapAppenderLayers(it->first, it->second, i);
	   }

	   for (std::multimap<std::string, G4double>::iterator it = mParticlesEnergiesLayers[i].begin();
		   it != mParticlesEnergiesLayers[i].end();
		   ++it)
	   {
		   //G4cout << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
		   runAction->ParticlesEnergiesAppenderLayers(it->first, it->second, i);
	   }

	   for (std::multimap<std::string, G4double>::iterator it = mParticlesKineticLayers[i].begin();
		   it != mParticlesKineticLayers[i].end();
		   ++it)
	   {
		   //G4cout << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
		   runAction->ParticlesKineticAppenderLayers(it->first, it->second, i);
	   }
	   /*
	   for (std::multimap<std::string, G4double>::iterator it = mParticlesSourceLayers[i].begin();
		   it != mParticlesSourceLayers[i].end();
		   ++it)
	   {
		   //G4cout << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
		   runAction->ParticlesAppenderSource(it->first, it->second, i);
	   }

	   for (std::multimap<std::string, G4double>::iterator it = mParticlesSourceNumLayers[i].begin();
		   it != mParticlesSourceLayers[i].end();
		   ++it)
	   {
		   //G4cout << "  [" << (*it).first << ", " << (*it).second << "]" << G4endl;
		   runAction->ParticlesAppenderNumSource(it->first, it->second, i);
	   }*/
   }

}

