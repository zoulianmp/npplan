# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 22.11.13
@summary: 
'''

import os
import shutil

releaseDir = r'C:\Temp\protvino_water2chambers_build\Stable'
releaseFile = 'heavy_ion_exp.exe'
releasePath = os.path.join(releaseDir, releaseFile)

nonChangeable = []
for i in os.listdir(releaseDir):
    if i != releaseFile:
        nonChangeable.append(os.path.join(releaseDir, i))

targetDir = r'C:\Temp\protvino_chambers_exp'
nps = 1e6
#nps = 100


class singleData(object):
    def __init__(self,
                 targetSeriesDir,
                 eId,
                 energy=5.4,
                 expLen=0,
                 outerMaterialType=0,
                 expType=0,
                 zVoxelSize=0,
                 xyVoxelSize=0,
                 filmWidth=0,
                 ):
        global nps
        #if os.path.exists(targetInstanceDir)
        self._cDir = _cDir = os.path.join(targetSeriesDir, eId)
        if not os.path.exists(_cDir):
            os.mkdir(_cDir)

        f = open(os.path.join(_cDir, 'expType.in'), 'w')
        f.write("%d\n" % expType)
        f.write("%d\n" % zVoxelSize)
        f.write("%d\n" % xyVoxelSize)
        f.write("%f\n" % filmWidth)
        f.close()

        f = open(os.path.join(_cDir, 'expData.in'), 'w')
        f.write("%d\n" % expLen)
        f.write("%d\n" % outerMaterialType)
        f.close()

        f = open(os.path.join(_cDir, 'run.mac'), 'w')
        f.write("/run/verbose 1\n")
        f.write("/event/verbose 0\n")
        f.write("/tracking/verbose 0\n")
        f.write("/process/list\n")
        f.write("/gun/particle ion\n")
        f.write("/gun/ion 6 12\n")
        f.write("/gun/energy %f GeV\n" % energy)
        f.write("/run/beamOn %d\n" % nps)

        shutil.copy(releasePath, _cDir)
        for i in nonChangeable:
            shutil.copy(i, _cDir)

    def getDir(self):
        return self._cDir


class singleSeries(object):
    def __init__(self, seriesDir, energy=5.4, expLen=20, outerMaterialType=0, expType=1, zVoxelSize=50, xyVoxelSize=50, filmWidth=1):
        self._cDir = _cDir = os.path.join(targetDir, seriesDir)
        os.mkdir(_cDir)

        self._f = open(os.path.join(_cDir, 'runAll.bat'), 'w')
        self._dName = 0

        if isinstance(energy, list):
            for i in energy:
                self.appendAndWrite(i, expLen, outerMaterialType, expType, zVoxelSize, xyVoxelSize, filmWidth)
        elif isinstance(expLen, list):
            for i in expLen:
                self.appendAndWrite(energy, i, outerMaterialType, expType, zVoxelSize, xyVoxelSize, filmWidth)
        elif isinstance(outerMaterialType, list):
            for i in outerMaterialType:
                self.appendAndWrite(energy, expLen, i, expType, zVoxelSize, xyVoxelSize, filmWidth)
        elif isinstance(expType, list):
            for i in expType:
                self.appendAndWrite(energy, expLen, outerMaterialType, i, zVoxelSize, xyVoxelSize, filmWidth)
        elif isinstance(zVoxelSize, list):
            for i in zVoxelSize:
                self.appendAndWrite(energy, expLen, outerMaterialType, expType, i, xyVoxelSize, filmWidth)
        elif isinstance(xyVoxelSize, list):
            for i in xyVoxelSize:
                self.appendAndWrite(energy, expLen, outerMaterialType, expType, zVoxelSize, i, filmWidth)
        elif isinstance(xyVoxelSize, filmWidth):
            for i in filmWidth:
                self.appendAndWrite(energy, expLen, outerMaterialType, expType, zVoxelSize, xyVoxelSize, i)
        else:
            self.appendAndWrite(energy, expLen, outerMaterialType, expType, zVoxelSize, xyVoxelSize, filmWidth)
        self._f.close()
        pass

    def appendAndWrite(self, energy=5.4, expLen=20, outerMaterialType=0, expType=1, zVoxelSize=50, xyVoxelSize=50, filmWidth=1):
        c = singleData(self._cDir, 'exp%d' % self._dName,
               energy, expLen, outerMaterialType, expType, zVoxelSize, xyVoxelSize, filmWidth)
        self._f.write("cd %s\n" % c.getDir())
        self._f.write("%s run.mac > run.log\n" % releaseFile)
        self._dName += 1

    def getDir(self):
        return self._cDir

if __name__ == '__main__':
    print nonChangeable

    #os.mkdir(os.path.join(targetDir, 'test1'))
    #singleSeries('test1')
    f = open(os.path.join(targetDir, 'runAll.bat'), 'w')
    s = singleSeries('el25o0e1_de',
                 energy=[5.1, 5.2, 5.3, 5.4, 5.5],
                 expLen=25, outerMaterialType=0, expType=1, zVoxelSize=50, xyVoxelSize=50, filmWidth=1)

    f.write("start %s\n" % os.path.join(s.getDir(), 'runAll.bat'))

    s = singleSeries('el25o1e1_de',
                 energy=[5.1, 5.2, 5.3, 5.4, 5.5],
                 expLen=25, outerMaterialType=1, expType=1, zVoxelSize=50, xyVoxelSize=50, filmWidth=1)

    f.write("start %s\n" % os.path.join(s.getDir(), 'runAll.bat'))

    s = singleSeries('e54o0e1_del',
                 energy=5.4,
                 expLen=[0, 19, 20, 21, 22, 25], outerMaterialType=0, expType=1, zVoxelSize=50, xyVoxelSize=50, filmWidth=1)

    f.write("start %s\n" % os.path.join(s.getDir(), 'runAll.bat'))

    s = singleSeries('e54o1e1_del',
                 energy=5.4,
                 expLen=[0, 19, 20, 21, 22], outerMaterialType=1, expType=1, zVoxelSize=50, xyVoxelSize=50, filmWidth=1)

    f.write("start %s\n" % os.path.join(s.getDir(), 'runAll.bat'))

    s = singleSeries('e54e2f1el20_do',
                 energy=5.4,
                 expLen=20, outerMaterialType=[0, 1], expType=2, zVoxelSize=50, xyVoxelSize=50, filmWidth=2)

    f.write("start %s\n" % os.path.join(s.getDir(), 'runAll.bat'))

    s = singleSeries('e54o0e1z12_del15-25',
                 energy=5.4,
                 expLen=range(15, 26), outerMaterialType=0, expType=1, zVoxelSize=12, xyVoxelSize=50, filmWidth=1)

    f.write("start %s\n" % os.path.join(s.getDir(), 'runAll.bat'))

    s = singleSeries('e54o0e1z12_del0-15',
                 energy=5.4,
                 expLen=range(15), outerMaterialType=0, expType=1, zVoxelSize=12, xyVoxelSize=50, filmWidth=1)

    f.write("start %s\n" % os.path.join(s.getDir(), 'runAll.bat'))

    f.close()