# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 03.03.14
@summary: 
'''

import vtk
import matplotlib
matplotlib.use('Agg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg

reader = vtk.vtkDICOMImageReader()
writer = vtk.vtkPNGWriter()
im = vtk.vtkImageMapToWindowLevelColors()
im.SetWindow(1000)
im.SetLevel(250)
reader.SetFileName(r'C:\Users\Chernukha\Pictures\DICOM\01111140\84269606')
im.SetInput(reader.GetOutput())

renderer = vtk.vtkRenderer()
renderer.SetBackground(1, 1, 1)
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)

#an interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)

#add the actors to the scene
atext = vtk.vtkTexture()
atext.SetInputConnection(im.GetOutputPort())
plane = vtk.vtkPlaneSource()
planeMapper = vtk.vtkPolyDataMapper()
planeMapper.SetInputConnection(plane.GetOutputPort())
planeMapper.SetScalarModeToUsePointData()
planeMapper.SetColorModeToMapScalars()
planeActor = vtk.vtkActor()

importer = vtk.vtkImageImport()

cubeTexture = vtk.vtkTexture()
cubeTexture.InterpolateOn()
cubeTexture.SetInput(importer.GetOutput())

planeActor.SetMapper(planeMapper)
planeActor.SetTexture(cubeTexture)
#planeActor.SetTexture(atext)
renderer.AddActor(planeActor)

fig = Figure()
canvas = FigureCanvasAgg(fig)
ax = fig.add_subplot(111)
ax.grid(True)
ax.set_xlabel('Hello from VTK!', size=16)
ax.bar(xrange(10), xrange(10))

# Powers of 2 image to be clean
w,h = 1024, 1024
dpi = canvas.figure.get_dpi()
fig.set_size_inches(w / dpi, h / dpi)
canvas.draw() # force a draw

# This is where we tell the image importer about the mpl image
extent = (0, w - 1, 0, h - 1, 0, 0)
#print canvas.tostring_rgb()

importer.SetDataScalarTypeToUnsignedChar()
#importer.SetDataScalarTypeToDouble()
importer.SetNumberOfScalarComponents(4)
importer.SetWholeExtent(extent)
importer.SetDataExtent(extent)
#importer.SetImportVoidPointer(canvas.buffer_rgba(), 1)
print type(canvas.tostring_argb())
importer.SetImportVoidPointer(canvas.tostring_argb(), 1)
importer.Update()

transform = vtk.vtkTransform()
#transform.Translate(1.0, 0.0, 0.0)

axes = vtk.vtkAxesActor()
#  The axes are positioned with a user transform
axes.SetUserTransform(transform)

# properties of the axes labels can be set as follows
# this sets the x axis label to red
axes.GetXAxisCaptionActor2D().GetCaptionTextProperty().SetColor(1,0,0)

# the actual text of the axis label can be changed:
# axes->SetXAxisLabelText("test");

renderer.AddActor(axes)

renderer.ResetCamera()
renderWindow.Render()

# begin mouse interaction
renderWindowInteractor.Start()