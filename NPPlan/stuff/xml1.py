# -*- coding: utf8 -*-

from lxml import etree

root = etree.Element("stages")

stage1 = etree.SubElement(root, "stage", order="1")
etree.SubElement(stage1, "name").text = u"Данные пациента"
etree.SubElement(stage1, "backend").text = "aaa_back()"

stage2 = etree.SubElement(root, "stage", order="2")
etree.SubElement(stage2, "name").text = u"Оконтуривание"
etree.SubElement(stage2, "backend").text = "aaa_back()"

stage3 = etree.SubElement(root, "stage", order="3")
etree.SubElement(stage3, "name").text = u"Параметры позиционирования"
etree.SubElement(stage3, "backend").text = "aaa_back()"

stage4 = etree.SubElement(root, "stage", order="4")
etree.SubElement(stage4, "name").text = u"3D-позиционирование"
etree.SubElement(stage4, "backend").text = "aaa_back()"

stage5 = etree.SubElement(root, "stage", order="5")
etree.SubElement(stage5, "name").text = u"Расчёт"
etree.SubElement(stage5, "backend").text = "aaa_back()"

stage6 = etree.SubElement(root, "stage", order="6")
etree.SubElement(stage6, "name").text = u"Результаты"
etree.SubElement(stage6, "backend").text = "aaa_back()"

handle = etree.tostring(root, pretty_print=True, encoding='utf-8', xml_declaration=True)
applic = open("-app.xml", "w")
applic.writelines(handle)
applic.close()