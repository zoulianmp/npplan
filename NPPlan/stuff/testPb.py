# -*- coding: utf8 -*-
'''

@author: 
@copyright: MRRC Obninsk 2012
@version: 1
@date: 30.11.12
@summary: 
'''

from twisted.spread import pb
from twisted.internet import reactor

global factory

def main():
    global factory
    factory = pb.PBClientFactory()
    reactor.connectTCP("localhost", 8800, factory)
    def1 = factory.getRootObject()
    def1.addCallbacks(got_obj1, err_obj1)
    reactor.run()

def err_obj1(reason):
    print "error getting first object", reason
    reactor.stop()

global q

def got_obj1(obj1):
    global q
    print "got first object:", obj1
    print "asking it to getTwo"
    def2 = obj1.callRemote("getMainDbAddress")
    def2.addCallbacks(got_obj2)

def got_obj2(obj2):
    print obj2
    reactor.stop()
#    setRemote()



main()