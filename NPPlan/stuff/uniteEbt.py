import os


def dirToFile(fDir, outFile):
    outFileStream = file(outFile, "w+")

    z = -1
    for inpFile in os.listdir(fDir):
        z += 1
        dataDict = {}
        inpFilePath = os.path.join(fDir, inpFile)
        for fileLine in file(inpFilePath):
            if 'mm' in fileLine:
                continue
            if '---' in fileLine:
                continue
            x, y, val = fileLine.split()
            x = int(float(x)*10)
            y = int(float(y)*10)
            val = float(val)
            try:
                dataDict[x][y] = val
            except KeyError:
                dataDict[x] = {y: val}

        for x in dataDict:
            for y in dataDict:
                outFileStream.write("%d %d %d %f\n" %(x, y, z, dataDict[x][y]))

    outFileStream.close()

if __name__ == '__main__':
    dirToFile('D:\dev\data\cubes_out\cube_1_out', 'D:\dev\data\cubes_out\cube1.txt')
    dirToFile('D:\dev\data\cubes_out\cube_2_out', 'D:\dev\data\cubes_out\cube2.txt')
    dirToFile('D:\dev\data\cubes_out\cube_3_out', 'D:\dev\data\cubes_out\cube3.txt')
    dirToFile('D:\dev\data\cubes_out\cube_4_out', 'D:\dev\data\cubes_out\cube4.txt')
    dirToFile('D:\dev\data\cubes_out\cube_5_out', 'D:\dev\data\cubes_out\cube5.txt')