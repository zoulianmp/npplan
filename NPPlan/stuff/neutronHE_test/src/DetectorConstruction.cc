#include "DetectorConstruction.hh"

#include "DetectorSDSingle.hh"
//#include "DetectorSDSinglePart.hh"
#include "DetectorSD1Layer.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Orb.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4RotationMatrix.hh"
#include "G4NistManager.hh"
#include "globals.hh"
#include "G4VisAttributes.hh" 
#include "G4SDManager.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

DetectorConstruction::DetectorConstruction() {}

DetectorConstruction::~DetectorConstruction() {}

// ������ ��� ����������� ������ �������
#define SIZE(x) sizeof(x)/sizeof(*x)


G4VPhysicalVolume* DetectorConstruction::Construct()
{
    G4NistManager* nistMan = G4NistManager::Instance();
  G4Material* air = nistMan->FindOrBuildMaterial("G4_AIR");
  //G4Material* plex = nistMan->FindOrBuildMaterial("G4_PLEXIGLASS");
  //G4Material* polystyrene = nistMan->FindOrBuildMaterial("G4_POLYSTYRENE");
  //G4Material* cells = nistMan->FindOrBuildMaterial("G4_MUSCLE_SKELETAL_ICRP");
  G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
  G4Material* concrete = nistMan->FindOrBuildMaterial("G4_CONCRETE");
  //G4Material* Fe2O3 = nistMan->FindOrBuildMaterial("G4_FERRIC_OXIDE");
  //G4Material* Al2O3 = nistMan->FindOrBuildMaterial("G4_ALUMINUM_OXIDE");
  G4Material* tissue = nistMan->FindOrBuildMaterial("G4_TISSUE_SOFT_ICRP");
  
  
 G4Element* elH
= new G4Element("Hydrogen", "H" , 1., 1.01*g/mole);
 G4Element* elC
= new G4Element("Carbon", "C" , 6., 12.01*g/mole);
 G4Element* elN
= new G4Element("Nitrogen", "N" , 7., 14.01*g/mole);
 G4Element* elO
= new G4Element("Oxygen", "O" , 8., 16.00*g/mole);
 G4Element* elLi
= new G4Element("Lithium", "Li" , 3., 6.94*g/mole);
 G4Element* elCl
= new G4Element("Chlorine", "Cl" , 17., 35.45*g/mole);
 G4Element* elK
= new G4Element("Potassium", "K", 19., 39.10*g/mole);
 G4Element* elBr
= new G4Element("Bromine", "Br", 35., 79.90*g/mole);
 G4Element* elCa 
= new G4Element("Calcium", "Ca", 20.0, 40.078*g/mole);
  G4Element* elFe 
= new G4Element("Iron", "Fe", 26, 56.845*g/mole);
 G4Element* elAl
= new G4Element("Aluminium", "Al", 13, 26.98*g/mole);
  G4Element* elSi 
= new G4Element("Silicium", "Si", 14, 28.086*g/mole);

  G4double world_x_length = 2*m;
  G4double world_y_length = 2*m;
  G4double world_z_length = 2*m;

  G4Box* world_box = new G4Box("world", 0.5*world_x_length, 0.5*world_y_length, 0.5*world_z_length);
  G4LogicalVolume* world_log = new G4LogicalVolume(world_box, air, "world");
  G4VPhysicalVolume* world_phys = new G4PVPlacement(0, G4ThreeVector(), world_log, "world", 0, false, 0);

  G4double phSx, phSy, phSz, zPos;
  phSx = 5*cm;
  phSy = 5*cm;
  phSz = 25*cm;

  zPos = 23*mm;

  nov = 50000;
  //G4Box* phBox = new G4Box("phantomBox", 0.5*phSx, 0.5*phSy, 0.5*phSz);
  //G4LogicalVolume* phLogic = new G4LogicalVolume(phBox, water, "phLogic");
  //new G4PVPlacement(0, G4ThreeVector(), phLogic, "world", world_log, false, 0);


	/*G4Box* solidBox =    
    new G4Box("OBoxS", phSx, phSy, phSz);
      
	G4LogicalVolume* logicBox =                         
		new G4LogicalVolume(solidBox,            //its solid
							water,              //its material
							"OBoxL");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    G4ThreeVector(0, 0, 0),  //at (0,0,0)
                    logicBox,                //its logical volume
                    "OBoxP",                   //its name
                    world_log,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     true);        //overlaps checking
                     */
  
  G4LogicalVolume* phantomLayerLog = addZVoxelsLayer(world_log, G4ThreeVector(0, 0, 0), "phantomMain", water, phSx, phSy, phSz, nov);
  


  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSD1Layer* det = new DetectorSD1Layer("phantom", nov, 1, 10*10*cm2*10*micrometer);
  //det->addIon(6, 12);
  mainDet = det;
  sdMan->AddNewDetector(det);
  phantomLayerLog->SetSensitiveDetector(det);
  qOfDets++;

  return world_phys;

}

void DetectorConstruction::addDet(G4LogicalVolume* vol, G4String name) {
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSDSingle* det = new DetectorSDSingle(name, vol->GetSolid()->GetCubicVolume());
  sdMan->AddNewDetector(det);
  vol->SetSensitiveDetector(det);
  qOfDets++;
  lOfDets.push_back(name);
}

#ifdef DetectorSD_Single_Particles_h
void DetectorConstruction::addDetParted(G4LogicalVolume* vol, G4String name) {
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSDSinglePart* det = new DetectorSDSinglePart(name, vol->GetSolid()->GetCubicVolume());
  det->addParticle("proton");
  det->addParticle("alpha");
  det->addParticle("e-");
  det->addParticle("neutronHE", "neutron", 20, 500);
  det->addParticle("neutronLE", "neutron", 0, 20);
  sdMan->AddNewDetector(det);
  vol->SetSensitiveDetector(det);
  qOfDets++;
  lOfDets.push_back(name);
}
#endif

void DetectorConstruction::addDet(G4LogicalVolume* vol, G4String name, G4double volume) {
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  DetectorSDSingle* det;
  if (0.0 == volume) {
    det = new DetectorSDSingle(name);
  } else {
    det = new DetectorSDSingle(name, volume);
  }
  sdMan->AddNewDetector(det);
  vol->SetSensitiveDetector(det);
  qOfDets++;
  lOfDets.push_back(name);
}

G4LogicalVolume* DetectorConstruction::addZVoxelsLayer(G4LogicalVolume* parentVolume, 
										  G4ThreeVector position, 
										  G4String lName, 
										  G4Material* material,
										  G4double blX,
										  G4double blY,
										  G4double blZ,
										  G4int qLayers
                      ) 
{
	//G4Material* water = nistMan->FindOrBuildMaterial("G4_WATER");
	// Voxels container
	G4Box* solidBox =    
    new G4Box(lName+"BoxS",                         //its name
        blX, blY, blZ); //its size
      
	G4LogicalVolume* logicBox =                         
		new G4LogicalVolume(solidBox,            //its solid
							material,              //its material
							lName+"BoxL");              //its name
          
    new G4PVPlacement(0,                     //no rotation
                    position,  //at (0,0,0)
                    logicBox,                //its logical volume
                    lName+"BoxP",                   //its name
                    parentVolume,              //its mother  volume
                    false,                   //no boolean operation
                     0,                     //copy number
                     true);        //overlaps checking

	// Voxels replica
	G4VSolid* layerX =
		new G4Box(lName+"layerZ", blX, blY, blZ / qLayers);
    
	G4LogicalVolume* logicLayer =
		new G4LogicalVolume(layerX, material, lName+"layerZL");
   
    new G4PVReplica(lName+"layerZR", logicLayer, 
                 logicBox, kZAxis, qLayers, 2 * blZ / qLayers);  

	G4LogicalVolume* det_log;
	G4Box* det_box = new G4Box(lName+"detector", blX, blY, blZ / qLayers);
  det_log = new G4LogicalVolume(det_box, material, lName+"detectorL");
  G4VPhysicalVolume* det_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), det_log, lName+"detectorP", logicLayer, false, 0);

	//DetectorSDVox* vox1 = new DetectorSDVox(lName+"vox", qLayers);
	//sdMan->AddNewDetector(vox1);
	//det_log->SetSensitiveDetector(vox1);

	return det_log;
}

G4int DetectorConstruction::getNumberOfLayers() {
  return nov;
}

DetectorSD1Layer* DetectorConstruction::getMainDetector() {
  return mainDet;
}