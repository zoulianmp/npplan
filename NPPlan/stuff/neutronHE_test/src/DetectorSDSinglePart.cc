#include "DetectorSDSinglePart.hh"

#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4RunManager.hh"
#include "RunAction.hh"

#include "G4ParticleTypes.hh"


Slayer::Slayer() : depEnergy(0.0), depEnergy2(0.0), dose(0.0), dose2(0.0),
  depEnergyError(0.0), doseError(0.0), nEvents(0), cellFlux(0.0)
{
}

particlesList::particlesList(G4String _on, G4String _g4n, G4double _le, G4double _he) : outName(_on), g4Name(_g4n), 
  lowEnergy(_le), highEnergy(_he)
{
}

DetectorSDSinglePart::DetectorSDSinglePart(G4String name) : G4VSensitiveDetector(name)
{
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
  outFileNameAll = name;
  setupSummator();
  _givenVolume = 0.0*cm3;
}

DetectorSDSinglePart::DetectorSDSinglePart(G4String name, G4double _vol) : G4VSensitiveDetector(name), _givenVolume(_vol)
{
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
  outFileNameAll = name;
  setupSummator();
}

void DetectorSDSinglePart::addParticle(G4String newParticle) {
  particlesList *pl = new particlesList(newParticle, newParticle, 0.0, -1.0);
  //listOfParticles.insert();
  //listOfParticles.insert(pl);
  //listOfParticles.insert(std::pair<G4String, particlesList*>(newParticle, pl));
  listOfParticles.push_back(pl);
}

void DetectorSDSinglePart::addParticle(G4String pName, G4String newParticle, G4double _le, G4double _he) {
  particlesList *pl = new particlesList(pName, newParticle, _le, _he);
  //listOfParticles.insert(std::pair<G4String, particlesList*>(newParticle, pl));
  listOfParticles.push_back(pl);
}

void DetectorSDSinglePart::setupSummator() {
  depEnergy = depEnergy2 = depEnergyError = dose = dose2 = doseError = 0.0;
  nEvents = 0;
  cellFlux = cellFlux2 = 0.0;
}

DetectorSDSinglePart::~DetectorSDSinglePart() {

}

void DetectorSDSinglePart::Initialize(G4HCofThisEvent*) {
  setupSummator();
  part2Energies.clear();
}

G4bool DetectorSDSinglePart::ProcessHits(G4Step* step, G4TouchableHistory* aHistory) {
  G4double density = step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetMaterial()->GetDensity();
  //G4double density = 1 * g / (cm * cm * cm);
  //G4double volume = 1 * cm * cm * cm;
  //G4double volume = (0.62/4.0)*(0.62/4.0)*(0.62/4.0) * 4.0 / 3.0 * 3.1415;
  //G4double volume = 4.0 / 3.0 * 3.1415 * sphDiam*sphDiam*sphDiam;
  G4double volume;
  if ((0.0*cm3) == _givenVolume) {
    // simple case, won't work with voxel volumes
    volume = step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetSolid()->GetCubicVolume();
  } else {
    volume = _givenVolume;
  }
  G4double stepLength = step->GetStepLength();

  G4double edep = step->GetTotalEnergyDeposit();
  edep *= step->GetPreStepPoint()->GetWeight();

	G4StepPoint* prePoint = step->GetPreStepPoint();
	G4TouchableHandle touchable = prePoint->GetTouchableHandle();
	G4String name = step->GetTrack()->GetDefinition()->GetParticleName();
  G4double kinetic = step->GetPreStepPoint()->GetKineticEnergy();


	G4int parentTrackId = step->GetTrack()->GetParentID();  

  G4double voxelMass, _dose;

  cellFlux += stepLength / volume;
  if (edep >= 0.0) {
    cellFlux2 += stepLength / volume;
  }

  voxelMass = density*volume;
  _dose=edep / voxelMass;

  dose += _dose;
  dose2 += _dose*_dose;
  depEnergy += edep;
  depEnergy2 += edep*edep;

  nEvents++;

  for (int i = 0; i < listOfParticles.size(); i++) {
    if (listOfParticles[i]->g4Name == name) {
      //G4cout<<"Got "<<name<<G4endl;
      if (-1.0 == listOfParticles[i]->highEnergy) {
        // accept all
        addParticleData(listOfParticles[i]->outName, edep, _dose, cellFlux);
      } else {
        // accept with energy braket
        if ( (kinetic >= listOfParticles[i]->lowEnergy) && (kinetic <= listOfParticles[i]->highEnergy) ) {
          // accept
          addParticleData(listOfParticles[i]->outName, edep, _dose, cellFlux);
        }
      }
      //break;
    }
  }

  return true;
}

void DetectorSDSinglePart::addParticleData(G4String pName, G4double edep, G4double dose, G4double flux) {
  if ( part2Energies.count(pName) == 0) {
    Slayer *obj = new Slayer();
    obj->depEnergy = edep;
    obj->depEnergy2 = edep*edep;
    obj->dose = dose;
    obj->dose2 = dose*dose;
    obj->cellFlux = flux;
    obj->nEvents = 1;
    part2Energies.insert(std::pair<G4String, Slayer*>(pName, obj));
  } else {
    Slayer *obj = part2Energies.find(pName)->second;
    obj->depEnergy += edep;
    obj->depEnergy2 += edep*edep;
    obj->dose += dose;
    obj->dose2 += dose*dose;
    obj->cellFlux += flux;
    obj->nEvents += 1;
  }
}

void DetectorSDSinglePart::EndOfEvent(G4HCofThisEvent*) {
  if (0.0 != depEnergy) {
    runAction->addDetData(outFileNameAll, depEnergy, depEnergy2, dose, dose2, nEvents, cellFlux);
  }

  for (std::map<G4String, Slayer*>::iterator it = part2Energies.begin();
    it != part2Energies.end();
		++it)
  {
    /*G4cout<<"Particle "<<it->first<<G4endl;
    G4cout<<it->second->depEnergy<<"\t";
    G4cout<<it->second->depEnergy2<<"\t";
    G4cout<<it->second->dose<<"\t";
    G4cout<<it->second->dose2<<"\t";
    G4cout<<it->second->nEvents<<"\t";
    G4cout<<G4endl;*/
    runAction->addDetParticleData(outFileNameAll, it->first, it->second->depEnergy, it->second->depEnergy2,
      it->second->dose, it->second->dose2, it->second->nEvents, it->second->cellFlux);
  }

}