#include "RunAction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
//#include "G4VUserDetectorConstruction.hh"
#include "DetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "DetectorSD1Layer.hh"
#include <ctime>

//#define NO_RA 1

SlayerRA::SlayerRA() : depEnergy(0.0), depEnergy2(0.0), depEnergyError(0.0),
  dose(0.0), dose2(0.0), doseError(0.0),
  cellFlux(0.0), cellFlux2(0.0),
  nEvents(0)
{
}

RunAction::RunAction()
  : useContinue(false), G4UserRunAction(), previousRuns(0)
{ 
  G4RunManager::GetRunManager()->SetPrintProgress(10000);
}

RunAction::RunAction(bool _useContinue)
 : useContinue(_useContinue), G4UserRunAction(), previousRuns(0)
{ 
  G4cout<<"  Run with continue"<<G4endl;
  G4RunManager::GetRunManager()->SetPrintProgress(10000);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{

}

SlayerRA RunAction::makeZero(SlayerRA o) {
  o.depEnergy = o.depEnergy2 = o.depEnergyError = 0.0;
  o.dose = o.dose2 = o.doseError = 0.0;
  o.cellFlux = o.cellFlux2 = 0.0;
  o.nEvents = 0;
  return o;
}

void RunAction::addDetData(G4String detName, G4double en, G4double en2, G4double dose, G4double dose2, G4int nEvent, G4double cellFlux) {
  //G4cout<<"Got RA "<<detName<<" det "<<en<<G4endl;
  SlayerRA q;
  q = depToDets.find(detName)->second;
  q.depEnergy += en;
  q.depEnergy2 += en2;
  q.dose += dose;
  q.dose2 += dose2;
  q.nEvents += nEvent;
  q.cellFlux += cellFlux;
  depToDets.find(detName)->second = q;
}

void RunAction::addLayeredDetData(G4String detName, G4int layer, G4double en, G4double en2, G4double dose, G4double dose2, G4int nEvent, G4double cellFlux) {
  if (0 == layerDepToDets.count(detName)) {
    G4int nov = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->nov;
    SlayerRA *data = new SlayerRA[nov];
    for (int i = 0; i < nov; i++) {
      if (i == layer) {
        data[i].dose = dose;
        data[i].dose2 = dose2;
        data[i].depEnergy = en;
        data[i].depEnergy2 = en2;
        data[i].nEvents = nEvent;
        data[i].cellFlux = cellFlux;
      } else {
        data[i].dose = 0.0;
        data[i].dose2 = 0.0;
        data[i].depEnergy = 0.0;
        data[i].depEnergy2 = 0.0;
        data[i].nEvents = 0;
        data[i].cellFlux = 0.0;
      }
      data[i].depEnergyError = 0.0;
      data[i].doseError = 0.0;
    }
    layerDepToDets.insert(std::pair<G4String, SlayerRA*>(detName, data));
  } else {
    SlayerRA *d = layerDepToDets.find(detName)->second;
    d[layer].dose += dose;
    d[layer].dose2 += dose2;
    d[layer].depEnergy += en;
    d[layer].depEnergy2 += en2;
    d[layer].nEvents += nEvent;
    d[layer].cellFlux += cellFlux;
  }
}

void RunAction::addDetParticleData(G4String detName, G4String particle, G4double en, G4double en2, G4double dose, G4double dose2, G4int nEvent, G4double cellFlux) {
  //G4cout<<"Got RA "<<detName<<" det "<<en<<G4endl;
  // 1. find detector
  ppr pairIndex(detName, particle);
  if (0 == dets2Energies.count(pairIndex)) {
    SlayerRA *data = new SlayerRA();
    data->depEnergy = en;
    data->depEnergy2 = en2;
    data->dose = dose;
    data->dose2 = dose2;
    data->nEvents = nEvent;
    data->cellFlux = cellFlux;
    dets2Energies.insert(std::pair<ppr, SlayerRA*>(pairIndex, data));
  } else {
    dets2Energies.find(pairIndex)->second->depEnergy += en;
    dets2Energies.find(pairIndex)->second->depEnergy2 += en2;
    dets2Energies.find(pairIndex)->second->dose += dose;
    dets2Energies.find(pairIndex)->second->dose2 += dose2;
    dets2Energies.find(pairIndex)->second->nEvents += nEvent;
    dets2Energies.find(pairIndex)->second->cellFlux += cellFlux;
  }

}

SlayerRA RunAction::prepareError(SlayerRA target) {
  G4int n;
  G4double v, vDo;
  v = target.nEvents*target.depEnergy2 - target.depEnergy*target.depEnergy;
  vDo = target.nEvents*target.dose2 - target.dose*target.dose;
  n = target.nEvents;
  if (n>1) {
    target.depEnergyError = 1.0*std::sqrt(v/(n-1));
    target.doseError = 1.0*std::sqrt(vDo/(n-1));
  }
  return target;
}

void RunAction::prepareErrorS(SlayerRA *target) {
  G4int n;
  G4double v, vDo;
  v = target->nEvents*target->depEnergy2 - target->depEnergy*target->depEnergy;
  vDo = target->nEvents*target->dose2 - target->dose*target->dose;
  n = target->nEvents;
  if (n>1) {
    target->depEnergyError = 1.0*std::sqrt(v/(n-1));
    target->doseError = 1.0*std::sqrt(vDo/(n-1));
  }
}

void RunAction::addLayeredDetData(G4String detName, detLayer* detData) {
  // -> detLayers;
  G4int nov = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->nov;
  if (0 == detLayers.count(detName)) {
    resLayer *res = new resLayer[nov];
    for (int i = 0; i < nov; i++) {
      res[i].Nullify();
      res[i] += detData[i];
    }
    detLayers.insert(std::pair<G4String, resLayer*>(detName, res));
  } else {
    for (int i = 0; i < nov; i++) {
      detLayers.find(detName)->second[i] += detData[i];
    }
  }

}

bool RunAction::checkDetListParticles(G4String detName, G4int pCount) {
  if (0 == allParticlesList.count(detName)) {
    // don't have this detector stored
    return false;
  }
  return allParticlesList.find(detName)->second.size() == pCount;
}

void RunAction::updateDetListParticles(G4String detName, pdvec pc) {
  if (0 == allParticlesList.count(detName)) {
    allParticlesList.insert(std::pair<G4String, pdvec>(detName, pc));
  } else {
    allParticlesList.find(detName)->second = pc;
  }
}

void RunAction::addLayeredDetData(G4String detName, G4ParticleDefinition* partData, detLayer* detData) {
  // -> detPartLayers;
  G4int nov = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->nov;
  detPartType pp = detPartType(detName, partData);
  if (0 == detPartLayers.count(pp)) {
    resLayer *res = new resLayer[nov];
    for (int i = 0; i < nov; i++) {
      res[i].Nullify();
      res[i] += detData[i];
    }
    detPartLayers.insert(std::pair<detPartType, resLayer*>(pp, res));
  } else {
    for (int i = 0; i < nov; i++) {
      detPartLayers.find(pp)->second[i] += detData[i];
    }    
  }
}

void RunAction::BeginOfRunAction(const G4Run* cRun)
{ 
#ifndef NO_RA
  layerDepToDets.clear();
  beginAt = clock();

  //((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->getMainDetector()->addIon(6, 12);
  DetectorSD1Layer *md = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->getMainDetector();
  md->addParticle("neutron");
  md->addParticle("proton");
  md->addParticle("alpha");
  md->addParticle("gamma");
  md->addParticle("e-");
  md->addIon(6, 12);
  md->addIon(8, 16);
#endif
}

void RunAction::EndOfRunAction(const G4Run* cRun)
{
#ifndef NO_RA
  std::ofstream fileoutPart;
  G4int nov = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->nov;

  for (std::map<G4String, resLayer*>::iterator it = detLayers.begin();
				it != detLayers.end();
				++it) {
    fileoutPart.open(it->first+"_sum.out");
    fileoutPart<<"Summary data\n";
    fileoutPart<<"Layer\tEnergy, MeV\tEnergy error, MeV\tDose, Mev/g\tDose error, MeV/g\tDose, Gy\tDose error, Gy\tNumber of events\tCell flux\t"<<G4endl;;
    for (int i = 0; i < nov; i++) {

      it->second[i].calculateError();
      fileoutPart<<std::setfill('0') << std::setw(5)<<i<<"\t";
      fileoutPart<<std::defaultfloat;
      fileoutPart<<it->second[i].depEnergy / MeV<<"\t";
      fileoutPart<<it->second[i].depEnergyError / MeV<<"\t";
      fileoutPart<<it->second[i].dose / (MeV / g)<<"\t";
      fileoutPart<<it->second[i].doseError / (MeV / g)<<"\t";
      fileoutPart<<G4BestUnit(it->second[i].dose, "Dose")<<"\t";
      fileoutPart<<G4BestUnit(it->second[i].doseError, "Dose")<<"\t";
      fileoutPart<<it->second[i].nEvents<<"\t";
      fileoutPart<<it->second[i].cellFlux<<"\t";
      fileoutPart<<G4endl;
    }
    fileoutPart.close();
  }

    for (std::map<detPartType, resLayer*>::iterator it = detPartLayers.begin();
				it != detPartLayers.end();
				++it) {
      fileoutPart.open(it->first.first+"_"+it->first.second->GetParticleName()+".out");
      fileoutPart<<"Particle "<<it->first.second->GetParticleName()<<"\n";
      fileoutPart<<"Layer\tEnergy, MeV\tEnergy error, MeV\tDose, Mev/g\tDose error, MeV/g\tDose, Gy\tDose error, Gy\tNumber of events\tCell flux\t"<<G4endl;
      for (int i = 0; i < nov; i++) {
        it->second[i].calculateError();
        fileoutPart<<i<<"\t";
        fileoutPart<<it->second[i].depEnergy / MeV<<"\t";
        fileoutPart<<it->second[i].depEnergyError / MeV<<"\t";
        fileoutPart<<it->second[i].dose / (MeV / g)<<"\t";
        fileoutPart<<it->second[i].doseError / (MeV / g)<<"\t";
        fileoutPart<<G4BestUnit(it->second[i].dose, "Dose")<<"\t";
        fileoutPart<<G4BestUnit(it->second[i].doseError, "Dose")<<"\t";
        fileoutPart<<it->second[i].nEvents<<"\t";
        fileoutPart<<it->second[i].cellFlux<<"\t";
        fileoutPart<<G4endl;
      }
      fileoutPart.close();
    }

  fileoutPart.open("test2.dump", std::ofstream::out);
  for (std::map<G4String, pdvec>::iterator it = allParticlesList.begin();
    it != allParticlesList.end();
    ++it) {
    fileoutPart<<"Detector: "<<it->first<<G4endl;
    for(int i = 0; i < it->second.size(); i++) {
      if (0 != detPartLayers.count(detPartType(it->first, it->second[i]))) {
        fileoutPart<<" * ";
      } else {
        fileoutPart<<"   ";
      }
      fileoutPart<<it->second[i]->GetParticleName()<<G4endl;
    }
  }
  fileoutPart.close();

  
  //std::ofstream fileoutPart;
  finishedAt = clock();
  double elapsedSecs = double(finishedAt - beginAt) / CLOCKS_PER_SEC;
  fileoutPart.open("runTimes.log", std::ofstream::out | std::ofstream::app);
  fileoutPart<<"Run "<<(G4int)G4RunManager::GetRunManager()->GetCurrentRun()->GetRunID()<<" time elapsed: "<<elapsedSecs<<G4endl;
  fileoutPart.close();
#endif
}

bool RunAction::checkHasBestSolution() {
  G4cout<<"Checking is best?"<<G4endl;
  bool hasBest = true;
  /*for (std::map<G4String, SlayerRA>::iterator it = depToDets.begin();
				it != depToDets.end();
				++it)
  {
    it->second = prepareError(it->second);
    if ((0 == it->second.nEvents) || ((1 == it->second.nEvents))) {
      G4cout<<"No entry particles at "<<it->first<<G4endl;
      hasBest = false;
      break;
    } else {
      G4int re = (int)(it->second.doseError / it->second.dose * 100);
      G4cout<<"Relative error at "<<it->first<<" is "<<re<<G4endl;
      if (re > 10) {
        hasBest = false;
        break;
      }
    }
  }*/

  G4int nov = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->nov;
  for (std::map<G4String, SlayerRA*>::iterator it = layerDepToDets.begin();
				it != layerDepToDets.end();
				++it) {
    G4cout<<"Check "<<it->first;
    SlayerRA* d = it->second;
    for (int i = 0; i < nov; i++) {
      if (d[i].doseError == 0.0) {
        return false;
      }
      G4int re = (int)(d[i].doseError / d[i].dose * 100);
      if (re > 10) {
        hasBest = false;
        G4cout<<"Voxel "<<i<<" has error: "<<re<<G4endl;
        return false;
        break;
      }
    }
  }

  return hasBest;
}

void RunAction::dumpStruct2File(SlayerRA target, G4String filename) {
  std::ofstream fileoutPart;
  fileoutPart.open(filename, std::ofstream::out | std::ofstream::trunc);
  fileoutPart<<target.depEnergy / MeV<<G4endl;
  fileoutPart<<target.depEnergy2 / MeV<<G4endl;
  fileoutPart<<target.dose / MeV<<G4endl;
  fileoutPart<<target.dose2 / MeV<<G4endl;
  fileoutPart<<target.nEvents<<G4endl;
  fileoutPart<<target.cellFlux / (1 / cm2)<<G4endl;
  fileoutPart.close();
}

void RunAction::dumpStruct2File(SlayerRA *target, G4String filename) {
  std::ofstream fileoutPart;
  fileoutPart.open(filename, std::ofstream::out | std::ofstream::trunc);
  fileoutPart<<target->depEnergy / MeV<<G4endl;
  fileoutPart<<target->depEnergy2 / MeV<<G4endl;
  fileoutPart<<target->dose / MeV<<G4endl;
  fileoutPart<<target->dose2 / MeV<<G4endl;
  fileoutPart<<target->nEvents<<G4endl;
  fileoutPart<<target->cellFlux / (1 / cm2)<<G4endl;
  fileoutPart.close();
}

SlayerRA RunAction::readStructFromFile(SlayerRA target, G4String filename) {
  std::ifstream fileoutPart;
  fileoutPart.open(filename, std::ifstream::in);
  fileoutPart>>target.depEnergy;
  fileoutPart>>target.depEnergy2;
  fileoutPart>>target.dose;
  fileoutPart>>target.dose2;
  fileoutPart>>target.nEvents;
  fileoutPart>>target.cellFlux;
  target.depEnergy *= MeV;
  target.depEnergy2 *= MeV;
  target.dose *= MeV;
  target.dose2 *= MeV;
  target.cellFlux *= (1 / cm2);
  fileoutPart.close();

  return target;
}

void RunAction::readStructParticlesFromFile(G4String det, G4String particle) {
  // 1. split "detector.particle" filename format
  /*addDetParticleData(det, particle, 0.0, 0.0,
      0.0, 0.0, 0, 0.0);*/
  // 2. add empty data with standart addDetParticleData
  std::ifstream fileoutPart;
  fileoutPart.open(det+"."+particle, std::ifstream::in);  
  G4double ene, ene2, d, d2, ne, cf;
  fileoutPart>>ene;
  fileoutPart>>ene2;
  fileoutPart>>d;
  fileoutPart>>d2;
  fileoutPart>>ne;
  fileoutPart>>cf;
  ene *= MeV;
  ene2 *= MeV;
  d *= MeV;
  d2 *= MeV;
  cf *= (1/cm2);
  fileoutPart.close();
  addDetParticleData(det, particle, ene, ene2,
      d, d2, ne, cf);
}

void RunAction::readList() {
  std::ifstream fileoutPart;
  fileoutPart.open("particles.list", std::ifstream::in);
  G4int szl;
  G4String det;
  G4String particle;
  fileoutPart>>szl;
  for (int i = 0; i < szl; i++) {
    fileoutPart>>det;
    fileoutPart>>particle;
    readStructParticlesFromFile(det, particle);
  }
}

void dumpList(std::vector<ppr> ll) {

}

void RunAction::dumpDep(G4String particle, SlayerRA *target, G4String filename) {
  prepareErrorS(target);
  std::ofstream fileoutPart;
  fileoutPart.open(filename, std::ofstream::app);
  fileoutPart<<"Particle "<<particle<<G4endl;

  G4double divider;
  if (!useContinue)
    divider = G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEvent();
  else 
    divider = G4double(previousRuns);

  fileoutPart<<G4BestUnit(target->depEnergy, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target->depEnergyError, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target->dose, "Dose")<<" \t";
  fileoutPart<<target->dose / (MeV /g)<<" \t";
  fileoutPart<<G4BestUnit(target->doseError, "Dose")<<" \t";
  fileoutPart<<target->cellFlux / (1 / cm2)<<" \t";
  fileoutPart<<target->nEvents<<" \t";
//  fileoutPart<<relativeError<<"%";
  fileoutPart<<" \n";

  fileoutPart<<G4BestUnit(target->depEnergy / divider, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target->depEnergyError, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target->dose / divider, "Dose")<<" \t";
  fileoutPart<<target->dose / divider / (MeV /g)<<" \t";
  fileoutPart<<G4BestUnit(target->doseError, "Dose")<<" \t";
  fileoutPart<<target->cellFlux / divider / (1 / cm2)<<" \t";
  fileoutPart<<target->nEvents<<" \t";
  fileoutPart<<" \n";

  fileoutPart.close();
}

void RunAction::dumpDep(SlayerRA target, G4String filename) {
  target = prepareError(target);

  std::ofstream fileoutPart;
  fileoutPart.open(filename);
  fileoutPart<<"Dep energy, MeV "<<" \t";
  fileoutPart<<"Energy error, MeV "<<" \t";
  fileoutPart<<"Dose, Gy "<<" \t";
  fileoutPart<<"Dose, MeV / g "<<" \t";
  fileoutPart<<"Dose error, Gy "<<" \t";
  fileoutPart<<"Cell flux, 1/cm2 "<<" \t";
  fileoutPart<<"Cell flux2, 1/cm2"<<" \t";
  fileoutPart<<"Events "<<" \t";
  fileoutPart<<"Dose error, percents";
  fileoutPart<<" \n";
  //fileoutPart<<"\n";
  fileoutPart<<std::defaultfloat;

  G4double v, vDo, n;
  v = target.nEvents*target.depEnergy2 - target.depEnergy*target.depEnergy;
  vDo = target.nEvents*target.dose2 - target.dose*target.dose;
  n = target.nEvents;
  if (n>1) {
    target.depEnergyError = 1.0*std::sqrt(v/(n-1));
    target.doseError = 1.0*std::sqrt(vDo/(n-1));
  }

  G4double divider;
  if (!useContinue)
    divider = G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEvent();
  else 
    divider = G4double(previousRuns);
  //G4cout<<"Divider: "<<divider<<G4endl;

  G4float relativeError;
  if (0.0 != target.doseError)
    relativeError = (int)(target.doseError / target.dose * 100) + 0.001*((int)(target.doseError / target.dose * 100000) % 1000);
  else
    relativeError = 100.0;

  if (relativeError < 10.0) {
    G4cout<<filename<<" has relative error < 10%"<<G4endl;
  } else {
    G4cout<<filename<<" has relative error > 10%"<<G4endl;
  }
  

  fileoutPart<<G4BestUnit(target.depEnergy, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target.depEnergyError, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target.dose, "Dose")<<" \t";
  fileoutPart<<target.dose / (MeV /g)<<" \t";
  fileoutPart<<G4BestUnit(target.doseError, "Dose")<<" \t";
  fileoutPart<<target.cellFlux / (1 / cm2)<<" \t";
  fileoutPart<<target.cellFlux2 / (1 / cm2)<<" \t";
  fileoutPart<<target.nEvents<<" \t";
  fileoutPart<<relativeError<<"%";
  fileoutPart<<" \n";

  fileoutPart<<G4BestUnit(target.depEnergy / divider, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target.depEnergyError, "Energy")<<" \t";
  fileoutPart<<G4BestUnit(target.dose / divider, "Dose")<<" \t";
  fileoutPart<<target.dose / divider / (MeV /g)<<" \t";
  fileoutPart<<G4BestUnit(target.doseError, "Dose")<<" \t";
  fileoutPart<<target.cellFlux / divider / (1 / cm2)<<" \t";
  fileoutPart<<target.cellFlux2 / divider / (1 / cm2)<<" \t";
  fileoutPart<<target.nEvents<<" \t";
  fileoutPart<<relativeError<<"%";
  fileoutPart<<" \n";

  fileoutPart.close();
}


void RunAction::dumpNoE() {
  std::ofstream fileoutPart;
  fileoutPart.open("_core.dump", std::ofstream::out | std::ofstream::trunc);
  fileoutPart<<previousRuns<<G4endl;
  fileoutPart.close();  
}
void RunAction::readNoE() {
  std::ifstream fileoutPart;
  fileoutPart.open("_core.dump", std::ifstream::in);
  fileoutPart>>previousRuns;
  fileoutPart.close();
}
