# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 01.04.14
@summary: 
'''
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os


def getColumnFromFile(filename, column):
    _fh = open(filename)
    res = []
    for i in _fh.readlines():
        res.append(float(i.split()[column]))
    return np.array(res)

#f1 = r'C:\Temp\geantProjects\neutronHE_test_build\Release\phantom0_vox.out'
#c1 = getColumnFromFile(f1, 7)
#for i in c1:
#    print i

#import sys
#sys.exit(0)

fig = plt.figure()
ax = fig.add_subplot(111)
#ax.set_yscale('log')
X = np.arange(500)
#print X

fdir = r'C:\Temp\geantProjects\neutronHE_test_build\ReleaseNHP4'
#fdir = r'C:\Temp\geantProjects\neutronHE_test_build\ProtonHP4'
#f1 = os.path.join(fdir, 'phantom0_vox.out')
#f2 = os.path.join(fdir, 'phantom1_vox.out')
#f3 = os.path.join(fdir, 'phantom2_vox.out')
#f4 = os.path.join(fdir, 'phantom3_vox.out')
#f5 = os.path.join(fdir, 'phantom4_vox.out')
f1 = os.path.join(fdir, 'nhp14')
f2 = os.path.join(fdir, 'nhp50')
f3 = os.path.join(fdir, 'nhp100')
f4 = os.path.join(fdir, 'nhp150')
f5 = os.path.join(fdir, 'nhp200')

#f2 = os.path.join(fdir, 'phantom1_vox.out')
#f3 = os.path.join(fdir, 'phantom2_vox.out')
#f4 = os.path.join(fdir, 'phantom3_vox.out')
#f5 = os.path.join(fdir, 'phantom4_vox.out')
#f5 = r'C:\Temp\geantProjects\neutronHE_test_build\Release\p100'

c1 = getColumnFromFile(f1, 5)
c2 = getColumnFromFile(f2, 5)
c3 = getColumnFromFile(f3, 5)
c4 = getColumnFromFile(f4, 5)
c5 = getColumnFromFile(f5, 5)

print len(X), len(c1)

q1 = []
q2 = []
q3 = []
q4 = []
q5 = []

for i in range(len(X)-1):
    q1.append(np.fabs(c1[i]-c1[i+1]))
    q2.append(np.fabs(c2[i]-c2[i+1]))
    q3.append(c3[i]-c3[i+1])
    q4.append(np.fabs(c4[i]-c4[i+1]))
    q5.append(c5[i]-c5[i+1])

X2 = np.arange(499)

l1, = plt.plot(X, c1)
l2, = plt.plot(X, c2)
l3, = plt.plot(X, c3)
l4, = plt.plot(X, c4)
#l4, = plt.plot(X, c4)
l5, = plt.plot(X, c5)

#plt.legend( (l1, l2, l3, l4, l5),
#            ("n 50 MeV", "n 100 MeV", "n 150 MeV", "n 200 MeV", "n 14 MeV"),
#            'upper left')
plt.show()