G4Material* air = new G4Material( "Air",
                               density = 0.001*g/cm3,
                               numberofElements = 2);
air->AddElement(elN,0.800);
air->AddElement(elO,0.200);

G4Material* adiposetissue = new G4Material( "AdiposeTissue",
                               density = 0.950*g/cm3,
                               numberofElements = 7);
adiposetissue->AddElement(elNA,0.001);
adiposetissue->AddElement(elC,0.589);
adiposetissue->AddElement(elS,0.001);
adiposetissue->AddElement(elN,0.007);
adiposetissue->AddElement(elCL,0.001);
adiposetissue->AddElement(elO,0.287);
adiposetissue->AddElement(elH,0.114);

G4Material* normaltissue = new G4Material( "NormalTissue",
                               density = 1.060*g/cm3,
                               numberofElements = 9);
normaltissue->AddElement(elCR,0.002);
normaltissue->AddElement(elBK,0.143);
normaltissue->AddElement(elMT,0.003);
normaltissue->AddElement(elBK,0.003);
normaltissue->AddElement(elPO,0.708);
normaltissue->AddElement(elBK,0.002);
normaltissue->AddElement(elBA,0.034);
normaltissue->AddElement(elBK,0.003);
normaltissue->AddElement(elNB,0.102);

G4Material* water = new G4Material( "Water",
                               density = 1.043*g/cm3,
                               numberofElements = 2);
water->AddElement(elO,0.333);
water->AddElement(elH,0.667);

G4Material* bone = new G4Material( "Bone",
                               density = 1.930*g/cm3,
                               numberofElements = 9);
bone->AddElement(elNA,0.003);
bone->AddElement(elC,0.159);
bone->AddElement(elP,0.094);
bone->AddElement(elMG,0.002);
bone->AddElement(elS,0.003);
bone->AddElement(elCA,0.213);
bone->AddElement(elN,0.042);
bone->AddElement(elO,0.448);
bone->AddElement(elH,0.036);
