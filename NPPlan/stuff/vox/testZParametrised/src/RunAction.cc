#include "RunAction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
//#include "G4VUserDetectorConstruction.hh"
#include "DetectorSD.hh"
#include "DetectorConstruction.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4THitsMap.hh"
#include "G4SDManager.hh"
#include <ctime>

#define NO_RA 1
#define NO_PRINT_RESULTS 1

RunAction::RunAction() : G4UserRunAction()
{
  G4RunManager::GetRunManager()->SetPrintProgress(10000);
}

RunAction::~RunAction() {
}

void RunAction::addDetData(G4String dName, G4int ix, G4int iy, G4int iz, detLayer dd) {
  G4RunManager::GetRunManager()->SetPrintProgress(10000);
  //G4cout<<"Got: "<<ix<<" "<<iy<<" "<<iz<<G4endl;
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  ax = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(dName))->getMX();
  ay = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(dName))->getMY();
  az = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(dName))->getMZ();
  if (0 == results.count(dName)) {
    resLayer ***rs = new resLayer**[ax];
    for (int i = 0; i < ax; i++) {
      rs[i] = new resLayer*[ay];
      for (int j = 0; j < ay; j++) {
        rs[i][j] = new resLayer[az];
        //for (int z = 0; z < aZ; z++) {
        rs[ix][iy][iz] += dd;
        //}
      }
    }
    results.insert(std::pair<G4String, resLayer***>(dName, rs));
  } else {
    results.find(dName)->second[ix][iy][iz] += dd;
  }
}

void RunAction::addDetData(G4String dName, G4int ix, detLayer dd) {
  //G4cout << "RA addDetData called with " << dName <<" "<< ix <<" "<< dd.nEvents <<" "<<dd.depEnergy<<G4endl;
  if (0 == resultsL.count(dName)) {
    G4SDManager* sdMan = G4SDManager::GetSDMpointer();
    ax = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(dName))->getMX();
    ay = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(dName))->getMY();
    az = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(dName))->getMZ();
    resLayer *rs = new resLayer[ax*ay*az];
    for(int i = 0; i < ax*ay*az; i++) {
      rs[i].Nullify();
    }
    rs[ix] += dd;
    resultsL.insert(std::pair<G4String, resLayer*>(dName, rs));
  } else {
    resultsL.find(dName)->second[ix] += dd;
  }
}

void RunAction::BeginOfRunAction(const G4Run* cRun)
{ 
  beginAt = clock();
#ifndef NO_RA
  layerDepToDets.clear();
  beginAt = clock();

  //((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->getMainDetector()->addIon(6, 12);
  DetectorSD1Layer *md = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->getMainDetector();
  md->addParticle("neutron");
  md->addParticle("proton");
  md->addParticle("alpha");
  md->addParticle("gamma");
  md->addParticle("e-");
  md->addIon(6, 12);
  md->addIon(8, 16);
#endif
}

bool RunAction::isBestVoxelSolution() {
  G4float we = 0.0;
  for (std::map<G4String, resLayer*>::iterator it = resultsL.begin();
    it != resultsL.end();
    ++it) {
      G4SDManager* sdMan = G4SDManager::GetSDMpointer();
      ax = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(it->first))->getMX();
      ay = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(it->first))->getMY();
      az = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(it->first))->getMZ();
      for (int ix = 0; ix < ax*ay*az; ix++)  {
        float curErr;
        curErr = getRelativeError(it->second[ix].depEnergy, it->second[ix].depEnergyError);
        if (curErr > 10.0) {
          return false;
        }
        if (curErr > we) {
          we = curErr;
        }
      }
  }
  if (we < 10.0) {
    G4cout<<"Worst voxel error < 10%"<<G4endl;
    return true;
  }
  return false;
}

void RunAction::dumpDetData(bool skipZero=false) {
  std::ofstream ofStream;
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  for (std::map<G4String, resLayer*>::iterator it = resultsL.begin();
    it != resultsL.end();
    ++it) {
      ofStream.open(G4String("det_")+it->first+".out", std::ios::out | std::ios::trunc);
      G4cout<<it->first;
      ax = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(it->first))->getMX();
      ay = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(it->first))->getMY();
      az = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(it->first))->getMZ();
      for (int ix = 0; ix < ax*ay*az; ix++)  {
            if (skipZero && it->second[ix].depEnergy == 0.0) {
              continue;
            }
            it->second[ix].calculateError();
            ofStream<<ix<<" ";
            ofStream<<it->second[ix].depEnergy / (MeV)<<" ";
            ofStream<<it->second[ix].depEnergyError / (MeV)<<" ";
            if (it->second[ix].nEvents > 1) {
              ofStream<<getRelativeError(it->second[ix].depEnergy, it->second[ix].depEnergyError)<<"% ";
            } else {
              ofStream<<"100"<<"% ";
            }
            ofStream<<it->second[ix].dose / (MeV / g)<<" ";
            ofStream<<it->second[ix].doseError / (MeV / g)<<" ";
            ofStream<<it->second[ix].dose / (CLHEP::gray)<<" ";
            ofStream<<it->second[ix].doseError / (CLHEP::gray)<<" ";
            ofStream<<G4BestUnit(it->second[ix].dose, "Dose")<<" ";
            ofStream<<G4BestUnit(it->second[ix].doseError, "Dose")<<" ";
            ofStream<<it->second[ix].cellFlux / (1/cm2)<<" ";
            ofStream<<it->second[ix].cellFluxError / (1/cm2)<<" ";
            if (it->second[ix].nEvents > 1) {
              ofStream<<getRelativeError(it->second[ix].cellFlux, it->second[ix].cellFluxError)<<"% ";
            } else {
              ofStream<<"100"<<"% ";
            }
            ofStream<<it->second[ix].nEvents<<" ";
            ofStream<<G4endl;
      }
      ofStream.close();
  }
}

void RunAction::EndOfRunAction(const G4Run* cRun)
{

  finishedAt = clock();
  double elapsedSecs = double(finishedAt - beginAt) / CLOCKS_PER_SEC;
  G4cout<<"Completed at: "<<elapsedSecs<<" sec."<<G4endl;

  /*for (std::map<G4String, resLayer***>::iterator it = results.begin();
    it != results.end();
    ++it) {
      G4cout<<it->first;
      for (int ix = 0; ix < ax; ix++) {
        for (int iy = 0; iy < ay; iy++) {
          for (int iz = 0; iz < az; iz++) {
            it->second[ix][iy][iz].calculateError();
            G4cout<<ix<<" "<<iy<<" "<<iz<<" ";
            G4cout<<it->second[ix][iy][iz].depEnergy / (MeV)<<" ";
            G4cout<<it->second[ix][iy][iz].depEnergyError / (MeV)<<" ";
            G4cout<<it->second[ix][iy][iz].dose / (MeV / g)<<" ";
            G4cout<<it->second[ix][iy][iz].doseError / (MeV / g)<<" ";
            G4cout<<it->second[ix][iy][iz].dose / (CLHEP::gray)<<" ";
            G4cout<<it->second[ix][iy][iz].doseError / (CLHEP::gray)<<" ";
            G4cout<<it->second[ix][iy][iz].cellFlux / (1/cm2)<<" ";
            G4cout<<it->second[ix][iy][iz].cellFluxError / (1/cm2)<<" ";
            G4cout<<G4endl;
          }
        }
      }
  }*/
  bool skipZero;
  dumpDetData(skipZero=true);
#ifndef NO_PRINT_RESULTS
  G4SDManager* sdMan = G4SDManager::GetSDMpointer();
  for (std::map<G4String, resLayer*>::iterator it = resultsL.begin();
    it != resultsL.end();
    ++it) {
      G4cout<<it->first;
      ax = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(it->first))->getMX();
      ay = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(it->first))->getMY();
      az = ((DetectorSD3DLayer*)sdMan->FindSensitiveDetector(it->first))->getMZ();
      for (int ix = 0; ix < ax*ay*az; ix++) {
            it->second[ix].calculateError();
            //G4cout<<ix<<" "<<iy<<" "<<iz<<" ";
            G4cout<<ix<<" ";
            G4cout<<it->second[ix].depEnergy / (MeV)<<" ";
            G4cout<<it->second[ix].depEnergyError / (MeV)<<" ";
            G4cout<<static_cast<float>((int)(it->second[ix].depEnergyError / it->second[ix].depEnergy * 10000))/100<<"% ";
            G4cout<<it->second[ix].dose / (MeV / g)<<" ";
            G4cout<<it->second[ix].doseError / (MeV / g)<<" ";
            G4cout<<it->second[ix].dose / (CLHEP::gray)<<" ";
            G4cout<<it->second[ix].doseError / (CLHEP::gray)<<" ";
            G4cout<<G4BestUnit(it->second[ix].dose, "Dose")<<" ";
            G4cout<<G4BestUnit(it->second[ix].doseError, "Dose")<<" ";
            G4cout<<it->second[ix].cellFlux / (1/cm2)<<" ";
            G4cout<<it->second[ix].cellFluxError / (1/cm2)<<" ";
            G4cout<<it->second[ix].nEvents<<" ";
            G4cout<<G4endl;
      }
  }
#endif
  //G4SDManager::GetSDMpointer()->GetCollectionID("testDet/DoseDeposit");
  //G4SDManager::GetSDMpointer()->GetCollectionID("testDet/FluxMap");
#ifndef NO_RA
  std::ofstream fileoutPart;
  G4int nov = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->nov;

  for (std::map<G4String, resLayer*>::iterator it = detLayers.begin();
				it != detLayers.end();
				++it) {
    fileoutPart.open(it->first+"_sum.out");
    fileoutPart<<"Summary data\n";
    fileoutPart<<"Layer\tEnergy, MeV\tEnergy error, MeV\tDose, Mev/g\tDose error, MeV/g\tDose, Gy\tDose error, Gy\tNumber of events\tCell flux\t"<<G4endl;;
    for (int i = 0; i < nov; i++) {

      it->second[i].calculateError();
      fileoutPart<<std::setfill('0') << std::setw(5)<<i<<"\t";
      fileoutPart<<std::defaultfloat;
      fileoutPart<<it->second[i].depEnergy / MeV<<"\t";
      fileoutPart<<it->second[i].depEnergyError / MeV<<"\t";
      fileoutPart<<it->second[i].dose / (MeV / g)<<"\t";
      fileoutPart<<it->second[i].doseError / (MeV / g)<<"\t";
      fileoutPart<<G4BestUnit(it->second[i].dose, "Dose")<<"\t";
      fileoutPart<<G4BestUnit(it->second[i].doseError, "Dose")<<"\t";
      fileoutPart<<it->second[i].nEvents<<"\t";
      fileoutPart<<it->second[i].cellFlux<<"\t";
      fileoutPart<<G4endl;
    }
    fileoutPart.close();
  }

    for (std::map<detPartType, resLayer*>::iterator it = detPartLayers.begin();
				it != detPartLayers.end();
				++it) {
      fileoutPart.open(it->first.first+"_"+it->first.second->GetParticleName()+".out");
      fileoutPart<<"Particle "<<it->first.second->GetParticleName()<<"\n";
      fileoutPart<<"Layer\tEnergy, MeV\tEnergy error, MeV\tDose, Mev/g\tDose error, MeV/g\tDose, Gy\tDose error, Gy\tNumber of events\tCell flux\t"<<G4endl;
      for (int i = 0; i < nov; i++) {
        it->second[i].calculateError();
        fileoutPart<<i<<"\t";
        fileoutPart<<it->second[i].depEnergy / MeV<<"\t";
        fileoutPart<<it->second[i].depEnergyError / MeV<<"\t";
        fileoutPart<<it->second[i].dose / (MeV / g)<<"\t";
        fileoutPart<<it->second[i].doseError / (MeV / g)<<"\t";
        fileoutPart<<G4BestUnit(it->second[i].dose, "Dose")<<"\t";
        fileoutPart<<G4BestUnit(it->second[i].doseError, "Dose")<<"\t";
        fileoutPart<<it->second[i].nEvents<<"\t";
        fileoutPart<<it->second[i].cellFlux<<"\t";
        fileoutPart<<G4endl;
      }
      fileoutPart.close();
    }

  fileoutPart.open("test2.dump", std::ofstream::out);
  for (std::map<G4String, pdvec>::iterator it = allParticlesList.begin();
    it != allParticlesList.end();
    ++it) {
    fileoutPart<<"Detector: "<<it->first<<G4endl;
    for(int i = 0; i < it->second.size(); i++) {
      if (0 != detPartLayers.count(detPartType(it->first, it->second[i]))) {
        fileoutPart<<" * ";
      } else {
        fileoutPart<<"   ";
      }
      fileoutPart<<it->second[i]->GetParticleName()<<G4endl;
    }
  }
  fileoutPart.close();

  
  //std::ofstream fileoutPart;

#endif
  finishedAt = clock();
  std::ofstream fileoutPart;
  fileoutPart.open("runTimes.log", std::ofstream::out | std::ofstream::app);
  fileoutPart<<"Run "<<(G4int)G4RunManager::GetRunManager()->GetCurrentRun()->GetRunID()<<" time elapsed: "<<elapsedSecs<<G4endl;
  fileoutPart.close();
}

