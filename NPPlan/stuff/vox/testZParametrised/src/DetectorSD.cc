#include "DetectorSD.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4RunManager.hh"
#include "RunAction.hh"
#include "layerData.hh"
#include "G4ThreeVector.hh"

DetectorSD3DLayer::DetectorSD3DLayer(G4String name, G4int nox, G4int noy, G4int noz): G4VSensitiveDetector(name), outName(name),
  noX(nox), noY(noy), noZ(noz), targetLayer(0), weighted(false)
{
  createData();
}

DetectorSD3DLayer::DetectorSD3DLayer(G4String name, G4ThreeVector voxelData): G4VSensitiveDetector(name), outName(name), weighted(false) {
  noX = (G4int)voxelData.getX();
  noY = (G4int)voxelData.getY();
  noZ = (G4int)voxelData.getZ();
  createData();
}

DetectorSD3DLayer::DetectorSD3DLayer(G4String name, G4int nox, G4int noy, G4int noz, G4int tl): G4VSensitiveDetector(name), outName(name),
  noX(nox), noY(noy), noZ(noz), targetLayer(tl), weighted(false)
{
  createData();
}

DetectorSD3DLayer::~DetectorSD3DLayer() {
  delete dataL;
}

void DetectorSD3DLayer::createData() {
  dataL = new detLayer[noX*noY*noZ];
}

void DetectorSD3DLayer::nullify() {
  for (int ix = 0; ix < noX*noY*noZ; ix++) {
    dataL[ix].Nullify();
  }
}

void DetectorSD3DLayer::Initialize(G4HCofThisEvent*) {
  nullify();
}

G4bool DetectorSD3DLayer::ProcessHits(G4Step* step, G4TouchableHistory*) {
	G4StepPoint* prePoint = step->GetPreStepPoint();
	G4TouchableHandle touchable = prePoint->GetTouchableHandle();
  G4double density = step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetMaterial()->GetDensity();
	/*G4cout<<" "<<touchable->GetCopyNumber(0)<<
		" "<<touchable->GetCopyNumber(1)<<
		" "<<touchable->GetCopyNumber(2)<<
		" "<<touchable->GetCopyNumber(3)
		<<"  energy: "<<step->GetTotalEnergyDeposit()
    <<step->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetMaterial()->GetName()
		<<G4endl;  */

  G4double stepLength = step->GetStepLength();
  G4double volume;
  if (0.0 != preVol) {
    //volume = 10*cm*10*cm*50*cm/500;
    volume = preVol;
  } else {
    G4int idx = ((G4TouchableHistory*)
	       (step->GetPreStepPoint()->GetTouchable()))
         ->GetReplicaNumber(targetLayer);
    volume = ComputeVolume(step, idx);
  }
  G4double weight = step->GetPreStepPoint()->GetWeight(); 
  G4double edep = step->GetTotalEnergyDeposit();
  G4double voxelMass = density*volume;
  G4double dose = edep / voxelMass;
  G4double cf = stepLength / volume;

  G4int iz = touchable->GetCopyNumber(targetLayer);
  G4int iy = touchable->GetCopyNumber(targetLayer+1);
  G4int ix = touchable->GetCopyNumber(targetLayer+2);

  //data[ix][iy][iz].Update(edep, dose, cf);
  //G4int copyID = iz + noZ*iy + noY*noZ*ix;
  G4int copyID = ix + iy*noX + iz*noX*noY;

  dataL[copyID].Update(edep, dose, cf);

  return true;
}

void DetectorSD3DLayer::EndOfEvent(G4HCofThisEvent*) {
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
  /*for (int ix = 0; ix < noX; ix++) {
    for (int iy = 0; iy < noY; iy++) {
      for (int iz = 0; iz < noX; iz++) {
        //data[ix][iy][iz].Nullify();
        runAction->addDetData(outName, ix, iy, iz, data[ix][iy][iz]);
      }
    }
  }*/
  for (int i = 0; i < noX*noY*noZ; i++) {
    if (dataL[i].nEvents != 0) {
      //G4cout<<i<<" "<<dataL[i].depEnergy<<G4endl;
      runAction->addDetData(outName, i, dataL[i]);
    }
  }
  //runAction->addDetData(outName, noX, noY, noZ, );
}

G4double DetectorSD3DLayer::ComputeVolume(G4Step* aStep, G4int idx){
  G4VPhysicalVolume* physVol = aStep->GetPreStepPoint()->GetPhysicalVolume();
  G4VPVParameterisation* physParam = physVol->GetParameterisation();
  G4VSolid* solid = 0;
  if(physParam)
  { // for parameterized volume
    if(idx<0)
    {
      G4ExceptionDescription ED;
      ED << "Incorrect replica number --- GetReplicaNumber : " << idx << G4endl;
      G4Exception("DetectorSD3DLayer::ComputeVolume","Detector",JustWarning,ED);
    }
    solid = physParam->ComputeSolid(idx, physVol);
    solid->ComputeDimensions(physParam,idx,physVol);
  }
  else
  { // for ordinary volume
    solid = physVol->GetLogicalVolume()->GetSolid();
  }
  
  return solid->GetCubicVolume();
}