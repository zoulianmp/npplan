//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: EventAction.cc 75604 2013-11-04 13:17:26Z gcosmo $
//
/// \file EventAction.cc
/// \brief Implementation of the EventAction class

#include "EventAction.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4UnitsTable.hh"
#include "RunAction.hh"

#include "Randomize.hh"
#include <iomanip>

#define NO_EA 1

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::EventAction()
 : G4UserEventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::~EventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4THitsMap<G4double>* 
EventAction::GetHitsCollection(G4int hcID,
                                  const G4Event* event) const
{
  G4THitsMap<G4double>* hitsCollection 
    = static_cast<G4THitsMap<G4double>*>(
        event->GetHCofThisEvent()->GetHC(hcID));
  
  if ( ! hitsCollection ) {
    G4ExceptionDescription msg;
    msg << "Cannot access hitsCollection ID " << hcID; 
    G4Exception("EventAction::GetHitsCollection()",
      "MyCode0003", FatalException, msg);
  }         

  return hitsCollection;
}    

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double EventAction::GetSum(G4THitsMap<G4double>* hitsMap) const
{
  G4double sumValue = 0;
  std::map<G4int, G4double*>::iterator it;
  for ( it = hitsMap->GetMap()->begin(); it != hitsMap->GetMap()->end(); it++) {
    sumValue += *(it->second);
  }
  return sumValue;  
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::BeginOfEventAction(const G4Event* /*event*/)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::EndOfEventAction(const G4Event* event)
{  
#ifndef NO_EA
   /*// Get hist collections IDs
  if ( fAbsoEdepHCID == -1 ) {
    fAbsoEdepHCID 
      = G4SDManager::GetSDMpointer()->GetCollectionID("Absorber/Edep");
    fGapEdepHCID 
      = G4SDManager::GetSDMpointer()->GetCollectionID("Gap/Edep");
    fAbsoTrackLengthHCID 
      = G4SDManager::GetSDMpointer()->GetCollectionID("Absorber/TrackLength");
    fGapTrackLengthHCID 
      = G4SDManager::GetSDMpointer()->GetCollectionID("Gap/TrackLength");
  }
  
  // Get sum values from hits collections
  //
  G4double absoEdep = GetSum(GetHitsCollection(fAbsoEdepHCID, event));
  G4double gapEdep = GetSum(GetHitsCollection(fGapEdepHCID, event));

  G4double absoTrackLength 
    = GetSum(GetHitsCollection(fAbsoTrackLengthHCID, event));
  G4double gapTrackLength 
    = GetSum(GetHitsCollection(fGapTrackLengthHCID, event));
    */
  
  ddId = G4SDManager::GetSDMpointer()->GetCollectionID("testDet/DoseDeposit");
  ffId = G4SDManager::GetSDMpointer()->GetCollectionID("testDet/FluxMap");

  G4THitsMap<G4double>* ddEvt = GetHitsCollection(ddId, event);
  G4THitsMap<G4double>* ffEvt = GetHitsCollection(ffId, event);


  std::map<G4int, G4double*>::iterator it;
  for ( it = ddEvt->GetMap()->begin(); it != ddEvt->GetMap()->end(); it++) {
//sumValue += *(it->second);
    G4cout<<it->first<<" "<<it->second<<" "<<*(it->second)<<G4endl;
  }
  G4cout<<"Flux"<<G4endl<<G4endl;
  
  for ( it = ffEvt->GetMap()->begin(); it != ffEvt->GetMap()->end(); it++) {
//sumValue += *(it->second);
    G4cout<<it->first<<" "<<it->second<<" "<<*(it->second)<<G4endl;
  }
#endif
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
