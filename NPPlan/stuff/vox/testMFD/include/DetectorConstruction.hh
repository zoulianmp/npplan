#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1

#include "G4VUserDetectorConstruction.hh"
#include <vector>
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"


class G4LogicalVolume;
//class G4ThreeVector;
//class G4Material;
class DetectorSD1Layer;

/*struct detData {
  G4String name;
  G4String outputName;
  G4LogicalVolume *associatedVolume;
  G4String type;
  G4int noX, noY, noZ;
};*/

class DetectorConstruction: public G4VUserDetectorConstruction
{                 
  public:
    DetectorConstruction();
    ~DetectorConstruction();

    G4VPhysicalVolume* Construct();
  //private:

  //std::vector<detData*> detectorsData;
};

#endif

