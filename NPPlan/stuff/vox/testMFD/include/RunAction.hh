#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"
#include "layerData.hh"
#include <map>
#include <vector>
#include <ctime>


class G4Run;
class RunAction : public G4UserRunAction
{
  public:
    RunAction();
    virtual ~RunAction();

    void addDetData(G4String, G4int, G4int, G4int, detLayer);

    virtual void BeginOfRunAction(const G4Run* run);
    virtual void   EndOfRunAction(const G4Run* run);

  private:
    G4int ax, ay, az;
    std::map<G4String, resLayer***> results;

	  clock_t beginAt;
	  clock_t finishedAt;
};

#endif