# -*- coding: utf8 -*-
'''

just 2 simple images - 1 from dicom, 2 - from png with transparency

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 03.03.14
@summary: 
'''

# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 03.03.14
@summary:
'''

import vtk
import matplotlib
matplotlib.use('Agg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg

reader = vtk.vtkDICOMImageReader()
im = vtk.vtkImageMapToWindowLevelColors()
im.SetWindow(1000)
im.SetLevel(250)
reader.SetFileName(r'C:\Users\Chernukha\Pictures\DICOM\01111140\84269606')
im.SetInput(reader.GetOutput())

layerReader = vtk.vtkPNGReader()
layerReader.SetFileName(r'C:\Temp\test1.png')

renderer = vtk.vtkRenderer()
renderer.SetBackground(1, 1, 1)
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)

#an interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)

#add the actors to the scene
atext = vtk.vtkTexture()
atext.SetInputConnection(im.GetOutputPort())
plane = vtk.vtkPlaneSource()
planeMapper = vtk.vtkPolyDataMapper()
planeMapper.SetInputConnection(plane.GetOutputPort())
planeMapper.SetScalarModeToUsePointData()
planeMapper.SetColorModeToMapScalars()
planeActor = vtk.vtkActor()

planeActor.SetMapper(planeMapper)
planeActor.SetTexture(atext)

atextLayer = vtk.vtkTexture()
atextLayer.SetInputConnection(layerReader.GetOutputPort())
planeLayer = vtk.vtkPlaneSource()
planeMapperLayer = vtk.vtkPolyDataMapper()
planeMapperLayer.SetInputConnection(planeLayer.GetOutputPort())
planeMapperLayer.SetScalarModeToUsePointData()
planeMapperLayer.SetColorModeToMapScalars()
planeActorLayer = vtk.vtkActor()
planeActorLayer.SetMapper(planeMapperLayer)
planeActorLayer.SetTexture(atextLayer)

renderer.AddActor(planeActor)
renderer.AddActor(planeActorLayer)


transform = vtk.vtkTransform()
#transform.Translate(1.0, 0.0, 0.0)

axes = vtk.vtkAxesActor()
#  The axes are positioned with a user transform
axes.SetUserTransform(transform)

# properties of the axes labels can be set as follows
# this sets the x axis label to red
axes.GetXAxisCaptionActor2D().GetCaptionTextProperty().SetColor(1,0,0)

# the actual text of the axis label can be changed:
# axes->SetXAxisLabelText("test");

#renderer.AddActor(axes)

renderer.ResetCamera()
renderWindow.Render()

# begin mouse interaction
renderWindowInteractor.Start()