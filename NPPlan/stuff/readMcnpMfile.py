# -*- coding: utf8 -*-
'''

@author: mrxak
@copyright: MRRC Obninsk 2013
@version: 1
@date: 11.02.13
@summary: 
'''

from time import strptime
import numpy as np

class mcnpReaderBase(object):
    """
    Базовый класс для чтения выходных данных mcnp-файла
    """
    def __init__(self, filename=''):
        """
        @param filename: путь к файлу
        @type filename: string
        """
        self._filename = filename
        self._filestream = open(self._filename, 'r')

        self._runnerInfo = {}
        self.readInfo()

        self._params = {}
        self._data = {}
        self.readValues()

    def makeOriginalFileName(self):
        """
        Обрезает последнюю букву файла
        @return: имя файла без m
        @rtype: string
        """
        return self._filename[:-1]

    def readInfo(self):
        """
        Читает информацию о запуске: версию расчётной программы, время запуска и имя программы
        """
        line = self._filestream.readline()
        #program, version, date, time = line.split(' ')
        splitted = [i for i in line.split(' ') if i != '']
        self._runnerInfo['version'] = '%s %s' %(splitted[0], splitted[1])
        self._runnerInfo['start'] = strptime("%s %s" %(splitted[2], splitted[3]), '%m/%d/%y %H:%M:%S')
        self._runnerInfo['name'] = self._filestream.readline()
        # print self._runnerInfo
        #print program, version, date, time

    def readValues(self):
        self.readTalliesParams()
        self.readTallyData()
        #for line in self._filestream.readlines():
        #    print line

    def _skinUntil(self):
        pass

    def readTalliesParams(self):
        """
        Читает количество и список tally (строка ntal и следующая)
        """
        line = self._filestream.readline()
        while 'ntal' not in line:
            # skipping lines before ntal
            line = self._filestream.readline()

        self._params['talliesQuantity'] = int(line.split(' ')[-1])
        line = self._filestream.readline()
        self._params['talliesList'] = [int(i) for i in line.split(' ') if i != '']

        #print self._params

    def readTallyData(self):
        """
        Читает в цикле данные для каждого tally.
        Записывает параметры в массив self._data
        Ключ для записи - номер tally
        Записываемые параметры:
          info - данные от tally до следующей строки
          quantity - количество
          fDataCells - np.array(номера ячеек, в которых считается tally)
          fValues - np.array(значения по порядку)
          fErrors - np.array(ошибки по порядку)
        """
        for i in self._params['talliesList']:
            self._data[i] = {}
            curData = self._data[i]
            while 1:
                line = self._filestream.readline()
                if 'tally' in line:
                    break
            type = [int(i) for i in line.split(' ')[1:] if i != '']
            # print type
            curData['info'] = type
            #print self._data
            while 1:
                line = self._filestream.readline()
                if 'f' == line[0]:
                    break
                try:
                    curData['info'].extend([int(i) for i in line.split(' ')[1:] if i != ''])
                except ValueError:
                    curData['name'] = line
            #a = np.array()
            curData['quantity'] = int(line.split(' ')[-1])
            #print self._data
            fDataCells = []
            while 1:
                line = self._filestream.readline()
                if line[0] == 'd':
                    break
                fDataCells.extend([int(i) for i in line.split(' ')[1:] if i != ''])
            curData['fDataCells'] = np.array(fDataCells, dtype=np.int16)
            curData['others'] = {}
            while 1:
                spl = line.split(' ')
                curData['others'][spl[0]] = int(spl[-1])
                line = self._filestream.readline()
                if 'vals' in line:
                    break
            #print self._data
            vals = []
            errs = []
            for i in range(curData['quantity']):
                line = self._filestream.readline()
                if 'tfc' in line:
                    break
                parsedLine = self.parseLine(line)
                vals.extend(parsedLine[0])
                errs.extend(parsedLine[1])
                pass
            curData['fValues'] = np.array(vals, dtype=np.float64)
            curData['fErrors'] = np.array(errs, dtype=np.float64)
            #print self._data
            pass

        pass

    def parseLine(self, line):
        """
        Парсит строку в два разных массива (значения и ошибки)
        @param line: строка
        @type line: string
        @return: ([val1, val2,...], [err1, err2,...])
        @rtype: tuple
        """
        q = [float(i) for i in line.split(' ')[1:] if i != '']
        return [q[i] for i in range(len(q)) if i % 2 == 0], [q[i] for i in range(len(q)) if i % 2 == 1]

class mcnpReaderSimple(mcnpReaderBase):
    def __init__(self, filename=''):
        mcnpReaderBase.__init__(self, filename)

    def getValueInCell(self, cell, tally=-1):
        """
        Возвращает значение в переданной ячейке cell для заданного tally
        @param cell: ячейка
        @type cell: int
        @param tally: tally
        @type tally: int
        @return: значение
        @rtype: float
        """
        if -1 == tally:
            tally = self._data.keys()[0]
        #ind = self._data[tally]['fDataCells'].index(cell)
        #ind = self._data[tally]['fDataCells'].where(item==cell)
        ind = np.where(self._data[tally]['fDataCells']==cell)
        return float(self._data[tally]['fValues'][ind])

    def getErrorInCell(self, cell, tally=-1):
        """
        Возвращает ошибку в переданной ячейке cell для заданного tally
        @param cell: ячейка
        @type cell: int
        @param tally: tally
        @type tally: int
        @return: ошибка
        @rtype: float
        """
        if -1 == tally:
            tally = self._data.keys()[0]
        ind = np.where(self._data[tally]['fDataCells']==cell)
        return float(self._data[tally]['fErrors'][ind])

    def getValueTupleInCell(self, cell, tally=-1):
        """
        Возвращает tuple(значение, ошибка) в переданной ячейке cell для заданного tally
        @param cell: ячейка
        @type cell: int
        @param tally: tally
        @type tally: int
        @return: (значение, ошибка)
        @rtype: tuple
        """
        if -1 == tally:
            tally = self._data.keys()[0]
        ind = np.where(self._data[tally]['fDataCells']==cell)
        return float(self._data[tally]['fValues'][ind]), float(self._data[tally]['fErrors'][ind])

class mcnpReaderGrid(mcnpReaderBase):
    def __init__(self, filename='', shape=(1, 0, 0)):
        self._shape = shape
        mcnpReaderBase.__init__(self, filename)
        self.reshape()

    def reshape(self):
        for i in self._data:
            vals = self._data[i]['fValues']
            errs = self._data[i]['fErrors']
            self._data[i]['shapedValues'] = vals.reshape(self._shape)
            self._data[i]['shapedErrors'] = errs.reshape(self._shape)
            self._data[i]['max'] = np.max(vals)
            self._data[i]['meanError'] = np.mean(errs[errs>0.0])
            self._data[i]['meanErrorsPerSlice'] = np.mean(np.mean(self._data[i]['shapedErrors'], axis=2), axis=1)
            self._data[i]['maxValuesPerSlice'] = np.max(np.max(self._data[i]['shapedErrors'], axis=2), axis=1)
            print len(np.mean(self._data[i]['shapedErrors'], axis=2))
            print np.mean(self._data[i]['shapedErrors'], axis=2)
        print self._data
        print np.min(self._data[6]['shapedErrors'])
        print np.max(self._data[6]['shapedErrors'])
        print np.mean(self._data[6]['shapedErrors'])

        print np.min(self._data[6]['fErrors'][self._data[6]['fErrors']>0.0])
        print np.max(self._data[6]['fErrors'][self._data[6]['fErrors']>0.0])
        print np.mean(self._data[6]['fErrors'][self._data[6]['fErrors']>0.0])
        pass


if '__main__' == __name__:
    import sys

    #c = mcnpReaderBase('tstrgsm')

    #c = mcnpReaderBase('tst5a2m_')

    #c = mcnpReaderBase('Tubusm')

    #c = mcnpReaderSimple('Tubusm')
    #print c.getValueInCell(8, 6)
    #print c.getValueInCell(8, 16)

    #print c.getValueTupleInCell(cell=15, tally=6)

    #c = mcnpReaderGrid('tstrgsm', shape=(41, 41, 41))
    #print c._data[6]['shapedValues'][19:21, 19:21, 19:21]
    #print '%1.16f' %c._data[6]['shapedErrors'][20, 20, 20]
    #print c._data[6]['shapedErrors'].shape

    fname = r'c:\mcnp5\mcnplanm'
    c = mcnpReaderGrid(fname, shape=(30, 30, 35))
    print c._data

    # c = mcnpReaderGrid('mcnplanm', shape=(41, 41, 41))

    sys.exit()
    # print sys.argv[0]
    inputFileName = sys.argv[1]
    c = mcnpReaderSimple(inputFileName)
    print 'Reading %s: ' %(inputFileName)
    if len(sys.argv) > 2:
        cell = int(sys.argv[2])
        tally = int(sys.argv[3])
    else:
        cell = int(raw_input('Cell: '))
        tally = int(raw_input('Tally: '))

    #print "{:>15} {:>15}".format("", *('Cell', 'Tally'))
    #print "{:>15} {:>15}".format("", *c.getValueTupleInCell(cell=cell, tally=tally))
    print c.getValueTupleInCell(cell, tally)
    try:
        print "%1.16f %1.4f" %c.getValueTupleInCell(cell, tally)
    except TypeError:
        print 'Not found'

