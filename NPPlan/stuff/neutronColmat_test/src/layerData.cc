#include "layerData.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

detLayer::detLayer() { 
  Nullify();
}

detLayer::detLayer(G4double en, G4double en2, G4double _do, G4double _do2, G4int nE, G4double cf): depEnergy(en),
  depEnergy2(en2), dose(_do), dose2(_do2), nEvents(nE), cellFlux(cf)
{ 
}

bool detLayer::Update(G4double ene, G4double _do, G4int ne, G4double cf) {
  depEnergy += ene;
  depEnergy2 += ene*ene;
  dose += _do;
  dose2 += _do*_do;
  nEvents += ne;
  cellFlux += cf;
  return true;
}

bool detLayer::Update(G4double ene, G4double _do, G4double cf) {
  depEnergy += ene;
  depEnergy2 += ene*ene;
  dose += _do;
  dose2 += _do*_do;
  nEvents++;
  cellFlux += cf;
  return true;
}

void detLayer::Nullify() {
  depEnergy = 0.0 * MeV;
  depEnergy2 = 0.0 * MeV;
  dose = 0.0 * MeV;
  dose2 = 0.0 * MeV;
  cellFlux = 0.0 * (1/cm2);
  nEvents = 0;
}

resLayer::resLayer(): _errorState(false) {

}

resLayer::resLayer(G4double ene, G4double ene2, G4double _do, G4double _do2, G4int nE, G4double cf): _errorState(false),
  depEnergy(ene), depEnergy2(ene2), dose(_do), dose2(_do2), nEvents(nE), cellFlux(cf)
{

}
resLayer::resLayer(detLayer data): _errorState(false) {
  depEnergy = data.depEnergy;
  depEnergy2 = data.depEnergy2;
  dose = data.dose;
  dose2 = data.dose2;
  nEvents = data.nEvents;
  cellFlux = data.cellFlux;
}

resLayer::resLayer(G4double ene, G4double ene2, G4double eneErr, G4double _do, G4double _do2, G4double _doErr, G4int nE, G4double cf): _errorState(true),
  depEnergy(ene), depEnergy2(ene2), dose(_do), dose2(_do2), nEvents(nE), cellFlux(cf), depEnergyError(eneErr), doseError(_doErr)
{

}

void resLayer::Nullify() {
  depEnergy = 0.0 * MeV;
  depEnergy2 = 0.0 * MeV;
  depEnergyError = 0.0 * MeV;
  dose = 0.0 * MeV;
  dose2 = 0.0 * MeV;
  doseError = 0.0 * MeV;
  nEvents = 0;
  cellFlux = 0.0 * (1/cm2);
}

resLayer resLayer::operator+ (const resLayer &other) {
  _errorState = false;
  G4double ene = depEnergy + other.depEnergy;
  G4double ene2 = depEnergy2 + other.depEnergy2;
  G4double _do = dose + other.dose;
  G4double _do2 = dose2 + other.dose2;
  G4double nE = nEvents + other.nEvents;
  G4double cf = cellFlux + other.cellFlux;
  return resLayer(ene, ene2, _do, _do2, nE, cf);
}

resLayer resLayer::operator+ (const detLayer &other) {
  _errorState = false;
  G4double ene = depEnergy + other.depEnergy;
  G4double ene2 = depEnergy2 + other.depEnergy2;
  G4double _do = dose + other.dose;
  G4double _do2 = dose2 + other.dose2;
  G4int nE = nEvents + other.nEvents;
  G4double cf = cellFlux + other.cellFlux;
  return resLayer(ene, ene2, _do, _do2, nE, cf);
}

resLayer &resLayer::operator+= (const resLayer &other) {
  _errorState = false;
  depEnergy += other.depEnergy;
  depEnergy2 += other.depEnergy2;
  dose += other.dose;
  dose2 += other.dose2;
  nEvents += other.nEvents;
  cellFlux += other.cellFlux;
  return *this;
}

resLayer &resLayer::operator+= (const detLayer &other) {
  _errorState = false;
  depEnergy += other.depEnergy;
  depEnergy2 += other.depEnergy2;
  dose += other.dose;
  dose2 += other.dose2;
  nEvents += other.nEvents;
  cellFlux += other.cellFlux;
  return *this;
}

void resLayer::calculateError() {
  G4double v, vDo;
  v = nEvents*depEnergy2 - depEnergy*depEnergy;
  vDo = nEvents*dose2 - dose*dose;
  if (nEvents>1) {
    depEnergyError = 1.0*std::sqrt(v/(nEvents-1));
    doseError = 1.0*std::sqrt(vDo/(nEvents-1));
  }
  _errorState = true;
}