#ifndef DetectorSD_Single_h
#define DetectorSD_Single_h 1

#include "G4VSensitiveDetector.hh"
#include <map>
class G4Step;
class RunAction;
//class DetectorSDMessenger;

class DetectorSDSingle: public G4VSensitiveDetector 
{
  public:
    DetectorSDSingle(G4String);
    DetectorSDSingle(G4String, G4double);
    ~DetectorSDSingle();
    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);

    G4String outFileNameAll;
	G4double depEnergy, depEnergy2;
  G4double depEnergyError;
  G4double dose, dose2;
  G4double doseError;
	G4int nEvents;
  G4double cellFlux;
  G4double cellFlux2;

    G4double sphDiam;


    // by definition
    //void addParticle2Store(G4ParticleDefinition*);
    // by string
    bool addParticle2Store(G4String);
    void addParticlesData(G4String, G4int, G4int, G4int, G4double, G4double);
    // by Z A
    //void addParticle2Store(G4int, G4int);

  private:
    RunAction* runAction;
    void setupSummator();
    std::ofstream fileoutPart;
    G4double _givenVolume;
};


#endif