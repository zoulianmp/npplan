#ifndef DetectorSD_1Layer_h
#define DetectorSD_1Layer_h 1
#include "G4VSensitiveDetector.hh"
#include <map>
#include <vector>
#include "layerData.hh"
class G4Step;
class RunAction;
//class DetectorSDMessenger;

class DetectorSD1Layer: public G4VSensitiveDetector 
{
  public:
    DetectorSD1Layer(G4String, G4int);
    DetectorSD1Layer(G4String, G4int, G4double);
    DetectorSD1Layer(G4String, G4int, G4int);
    DetectorSD1Layer(G4String, G4int, G4int, G4double);
    ~DetectorSD1Layer();
    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);
    void addParticle(G4String);
    void addIon(G4int, G4int);
    //void addIon(G4int, G4int);
  private:
    RunAction* runAction;
    G4String outName;
    G4int nov;
    detLayer *data;
    G4int targetLayer;
    G4double preVol;
    std::map<G4ParticleDefinition*, detLayer*> partedData;

    std::vector<G4ParticleDefinition*> allParticlesList;

    G4double ComputeVolume(G4Step* aStep, G4int idx);
};

#endif