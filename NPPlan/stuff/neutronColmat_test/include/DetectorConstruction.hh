#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1

#include "G4VUserDetectorConstruction.hh"
#include <vector>
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"


//class G4LogicalVolume;
//class G4ThreeVector;
//class G4Material;
class DetectorSD1Layer;

class DetectorConstruction: public G4VUserDetectorConstruction
{                 
  public:
    DetectorConstruction();
    ~DetectorConstruction();

    G4VPhysicalVolume* Construct();
    G4int nLayers1;
    G4int nLayers2;
    G4int nLayers3;
    G4int nLayers4;
    G4int nLayers5;
    G4int nLayers6;

    G4int qOfDets;
    G4int nov;
    std::vector<G4String> lOfDets;

    void addDet(G4LogicalVolume*, G4String);
    void addDet(G4LogicalVolume*, G4String, G4double);
    void addDetParted(G4LogicalVolume*, G4String);

    G4int getNumberOfLayers();

    // hack ?
    DetectorSD1Layer* getMainDetector();
  private:
    G4LogicalVolume* addZVoxelsLayer(G4LogicalVolume*, G4ThreeVector, G4String, G4Material*, 
      G4double, G4double, G4double, G4int);
    DetectorSD1Layer* mainDet;

    void Collimator(G4LogicalVolume *);
};

#endif

