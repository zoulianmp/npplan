#ifndef DetectorSD_Single_Particles_h
#define DetectorSD_Single_Particles_h 1

#include "G4VSensitiveDetector.hh"
#include <map>
#include <vector>
class G4Step;
class RunAction;
//class DetectorSDMessenger;

struct Slayer
{
	G4double depEnergy, depEnergy2, depEnergyError, dose, dose2, doseError;
	G4int nEvents;
  G4double cellFlux;
  Slayer();
};

struct particlesList
{
  G4String outName;
  G4String g4Name;
  G4double lowEnergy;
  G4double highEnergy;
  particlesList(G4String, G4String, G4double, G4double);
};

class DetectorSDSinglePart: public G4VSensitiveDetector 
{
  public:
    DetectorSDSinglePart(G4String);
    DetectorSDSinglePart(G4String, G4double);
    ~DetectorSDSinglePart();
    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);

    G4String outFileNameAll;
	G4double depEnergy, depEnergy2;
  G4double depEnergyError;
  G4double dose, dose2;
  G4double doseError;
	G4int nEvents;
  G4double cellFlux;
  G4double cellFlux2;

    G4double sphDiam;

    void addParticle(G4String);
    void addParticle(G4String, G4String, G4double, G4double);

  private:
    RunAction* runAction;
    void setupSummator();
    std::ofstream fileoutPart;
    G4double _givenVolume;

    //std::map<G4String, particlesList*> listOfParticles;
    std::vector<particlesList*> listOfParticles;
    std::map<G4String, Slayer*> part2Energies;

    //G4String findParticleName(G4String);
    void addParticleData(G4String, G4double, G4double, G4double);
};


#endif