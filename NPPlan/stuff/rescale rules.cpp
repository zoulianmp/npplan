  int rescale_slope, rescale_intercept;
  if (file.Find("RescaleSlope", rescale_slope) &&
      file.Find("RescaleIntercept", rescale_intercept))
  if (rescale_slope != 0) {
    width /= rescale_slope;
    center = (center - rescale_intercept)/rescale_slope;
  }

  // rescale to unsigned 8 bit
  T data_max = std::numeric_limits<T>::max();
  T data_min = std::numeric_limits<T>::min();
  if ((center + width/2) < data_max)
  data_max = static_cast<T>(center + width/2);

  if ((center - width/2) > data_min)
  data_min = static_cast<T>(center - width/2);

  wxUint8 char_max = std::numeric_limits<wxUint8>::max();
  int max_index = data.size();


  if ((data_max - data_min) != 0) {
    float scaling = static_cast<float>(char_max)/static_cast<float>(data_max - data_min);
    T* c_ptr = &data.front();
    for (int i = 0; i < max_index; ++i) {
      if (*c_ptr > data_max) *c_ptr = data_max;
      else if (*c_ptr < data_min) *c_ptr = data_min;
      *c_ptr = static_cast<T>((*c_ptr) * scaling - data_min * scaling);
      ++c_ptr;
    }
  }

