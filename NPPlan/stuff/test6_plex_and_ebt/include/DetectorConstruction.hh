/*
 * Base file for geant projects
 * Detector construction definition
 * @author: mrxak
 * (c) MRRC, 2013
*/

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4NistManager.hh"
#include "G4ThreeVector.hh"
//#include parametrised

class G4Material;
class G4Box;
class G4LogicalVolume;
//class G4ThreeVector;

class DetectorMessenger;

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    DetectorConstruction();
    ~DetectorConstruction();

    G4VPhysicalVolume* physWorld;

    G4VPhysicalVolume* Construct();
    void setWorldSize(G4double);
  protected:
    void addGeometry(G4LogicalVolume*);
    void addSingleFilm(G4LogicalVolume*);
    G4LogicalVolume * addSingleFilmPart(G4LogicalVolume*, G4Material*, G4double, G4double, G4String);
    void setDetectors(G4LogicalVolume*);
  private:
    G4double worldSizeXYZ;
    G4LogicalVolume* logicWorld;
    DetectorMessenger*  fMessenger;
    G4NistManager* nistManager;
    void DefineMaterials();
    G4VPhysicalVolume* DefineVolumes();
    G4Box* solidWorld;
    G4Element* elC;
    G4Element* elH;
    G4Element* elLi;
    G4Element* elK;
    G4Element* elCl;
    G4Element* elN;
    G4Element* elO;
    G4Element* elS;
    G4Element* elNa;
    G4Element* elBr;
    G4Material* l1m;
    G4Material* l2m;
    G4Material* l3m;
    G4Material* l4m;
    G4Material* l5m;
    G4LogicalVolume* addZVoxelsLayer(G4LogicalVolume*, G4ThreeVector, G4String, G4Material*, G4double, G4double, G4double, G4int);
    G4LogicalVolume* addVoxelsLayer(G4LogicalVolume*, G4ThreeVector, G4String, G4Material*, G4double, G4double, G4double, G4int);
};

#endif