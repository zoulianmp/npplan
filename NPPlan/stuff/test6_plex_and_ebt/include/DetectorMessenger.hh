/*
 * Base file for geant projects
 * Detector messenger definition
 * @author: mrxak
 * (c) MRRC, 2013
*/

#ifndef DetectorMessenger_h
#define DetectorMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class DetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;

class DetectorMessenger: public G4UImessenger
{
  public:
    DetectorMessenger(DetectorConstruction* );
    virtual ~DetectorMessenger();
    
    virtual void SetNewValue(G4UIcommand*, G4String);
    
  private:
    G4UIdirectory*           topDirectory;
    G4UIdirectory*           dDetDirectory;

    G4UIcmdWithADoubleAndUnit* fWorldSizeXYZ;

    DetectorConstruction*  fDetectorConstruction;
};

#endif