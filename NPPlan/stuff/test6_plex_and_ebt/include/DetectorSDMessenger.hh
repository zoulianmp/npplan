#ifndef DetectorSDMessenger_h
#define DetectorSDMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class DetectorSD;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;

class DetectorSDMessenger: public G4UImessenger
{
  public:
    DetectorSDMessenger(DetectorSD* );
    virtual ~DetectorSDMessenger();
  private:
    void SetNewValue(G4UIcommand* command,G4String newValue);
    DetectorSD*  fDetectorSD;

    G4UIdirectory*           topDirectory;
    G4UIdirectory*           dDetDirectory;
    G4UIcmdWithAString*      outFileName;

};
#endif