/*#include "OpticalConstructor.hh"

#include "G4ProcessManager.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"

OpticalConstructor::OpticalConstructor() {}

OpticalConstructor::~OpticalConstructor() {}

void OpticalConstructor::ConstructParticle()
{
  // фотон
  G4OpticalPhoton::OpticalPhotonDefinition();
}

void OpticalConstructor::ConstructProcess()
{
  ConstructOp();
}

// optical photon
#include "G4Cerenkov.hh"
#include "G4Scintillation.hh"
#include "G4OpAbsorption.hh"
#include "G4OpRayleigh.hh"
#include "G4OpBoundaryProcess.hh"

void OpticalConstructor::ConstructOp()
{
  G4Cerenkov*          theCerenkovProcess = new G4Cerenkov("Cerenkov");;
  G4Scintillation*     theScintillationProcess = new G4Scintillation("Scintillation");

  // цикл по всем зарегистрированным частицам
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();

    // подключаем к частицам процессы черенковского и сцинтилляционного излучения
    if (theCerenkovProcess->IsApplicable(*particle)) {
      pmanager->AddProcess(theCerenkovProcess);
      pmanager->SetProcessOrdering(theCerenkovProcess,idxPostStep);
    }

    if (theScintillationProcess->IsApplicable(*particle)) {
      pmanager->AddProcess(theScintillationProcess);
      pmanager->SetProcessOrderingToLast(theScintillationProcess, idxAtRest);
      pmanager->SetProcessOrderingToLast(theScintillationProcess, idxPostStep);
    }

    // для фотонов света:
    if (particleName == "opticalphoton") {
      pmanager->AddDiscreteProcess(new G4OpAbsorption());      // поглощение средой
      pmanager->AddDiscreteProcess(new G4OpRayleigh());        // рассеяние средой
      pmanager->AddDiscreteProcess(new G4OpBoundaryProcess()); // преломление и отражение
    }
  }
}

*/