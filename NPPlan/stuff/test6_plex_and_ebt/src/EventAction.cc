#include "EventAction.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4UnitsTable.hh"

EventAction::EventAction()
 : G4UserEventAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::~EventAction()
{

}

void EventAction::BeginOfEventAction(const G4Event* aEvent)
{

}

void EventAction::EndOfEventAction(const G4Event* event)
{  
  //G4int d1 = G4SDManager::GetSDMpointer()->GetCollectionID("Absorber/Edep");
  G4int d1 = G4SDManager::GetSDMpointer()->GetCollectionID("Absorber/Edep");
  testGetValue(GetHitsCollection(d1, event));
  G4int d2 = G4SDManager::GetSDMpointer()->GetCollectionID("Absorber/DDep");
  testGetValue(GetHitsCollection(d2, event));
  G4int d3 = G4SDManager::GetSDMpointer()->GetCollectionID("Absorber/CellFlux");
  testGetValue(GetHitsCollection(d3, event));
  G4int d4 = G4SDManager::GetSDMpointer()->GetCollectionID("Absorber/TrackLength");
  testGetValue(GetHitsCollection(d4, event));
  //G4double absoEdep = GetSum(GetHitsCollection(d1, event));
  //G4cout<<"Abso dep: "<<absoEdep<<G4endl;
}

G4THitsMap<G4double>* 
EventAction::GetHitsCollection(G4int hcID,
                                  const G4Event* event) const
{
  G4THitsMap<G4double>* hitsCollection 
    = static_cast<G4THitsMap<G4double>*>(
        event->GetHCofThisEvent()->GetHC(hcID));
  
  if ( ! hitsCollection ) {
    G4ExceptionDescription msg;
    msg << "Cannot access hitsCollection ID " << hcID; 
    G4Exception("B4dEventAction::GetHitsCollection()",
      "MyCode0003", FatalException, msg);
  }         

  return hitsCollection;
}    

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4double EventAction::GetSum(G4THitsMap<G4double>* hitsMap) const
{
  G4double sumValue = 0;
  std::map<G4int, G4double*>::iterator it;
  for ( it = hitsMap->GetMap()->begin(); it != hitsMap->GetMap()->end(); it++) {
    sumValue += *(it->second);
  }
  return sumValue;  
}  

G4double EventAction::testGetValue(G4THitsMap<G4double>* hitsMap) {
  std::map<G4int, G4double*>::iterator it;
  for ( it = hitsMap->GetMap()->begin(); it != hitsMap->GetMap()->end(); it++) {
    G4cout<<"Event tg: "<<it->first<<" "<<it->second<<" "<<*(it->second)<<G4endl;
  }
  return 0.0;
}