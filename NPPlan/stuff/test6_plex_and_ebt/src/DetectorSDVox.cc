#include "DetectorSDVox.hh"
#include "RunAction.hh"
#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4Step.hh"

#include <map>

DetectorSDVox::DetectorSDVox(G4String name, G4int _nLayers): G4VSensitiveDetector(name)
{
	runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
	detName = name;
	enSum = 0.0;

	//nLayers = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction())->nLayers;
	nLayers = _nLayers;
	mat = new G4double*[nLayers];
	matC = new G4int*[nLayers];
	for (int i = 0; i < nLayers; i++) {
		mat[i] = new G4double[nLayers];
		matC[i] = new G4int[nLayers];
	}
	
	for (int i = 0; i < nLayers; i++) {
		for (int j = 0; j < nLayers; j++) {
			mat[i][j] = 0.0;
			matC[i][j] = 0;
		}
	}
}

DetectorSDVox::~DetectorSDVox()
{

}

void DetectorSDVox::_createStorage(void)
{

}

void DetectorSDVox::Initialize(G4HCofThisEvent*)
{
	G4String fnamePart = "vox_" + detName + ".out";
	fileoutPart.open(fnamePart);	
}

G4bool DetectorSDVox::ProcessHits(G4Step* step, G4TouchableHistory*)
{
	G4StepPoint* prePoint = step->GetPreStepPoint();
	G4TouchableHandle touchable = prePoint->GetTouchableHandle();
	G4String name = step->GetTrack()->GetDefinition()->GetParticleName();
	G4int parentTrackId = step->GetTrack()->GetParentID();

	G4int layer0 = touchable->GetCopyNumber(0);
	G4int layer1 = touchable->GetCopyNumber(1);
	G4int layer2 = touchable->GetCopyNumber(2);
	G4int layer3 = touchable->GetCopyNumber(3);

	/*G4cout<<" "<<layer0
		<<" "<<layer1
		<<" "<<layer2
		<<" "<<layer3
		<<"  energy: "<<step->GetTotalEnergyDeposit()
		<<G4endl;*/

	mat[layer1][layer2] += step->GetTotalEnergyDeposit();
	matC[layer1][layer2] += 1;
	enSum += step->GetTotalEnergyDeposit();

	return true;
}

void DetectorSDVox::EndOfEvent(G4HCofThisEvent* l)
{
	G4double sum;
	G4int sumC;
	sum = 0.0;
	sumC = 0;
	for (int i = 0; i < nLayers; i++) {
		for (int j = 0; j < nLayers; j++) {
			fileoutPart<<i<<" "<<j<<" "<<mat[i][j]<<" "<<matC[i][j]<<G4endl;
			sum += mat[i][j];
			sumC += matC[i][j];
		}
	}
	fileoutPart<<"Summary edep layer: "<<sum<<G4endl;
	fileoutPart<<"Summary edep layer (v2): "<<enSum<<G4endl;
	fileoutPart<<"Summary events in layer: "<<sumC<<G4endl;
	fileoutPart.close();
}