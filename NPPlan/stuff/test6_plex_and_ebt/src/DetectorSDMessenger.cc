#include "DetectorSDMessenger.hh"
#include "DetectorSD.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"

DetectorSDMessenger::DetectorSDMessenger(DetectorSD* Det)
 : G4UImessenger(),
   fDetectorSD(Det)
{
  topDirectory = new G4UIdirectory("/test2/");
  topDirectory->SetGuidance("UI commands specific to this example.");

  dDetDirectory = new G4UIdirectory("/test2/sd/");
  dDetDirectory->SetGuidance("Detector construction control");

  outFileName = new G4UIcmdWithAString("/test2/sd/outFileName",this);
  outFileName->SetGuidance("Define output filename for SD");
  outFileName->SetParameterName("outFileName", false);   // can't be ommitted 
  outFileName->AvailableForStates(G4State_Idle);
}

DetectorSDMessenger::~DetectorSDMessenger()
{

}

void DetectorSDMessenger::SetNewValue(G4UIcommand* command,G4String newValue) {
  if (outFileName == command) {
    fDetectorSD->outFileName = newValue;
  }
}