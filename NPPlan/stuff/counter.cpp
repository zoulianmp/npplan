// multimap_ctor.cpp
// compile with: /EHsc
#include <map>
#include <iostream>
#include "stdafx.h"
#include <string>
#include <iostream>

int main( )
{
	using namespace std;
	multimap<string, int> m;
	m.insert(pair<string, int>("a", 1));
	m.insert(pair<string, int>("c", 2));
	m.insert(pair<string, int>("b", 3));
	m.insert(pair<string, int>("b", 4));
	m.insert(pair<string, int>("a", 5));
	m.insert(pair<string, int>("b", 6));

	cout << "Number of elements with key a: " << m.count("a") << endl;
	cout << "All " << m.size() << endl;
	getchar();
}